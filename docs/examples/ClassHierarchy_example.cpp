class AtmosphericModel
{
 public:
  AtmosphericModel(const std::string &model_name);
  AtmosphericModel(const AtmosphericModel &model);
  virtual ~AtmosphericModel();
  AtmosphericModel& operator=(const AtmosphericModel &model);

  std::string     getModelName() const;
  Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &r_sun,
                               const EarthOrientation &earth_orientation,
                               const std::shared_ptr<Satellite> &sat);

 protected:
  std::string model_name_;

//------------------------------------------------------------------------------
//  double CalculateAtmosphericDensity(const Eigen::Vector3d  &r_sun, 
//                                     const Eigen::Vector3d  &r_sat,
//                                     const EarthOrientation &earth_orientation)
//------------------------------------------------------------------------------
/** Virtual method for calculting the Atmospheric Density at location of 
  * spacecraft in ICRF
  * 
  * \param r_sun Eigen::Vector3d Position of the sun in ICRF with Earth origin 
  *              \f$[m]\f$.
  * \param r_sat Eigen::Vector3d Position of satellite in ICRF with Earth origin
  *              \f$[m]\f$.
  * \param earth_orientation EarthOrientation object defining the conversion 
  *                          between reference frames ICRF, ITRF, LOD and 
  *                          Lat/Lon.
  * 
  * \return double Atmospheric density at given satellite position \f$[kg/m^3]\f$
  */
  virtual double CalculateAtmosphericDensity(const Eigen::Vector3d  &r_sun, 
                                             const Eigen::Vector3d  &r_sat,
                                             const EarthOrientation &earth_orientation) const = 0;
};

class EarthGravityField
{
 public:
  EarthGravityField(const std::string &model_name);
  EarthGravityField(const EarthGravityField &model);
  virtual ~EarthGravityField();
  EarthGravityField& operator=(const EarthGravityField &model);

  std::string getModelName() const;

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAccelITRF(const Eigen::Vector3d &itrf_pos)
//------------------------------------------------------------------------------
/** Virtual function to calculate the Acceleration acting on a spacecraft from 
  * earths gravity field in ITRF.
  * 
  * \param itrf_pos Eigen::Vector3d Position of object in ITRF \f$[x, y, z]\f$ 
  *                 \f$[m]\f$.
  * 
  * \return Eigen::Vector3d Acceleration vector acting on object from Earths's 
  *         gravity field \f$[\a_x, \a_y, \a_z]\f$ \f$[m/s^2]\f$.
  */
  virtual Eigen::Vector3d getAccelITRF(const Eigen::Vector3d &itrf_pos) = 0;

 protected:
  std::string model_name_;
};

class EarthGravityField
{
 public:
  EarthGravityField(const std::string &model_name);
  EarthGravityField(const EarthGravityField &model);
  virtual ~EarthGravityField();
  EarthGravityField& operator=(const EarthGravityField &model);

  std::string getModelName() const;

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAccelITRF(const Eigen::Vector3d &itrf_pos)
//------------------------------------------------------------------------------
/** Virtual function to calculate the Acceleration acting on a spacecraft from 
  * earths gravity field in ITRF.
  * 
  * \param itrf_pos Eigen::Vector3d Position of object in ITRF \f$[x, y, z]\f$ 
  *                 \f$[m]\f$.
  * 
  * \return Eigen::Vector3d Acceleration vector acting on object from Earths's 
  *         gravity field \f$[\a_x, \a_y, \a_z]\f$ \f$[m/s^2]\f$.
  */
  virtual Eigen::Vector3d getAccelITRF(const Eigen::Vector3d &itrf_pos) = 0;

 protected:
  std::string model_name_;
};

class EarthMagneticField
{
 public:
  EarthMagneticField(const std::string &model_name);
  EarthMagneticField(const EarthMagneticField &model);
  virtual ~EarthMagneticField();
  EarthMagneticField& operator=(const EarthMagneticField &model);

  std::string getModelName() const;

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateMagneticField(const Eigen::Vector3d &latlon_position,
//                                         const double mjd_utc)
//------------------------------------------------------------------------------
/** Virtual method for calculting the Earth's magnetic field at location of 
  * spacecraft in ITRF Lat/Lon.
  * 
  * \param latlon_position Eigen::Vector3d Position of the satellite in ITRF 
  *                        in Latitude/Longitude \f$[Lat, Lon, Alt]\f$ 
  *                        \f$[rad]\f$ and \f$[m]\f$.
  * \param mjd_utc double UTC modified julian date.
  * 
  * \return Eigen::Vector3d Earth magnetic field vector at satellite position
  *         \f$[M_x, M_y, M_z]\f$ \f$[T]\f$
  */
  virtual Eigen::Vector3d CalculateMagneticField(const Eigen::Vector3d &latlon_position,
                                                 const double           mjd_utc) = 0;
  
 protected:
  std::string model_name_;
};

class EarthOrientation
{
 public:
  EarthOrientation();
  EarthOrientation(const std::vector<std::string> &filepaths, 
                   const std::string &model_name);
  EarthOrientation(const EarthOrientation &model);
  ~EarthOrientation();
  EarthOrientation& operator=(const EarthOrientation &model);

  std::string getModelName()     const;
  double      getXPole()         const;
  double      getYPole()         const;
  double      getTAIUTCOffset()  const;
  double      getUT1UTCOffset()  const;
  double      getLOD()           const;
  std::string getEOPDataPeriod() const;

  Eigen::Vector3d ICRS2ITRS    (const Eigen::Vector3d &icrs_vector)     const;
  Eigen::Vector3d ICRS2TOD     (const Eigen::Vector3d &icrs_vector)     const;
  Eigen::Vector3d ITRS2ICRS    (const Eigen::Vector3d &itrs_vector)     const;
  Eigen::Vector3d TOD2ICRS     (const Eigen::Vector3d &tod_vector )     const;
  Eigen::Vector3d Cart2Geodetic(const Eigen::Vector3d &ecef_vector)     const;
  Eigen::Vector3d Geodetic2Cart(const Eigen::Vector3d &geodetic_vector) const;

  void InitializeEarthOrientationParameters(const double mjd_utc);
  void CalculateRotationMatrices(const double julian_century_tt, 
                                 const double mjd_ut1);

 protected:
  std::string model_name_;
  
 private:
  Eigen::ArrayXXd eop_data_observed_;
  Eigen::ArrayXXd eop_data_predicted_;
  std::string eop_data_period_;
  int eop_points_observed_;
  int eop_points_predicted_;

  //  NGA EOPP Coefficients
  double eopp_ta_;
  double eopp_a_;
  double eopp_b_;
  Eigen::Array2d eopp_c_;
  Eigen::Array2d eopp_d_;
  Eigen::Array2d eopp_p_;
  double eopp_e_;
  double eopp_f_;
  Eigen::Array2d eopp_g_;
  Eigen::Array2d eopp_h_;
  Eigen::Array2d eopp_q_;
  double eopp_tb_;
  double eopp_i_;
  double eopp_j_;
  Eigen::Array4d eopp_k_;
  Eigen::Array4d eopp_l_;
  Eigen::Array4d eopp_r_;
  double eopp_tai_utc_offset_;

  double x_pole_;           
  double y_pole_;          
  double ut1_utc_offset_;   
  double lod_;                    
  double tai_utc_offset_;   

  Eigen::ArrayXXd luni_solar_nutation_series_;
  Eigen::ArrayXXd planetary_nutation_series_;
  Eigen::ArrayXXd cio_s0_terms_; 
  Eigen::ArrayXXd cio_s1_terms_;
  Eigen::ArrayXXd cio_s2_terms_;
  Eigen::ArrayXXd cio_s3_terms_;
  Eigen::ArrayXXd cio_s4_terms_;
    
  Eigen::Matrix3d polar_motion_rotation_matrix_; 
  Eigen::Matrix3d precession_nutation_rotation_matrix_; 
  Eigen::Matrix3d sidereal_rotation_matrix_;
  Eigen::Matrix3d icrs2itrs_;          

  void ReadEarthOrientationParametersFileAuxiliary(const std::string &filepath);
  void SetNGACoefficients(const std::vector<std::string> &nga_data);
  void ReadEarthOrientationParametersFile(const std::string &filepath);
  Eigen::ArrayXXd getCoefficientsFromCSVFile(const std::string &filepath) const;
  double Lerp(const double prior, 
              const double post, 
              const double interval) const;

  Eigen::Matrix3d CalculatePolarMotionRotationMatrix(const double tio_locator_s, 
                                                     const double x_pole,
                                                     const double y_pole) const;
  double CalculateTIOLocatorS(const double julian_century_tt) const;

  Eigen::Matrix3d CalculatePrecessionNutationRotationMatrix(const double julian_century_tt) const;
  Eigen::Vector2d CalculateIAUNutation2000A (const double julian_century_tt) const;
  Eigen::Vector2d CalculateLuniSolarNutation(const double julian_century_tt) const;
  Eigen::Vector2d CalculatePlanetaryNutation(const double julian_century_tt) const;

  Eigen::Matrix3d CalculateSiderealRotationMatrix(const double greenwich_sidereal_time) const;
  double CalculateGreenwichSiderealTime(const double           cio_locator_s, 
                                        const double           mjd_ut1, 
                                        const Eigen::Matrix3d &nutation_rotation_matrix) const;   
  double CalculateCIOLocatorS(const double           julian_century_tt, 
                              const Eigen::Matrix3d &nutation_rotation_matrix) const;
  double CalculateCIOCoefficient(const double            starting_coeff, 
                                 const Eigen::MatrixXd  &s_terms, 
                                 const std::vector<int> &col_indices, 
                                 const Eigen::VectorXd  &fundamental_args) const;

  double MoonMeanAnomaly           (const double julian_century_tt) const;
  double SunMeanAnomaly            (const double julian_century_tt) const; 
  double MoonMeanElongation_Sun    (const double julian_century_tt) const; 
  double MoonAscendingNode         (const double julian_century_tt) const; 
  double MoonMeanLongitude         (const double julian_century_tt) const; 
  double MercuryMeanLongitude      (const double julian_century_tt) const;
  double VenusMeanLongitude        (const double julian_century_tt) const;
  double EarthMeanLongitude        (const double julian_century_tt) const; 
  double MarsMeanLongitude         (const double julian_century_tt) const; 
  double JupiterMeanLongitude      (const double julian_century_tt) const;
  double SaturnMeanLongitude       (const double julian_century_tt) const; 
  double UranusMeanLongitude       (const double julian_century_tt) const;
  double NeptuneMeanLongitude      (const double julian_century_tt) const; 
  double GeneralPrecessionLongitude(const double julian_century_tt) const; 
};

class GraceGravityModel : public EarthGravityField
{
 public:
  GraceGravityModel(const std::string &filepath, 
                    const std::string &model_name, 
                    const int          max_degree);
  GraceGravityModel(const GraceGravityModel &model);
  ~GraceGravityModel();
  GraceGravityModel& operator=(const GraceGravityModel &model);                    

  Eigen::Vector3d getAccelITRF(const Eigen::Vector3d &itrf_pos) override;

 private:
  int             applied_max_degree_;
  int             model_max_degree_;
  double          earth_radius_;
  double          earth_gm_;
  Eigen::ArrayXXd cnm_;
  Eigen::ArrayXXd snm_;
  Eigen::ArrayXXd legendre_polynomials_;
  Eigen::ArrayXXd differential_legendre_polynomials_;

  void setGravitationalModelFileParameters(const std::string &filepath);
  void setGravitationalModelFileCoefficients(const std::string &filepath);

  void InitializeLegendrePolynomials(const double angle);
  Eigen::Vector3d getSphericalCoordinates(const Eigen::Vector3d &cart_vec) const;
};

class HarrisPriester : public AtmosphericModel
{
 public:
  HarrisPriester(const std::string &hp_coeff_filepath, 
                 const std::string &model_name, 
                 const int hp_parameter, 
                 const double min_altitude,
                 const double max_altitude,
                 const int f107);
  HarrisPriester(const HarrisPriester &model);
  ~HarrisPriester();
  HarrisPriester& operator=(const HarrisPriester &model);                 

  double CalculateAtmosphericDensity(const Eigen::Vector3d  &r_sun,
                                     const Eigen::Vector3d  &r_sat,
                                     const EarthOrientation &earth_orientation) const override;
  
  void setF107(const int f107);

  void setHPParameter(const int hp_parameter);
                      
 private:
  int hp_parameter_;  // 2 for low inclination, 6 for polar
  int min_altitude_;
  int max_altitude_;
  int f107_index_;

  Eigen::MatrixXd harris_priester_coeffs_;
  Eigen::VectorXd h_coeffs_;      //  Height Coefficients 
  Eigen::VectorXd c_min_coeffs_;  //  Anatapex Density Coefficients
  Eigen::VectorXd c_max_coeffs_;  // Apex Density Coefficient

  void InitializeHarrisPriesterCoefficients(const int f107_index);
  double CalculateHPModelDensity(const Eigen::Vector3d &r_sun,
                                 const Eigen::Vector3d &r_sat, 
                                 const double           altitude) const;
  double CalculateTrueAltitude(const Eigen::Vector3d &ecef_pos) const;

  Eigen::MatrixXd getCoefficientsFromCSVFile(const std::string &filepath) const;

  Eigen::Vector3d getSphericalCoordinates(const Eigen::Vector3d &xyz_vec) const;
};

class Igrf : public EarthMagneticField
{
 public:
  Igrf(const std::string &filepath, 
       const std::string &model_name,  
       const int          max_degree);
  Igrf(const Igrf &model);
  ~Igrf();
  Igrf& operator=(const Igrf &model);
  
  Eigen::Vector3d CalculateMagneticField(const Eigen::Vector3d &latlon_position, 
                                         const double           mjd_utc) override;
  
 private:
  Eigen::ArrayXXd g2000_;
  Eigen::ArrayXXd h2000_;
  Eigen::ArrayXXd g2005_;
  Eigen::ArrayXXd h2005_;
  Eigen::ArrayXXd g2010_;
  Eigen::ArrayXXd h2010_;
  Eigen::ArrayXXd g2015_;
  Eigen::ArrayXXd h2015_;
  Eigen::ArrayXXd g2020_;
  Eigen::ArrayXXd h2020_;
  Eigen::ArrayXXd gsv_;
  Eigen::ArrayXXd hsv_;
  int             max_degree_;

  Eigen::ArrayXXd h_;
  Eigen::ArrayXXd g_;
  double          delta_t_;
  Eigen::ArrayXXd legendre_polynomials_;
  Eigen::ArrayXXd differential_legendre_polynomials_;

  void ReadIGRFCoefficientsFile(const std::string &filepath);
  void InitializeLegendrePolynomials(const double angle);
  void CalculateCoefficients(const double mjd_utc);
};

  class EphemerisConstants
  {
   public:
    EphemerisConstants() :
      starting_address_(0),
      num_coefficients_(0),
      num_subintervals_(0),
      dimensionality_  (0)
    {

    }
    EphemerisConstants(Eigen::VectorXi constants)
    {
      starting_address_ = constants[0];
      num_coefficients_ = constants[1];
      num_subintervals_ = constants[2];
      dimensionality_   = constants[3];
    }
    int getStartingAddress() const
    {
      return starting_address_;
    }
    int getNumCoefficients() const
    {
      return num_coefficients_;
    }
    int getNumSubIntervals() const
    {
      return num_subintervals_;
    }
    int getDimensionality() const
    {
      return dimensionality_;
    }
   private:
    int starting_address_;
    int num_coefficients_;
    int num_subintervals_;
    int dimensionality_;
  };

  EphemerisConstants mercury_constants_;
  EphemerisConstants venus_constants_;
  EphemerisConstants earth_moon_constants_;
  EphemerisConstants mars_constants_;
  EphemerisConstants jupiter_constants_;
  EphemerisConstants saturn_constants_;
  EphemerisConstants uranus_constants_;
  EphemerisConstants neptune_constants_;
  EphemerisConstants pluto_constants_;
  EphemerisConstants moon_constants_;
  EphemerisConstants sun_constants_;
  EphemerisConstants earth_nutations_constants_;
  EphemerisConstants lunar_libration_constants_;
  EphemerisConstants lunar_mantle_velocity_constants_;
  EphemerisConstants tt_tdb_constants_;

  void ReadJPLEphemerisHeaderFile(const std::string &filepath);
  Eigen::ArrayXXd ReadJPLEphemerisCoefficientsFile(const std::string &filepath) const;

  Eigen::Vector3d CalculateSunPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateMercuryPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateVenusPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateEarthPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateMoonPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateMarsPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateJupiterPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateSaturnPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateUranusPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateNeptunePosition(double mjd_tdb) const;
  Eigen::Vector3d CalculatePlutoPosition(double mjd_tdb) const;

  Eigen::MatrixXd FindChebyshevCoefficients(const EphemerisConstants &orbiting_body) const;
  Eigen::VectorXd EvaluateChebyshevPolynomial(const double mjd_tdb, const EphemerisConstants &orbiting_body, 
                                              const Eigen::MatrixXd &coefficients) const;

  Eigen::Vector3d CalculatePointMassAcceleration(const Eigen::Vector3d &icrf_pos, 
                                                       const Eigen::Vector3d &mass_icrf_pos, 
                                                       const double gm) const;
};

class Magnetorquer
{
public:
  Magnetorquer(const Eigen::Vector3d &max_magnetic_moment);
  Magnetorquer(const Magnetorquer    &mt);
  ~Magnetorquer();
  Magnetorquer& operator=(const Magnetorquer &mt);

  std::string     getMode()           const;
  Eigen::Vector3d getMagneticMoment() const;
  double          getOperatingLoad()  const;

  void            setOperatingFraction(const double load);
  Eigen::Vector3d CalculateMagneticMoment();

private:
  std::string     mode_;
  Eigen::Vector3d magnetic_moment_;
  Eigen::Vector3d max_magnetic_moment_;
  double          load_;
};

class PlanetaryPerturbations
{
 public:
  PlanetaryPerturbations(const std::string &model_name);
  PlanetaryPerturbations(const PlanetaryPerturbations &model);
  virtual ~PlanetaryPerturbations();
  PlanetaryPerturbations& operator=(const PlanetaryPerturbations &model);

  std::string     getModelName()                   const;
  Eigen::Vector3d getSunPositionICRF()             const;
  Eigen::Vector3d getMercuryPositionICRF()         const;
  Eigen::Vector3d getVenusPositionICRF()           const;
  Eigen::Vector3d getEarthPositionICRFBarycenter() const;
  Eigen::Vector3d getMoonPositionICRF()            const;
  Eigen::Vector3d getMarsPositionICRF()            const;
  Eigen::Vector3d getJupiterPositionICRF()         const;
  Eigen::Vector3d getSaturnPositionICRF()          const;
  Eigen::Vector3d getUranusPositionICRF()          const;
  Eigen::Vector3d getNeptunePositionICRF()         const;
  Eigen::Vector3d getPlutoPositionICRF()           const;

//------------------------------------------------------------------------------
//  void CalculatePlanetaryPositions(const double mjd_tdb) 
//------------------------------------------------------------------------------
/** Virtual method to calculate the position of all planetary bodies with earth 
  * origin at the given UTC modified julian date in \f$[m]\f$ in ICRF. 
  * 
  * \param mjd_tdb UTC modified julian date.
  */
  virtual void CalculatePlanetaryPositions(const double mjd_tdb) = 0;

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &icrf_pos) 
//------------------------------------------------------------------------------
/** Virtual method to calculate the acceleration acting on the satellite from 
  * all planetary bodies in ICRF.
  * 
  * \param icrf_pos Position vector of the satellite in ICRF \f$[m]\f$. 
  */
  virtual Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &icrf_pos) = 0;

 protected:
  std::string model_name_;

  Eigen::Vector3d sun_position_icrf_;
  Eigen::Vector3d mercury_position_icrf_; 
  Eigen::Vector3d venus_position_icrf_;
  Eigen::Vector3d earth_position_icrf_barycenter_;
  Eigen::Vector3d moon_position_icrf_;
  Eigen::Vector3d mars_position_icrf_;
  Eigen::Vector3d jupiter_position_icrf_;
  Eigen::Vector3d saturn_position_icrf_; 
  Eigen::Vector3d uranus_position_icrf_;
  Eigen::Vector3d neptune_position_icrf_;
  Eigen::Vector3d pluto_position_icrf_;
};

class ReactionWheel
{
public:
  ReactionWheel(const Eigen::Vector3d &spin_axis_bf,
                const double           inertia, 
                const double           max_speed, 
                const double           max_motor_torque);              
  ReactionWheel(const ReactionWheel &rw);
  ~ReactionWheel();   
  ReactionWheel& operator=(const ReactionWheel &rw);                               

  std::string     getMode()                 const;
  Eigen::Vector3d getSpinAxis()             const;
  double          getInertia()              const;
  double          getAngularVelocity()      const;
  double          getAngularAcceleration()  const;  
  double          getMotorTorque()          const;
  
  void            setMotorTorque(const double torque);

  void            CalculateAngularVelocity(const double timestep);
  void            CalculateAngularAcceleration(const Eigen::Vector3d &sat_ang_accel);

private:
  std::string     mode_;
  Eigen::Vector3d spin_axis_bf_;         
  double          inertia_;    
  double          angular_velocity_;
  double          angular_acceleration_;
  double          motor_torque_;
  double          max_angular_velocity_;  
  double          max_motor_torque_;
};

class Satellite
{
public:
  Satellite(const std::string     &satellite_id, 
            const Eigen::Vector3d &icrf_pos,
            const Eigen::Vector3d &icrf_vel,
            const double           mass, 
            const double           cross_section,
            const double           drag_coeff, 
            const double           radiation_pressure_coeff);
  Satellite(const Satellite &sat);
  virtual ~Satellite();
  Satellite& operator=(const Satellite &sat);

          std::string                   getSatelliteID()                  const;
          Eigen::Vector3d               getLatLonPosition()               const;
          Eigen::Vector3d               getITRFPosition()                 const;
          Eigen::Vector3d               getICRFPosition()                 const;
          Eigen::Vector3d               getICRFVelocity()                 const;
          Eigen::Vector3d               getICRFAcceleration()             const;
          double                        getEccentricity()                 const;
          double                        getSemiMajorAxis()                const;
          double                        getInclincation()                 const;
          double                        getAscendingNode()                const;
          double                        getArgumentOfPeriapsis()          const;
          double                        getTrueAnomaly()                  const;
          Eigen::Vector3d               getAtmosphericDragForce()         const;
          Eigen::Vector3d               getEarthGravityForce()            const;
          Eigen::Vector3d               getPlanetaryGravityForce()        const;
          Eigen::Vector3d               getSolarPressureForce()           const;  
          Eigen::Vector3d               getTotalThrusterForce()           const;
          Eigen::Vector3d               getAppliedForce()                 const;
          double                        getMass()                         const;
          double                        getCrossSection()                 const;
          double                        getDragCoeff()                    const;
          double                        getRadiationPressureCoeff()       const;

  virtual Eigen::Quaterniond            getAttitude()                     const;
  virtual Eigen::Vector3d               getAngularVelocity()              const;
  virtual Eigen::Vector3d               getAngularAcceleration()          const; 
  virtual Eigen::Vector3d               getAtmosphericDragTorque()        const;
  virtual Eigen::Vector3d               getGravityGradientTorque()        const;
  virtual Eigen::Vector3d               getMagneticTorque()               const;
  virtual Eigen::Vector3d               getSolarPressureTorque()          const;
  virtual Eigen::Vector3d               getTotalThrusterTorque()          const;
  virtual Eigen::Vector3d               getAppliedTorque()                const;
  virtual double                        getNumberOfMagnetorquers()        const;
  virtual double                        getNumberOfReactionWheels()       const;
  virtual double                        getNumberOfThrusters()            const;
  virtual std::vector<std::string>      getMagnetorquersState()           const;
  virtual std::vector<Eigen::Vector3d>  getMagnetorquersMagneticMoment()  const;
  virtual std::vector<double>           getMagnetorquersOperatingLoad()   const;
  virtual std::vector<std::string>      getReactionWheelsState()          const;
  virtual std::vector<double>           getReactionWheelsAngularVel()     const;
  virtual std::vector<double>           getReactionWheelsAngularAccel()   const;  
  virtual std::vector<double>           getReactionWheelsMotorTorque()    const;
  virtual std::vector<double>           getThrustersThrottle()            const;
  virtual std::vector<Eigen::Vector3d>  getThrustersForce()               const; 
  virtual std::vector<Eigen::Vector3d>  getThrustersTorque()              const;   
  virtual std::vector<double>           getThrustersMassFlowRate()        const;
  virtual Eigen::Vector3d               getAngularMomentum()              const;
  virtual Eigen::Vector3d               getAppliedAngularMomentumDelta()  const;
  virtual Eigen::Vector3d               getRWAngularMomentum()            const;  

          void setEarthGravitationalForce    (const Eigen::Vector3d &icrf_accel);
          void setPlanetaryGravitationalForce(const Eigen::Vector3d &icrf_accel);
          void setDragForce                  (const Eigen::Vector3d &icrf_accel);
          void setSolarPressureForce         (const Eigen::Vector3d &icrf_accel);
          void setThrustersForce             (const Eigen::Vector3d &icrf_force);
  virtual void setMagnetorquersLoad          (const Eigen::VectorXd &operating_load);
  virtual void setReactionWheelsMotorTorque  (const Eigen::VectorXd &motor_torque);
  virtual void setThrustersThrottle          (const Eigen::VectorXd &throttle);

          void InitializeTrajectoryRefFrames(const EarthOrientation &earth_orientation);
          void CalculateTrajectoryState();
          void PropagateTrajectory(const double timestep);

  virtual void InitializeAttitudeRefFrames();
  virtual void CalculateAttitudeState(const double mjd_utc, 
                                      std::unique_ptr<EarthMagneticField> &magnetic_field,
                                      const EarthOrientation &earth_orientation);
  virtual void PropagateAttitude(const double timestep);

protected:
  std::string     satellite_id_;
  Eigen::Vector3d icrf_position_;
  Eigen::Vector3d itrf_position_;
  Eigen::Vector3d lat_lon_position_;
  Eigen::Vector3d icrf_velocity_;
  Eigen::Vector3d itrf_velocity_;
  Eigen::Vector3d icrf_acceleration_;
  Eigen::Vector3d atmospheric_drag_force_icrf_; 
  Eigen::Vector3d earth_gravitational_force_icrf_;
  Eigen::Vector3d planterary_gravitational_force_icrf_;
  Eigen::Vector3d solar_pressure_force_icrf_;
  Eigen::Vector3d thrusters_force_icrf_;
  Eigen::Vector3d applied_force_icrf_;
  double          eccentricity_;
  double          semimajor_axis_;
  double          inclination_;
  double          ascending_node_;
  double          arg_periapsis_;
  double          true_anomaly_;
  double          mass_;
  double          mass_dot_;
  double          cross_section_;
  double          drag_coeff_;
  double          radiation_pressure_coeff_;

          void            CalculateNewMass(const double timestep);
          void            StateVectorToOrbitalElements();
  virtual Eigen::Vector3d CalculateThrustersForce();
  virtual double          CalculateThrusterMassFlowRate();          
};

class SimulationTime
{
 public:
  SimulationTime();
  SimulationTime(const int    year,
                 const int    month, 
                 const int    day, 
                 const double hour, 
                 const double min, 
                 const double sec);
  SimulationTime(const SimulationTime &time);
  ~SimulationTime();
  SimulationTime& operator=(const SimulationTime &time);                 

  double      getMjdUTC()               const;
  double      getMjdTT()                const;
  double      getMjdUT1()               const;
  double      getMjdTDB()               const;
  double      getJulianCenturyTT()      const;
  std::string getGregorianTime()        const;
  double      getSimulationIterations() const;
  double      getRuntime()              const;
  double      getTimestep()             const;


  void IncrementTime(const double timestep);
  void UpdateTimescales(const double tai_utc_offset, 
                        const double ut1_utc_offset);

 private:
  double year_;
  double month_;
  double day_;
  double hour_;
  double min_;
  double sec_;

  double timestep_;
  double runtime_;
  int    iterations_;

  double mjd_utc_;
  double mjd_tt_;
  double mjd_ut1_;
  double mjd_tdb_;
  double julian_century_tt_;

  double CalculateMjdUTC(const int year, 
                         const int month, 
                         const int day, 
                         const double hour, 
                         const double min, 
                         const double sec) const;
  double CalculateMjdTT(const double mjd_utc, const double tai_utc_offset) const;
  double CalculateMjdUT1(const double mjd_utc, const double ut1_utc_offset) const;
  double CalculateMjdTDB(const double mjd_tt, const double julian_century_tt) const;
  double CalculateJulianCenturyTT(const double tt) const;

  void Mjd2Gregorian(const double mjd);
};

class Simulator
{
 public:
  Simulator(const std::string              &sim_config_xml_filepath, 
            const std::vector<std::string> &sat_config_xml_filepaths,
            const std::string              &sim_identifier = "",
            const bool                      monte_carlo_method = false,
            const int                       monte_carlo_runs = 0);          
  ~Simulator();

  void                                    InitializeState();
  void                                    CalculateTrajectory();
  void                                    CalculateAttitude();
  void                                    LogData();
  std::vector<std::shared_ptr<Satellite>> getSatellitePointers();
  void                                    Propagate(const double timestep);

 private:
  bool monte_carlo_method_;
  int monte_carlo_runs_;
  std::string results_directory_;

  SimulationTime   time_;
  EarthOrientation earth_orientation_;

  std::unique_ptr<AtmosphericModel>       atmospheric_model_;
  std::unique_ptr<EarthGravityField>      earth_gravity_field_;
  std::unique_ptr<EarthMagneticField>     earth_magnetic_field_;
  std::unique_ptr<PlanetaryPerturbations> planetary_perturbations_;
  std::unique_ptr<SolarRadiation>         solar_radiation_model_;

  std::vector<std::shared_ptr<Satellite>> satellites_;

  std::ofstream sim_config_log_file_;
  std::vector<std::ofstream> satellite_log_files_;
  
  void                                    ParseSimulationConfiguration     (const std::string &sim_config);
  EarthOrientation                        ParseEarthOrientationParameters  (const rapidxml::xml_node<> *node);
  SimulationTime                          ParseSimulationTimeParameters    (const rapidxml::xml_node<> *node);
  std::unique_ptr<AtmosphericModel>       ParseAtmosphericModelParameters  (const rapidxml::xml_node<> *node);
  std::unique_ptr<EarthGravityField>      ParseEarthGravityFieldParameters (const rapidxml::xml_node<> *node);
  std::unique_ptr<EarthMagneticField>     ParseEarthMagneticFieldParameters(const rapidxml::xml_node<> *node);
  std::unique_ptr<PlanetaryPerturbations> ParsePlanetaryEphemeris          (const rapidxml::xml_node<> *node);
  std::unique_ptr<SolarRadiation>         ParseSolarRadiationParameters    (const rapidxml::xml_node<> *node);
  std::shared_ptr<Satellite>              ParseSatelliteParameters         (const std::string &sat_config, 
                                                                            const std::string &operational_mode);
  double getParsedNumericValue(const rapidxml::xml_node<> *node, 
                               const std::string xml_tag, 
                               const std::string mode = "Nominal");
  double UniformDistributionValue(const double min, const double max);                           

  std::string getSatelliteLogFilepath(const std::string &sim_identifier,
                                      const std::string &satellite_id,
                                      const std::string &operating_mode,
                                      const int monte_carlo_run_num = 0);
  void LogSimHeader(std::ofstream &file, int num_sats, const std::string &XML_filepath);
  void LogSimData  (std::ofstream &file);
  void LogSatHeader(std::ofstream &file, 
                    const std::shared_ptr<Satellite> &sat,
                    const std::string &XML_filepath,
                    const std::string &operating_mode,
                    int monte_carlo_run_num = 0);
  void LogSatData  (std::ofstream &file, const std::shared_ptr<Satellite> &sat);
};

class SolarRadiationAnalytical: public SolarRadiation
{
 public:
  SolarRadiationAnalytical(const std::string &model_name, 
                           const double solar_intensity);
  SolarRadiationAnalytical(const SolarRadiationAnalytical &model);
  ~SolarRadiationAnalytical();
  SolarRadiationAnalytical& operator=(const SolarRadiationAnalytical &model);                           

  Eigen::Vector3d getAccelICRF(const Eigen::Vector3d            &r_sun, 
                               const Eigen::Vector3d            &r_moon,   
                               const std::shared_ptr<Satellite> &sat) override;
  Eigen::Vector3d getSolarIntensityICRF() override;

 private:
  double          solar_intensity_1au_;
  double          solar_fraction_;
  Eigen::Vector3d solar_intensity_;
  double          au_;
  double          earth_radius_;
  double          sun_radius_;
  double          moon_radius_;

  double getVisibleSolarFraction(const Eigen::Vector3d &r_sat,  
                                 const Eigen::Vector3d &r_sun,     
                                 const Eigen::Vector3d &r_body, 
                                 const double           body_radius) const;
};

class SolarRadiation
{
 public:
  SolarRadiation(const std::string &model_name);
  SolarRadiation(const SolarRadiation &model);
  virtual ~SolarRadiation();
  SolarRadiation& operator=(const SolarRadiation &model);

  virtual Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &r_sun, 
                                       const Eigen::Vector3d &r_moon,   
                                       const std::shared_ptr<Satellite> &sat);
  virtual Eigen::Vector3d getSolarIntensityICRF();
  std::string getModelName() const;
  
 protected:
  std::string model_name_;
};

class Spacecraft : public Satellite
{
 public:
  Spacecraft(const std::string                &satellite_id,
             const Eigen::Vector3d            &icrf_pos, 
             const Eigen::Vector3d            &icrf_vel, 
             const double                      mass, 
             const double                      cross_section, 
             const double                      drag_coeff, 
             const double                     radiation_pressure_coeff,
             const Eigen::Matrix3d            &inertia_matrix,
             const Eigen::Vector3d            &center_of_mass_offset,
             const Eigen::Vector3d            &static_magnetic_moment,
             const Eigen::Quaterniond         &quaternion,
             const Eigen::Vector3d            &angular_velocity_bf,
             const std::vector<Magnetorquer>  &magnetorquers,
             const std::vector<ReactionWheel> &reaction_wheels,
             const std::vector<Thruster>      &thrusters);
  Spacecraft(const Spacecraft &sc);
  ~Spacecraft();
  Spacecraft& operator=(const Spacecraft &sc);

  Eigen::Quaterniond            getAttitude()                     const override;
  Eigen::Vector3d               getAngularVelocity()              const override;
  Eigen::Vector3d               getAngularAcceleration()          const override;
  Eigen::Vector3d               getAtmosphericDragTorque()        const override;
  Eigen::Vector3d               getGravityGradientTorque()        const override;
  Eigen::Vector3d               getMagneticTorque()               const override;
  Eigen::Vector3d               getSolarPressureTorque()          const override;
  Eigen::Vector3d               getTotalThrusterTorque()          const override;
  Eigen::Vector3d               getAppliedTorque()                const override;
  double                        getNumberOfMagnetorquers()        const override;
  double                        getNumberOfReactionWheels()       const override;
  double                        getNumberOfThrusters()            const override;
  std::vector<std::string>      getMagnetorquersState()           const override;
  std::vector<Eigen::Vector3d>  getMagnetorquersMagneticMoment()  const override;
  std::vector<double>           getMagnetorquersOperatingLoad()   const override;
  std::vector<std::string>      getReactionWheelsState()          const override;
  std::vector<double>           getReactionWheelsAngularVel()     const override;
  std::vector<double>           getReactionWheelsAngularAccel()   const override;  
  std::vector<double>           getReactionWheelsMotorTorque()    const override;
  std::vector<double>           getThrustersThrottle()            const override;
  std::vector<Eigen::Vector3d>  getThrustersForce()               const override;
  std::vector<Eigen::Vector3d>  getThrustersTorque()              const override;  
  std::vector<double>           getThrustersMassFlowRate()        const override;
  Eigen::Vector3d               getAngularMomentum()              const override;
  Eigen::Vector3d               getAppliedAngularMomentumDelta()  const override;
  Eigen::Vector3d               getRWAngularMomentum()            const override;         

  void setMagnetorquersLoad        (const Eigen::VectorXd &operating_fraction) override;
  void setReactionWheelsMotorTorque(const Eigen::VectorXd &motor_torque) override;
  void setThrustersThrottle        (const Eigen::VectorXd &throttle) override;

  void InitializeAttitudeRefFrames() override;
  void CalculateAttitudeState(const double mjd_utc, 
                              std::unique_ptr<EarthMagneticField> &magnetic_field,
                              const EarthOrientation &earth_orientation) override;
  void PropagateAttitude(const double timestep) override;


 private:
  Eigen::Matrix3d             inertia_matrix_;
  Eigen::Vector3d             center_of_mass_offset_;
  Eigen::Vector3d             static_magnetic_moment_;
  Eigen::Vector3d             sc_magnetic_moment_;  
  Eigen::Quaterniond          quaternion_;
  Eigen::Vector3d             angular_vel_bf_;
  Eigen::Vector3d             angular_accel_bf_; 
  Eigen::Vector3d             sc_angular_momentum_; 
  Eigen::Vector3d             applied_angular_momentum_delta_;
  Eigen::Vector3d             rw_angular_momentum_;
  Eigen::Vector3d             applied_rw_motor_torque_;
  std::vector<Magnetorquer>   magnetorquers_;
  std::vector<ReactionWheel>  reaction_wheels_;
  std::vector<Thruster>       thrusters_;
  Eigen::Matrix3d             bf2icrf_rot_matrix_;
  Eigen::Matrix3d             icrf2bf_rot_matrix_; 
  Eigen::Vector3d             atmospheric_drag_torque_;
  Eigen::Vector3d             gravity_gradient_torque_;
  Eigen::Vector3d             magnetic_torque_;
  Eigen::Vector3d             solar_pressure_torque_;
  Eigen::Vector3d             thruster_torque_; 
  Eigen::Vector3d             applied_torque_;

  Eigen::Matrix3d CalculateBF2ICRFRotationMatrix();

  Eigen::Vector3d CalculateAppliedTorque(const double mjd_utc, 
                                         std::unique_ptr<EarthMagneticField> &magnetic_field,
                                         const EarthOrientation &earth_orientation);
  Eigen::Vector3d CalculateGravityGradientTorque();
  Eigen::Vector3d CalculateSolarPressureTorque();
  Eigen::Vector3d CalculateAtmosphericDragTorque();
  Eigen::Vector3d CalculateMagneticTorque(const double mjd_utc, 
                                          std::unique_ptr<EarthMagneticField> &magnetic_field,
                                          const EarthOrientation &earth_orientation);
  Eigen::Vector3d CalculateThrustersTorque();
  Eigen::Vector3d CalculateThrustersForce();
  
  double CalculateThrusterMassFlowRate();
  void CalculateNewAttitude(const double timestep);
};

class Thruster
{
public:
  Thruster(const Eigen::Vector3d &thrust_vector, 
           const Eigen::Vector3d &thruster_position, 
           const double           max_mass_flow_rate, 
           const double           max_exit_velocity);
  Thruster(const Thruster &th);
  ~Thruster();
  Thruster& operator=(const Thruster &th);

  Eigen::Vector3d getThrusterForce()                  const;
  Eigen::Vector3d getThrusterTorque()                 const;
  double          getThrottle()                       const;
  double          getMassFlowRate()                   const;

  void            setThrottle(const double throttle);
  void            CalculateThrust();  
  
private:
  Eigen::Vector3d thrust_vector_;
  Eigen::Vector3d thruster_position_;
  Eigen::Vector3d thruster_force_;
  Eigen::Vector3d thruster_torque_;
  double          throttle_;
  double          max_mass_flow_rate_;
  double          max_exit_velocity_;

};