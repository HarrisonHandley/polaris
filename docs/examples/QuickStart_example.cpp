#include "simulator.hpp"
#include <string>

int main()
{
  
  std::string sim_config = "C:\\Users\\example\\Polaris\\input\\sim_config.xml";
  std::vector<std::string> sat_configs = {"C:\\Users\\example\\Polaris\\input\\sat_config.xml"};

  polaris::Simulator sim(sim_config, sat_configs, "Nominal", "Sim_1");

  for(std::size_t i = 0; i <= 10000; ++i)
  {
    sim.InitializeState();
    sim.CalculateTrajectory();
    sim.CalculateAttitude();
    sim.LogData();
    sim.Propagate(0.0001);
  }
}