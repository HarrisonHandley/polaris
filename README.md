# Polaris Spacecraft Simulator

Polaris is a spacecraft 6 Degree of Freedom simulator C++ library for use in the development of attitude control algorithms, and Software and Hardware In The Loop Testing. The simulator utilizes mathematical models and numerical approximations to provide a full environmental and dynamic simulation of the satellite in operation in faster than real time.

Features:
- High Fidelity Orbital Force Model 
- Rigid Body Attitude Propagation
- Multiple Spacecraft/Constellations
- Monte Carlo Simulations
- Supports Both Spacecraft Orbit and/or Attitude Simulations
- Simple API for Custom Integration
- Uses Simulator and Spacecrafts XML Configuration Files for Initialization
- Simulator Data Exported as CSV Files
- Validated against STK and NASA GMAT
- Variable Environmental Models Configurations
  - Atmospheric Drag Force
  - Earth Gravity Field Force
  - Earth Magnetic Field Force
  - Planetary Gravity Field Forces
  - Solar Radiation Pressure Force
- Custom Spacecraft Actuator Configurations
  - Magnetic Torque Rods
  - Reaction Wheels
  - Thrusters

Project documentation can be found here: https://harrisonhandley.gitlab.io/polaris/

# How To Use Polaris

Here is a rather simple example program to get you started.

```
#include "simulator.hpp"
#include <string>

int main()
{
  std::string sim_config = "C:\\Users\\example\\Polaris\\input\\sim_config.xml";
  std::vector<std::string> sat_configs = {"C:\\Users\\example\\Polaris\\input\\sat_config.xml"};
  polaris::Simulator sim(sim_config, sat_configs, "ExampleSimulation", true, 100); // ExampleSimulation with 100 Monte Carlo Runs
 
  for(std::size_t i = 0; i <= 10000; ++i)
  {
    sim.InitializeState();
    sim.CalculateTrajectory();
    sim.CalculateAttitude();
    sim.LogData();
    sim.Propagate(0.0001); // Propagate by 0.0001 seconds
  }
}
```

The simulator header file defines the wrapper interface for the simulator. To run a simulation, a simulator instance needs to be constructed. The constructor requires:
- Filepath to the Simulation XML configuration file
- Vector of filepaths to each Satellite XML configuration file. This is a vector of filepaths to support the simulation of multiple satellites and spacecrafts.
- String argument that will be appended to the results filename and act as a unique identifier for the simulation run. Useful when running multiple simulations autonomously.
- Optional: Boolean argument for enabling Monte Carlo simulations. Default is set to False.
- Optional:  Int argument for the number of Monte Carlo simulation runs that will be completed. Default is set to 0.

A standard approach to running the Polaris simulator is to use a for loop to iterate over the simulator methods for a given number of iterations.


The first line for running the Polaris Simulator is the method InitializeState. The method initializes the state of the simulation environment and each satellite/spacecraft. This method is required to be used after the simulator constructor and propagating the simulation. The method updates the state of the simulation in accordance with the new simulation time, and satellite/spacecraft dynamic state.


The second line for running the Polaris Simulator is the method CalculateTrajectory. This method calculates the forces acting on the satellite and it's associated trajectory acceleration in ICRF.


The third line for running the Polaris Simulator is the method CalculateAttitude. This method calculates the torques and momentum acting on the satellite from actuators and environmental sources. Followed by calculating the spacecrafts angular momentum and angular acceleration for the current satellite attitudes.


The fourth line for running the Polaris Simulator is the method LogData. This method is optional but is used to log the current state of the simulation into the associated log files for each satellite/spacecraft. And a separate file for all associated environmental data.


For implementation of attitude controllers and getting satellite/spacecraft sensor state to Hardware In The Loop or Software In The Loop. The method getSatellitePointers can be used to access the getters of the Satellite\Spacecraft sensors and the setters for the spacecraft actuators. This public methods are documented in the Satellite class.


The final line for running the Polaris Simulator is the method Propagate. This method propagates the state of the simulation by a timestep in seconds (for this example it is 0.0001 seconds). It calculates the new state vector and attitude of the satellite by propagating acceleration and angular acceleration forward.

# Polaris Simulator Class Diagram

The simulation uses a class wrapper ("Simulator") to provide the library API to the user for simplification. The internal program structure of the simulation is shown below in the class diagram.
![Class_Diagram](https://static.wixstatic.com/media/6f2977_5b58437eeecf4b9ea227a6b088bf1610~mv2.png/v1/fill/w_1175,h_1342,al_c,q_90,usm_0.66_1.00_0.01/6f2977_5b58437eeecf4b9ea227a6b088bf1610~mv2.webp)
The program structure can be separated into the following sections, discussed in further detail below.

- Time and State of Simulation
- Environmental Models
- Satellite and Spacecraft Objects
- Spacecraft Attitude Actuators

# Time and State of Simulation

The simulation uses the reference frames:
- International Terrestrial Reference Frame
- International Celestial Reference Frame
- Latitude and Longitude for WGS84
- True of Day Reference Frame

To enable conversion between the different reference frames, knowledge of the Earth's orientation is required. This is achieved using the International Earth Rotation and Reference Systems Services Earth Orientation Parameters (IERS EOP). Polaris has a designated "EarthOrientation" class devoted to the calculation of the Earth's orientation and associated rotation matrices.


Calculation of the Earth's orientation requires the current time in the following formats:
- Coordinated Universal Time (UTC) - Modified Julian Date
- Universal Time (UT1) - Modified Julian Date
- Barycentric Dynamical Time (TDB) - Modified Julian Date
- Terrestrial Time (TT) - Modified Julian Date
- Terrestrial Time (TT) - Julian Century


This requirement is in addition to the need to regulate and control the state of the simulation time. Therefore, a dedicated "SimulationTime" class is used to control the simulation time for each time scale.


# Environmental Models

Satellite motion and dynamics are governed by environmental forces. For an Earth orbiting satellite the dominant forces are as follows:

- Atmospheric Drag Force
- Earth Gravity Field Force
- Earth Magnetic Field Force
- Planetary Gravity Field Forces
- Solar Radiation Pressure Force


To enable a high fidelity simulation, each environmental force needs to be modeled with a high degree of accuracy as part of a force based orbital model.


# Atmospheric Drag Model

Atmospheric drag forces are the dominant non-gravitational force acting on a low altitude satellites. As the drag force depends on the atmospheric density at the satellite location, a variety of empirical atmospheric density models have been created. Each model differs on their model accuracy, computational intensity, valid altitude range, and their required input parameters.


The Polaris simulation has been configured to support multiple of these atmospheric density models through inheritance of the abstract class interface "AtmosphericModel" and the virtual "CalculateAtmosphericDensity" method. Each model uses the same closed form drag force equation to calculate the acceleration acting on the satellite due to atmospheric drag based on the calculated atmospheric density.


The following is a list of prominent atmospheric density models, along with their associated tradeoffs.


**Analytical Model**

The Analytical model is a closed form numerical equation based on the input parameters of the satellites position. This model is computationally fast, but suffers from a high model inaccuracy across a wide range of altitudes. Due to the models independency from requiring space weather indices data, it is suitable for use in predictive simulations.


**Harris-Priester**

The Harris-Priester model uses a linear interpolation method on a look-up table based on the altitude, and Sun's solar radio flux at 10.7 cm.  The model is computationally fast for non-closed formed solution based models due its nature of using a lookup table. But suffers from model inaccuracy, a limited range of 110-2000 km altitude, and a discontinuous density profile over the range. The models dependency on only the Sun's solar radio flux at 10.7 cm, allows the model to be suitable for use in predictive simulations, for a given predicted radio flux value.


**Jacchia 1971**

Jacchia 1971 model is based on an empirical temperature profile for an altitude range of 90 to 2500 km. The temperature parameter is the exospheric temperature. The temperature can be computed from solar activity, geomagnetic index, and diurnal variations in the atmosphere. Due to the complexity of the calculation, simplifications and assumptions can be made. 


With the exospheric temperature computed, the model requires the input of the molecular weights and the fraction by volume of the atmospheric species nitrogen, oxygen, argon and helium at sea level. It can then be used to calculate the atmospheric density through the integration of a differential equation.


The model has the capacity to be accurate based on the accuracy of the input parameters. Due to the nature of the model using integration, it is reasonably computationally intensive, but remains suitable for real-time simulations.


**NRLMSISE‐00**

The NRLMSISE-00 empirical atmospheric model is the most accurate model available and is the standard. It is a major upgrade on the previous MSISE-90 model, due to its use of additional satellite data. The model requires inputs of time, position, and space weather indices.  The evaluation process of the model is computationally intensive, and as a result is not suitable for real-time simulations, without multi-threading. 


Due to the necessity of Polaris to simulate future satellite missions scheduled to launch in 2021. I've opted to implement the Harris-Priester model first, which is not dependent on space weather indices, and is moderately accurate without requiring significant CPU time.


The following atmospheric density models will be implemented at a future date.

- Analytical Model
- Jacchia 1971
- Jacchia-Roberts
- MSIS-2000



# Earth Gravity Field Model

The Earth is an oblate spheroid, and because the internal mass distribution of the Earth is unknown, the Earth's gravity field has to be calculated from measurements and observations.


The Earth gravity models are calculated from a spherical harmonic function for a given set of coefficients and the position in spherical coordinates. In a similar manner to Atmospheric Density models, there are a number of available models differing based on coefficient values, and order terms.


The Polaris simulation has been configured to support multiple of these Earth gravity field models through inheritance of the abstract class interface "EarthGravityField" and the virtual "getAccelITRF" method.


The following is a list of prominent Earth gravity field models, along with their differences.


**GEM-3**

NASA Goddard Earth Model uses empirical data from 31 close earth satellites.


**JGM-3**

Joint Gravity Model is the combination of NASA Goddard Earth gravity field models with universities and private companies satellite tracking data.


**EGM2008**

Earth Gravitational Model 2008 is a least squares combination of the GGM03s gravitational model and data obtained from airborne gravity data, and data implied from topography.


**GGM03s**

Builds upon prior Earth gravity field models GGM01 and GGM02 based on the measurements from the GRACE satellite missions.


Polaris has implemented the Grace Gravity model (GGM03s) first, due to its high accuracy using all terms, and its ability to be computed with lower number of terms if faster computation is required.


The following Earth gravity field models will be implemented at a future date.

- GEM-3
- JGM-3
- EGM2008



# Earth Magnetic Field Model

Satellite residual and permanent magnetic fields from the electronics and actuators interact with the Earth's magnetic field. Inducing torques acting on the satellite and affecting its attitude.


The Earth's magnetic field is generated by electric currents from the motion of the Earth's inner core. The field has both short-term and secular variations, affecting the magnetic field experienced by satellites in orbit. 


To model the magnetic field accurately, empirical data is used to develop a spherical harmonic numerical model by the International Association of Geomagnetism and Aeronomy. The model is known as the International Geomagnetic Reference Field (IGRF), and is updated every 5 years based on newly collected data. Polaris implements IGRF-13, the most recent version of the model. 


The Polaris simulation has been configured to support multiple Earth Magnetic Field models through inheritance of the abstract class interface "EarthMagneticField" and the virtual "getMagneticField" method. Future developments will implement an analytical model as an additional model option. This approach offers the alternative to sacrifice accuracy at the gain of computational speed.


# Planetary Gravity Field Model

The orbit of satellites and spacecraft is influenced by the gravitational forces of the solar systems planets. To accurately calculate the forces, accurate positioning of each planetary body is required. The applied force to the satellite can then be calculated treating each body as a point mass (excluding the Earth). The addition of other celestial bodies such as asteroids is an option, but the effect is approximately the same or smaller order of magnitude as the error in other environmental models. And therefore is not included in Polaris to save on computation time. 


The standard numerical model for planetary positions is the high precision JPL Ephemerides. These ephemerides are used for interplanetary missions, and are continually updated under the name "DE4--". With the most recent model named DE436.


Evaluation of the JPL Ephemerides is conducted through the evaluation of the Chebyshev polynomials using the provided coefficients and current Terrestrial Time Julian date century. 


The Polaris simulation has been configured to support multiple Planetary Position models through inheritance of the abstract class interface "PlanetaryPertubrations" and the virtual "getAccelICRF" method. Future developments will implement a Kepler orbital element model for each celestial body as an additional option. This approach offers the alternative to sacrifice accuracy at the gain of computational speed.


# Solar Radiation Pressure Model

When a satellite is exposed to solar radiation it experiences a small force from the absorption and reflection of photons. This force overtime affects the satellite orbit and attitude. To ensure a high degree of accuracy is achieved in the simulation, modeling the solar radiation pressure force is a necessity.


To effectively model solar radiation pressure, it is first required to determine whether a satellite is fully illuminated by the Sun or if it is partial/fully eclipsed by a celestial body. For an Earth orbiting satellite, the Earth and the Moon are the occulting bodies. The ratio of the eclipse condition can be computed numerically using a geometric conical model when neglecting the oblateness of the occulting body. The following figure shows the simplified geometric problem that can be solved to find the Sun eclipse percentage.
![Solar_Occultation](https://static.wixstatic.com/media/6f2977_24356ff414c84c748ea78a807834a18a~mv2.png/v1/fill/w_756,h_524,al_c,lg_1,q_90/6f2977_24356ff414c84c748ea78a807834a18a~mv2.webp)

The solar radiation pressure force can subsequently be calculated using a closed form equation.

# Satellite and Spacecraft Objects

The Polaris simulator is built around the satellite base class and its derived spacecraft class. These objects are constructed from the XML configuration files passed to the simulation constructor. This approach of using inheritance allows the simulation to support 2 different configurations of objects.


The satellite class is responsible for maintaining the state vector and all trajectory associated parameters during the simulation. This class is designed to support the simulation of celestial objects and satellites where only the trajectory is of interest.  


The spacecraft class inherits the satellite class and its associated trajectory parameters. And adds the functionality of maintaining the attitude state of the spacecraft and its attitude actuators. This class is designed to support the simulation of spacecraft objects, where both attitude and trajectory is of interest.


As a result of using this class structure, end users can choose to simulate spacecraft and celestial objects within the same simulation. Allowing for the simulation of rendezvousing and other additional space operations. While also providing the functionality to do faster simulations of only satellite trajectories if desired. 


# Spacecraft Attitude Actuators

The spacecraft class handles the attitude of the simulated spacecraft. As part of constructing this object, the spacecraft can be assigned actuators used in the attitude control. The simulation currently supports these actuators:

- Magnetic Torque Rods (Magnetorquers)
- Reaction Wheels
- Thrusters 


In the XML configuration file for the spacecraft, the actuator configuration can support any number and combination of these actuators. Ensuring support for custom spacecrafts. Additional attitude actuators will be added in future development
