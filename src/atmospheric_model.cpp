//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  AtmosphericModel Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "atmospheric_model.h"

#include "constants.h"

namespace polaris {

//------------------------------------------------------------------------------
//  AtmosphericModel(const std::string &model_name)
//------------------------------------------------------------------------------
/** Default constructor requiring configuration of atmospheric model.
  *
  * \param model_name Name of the atmospheric model used. 
  */
AtmosphericModel::AtmosphericModel(const std::string &model_name) : 
                                   model_name_(model_name)
{

}

//------------------------------------------------------------------------------
//  AtmosphericModel(const AtmosphericModel &model) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param model The object being copied.
  */
AtmosphericModel::AtmosphericModel(const AtmosphericModel &model) :
                                   model_name_(model.model_name_)
{

}

//------------------------------------------------------------------------------
//  ~AtmosphericModel() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
AtmosphericModel::~AtmosphericModel()
{

}

//------------------------------------------------------------------------------
//  AtmosphericModel& operator = (const AtmosphericModel &model) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param model The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
AtmosphericModel& AtmosphericModel::operator = (const AtmosphericModel &model)
{
  if(&model == this)
  {
    return *this;
  }
  model_name_ = model.model_name_;
  return *this;
}

//------------------------------------------------------------------------------
//  std::string getModelName()
//------------------------------------------------------------------------------
/** Get the name of the model used to calculate the earth atmospheric density. 
  *
  * \return std::string of the name of the model used.
  */
std::string AtmosphericModel::getModelName() const
{
  return model_name_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &r_sun,
//                               const EarthOrientation &earth_orientation,
//                               const std::shared_ptr<Satellite> &sat) 
//------------------------------------------------------------------------------
/** Calculates the acceleration acting on the satellite from atmospheric drag in
  * ICRF using equations:
  * 
  * \f$\vec{v}_{relative} = \vec{v}_{TOD} - \omega_{earth} \times \vec{r}_{TOD} \f$
  * 
  * \f$\vec{a} = \frac{-1}{2} C_D \frac{A}{M} \rho \|\vec{v}\| 
  * \vec{v}_{relative}\f$
  * 
  * \param r_sun Position vector of the sun in ICRF \f$[x, y, z]\f$ in \f$[m]\f$
  * \param earth_orientation EarthOrientation object to define the rotation
  *                          matrices to convert between ICRF, ITRF and True
  *                          of Date reference frame.
  * \param sat Satellite object, providing the required drag coefficient, cross 
  *            sectional area \f$[m^2]\f$ and mass \f$[kg]\f$ of spacecraft 
  *            for the calculation.
  * \return Eigen::Vector3d Acceleration vector \f$[\a_x, \a_y, \a_z]\f$ 
  *         in ICRF/ICRS ref frame in SI units [m/s^2].
  */
Eigen::Vector3d AtmosphericModel::getAccelICRF(const Eigen::Vector3d  &r_sun,
                                               const EarthOrientation &earth_orientation,
                                               const std::shared_ptr<Satellite> &sat)
{
  double density = CalculateAtmosphericDensity(r_sun, sat->getICRFPosition(), earth_orientation);

  Eigen::Vector3d earth_rotation_rate = {0, 
                                         0, 
                                         kEarthRotationRate - kLODScale * earth_orientation.getLOD()};
  Eigen::Vector3d r_sat_tod = earth_orientation.ICRS2TOD(sat->getICRFPosition());
  Eigen::Vector3d v_sat_tod = earth_orientation.ICRS2TOD(sat->getICRFVelocity());
  Eigen::Vector3d v_rel = v_sat_tod - earth_rotation_rate.cross(r_sat_tod);
  double v_abs = v_rel.norm();

  Eigen::Vector3d acceleration_tod = -0.5 * sat->getDragCoeff() 
                                   * (sat->getCrossSection() / sat->getMass())
                                   * density * v_abs * v_rel;
  Eigen::Vector3d acceleration_icrf = earth_orientation.TOD2ICRS(acceleration_tod);
  return acceleration_icrf;
}

} // end namespace polaris