//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  SolarRadiation Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "solar_radiation.h"

#include <iostream>

namespace polaris {

//------------------------------------------------------------------------------
//  SolarRadiation(const std::string &model_name)
//------------------------------------------------------------------------------
/** Default constructor requiring the analytical solar pressure model name.
  *
  * \param model_name Name of the solar pressure model used
  */
SolarRadiation::SolarRadiation(const std::string &model_name) :
                               model_name_(model_name)
{

}

//------------------------------------------------------------------------------
//  SolarRadiation(const SolarRadiation &model) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param model The object being copied.
  */
SolarRadiation::SolarRadiation(const SolarRadiation &model) :
                               model_name_(model.model_name_)

{

}

//------------------------------------------------------------------------------
//  ~SolarRadiation() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
SolarRadiation::~SolarRadiation()
{

}

//------------------------------------------------------------------------------
//  SolarRadiation& operator=(const SolarRadiation &model) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param model The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
SolarRadiation& SolarRadiation::operator=(const SolarRadiation &model)
{
  if(&model == this)
  {
    return *this;
  }
  model_name_ = model.model_name_;
  return *this;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &r_sun,
//                               const Eigen::Vector3d &r_moon, 
//                               const std::shared_ptr<Satellite> &sat) 
//------------------------------------------------------------------------------
/** Virtual method to calculate the acceleration acting on a satellite from 
  * solar pressure at a given location in ICRF using an analytical model.
  *
  * \param r_sun Position vector of the Sun in ICRF with Earth origin 
  *              \f$[x, y, z]\f$ \f$[m]\f$.
  * \param r_moon Position vector of the Moon in ICRF with Earth origin 
  *               \f$[x, y, z]\f$ \f$[m]\f$.
  * \param sat Satellite Class Object required for satellite parameters in the 
  *            calculation (Cross sectional area, radiation pressure 
  *            coefficient, and position).
  * 
  * \return Eigen::Vector3d Acceleration vector acting on satellite in ICRF 
  *         \f$[m/s^2]\f$.
  */
Eigen::Vector3d SolarRadiation::getAccelICRF(const Eigen::Vector3d &r_sun,
                                                          const Eigen::Vector3d &r_moon, 
                                                          const std::shared_ptr<Satellite> &sat)
{ 
  Eigen::Vector3d acceleration = {0, 0, 0};
  return acceleration;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getSolarIntensityICRF() 
//------------------------------------------------------------------------------
/** Virtual method to get the solar intensity acting on a satellite from solar 
  * pressure at a given location in ICRF using an analytical model.
  * 
  * \return Eigen::Vector3d Solar intensity vector acting on satellite in ICRF 
  *         \f$[W/m^2]\f$.
  */
Eigen::Vector3d SolarRadiation::getSolarIntensityICRF()
{
  Eigen::Vector3d solar_intensity = {0, 0, 0};
  return solar_intensity;
}

//------------------------------------------------------------------------------
//  std::string getModelName()
//------------------------------------------------------------------------------
/** Get the name of the model used to calculate the solar radiation pressure. 
  *
  * \return std::string of the name of the model used.
  */
std::string SolarRadiation::getModelName() const
{
  return model_name_;
}

} // end namespace polaris