//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  IGRF Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "igrf.h"

#include <fstream>
#include <iostream>
#include <vector>

#include "constants.h"

namespace polaris
{

//------------------------------------------------------------------------------
//  Igrf(const std::string &model_name, 
//       const std::string &filepath, 
//       const int          max_degree)
//------------------------------------------------------------------------------
/** Default constructor requiring IGRF model configuration data.
  *
  * \param filepath IGRF model coefficients filepath.
  * \param model_name Name of the earth magnetic field model used.
  * \param max_degree Maximum degree of coefficients used for the model 
  *                   calculations.
  */
Igrf::Igrf(const std::string &filepath,
           const std::string &model_name,  
           const int          max_degree) : 
           EarthMagneticField                (model_name),
           g2000_                            (Eigen::ArrayXXd::Zero(0, 0)),
           h2000_                            (Eigen::ArrayXXd::Zero(0, 0)),
           g2005_                            (Eigen::ArrayXXd::Zero(0, 0)),
           h2005_                            (Eigen::ArrayXXd::Zero(0, 0)),
           g2010_                            (Eigen::ArrayXXd::Zero(0, 0)),
           h2010_                            (Eigen::ArrayXXd::Zero(0, 0)),
           g2015_                            (Eigen::ArrayXXd::Zero(0, 0)),
           h2015_                            (Eigen::ArrayXXd::Zero(0, 0)),
           g2020_                            (Eigen::ArrayXXd::Zero(0, 0)),
           h2020_                            (Eigen::ArrayXXd::Zero(0, 0)),
           gsv_                              (Eigen::ArrayXXd::Zero(0, 0)),
           hsv_                              (Eigen::ArrayXXd::Zero(0, 0)),           
           max_degree_                       (max_degree),
           h_                                (Eigen::ArrayXXd::Zero(0, 0)),
           g_                                (Eigen::ArrayXXd::Zero(0, 0)),
           delta_t_                          (0),
           legendre_polynomials_             (Eigen::ArrayXXd::Zero(0, 0)),
           differential_legendre_polynomials_(Eigen::ArrayXXd::Zero(0, 0))
{
  ReadIGRFCoefficientsFile(filepath);
}

//------------------------------------------------------------------------------
//  Igrf(const Igrf &model) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param model The object being copied.
  */
Igrf::Igrf(const Igrf &model) :
           EarthMagneticField                (model.model_name_),
           g2000_                            (model.g2000_),
           h2000_                            (model.h2000_),
           g2005_                            (model.g2005_),
           h2005_                            (model.h2005_),
           g2010_                            (model.g2010_),
           h2010_                            (model.h2010_),
           g2015_                            (model.g2015_),
           h2015_                            (model.h2015_),
           g2020_                            (model.g2020_),
           h2020_                            (model.h2020_),
           gsv_                              (model.gsv_),
           hsv_                              (model.hsv_),           
           max_degree_                       (model.max_degree_),
           h_                                (model.h_),
           g_                                (model.g_),
           delta_t_                          (model.delta_t_),
           legendre_polynomials_             (model.legendre_polynomials_),
           differential_legendre_polynomials_(model.differential_legendre_polynomials_)
{

}

//------------------------------------------------------------------------------
//  ~Igrf() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
Igrf::~Igrf()
{

}

//------------------------------------------------------------------------------
//  Igrf& operator=(const Igrf &model) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param model The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
Igrf& Igrf::operator=(const Igrf &model)
{
  if(&model == this)
  {
    return *this;
  }
  EarthMagneticField::operator = (model),

  g2000_                             = model.g2000_;
  h2000_                             = model.h2000_;
  g2005_                             = model.g2005_;
  h2005_                             = model.h2005_;
  g2010_                             = model.g2010_;
  h2010_                             = model.h2010_;
  g2015_                             = model.g2015_;
  h2015_                             = model.h2015_;
  g2020_                             = model.g2020_;
  h2020_                             = model.h2020_;
  gsv_                               = model.gsv_;
  hsv_                               = model.hsv_;           
  max_degree_                        = model.max_degree_;
  h_                                 = model.h_;
  g_                                 = model.g_;
  delta_t_                           = model.delta_t_;
  legendre_polynomials_              = model.legendre_polynomials_;
  differential_legendre_polynomials_ = model.differential_legendre_polynomials_;
  return *this;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateMagneticField(const Eigen::Vector3d &latlon_position,
//                                         const double mjd_utc)
//------------------------------------------------------------------------------
/** Inherited method from the abstract class EarthMagneticField that is 
  * overriden to calculate the Earth's magnetic field at the location of the 
  * spacecraft in ICRF using the IGRF model.
  * 
  * The model calculates the earth magnetic field by solving a Schmidt 
  * semi-normalized spherical harmonic equation. 
  * https://www.ngdc.noaa.gov/IAGA/vmod/igrf.html
  * 
  * \param latlon_position Eigen::Vector3d Position of the satellite in ITRF 
  *                        in Latitude/Longitude \f$[Lat, Lon, Alt]\f$ 
  *                        \f$[rad]\f$ and \f$[m]\f$.
  * \param mjd_utc double UTC modified julian date.
  * 
  * \return Eigen::Vector3d Earth magnetic field vector at satellite position
  *         \f$[M_x, M_y, M_z]\f$ \f$[T]\f$.
  */
Eigen::Vector3d Igrf::CalculateMagneticField(const Eigen::Vector3d &latlon_position, 
                                             const double           mjd_utc)
{
  CalculateCoefficients(mjd_utc);
  InitializeLegendrePolynomials(latlon_position[0]);

  Eigen::VectorXd integer_array = Eigen::ArrayXd::LinSpaced (max_degree_ + 1, 0, max_degree_);
  Eigen::VectorXd ones_array = Eigen::ArrayXd::Ones(max_degree_ + 1);
  Eigen::ArrayXXd degree = integer_array * ones_array.transpose();
  Eigen::ArrayXXd order  = degree.transpose();

  Eigen::ArrayXXd coeff = pow(kEarthRadius/(kEarthRadius + latlon_position[2]), degree + 2);
  Eigen::ArrayXXd sinphi = sin(latlon_position[1] * order);
  Eigen::ArrayXXd cosphi = cos(latlon_position[1] * order);  
  Eigen::ArrayXXd br_array = coeff * (degree + 1) * (g_ * cosphi + h_ * sinphi) * legendre_polynomials_;
  Eigen::ArrayXXd bt_array = coeff * (g_ * cosphi + h_ * sinphi) * differential_legendre_polynomials_;
  Eigen::ArrayXXd bp_array = coeff * order * (-g_ * sinphi + h_ * cosphi) * legendre_polynomials_;

  double br = br_array.sum();
  double bt = bt_array.sum();
  double bp = bp_array.sum();
  Eigen::Vector3d spherical = {br, bt, bp};
  Eigen::Matrix3d sphframe2cartframe;
  sphframe2cartframe(0,0) =  sin(kPI/2 - latlon_position(0)) * cos(latlon_position(1));
  sphframe2cartframe(0,1) =  cos(kPI/2 - latlon_position(0)) * cos(latlon_position(1));
  sphframe2cartframe(0,2) = -sin(latlon_position(1));
  sphframe2cartframe(1,0) =  sin(kPI/2 - latlon_position(0)) * sin(latlon_position(1));
  sphframe2cartframe(1,1) =  cos(kPI/2 - latlon_position(0)) * sin(latlon_position(1));
  sphframe2cartframe(1,2) =  cos(latlon_position(1));
  sphframe2cartframe(2,0) =  cos(kPI/2 - latlon_position(0));
  sphframe2cartframe(2,1) = -sin(kPI/2 - latlon_position(0));
  sphframe2cartframe(2,2) =  0;

  return sphframe2cartframe * spherical * 1e-9;
}

//------------------------------------------------------------------------------
//  void ReadIGRFCoefficientsFile(const std::string &filepath)
//------------------------------------------------------------------------------
/** Parses the IGRF coefficients file to extract the g and h coefficients for
  * each time period (2000, 2005, 2010, 2015, 2020, and secular variance). Prior
  * time periods are ignored for simplicity. This method initializes all IGRF 
  * data members.
  * 
  * \param filepath IGRF model coefficients filepath.
  */
void Igrf::ReadIGRFCoefficientsFile(const std::string &filepath)
{
  char gh[1];
  int n = 0;
  int m = 0;
  std::vector<float> coefficients;
  coefficients.reserve(6);

  g2000_ = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  h2000_ = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  g2005_ = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  h2005_ = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  g2010_ = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  h2010_ = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  g2015_ = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  h2015_ = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  g2020_ = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  h2020_ = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  gsv_   = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  hsv_   = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);

  bool end_of_header_flag = false;

  std::ifstream data(filepath);
  std::string line;

  while (std::getline(data, line))
  {
    if ((end_of_header_flag == false) && (line.find("g/h") != std::string::npos))
    {
      end_of_header_flag = true;
    }
    else if (end_of_header_flag == true)
    {
      sscanf(line.c_str(), "%1s %i %i %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %f %f %f %f %f %f", gh, &n, &m, &coefficients[0], &coefficients[1], &coefficients[2], &coefficients[3],
                       &coefficients[4], &coefficients[5]);
      if (strcmp(gh, "g") == 0)
      {
        g2000_(n, m) = static_cast<double>(coefficients[0]);
        g2005_(n, m) = static_cast<double>(coefficients[1]);
        g2010_(n, m) = static_cast<double>(coefficients[2]);
        g2015_(n, m) = static_cast<double>(coefficients[3]);
        g2020_(n, m) = static_cast<double>(coefficients[4]);
        gsv_  (n, m) = static_cast<double>(coefficients[5]);
      }
      else if (strcmp(gh, "h") == 0)
      {
        h2000_(n, m) = static_cast<double>(coefficients[0]);
        h2005_(n, m) = static_cast<double>(coefficients[1]);
        h2010_(n, m) = static_cast<double>(coefficients[2]);
        h2015_(n, m) = static_cast<double>(coefficients[3]);
        h2020_(n, m) = static_cast<double>(coefficients[4]);
        hsv_  (n, m) = static_cast<double>(coefficients[5]);
      }
    }
  }
}

//------------------------------------------------------------------------------
//  void InitializeLegendrePolynomials(const double angle)
//------------------------------------------------------------------------------
/** Calculates the associated legendre polynomial and differential associated 
  * legendre polynomial matrices of degree n (rows) and order m (cols) for an 
  * angle [rad] input.The matrices are assigned to class members 
  * legendre_polynomials_ and differential_legendre_polynomials_.
  * 
  * The normalized associated legendre polynomials matrix of degree n (rows) 
  * and order m (cols) for sin(x) is calculated with the following method.
  * 
  * 1) Calculate and assign the diagonal associated legendre polynomials 
  *    (degree n and order n).
  * 2) Calculate and assign the associated legendre polynomials of (degree n+1 
  *    and order n). This calculation requires the associated legendre 
  *    polynomial of degree n and order n as input.
  * 3) Calculate and assign the remaining associated legendre polynomials. Their 
  *    evaluation requires both lower degree and order legender polynomials.
  *  
  * \param angle Angle input parameter for the normalized associated legendre 
  *              polynomails in \f$[rad]\f$.
  */
void Igrf::InitializeLegendrePolynomials(const double angle)
{
  Eigen::ArrayXXd pnm = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  pnm(0, 0) = 1;
  pnm(1, 1) = sin(angle);

  Eigen::ArrayXXd dpnm = Eigen::ArrayXXd::Zero(max_degree_ + 1, max_degree_ + 1);
  dpnm(0, 0) = 0;
  dpnm(1, 1) = cos(angle);

  //  Diagonal Coefficients
  for (int n = 2; n <= max_degree_; ++n)
  {
    pnm (n, n) = sqrt(1 - 1/(2*n)) *  sin(angle) *  pnm(n - 1, n - 1);
    dpnm(n, n) = sqrt(1 - 1/(2*n)) * (sin(angle) * dpnm(n - 1, n - 1) + cos(angle) * pnm(n - 1, n - 1));
  }

  //  First Step Coefficients (Diagonal - 1)
  for (int i = 1; i <= max_degree_; ++i)
  {
    pnm (i, i-1) = cos(angle) *  pnm(i - 1, i - 1);
    dpnm(i, i-1) = cos(angle) * dpnm(i - 1, i - 1) - sin(angle) * pnm(i - 1, i - 1);
  }

  //  Second and further step coefficients
  int k = 2;
  for (int m = 0; m < max_degree_ - 1; ++m)
  {
    for (int n = k; n <= max_degree_; ++n)
    {
      pnm (n, m) = cos(angle) *  pnm(n - 1, m) - (pow(n-1, 2) - pow(m, 2))/((2*n - 1)*(2*n - 3)) * pnm(n - 2, m);
      dpnm(n, m) = cos(angle) * dpnm(n - 1, m) - sin(angle) * pnm(n - 1, m) - (pow(n-1, 2) - pow(m, 2))/((2*n - 1)*(2*n - 3)) * dpnm(n - 2, m);
    }
    k = k + 1;
  }
  legendre_polynomials_ = pnm;
  differential_legendre_polynomials_ = dpnm;
}

//------------------------------------------------------------------------------
//  void CalculateCoefficients(const double mjd_utc)
//------------------------------------------------------------------------------
/** Calculates the IGRF coefficients for a given UTC modified julian date using
  * linear interpolation between time period coefficients. And sets the 
  * calculated values to members h_ and g_.
  *  
  * \param mjd_utc UTC modified julian date.
  */
void Igrf::CalculateCoefficients(const double mjd_utc)
{
  if((mjd_utc + kMjdEpoch) > (kJd2020Epoch - 0.5))
  {
    delta_t_ = (mjd_utc + kMjdEpoch - kJd2020Epoch)/365;
    g_ = g2020_ + gsv_ * delta_t_;
    h_ = h2020_ + hsv_ * delta_t_;
  }
  else if((mjd_utc + kMjdEpoch) > (kJd2015Epoch - 0.5))
  {
    delta_t_ = (mjd_utc + kMjdEpoch - kJd2015Epoch)/365;
    g_ = g2015_ + ((g2020_ + g2015_)/5) * delta_t_;
    h_ = h2015_ + ((h2020_ + h2015_)/5) * delta_t_;
  }
  else if((mjd_utc + kMjdEpoch) > (kJd2010Epoch - 0.5))
  {
    delta_t_ = (mjd_utc + kMjdEpoch - kJd2010Epoch)/365;
    g_ = g2010_ + ((g2015_ + g2010_)/5) * delta_t_;
    h_ = h2010_ + ((h2015_ + h2010_)/5) * delta_t_;
  }
  else if((mjd_utc + kMjdEpoch) > (kJd2005Epoch - 0.5))
  {
    delta_t_ = (mjd_utc + kMjdEpoch - kJd2005Epoch)/365;
    g_ = g2005_ + ((g2010_ + g2005_)/5) * delta_t_;
    h_ = h2005_ + ((h2010_ + h2005_)/5) * delta_t_;
  }
  else if((mjd_utc + kMjdEpoch) > (kJd2000Epoch - 0.5))
  {
    delta_t_ = (mjd_utc + kMjdEpoch - kJd2000Epoch)/365;
    g_ = g2000_ + ((g2005_ + g2000_)/5) * delta_t_;
    h_ = h2000_ + ((h2005_ + h2000_)/5) * delta_t_;
  }
  else
  {
    std::cout << "Error: Simulation Time starts before the year 2000. Models are incompatible\n";
  }
}

} // end namespace polaris