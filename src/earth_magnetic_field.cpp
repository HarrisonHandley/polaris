//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  EarthMagneticField Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "earth_magnetic_field.h"

namespace polaris {

//------------------------------------------------------------------------------
//  EarthMagneticField(const std::string &model_name)
//------------------------------------------------------------------------------
/** Default constructor requiring configuration of earth magnetic field model.
  *
  * \param model_name Name of the earth magnetic field model used. 
  */
EarthMagneticField::EarthMagneticField(const std::string &model_name) : 
                                       model_name_(model_name)
{

}

//------------------------------------------------------------------------------
//  EarthMagneticField(const EarthMagneticField &model) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param model The object being copied.
  */
EarthMagneticField::EarthMagneticField(const EarthMagneticField &model) :
                                       model_name_(model.model_name_)
{

}

//------------------------------------------------------------------------------
//  ~EarthMagneticField() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
EarthMagneticField::~EarthMagneticField()
{

}

//------------------------------------------------------------------------------
//  EarthMagneticField& operator = (const EarthMagneticField &model) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param model The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
EarthMagneticField& EarthMagneticField::operator=(const EarthMagneticField &model)
{
  if(&model == this)
  {
    return *this;
  }
  model_name_ = model.model_name_;
  return *this;
}

//------------------------------------------------------------------------------
//  std::string getModelName()
//------------------------------------------------------------------------------
/** Get the name of the model used to calculate the earth magnetic field. 
  *
  * \return std::string of the name of the model used.
  */
std::string EarthMagneticField::getModelName() const
{
  return model_name_;
}

} // end namespace polaris