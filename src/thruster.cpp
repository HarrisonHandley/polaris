//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Thruster Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "thruster.h"

namespace polaris {

//------------------------------------------------------------------------------
//  Thruster(const Eigen::Vector3d &thrust_vector, 
//           const Eigen::Vector3d &thruster_position, 
//           const double max_mass_flow_rate, 
//           const double max_exit_velocity) 
//------------------------------------------------------------------------------
/** Default constructor requiring thruster configuration.
  *
  * \param thrust_vector Unit vector of thrust force direction in body reference frame.
  * \param thruster_position Position vector of thruster in body reference frame.
  * \param max_mass_flow_rate Maximum mass flow rate of thruster \f$[kg/s]\f$.
  * \param max_exit_velocity Maximum exit velocity of thruster \f$[m/s]\f$.
  */
Thruster::Thruster(const Eigen::Vector3d &thrust_vector, 
                   const Eigen::Vector3d &thruster_position,
                   const double max_mass_flow_rate, 
                   const double max_exit_velocity) : 
                   thrust_vector_     (thrust_vector.normalized()),
                   thruster_position_ (thruster_position.normalized()),
                   thruster_force_    (Eigen::Vector3d::Zero()),
                   thruster_torque_   (Eigen::Vector3d::Zero()),
                   throttle_          (0),    
                   max_mass_flow_rate_(max_mass_flow_rate),
                   max_exit_velocity_ (max_exit_velocity)
{

}

//------------------------------------------------------------------------------
//  Thruster(const Thruster &th) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param th The object being copied.
  */
Thruster::Thruster(const Thruster &th) :
                   thrust_vector_     (th.thrust_vector_),
                   thruster_position_ (th.thruster_position_),
                   thruster_force_    (th.thruster_force_),
                   thruster_torque_   (th.thruster_torque_),                   
                   throttle_          (th.throttle_),   
                   max_mass_flow_rate_(th.max_mass_flow_rate_),
                   max_exit_velocity_ (th.max_exit_velocity_)                            
{

}

//------------------------------------------------------------------------------
//  ~Thruster() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
Thruster::~Thruster()
{

}

//------------------------------------------------------------------------------
//  Thruster& operator=(const Thruster &th) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param th The object being copied.
  * 
  * \return this object, with parameters set to the input object's parameters.
  */
Thruster& Thruster::operator=(const Thruster &th)
{
  if(&th == this)
  {
    return *this;
  }
  thrust_vector_      = th.thrust_vector_;
  thruster_position_  = th.thruster_position_;
  thruster_force_     = th.thruster_force_;
  thruster_torque_    = th.thruster_torque_;
  throttle_           = th.throttle_;
  max_mass_flow_rate_ = th.max_mass_flow_rate_;
  max_exit_velocity_  = th.max_exit_velocity_;

  return *this;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getThrusterForce() const 
//------------------------------------------------------------------------------
/** Get current thruster force in body reference frame.
  * 
  * \return Current thrust force vector \f$[x, y, z]\f$ in body reference frame 
  *         \f$[N]\f$.
  */ 
Eigen::Vector3d Thruster::getThrusterForce() const
{
  return thruster_force_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getThrusterTorque() const 
//------------------------------------------------------------------------------
/** Get current thruster torque in body reference frame.
  * 
  * \return Current thrust torque vector \f$[x, y, z]\f$ in body reference frame 
  *         \f$[N \cdot m]\f$.
  */ 
Eigen::Vector3d Thruster::getThrusterTorque
() const
{
  return thruster_torque_;
}

//------------------------------------------------------------------------------
//  double getThrottle() const 
//------------------------------------------------------------------------------
/** Get current thruster throttle.
  * 
  * \return Thruster throttle percentage \f$[\%]\f$ set by \f$[0 \to 1]\f$.
  */ 
double Thruster::getThrottle() const
{
  return throttle_;
}

//------------------------------------------------------------------------------
//  double getMassFlowRate() const 
//------------------------------------------------------------------------------
/** Get current thruster mass flow rate using equation:
  * 
  * \f$\dot{m} = \dot{m}_{max} \times throttle\f$
  * 
  * \return Current thruster mass flow rate \f$[kg/s]\f$.
  */ 
double Thruster::getMassFlowRate() const
{
  return throttle_ * max_mass_flow_rate_;
}

//------------------------------------------------------------------------------
//  void setThrottle() const 
//------------------------------------------------------------------------------
/** Sets the thruster throttle percentage.
  * 
  * \param throttle Applied thruster throttle percentage \f$[\%]\f$ defined by
  *                 \f$[0 \to 1]\f$.
  */ 
void Thruster::setThrottle(const double throttle)
{
  throttle_ = throttle;
}

//------------------------------------------------------------------------------
//  void CalculateThrust() 
//------------------------------------------------------------------------------
/** Calculates the thruster force and torque vectors applied to the spacecraft 
  * in the spacecraft body reference frame. 
  */ 
void Thruster::CalculateThrust()
{
  thruster_force_ = (throttle_ * max_mass_flow_rate_ * max_exit_velocity_) * thrust_vector_.array();
  thruster_torque_ = thruster_position_.cross(thruster_force_);
}

} // end namespace polaris