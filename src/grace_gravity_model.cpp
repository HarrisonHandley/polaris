//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  GraceGravityModel Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "grace_gravity_model.h"

#include <algorithm>
#include <fstream>

#include "constants.h"

namespace polaris {

//------------------------------------------------------------------------------
//  GraceGravityModel(const std::string &filepath, 
//                    const std::string &model_name, 
//                    const int max_degree)
//------------------------------------------------------------------------------
/** Default constructor requiring grace gravity model configuration data.
  *
  * \param filepath Grace Gravity Model coefficients filepath
  * \param model_name Name of the earth gravity field model used
  * \param max_degree Maximum degree of coefficients used for the model 
  *                   calculations
  */
GraceGravityModel::GraceGravityModel(const std::string &filepath, 
                                     const std::string &model_name, 
                                     const int          max_degree) : 
                                     EarthGravityField                 (model_name), 
                                     applied_max_degree_               (max_degree),
                                     model_max_degree_                 (0),
                                     earth_radius_                     (0),
                                     earth_gm_                         (0),
                                     cnm_                              (Eigen::ArrayXXd::Zero(0, 0)),
                                     snm_                              (Eigen::ArrayXXd::Zero(0, 0)),
                                     legendre_polynomials_             (Eigen::ArrayXXd::Zero(0, 0)),
                                     differential_legendre_polynomials_(Eigen::ArrayXXd::Zero(0, 0))
{
  setGravitationalModelFileParameters(filepath);
  setGravitationalModelFileCoefficients(filepath);
}

//------------------------------------------------------------------------------
//  GraceGravityModel(const GraceGravityModel &model) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param model The object being copied.
  */
GraceGravityModel::GraceGravityModel(const GraceGravityModel &model) :
                                     EarthGravityField                 (model),
                                     applied_max_degree_               (model.applied_max_degree_),
                                     model_max_degree_                 (model.model_max_degree_),
                                     earth_radius_                     (model.earth_radius_),
                                     earth_gm_                         (model.earth_gm_),
                                     cnm_                              (model.cnm_),
                                     snm_                              (model.snm_),
                                     legendre_polynomials_             (model.legendre_polynomials_),
                                     differential_legendre_polynomials_(model.differential_legendre_polynomials_)
{

}

//------------------------------------------------------------------------------
//  ~GraceGravityModel() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
GraceGravityModel::~GraceGravityModel()
{

}

//------------------------------------------------------------------------------
//  GraceGravityModel& operator = (const GraceGravityModel &model) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param model The object being copied.
  * 
  * \return this object, with parameters set to the input object's parameters.
  */
GraceGravityModel& GraceGravityModel::operator = (const GraceGravityModel &model)
{
  if(&model == this)
  {
    return *this;
  }
  EarthGravityField::operator = (model);

  applied_max_degree_                = model.applied_max_degree_;
  model_max_degree_                  = model.model_max_degree_;
  earth_radius_                      = model.earth_radius_;
  earth_gm_                          = model.earth_gm_;
  cnm_                               = model.cnm_;
  snm_                               = model.snm_;
  legendre_polynomials_              = model.legendre_polynomials_;
  differential_legendre_polynomials_ = model.differential_legendre_polynomials_;
  return *this;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAccelITRF(const Eigen::Vector3d &itrf_pos) 
//------------------------------------------------------------------------------
/** Inherited method from the abstract class EarthGravityField that is overriden
  * to accommodate the use of the Grace Gravity model. The method performs the
  * following calculations:
  * 
  * 1) Calculates the normalized associated legendre polynomials and first order 
  *    differential associated legendre polynomials for the spacecraft ITRF 
  *    polar angle.
  * 
  * 2) Calculate the gravitational potential equation in spherical coordinates 
  *    in terms of harmonic series for degree n and order m. 
  * 
  * 3) Calculate for acceleration by taking the differential of the 
  *    gravitational potential equation
  * 
  * \param itrf_pos Eigen::Vector3d Position vector of object in ITRF 
  *                 \f$[x, y, z]\f$ \f$[m]\f$.
  * 
  * \return Eigen::Vector3d Acceleration vector acting on object from Earths's 
  *         gravity field \f$[\a_x, \a_y, \a_z]\f$ \f$[m/s^2]\f$.
  */
Eigen::Vector3d GraceGravityModel::getAccelITRF(const Eigen::Vector3d &itrf_pos)
{
  Eigen::Vector3d itrf_spherical_pos = getSphericalCoordinates(itrf_pos);
  InitializeLegendrePolynomials(itrf_spherical_pos(1)); // Polar Angle

  double r = itrf_spherical_pos.norm();

  Eigen::VectorXd integer_array = Eigen::VectorXd::LinSpaced (applied_max_degree_ + 1, 0, applied_max_degree_);
  Eigen::VectorXd ones_array = Eigen::VectorXd::Ones(applied_max_degree_ + 1);
  Eigen::ArrayXXd degree = integer_array * ones_array.transpose();
  Eigen::ArrayXXd order  = degree.transpose();

  Eigen::ArrayXXd energy_coeff_1 = -earth_gm_/pow(r,2) * pow((earth_radius_/r), degree) * (degree + 1);
  Eigen::ArrayXXd energy_coeff_2 =  earth_gm_/r * pow((earth_radius_/r), degree);

  Eigen::ArrayXXd diff_energy_r_array   = energy_coeff_1 * (legendre_polynomials_ * 
   (cnm_.block(0,0,applied_max_degree_ + 1, applied_max_degree_ + 1) * cos(itrf_spherical_pos(2)*order) 
  + snm_.block(0,0,applied_max_degree_ + 1, applied_max_degree_ + 1) * sin(itrf_spherical_pos(2)*order))); 
 
  Eigen::ArrayXXd diff_energy_theta_array = energy_coeff_2 * (differential_legendre_polynomials_ * 
   (cnm_.block(0,0,applied_max_degree_ + 1, applied_max_degree_ + 1) * cos(itrf_spherical_pos(2)*order)
  + snm_.block(0,0,applied_max_degree_ + 1, applied_max_degree_ + 1) * sin(itrf_spherical_pos(2)*order)));

  Eigen::ArrayXXd diff_energy_sigma_array = energy_coeff_2 * (order * legendre_polynomials_ * 
   (snm_.block(0,0,applied_max_degree_ + 1, applied_max_degree_ + 1) * cos(itrf_spherical_pos(2)*order) 
  - cnm_.block(0,0,applied_max_degree_ + 1, applied_max_degree_ + 1) * sin(itrf_spherical_pos(2)*order))); 

  double diff_energy_r     = diff_energy_r_array.sum();
  double diff_energy_theta = diff_energy_theta_array.sum();
  double diff_energy_sigma = diff_energy_sigma_array.sum();

  Eigen::Vector3d acceleration;
  double sq_xy_projection = pow(itrf_pos(0),2) + pow(itrf_pos(1),2);
  acceleration(0) = (1/r * diff_energy_r - itrf_pos(2)/(pow(r,2)*sq_xy_projection) * diff_energy_theta) 
                   * itrf_pos(0) - (1/sq_xy_projection * diff_energy_sigma) * itrf_pos(1);
  acceleration(1) = (1/r * diff_energy_r - itrf_pos(2)/(pow(r,2)*sq_xy_projection) * diff_energy_theta) 
                   * itrf_pos(1) - (1/sq_xy_projection * diff_energy_sigma) * itrf_pos(0);
  acceleration(2) =  1/r * diff_energy_r * itrf_pos(2) + sqrt(sq_xy_projection)/pow(r,2) * diff_energy_theta;

  return acceleration;
}

//------------------------------------------------------------------------------
//  void setGravitationalModelFileParameters(const std::string &filepath) 
//------------------------------------------------------------------------------
/** Parses the Grace Gravitational Parameter file and sets the members
  * model_max_degree_, earth_radius_, and earth_gm_ from the file header.
  * 
  * \param filepath Grace Gravity Model coefficients filepath.
  */
void GraceGravityModel::setGravitationalModelFileParameters(const std::string &filepath)
{
  float earth_radius;
  float earth_gm;
  std::ifstream data(filepath);
  std::string line;

  while (std::getline(data, line))
  {
    if (line.find("earth_gravity_constant") != std::string::npos)
    {
      sscanf(line.c_str(), "%*s %f", &earth_gm);
      earth_gm_ = static_cast<double>(earth_gm);
    }

    if (line.find("radius") != std::string::npos)
    {
      sscanf(line.c_str(), "%*s %f", &earth_radius);
      earth_radius_ = static_cast<double>(earth_radius);
    }

    if (line.find("max_degree") != std::string::npos)
    {
      sscanf(line.c_str(), "%*s %i", &model_max_degree_);
    }

    if (line.find("end_of_head") != std::string::npos)
    {
      break;
    }
  }
}

//------------------------------------------------------------------------------
//  void setGravitationalModelFileCoefficients(const std::string &filepath) 
//------------------------------------------------------------------------------
/** Parses the Grace Gravitational Parameter file and sets the coefficient
  * matrix members cnm_ and snm_ with the Grace gravity field coefficients. 
  * 
  * \param filepath Grace Gravity Model coefficients filepath.
  */
void GraceGravityModel::setGravitationalModelFileCoefficients(const std::string &filepath)
{
  cnm_ = Eigen::ArrayXXd::Zero(model_max_degree_ + 1, model_max_degree_ + 1);
  snm_ = Eigen::ArrayXXd::Zero(model_max_degree_ + 1, model_max_degree_ + 1);

  int n = 0;
  int m = 0;
  float c = 0;
  float s = 0;
  bool end_of_header_flag = false;

  std::ifstream data(filepath);
  std::string line;

  while (std::getline(data, line))
  {
    if ((end_of_header_flag == false) && (line.find("end_of_head") != std::string::npos))
    {
      end_of_header_flag = true;
    }
    else if (end_of_header_flag == true)
    {
      std::replace(line.begin(), line.end(), 'D', 'E');
      sscanf(line.c_str(), "%*s %i %i %f %f %*f %*f", &n, &m, &c, &s);
      cnm_(n, m) = static_cast<double>(c);
      snm_(n, m) = static_cast<double>(s);
    }
  }
}

//------------------------------------------------------------------------------
//  void InitializeLegendrePolynomials(const double angle) 
//------------------------------------------------------------------------------
/** Calculates the associated legendre polynomial and differential associated 
  * legendre polynomial matrices of degree n (rows) and order m (cols) for an 
  * angle [rad] input.The matrices are assigned to class members 
  * legendre_polynomials_ and differential_legendre_polynomials_.
  * 
  * The normalized associated legendre polynomials matrix of degree n (rows) 
  * and order m (cols) for sin(x) is calculated with the following method.
  * 
  * 1) Calculate and assign the diagonal associated legendre polynomials 
  *    (degree n and order n)
  * 2) Calculate and assign the associated legendre polynomials of (degree n+1 
  *    and order n). This calculation requires the associated legendre 
  *    polynomial of degree n and order n as input.
  * 3) Calculate and assign the remaining associated legendre polynomials. Their 
  *    evaluation requires both lower degree and order legender polynomials.
  *  
  * \param angle angle input parameter for the normalized associated legendre 
  *              polynomails in \f$[rad]\f$.
  */
void GraceGravityModel::InitializeLegendrePolynomials(const double angle)
{
  Eigen::ArrayXXd pnm = Eigen::ArrayXXd::Zero(applied_max_degree_ + 1, applied_max_degree_ + 1);
  pnm(0, 0) = 1;
  pnm(1, 1) = sqrt(3) * cos(angle);

  Eigen::ArrayXXd dpnm = Eigen::ArrayXXd::Zero(applied_max_degree_ + 1, applied_max_degree_ + 1);
  dpnm(0, 0) = 0;
  dpnm(1, 1) = -sqrt(3) * sin(angle);

  //  Diagonal Coefficients
  for (int i = 2; i <= applied_max_degree_; ++i)
  {
    pnm (i, i) = sqrt((2.0 * i + 1) / (2 * i)) * cos(angle) * pnm(i - 1, i - 1);
    dpnm(i, i) = sqrt((2.0 * i + 1) / (2 * i)) * (cos(angle) * dpnm(i - 1, i - 1) - 
                 sin(angle) * pnm(i - 1, i - 1));
  }

  //  First Step Coefficients (Diagonal - 1)
  for (int i = 1; i <= applied_max_degree_; ++i)
  {
    pnm (i, i-1) = sqrt(2.0 * i + 1) * sin(angle) * pnm(i - 1, i - 1);
    dpnm(i, i-1) = sqrt(2.0 * i + 1) * (sin(angle) * dpnm(i - 1, i - 1) + cos(angle) * pnm(i - 1, i - 1));
  }

  //  Second and further step coefficients
  int k = 2;
  for (int j = 0; j < applied_max_degree_ - 1; ++j)
  {
    for (int i = k; i <= applied_max_degree_; ++i)
    {
      pnm (i, j) = sqrt((2.0 * i + 1) / ((i - j) * (i + j))) * (sqrt(2.0 * i - 1) * sin(angle) * pnm(i - 1, j) - 
                   sqrt(((i + j - 1.0) * (i - j - 1)) / ((2 * i - 3))) * pnm(i - 2, j));
      dpnm(i, j) = sqrt((2.0 * i + 1) / ((i - j) * (i + j))) * (sqrt(2.0 * i - 1) * sin(angle) * dpnm(i - 1, j) + 
                   sqrt(2.0 * i - 1) * cos(angle) * pnm(i - 1, j) - sqrt(((i + j - 1.0) * 
                   (i - j - 1)) / ((2 * i - 3))) * dpnm(i - 2, j));
    }
    k = k + 1;
  }
  legendre_polynomials_ = pnm;
  differential_legendre_polynomials_ = dpnm;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getSphericalCoordinates(const Eigen::Vector3d &cart_vec) const
//------------------------------------------------------------------------------
/** Converts a cartesian position vector to Spherical Coordinate System.
  *  
  * \param cart_vec Input cartesian position vector to be converted to spherical
  *                 coordinates.
  * \return Eigen::Vector3d of position vector in spherical coordinates 
  *         \f$[radial distance (rho), polar angle (theta), azimuth (phi)]\f$.
  */
Eigen::Vector3d GraceGravityModel::getSphericalCoordinates(const Eigen::Vector3d &cart_vec) const
{
  Eigen::Vector3d spherical_position;

  // Rho
  spherical_position(0) = cart_vec.norm();

  // Theta
  double xy_projection = sqrt(pow(cart_vec(0), 2) + pow(cart_vec(1), 2));
  if ((cart_vec(2) == 0) && (xy_projection == 0))
  {
    spherical_position(1) = 0;
  }
  else
  {
    spherical_position(1) = atan2(cart_vec(2), xy_projection);
  }

  // Azimuth
  if ((cart_vec(0) == 0) && (cart_vec(1) == 0))
  {
    spherical_position(2) = 0;
  }
  else
  {
    spherical_position(2) = atan2(cart_vec(1), cart_vec(0));
  }
  if (spherical_position(2) < 0)
  {
    spherical_position(2) = spherical_position(2) + 2 * kPI;
  }
  return spherical_position;
}

} // end namespace polaris