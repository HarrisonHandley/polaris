//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Reaction Wheel Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "reaction_wheel.h"

namespace polaris {

//------------------------------------------------------------------------------
//  ReactionWheel(const Eigen::Vector3d &spin_axis_bf, 
//                const double inertia, 
//                const double max_angular_velocity, 
//                const double max_motor_torque)
//------------------------------------------------------------------------------
/** Default constructor requiring configuration of reaction wheel.
  *
  * \param spin_axis_bf Spin axis unit vector of reaction wheel in body reference frame
  * \param inertia Reaction wheel moment of inertia about the axis of spin \f$[kg/m^2]\f$
  * \param max_angular_velocity Maximum angular velocity of reaction wheel \f$[rad/s]\f$
  * \param max_motor_torque Maximum applicable motor torque to reaction wheel \f$[N \cdot m]\f$
  */
ReactionWheel::ReactionWheel(const Eigen::Vector3d &spin_axis_bf, 
                             const double inertia, 
                             const double max_angular_velocity, 
                             const double max_motor_torque) :
                             mode_("Operational"),
                             spin_axis_bf_(spin_axis_bf.normalized()),
                             inertia_(inertia),
                             angular_velocity_(0),
                             angular_acceleration_(0),
                             motor_torque_(0),
                             max_angular_velocity_(max_angular_velocity),
                             max_motor_torque_(max_motor_torque)
{

}

//------------------------------------------------------------------------------
//  ReactionWheel(const ReactionWheel &rw) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param rw The object being copied.
  */
ReactionWheel::ReactionWheel(const ReactionWheel &rw) :
                             mode_(rw.mode_),
                             spin_axis_bf_(rw.spin_axis_bf_.normalized()),
                             inertia_(rw.inertia_),
                             angular_velocity_(rw.angular_velocity_),
                             angular_acceleration_(rw.angular_acceleration_),
                             motor_torque_(rw.motor_torque_),
                             max_angular_velocity_(rw.max_angular_velocity_),
                             max_motor_torque_(rw.max_motor_torque_)                             
{

}

//------------------------------------------------------------------------------
//  ~ReactionWheel() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
ReactionWheel::~ReactionWheel()
{

}


//------------------------------------------------------------------------------
//  ReactionWheel& operator=(const ReactionWheel &rw) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param rw The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
ReactionWheel& ReactionWheel::operator=(const ReactionWheel &rw)
{
  if(&rw == this)
  {
    return *this;
  }
  mode_                 = rw.mode_;
  spin_axis_bf_         = rw.spin_axis_bf_.normalized();
  inertia_              = rw.inertia_;
  angular_velocity_     = rw.angular_velocity_;
  angular_acceleration_ = rw.angular_acceleration_;
  motor_torque_         = rw.motor_torque_;
  max_angular_velocity_ = rw.max_angular_velocity_;
  max_motor_torque_     = rw.max_motor_torque_;  
  
  return *this;
}

//------------------------------------------------------------------------------
//  std::string getMode() const 
//------------------------------------------------------------------------------
/** Get operating mode of reaction wheel.
  * 
  * \return Reaction wheel current operating mode (Operational, Saturated).
  */ 
std::string ReactionWheel::getMode() const
{
  return mode_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getSpinAxis() const 
//------------------------------------------------------------------------------
/** Get reaction wheel spin axis unit vector in body reference frame.
  * 
  * \return Spin axis as \f$[x, y, z]\f$ unit vector.
  */ 
Eigen::Vector3d ReactionWheel::getSpinAxis() const
{
  return spin_axis_bf_;
}

//------------------------------------------------------------------------------
//  double getInertia() const 
//------------------------------------------------------------------------------
/** Get reaction wheel moment of inertia about the axis of spin .
  * 
  * \return Inertia about axis of spin \f$[kg/m^2]\f$.
  */ 
double ReactionWheel::getInertia() const
{
  return inertia_;
}

//------------------------------------------------------------------------------
//  double getAngularVelocity() const 
//------------------------------------------------------------------------------
/** Get current angular velocity of reaction wheel about spin axis.
  * 
  * \return Angular velocity about spin axis, defined by right hand rule 
  *         \f$[rad/s]\f$.
  */ 
double ReactionWheel::getAngularVelocity() const
{
  return angular_velocity_;
}

//------------------------------------------------------------------------------
//  double getAngularAcceleration() const 
//------------------------------------------------------------------------------
/** Get current angular acceleration of reaction wheel about spin axis.
  * 
  * \return Angular acceleration about spin axis, defined by right hand rule
  *         \f$[rad/s^2]\f$.
  */ 
double ReactionWheel::getAngularAcceleration() const
{
  return angular_acceleration_;
}

//------------------------------------------------------------------------------
//  double getMotorTorque() const 
//------------------------------------------------------------------------------
/** Get current motor torque applied to reaction wheel.
  * 
  * \return Applied motor torque, defined by right hand rule \f$[N \cdot m]\f$.
  */ 
double ReactionWheel::getMotorTorque() const
{
  return motor_torque_;
}

//------------------------------------------------------------------------------
//  void setMotorTorque(const double torque) 
//------------------------------------------------------------------------------
/** Sets the motor torque applied to the reaction wheel. If the set value is 
  * greater than the reaction wheels max motor torque. The value is set to the
  * max motor torque.
  *
  * \param torque Applied motor torque to the reaction wheel \f$[N \cdot m]\f$.
  */
void ReactionWheel::setMotorTorque(const double torque)
{
  if(torque > max_motor_torque_)
  {
    motor_torque_ = max_motor_torque_;
  }
  else if(torque < -max_motor_torque_)
  {
    motor_torque_ = -max_motor_torque_;
  }
  else
  {
    motor_torque_ = torque;
  }
}

//------------------------------------------------------------------------------
//  void CalculateAngularVelocity(double timestep)
//------------------------------------------------------------------------------
/** Calculates and sets the angular velocity of the reaction wheel based on the
  * reaction wheels current angular acceleration and the timestep between 
  * simulation steps. If the calculated value is greater than the reaction 
  * wheels max angular velocity. The value is set to the max angular velocity.
  * Using equation:
  * 
  * \f$\omega_{rw} = \alpha_{rw} \Delta t_{step}\f$
  * 
  * \param timestep Time between simulation steps \f$[s]\f$.
  */
void ReactionWheel::CalculateAngularVelocity(const double timestep)
{
  double angular_velocity = angular_velocity_ + angular_acceleration_ * timestep;

  if(angular_velocity > max_angular_velocity_)
  {
    angular_velocity_ = max_angular_velocity_;
    mode_ = "Saturated";
  }
  else if(angular_velocity < -max_angular_velocity_)
  {
    angular_velocity_ = -max_angular_velocity_;
    mode_ = "Saturated";
  }
  else
  {
    angular_velocity_ = angular_velocity;
    mode_ = "Operational";
  }
}

//------------------------------------------------------------------------------
//  void CalculateAngularAcceleration(const Eigen::Vector3d &sat_ang_accel) 
//------------------------------------------------------------------------------
/** Calculates the angular acceleration of the reaction wheel based on the
  * applied motor torque and current angular acceleration of the spacecraft.
  * Using equation: 
  * 
  * \f$\alpha_{rw }= I^{-1} (\tau - I \times \vec{S_{rw}}\cdot 
  * \dot{\vec{\omega}}_{sat})\f$
  * 
  * \param sat_ang_accel Angular acceleration of spacecraft in body reference
  *                      frame \f$[rad/s^2]\f$.
  */
void ReactionWheel::CalculateAngularAcceleration(const Eigen::Vector3d &sat_ang_accel)
{
  angular_acceleration_ = (1 / inertia_) * (motor_torque_ - inertia_ 
                        * spin_axis_bf_.dot(sat_ang_accel));
}

} //  end namespace polaris