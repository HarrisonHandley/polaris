//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  SimulationTime Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "simulation_time.h"

#include <cmath>
#include <sstream>

#include "constants.h"

namespace polaris {

//------------------------------------------------------------------------------
//  SimulationTime()
//------------------------------------------------------------------------------
/** Default constructor.
  * 
  */
SimulationTime::SimulationTime()
{

}

//------------------------------------------------------------------------------
//  SimulationTime(const int    year,
//                 const int    month, 
//                 const int    day, 
//                 const double hour, 
//                 const double min, 
//                 const double sec)
//------------------------------------------------------------------------------
/** Default constructor for simulation time, requiring initial time data.
  * 
  * \param year Year in UTC gregorian calendar.
  * \param month Month in UTC gregorian calendar.
  * \param day Day in UTC gregorian calendar.
  * \param hour Hours into 24h day in UTC gregorian calendar.
  * \param min Minutes into hour in UTC gregorian calendar.
  * \param sec Seconds into minute in UTC gregorian calendar.
  */
SimulationTime::SimulationTime(const int    year, 
                               const int    month, 
                               const int    day, 
                               const double hour, 
                               const double min, 
                               const double sec) :
                               year_             (year), 
                               month_            (month), 
                               day_              (day), 
                               hour_             (hour), 
                               min_              (min), 
                               sec_              (sec),
                               timestep_         (0),
                               runtime_          (0),
                               iterations_       (0),
                               mjd_utc_          (0),
                               mjd_tt_           (0),
                               mjd_ut1_          (0),
                               mjd_tdb_          (0),
                               julian_century_tt_(0)

{
  mjd_utc_ = CalculateMjdUTC(year, month, day, hour, min, sec);
}

//------------------------------------------------------------------------------
//  SimulationTime(const SimulationTime &time) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param time The object being copied.
  */
SimulationTime::SimulationTime(const SimulationTime &time) :
                               year_             (time.year_), 
                               month_            (time.month_), 
                               day_              (time.day_), 
                               hour_             (time.hour_), 
                               min_              (time.min_), 
                               sec_              (time.sec_),
                               timestep_         (time.timestep_),
                               runtime_          (time.runtime_),
                               iterations_       (time.iterations_),
                               mjd_utc_          (time.mjd_utc_),
                               mjd_tt_           (time.mjd_tt_),
                               mjd_ut1_          (time.mjd_ut1_),
                               mjd_tdb_          (time.mjd_tdb_),
                               julian_century_tt_(time.julian_century_tt_)
{

}

//------------------------------------------------------------------------------
//  ~SimulationTime() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
SimulationTime::~SimulationTime()
{

}

//------------------------------------------------------------------------------
//  SimulationTime& operator=(const SimulationTime &time) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param time The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
SimulationTime& SimulationTime::operator=(const SimulationTime &time)
{
  if(&time == this)
  {
    return *this;
  }
  year_              = time.year_;
  month_             = time.month_; 
  day_               = time.day_;
  hour_              = time.hour_; 
  min_               = time.min_; 
  sec_               = time.sec_;
  timestep_          = time.timestep_;
  runtime_           = time.runtime_;
  iterations_        = time.iterations_;
  mjd_utc_           = time.mjd_utc_;
  mjd_tt_            = time.mjd_tt_;
  mjd_ut1_           = time.mjd_ut1_;
  mjd_tdb_           = time.mjd_tdb_;
  julian_century_tt_ = time.julian_century_tt_;
  return *this;
}

//------------------------------------------------------------------------------
//  double getMjdUTC() const 
//------------------------------------------------------------------------------
/** Get modified julian date in UTC of current simulation time.
  * 
  * \return double UTC time in modified julian date.
  */ 
double SimulationTime::getMjdUTC() const
{
  return mjd_utc_;
}

//------------------------------------------------------------------------------
//  double getJulianCenturyTT() const 
//------------------------------------------------------------------------------
/** Get julian century in TT of current simulation time.
  * 
  * \return double TT time in julian centuries.
  */ 
double SimulationTime::getJulianCenturyTT() const
{
  return julian_century_tt_;
}

//------------------------------------------------------------------------------
//  double getMjdTT() const 
//------------------------------------------------------------------------------
/** Get modified julian date in TT of current simulation time.
  * 
  * \return double TT time in modified julian date.
  */ 
double SimulationTime::getMjdTT() const
{
  return mjd_tt_;
}

//------------------------------------------------------------------------------
//  double getMjdUT1() const 
//------------------------------------------------------------------------------
/** Get modified julian date in UT1 of current simulation time.
  * 
  * \return double UT1 time in modified julian date.
  */ 
double SimulationTime::getMjdUT1() const
{
  return mjd_ut1_;
}

//------------------------------------------------------------------------------
//  double getMjdTDB() const 
//------------------------------------------------------------------------------
/** Get modified julian date in TDB of current simulation time.
  * 
  * \return double TDB time in modified julian date.
  */ 
double SimulationTime::getMjdTDB() const
{
  return mjd_tdb_;
}

//------------------------------------------------------------------------------
//  std::string getMjdTDB() const 
//------------------------------------------------------------------------------
/** Get gregorian date and time of current simulation time.
  * 
  * \return std::string Gregorian date and time string of day-month-year h:m:s.
  */ 
std::string SimulationTime::getGregorianTime() const
{
  std::ostringstream temp;
  temp << day_ << "-" << month_ << "-" << year_ << " " << hour_ << ":" << min_ << ":" << sec_;
  return temp.str();
}

//------------------------------------------------------------------------------
//  double getSimulationIterations() const 
//------------------------------------------------------------------------------
/** Get number of iterations of current simulation.
  * 
  * \return double Number of iterations current completed by simulation.
  */ 
double SimulationTime::getSimulationIterations() const
{
  return iterations_;
}

//------------------------------------------------------------------------------
//  double getRuntime() const 
//------------------------------------------------------------------------------
/** Get simulation runtime in simulation time in seconds.
  * 
  * \return double Simulation runtime in seconds.
  */ 
double SimulationTime::getRuntime() const
{
  return runtime_;
}

//------------------------------------------------------------------------------
//  double getTimestep() const 
//------------------------------------------------------------------------------
/** Get current simulation timestep.
  * 
  * \return double Current simulation timestep in seconds.
  */ 
double SimulationTime::getTimestep() const
{
  return timestep_;
}

//------------------------------------------------------------------------------
//  void IncrementTime(const double timestep)
//------------------------------------------------------------------------------
/** Increments simulation runtime and MJD_UTC time by value of timestep. And 
  * converts MJD_UTC time to Gregorian.
  * 
  * \param timestep Value to increment simulation time by in seconds.
  */ 
void SimulationTime::IncrementTime(const double timestep)
{
  mjd_utc_ += timestep/86400;
  runtime_ += timestep;
  iterations_ = iterations_ + 1;
  Mjd2Gregorian(mjd_utc_);
}

//------------------------------------------------------------------------------
//  void UpdateTimescales(const double tai_utc_offset, 
//                        const double ut1_utc_offset
//------------------------------------------------------------------------------
/** Updates TT, UT1, and TDB timescales based on MJD_UTC time and TAI_UTC and 
  * UT1_UTC offsets.
  * 
  * \param tai_utc_offset Time difference between TAI and UTC in seconds.
  * \param ut1_utc_offset Time difference between UT1 and UTC in seconds.
  */ 
void SimulationTime::UpdateTimescales(const double tai_utc_offset, 
                                      const double ut1_utc_offset)
{
  mjd_tt_ = CalculateMjdTT(mjd_utc_, tai_utc_offset);
  mjd_ut1_ = CalculateMjdUT1(mjd_utc_, ut1_utc_offset);
  julian_century_tt_ = CalculateJulianCenturyTT(mjd_tt_);
  mjd_tdb_ = CalculateMjdTDB(mjd_tt_, julian_century_tt_);
}

//------------------------------------------------------------------------------
//  double CalculateMjdUTC(const int year, 
//                         const int month, 
//                         const int day, 
//                         const double hour, 
//                         const double min, 
//                         const double sec) const
//------------------------------------------------------------------------------
/** Calculates modified julian date in UTC from a gregorian date.
  * 
  * \param year Year in UTC gregorian calendar.
  * \param month Month in UTC gregorian calendar.
  * \param day Day in UTC gregorian calendar.
  * \param hour Hours into 24h day in UTC gregorian calendar.
  * \param min Minutes into hour in UTC gregorian calendar.
  * \param sec Seconds into minute in UTC gregorian calendar.
  */ 
double SimulationTime::CalculateMjdUTC(const int year, 
                                       const int month, 
                                       const int day, 
                                       const double hour, 
                                       const double min, 
                                       const double sec) const
{
  double mjd_utc = ((1461 * (year + 4800 + ((month - 14)/12)))/4)
                 + ((367 * (month - 2 - (12 * ((month - 14)/12))))/12)
                 - ((3 * ((year + 4900 +((month - 14)/12))/100))/4)
                 + day - 32075 - 2400000.5
                 + ((hour - 12)/24) + (min/1440) + (sec/86400);
  return mjd_utc;
}

//------------------------------------------------------------------------------
//  double CalculateMjdTT(const double mjd_utc, 
//                        const double tai_utc_offset) const
//------------------------------------------------------------------------------
/** Calculates modified julian date in TT from UTC modified julian date and
  * time offset between TAI and UTC.
  * 
  * \param mjd_utc UTC modified julian date.
  * \param tai_utc_offset Time difference between TAI and UTC in seconds.
  * 
  * \return double TT modified julian date.
  */ 
double SimulationTime::CalculateMjdTT(const double mjd_utc, 
                                      const double tai_utc_offset) const
{
  double mjd_tt = mjd_utc + (32.184 + tai_utc_offset)/86400;
  return mjd_tt;
}

//------------------------------------------------------------------------------
//  double CalculateMjdUT1(const double mjd_utc, 
//                         const double ut1_utc_offset) const
//------------------------------------------------------------------------------
/** Calculates modified julian date in UT1 from UTC modified julian date and
  * time offset between UT1 and UTC.
  * 
  * \param mjd_utc UTC modified julian date.
  * \param ut1_utc_offset Time difference between UT1 and UTC in seconds.
  * 
  * \return double UT1 modified julian date.
  */ 
double SimulationTime::CalculateMjdUT1(const double mjd_utc, 
                                       const double ut1_utc_offset) const
{
  double mjd_ut1 = mjd_utc + ut1_utc_offset/86400;
  return mjd_ut1;
}

//------------------------------------------------------------------------------
//  double CalculateMjdTDB(const double mjd_utc, 
//                         const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates modified julian date in TDB from UTC modified julian date and
  * TT julian century.
  * 
  * \param mjd_utc UTC modified julian date.
  * \param julian_century_tt TT in julian centuries.
  * 
  * \return double TDB modified julian date.
  */ 
double SimulationTime::CalculateMjdTDB(const double mjd_tt, 
                                       const double julian_century_tt) const
{
  double mjd_tdb = mjd_tt +  (0.001658 * sin(628.3076  * julian_century_tt + 6.2401)
                          +   0.000022 * sin(575.3385  * julian_century_tt + 4.2970)
                          +   0.000014 * sin(1256.6152 * julian_century_tt + 6.1969)
                          +   0.000005 * sin(606.9777  * julian_century_tt + 4.0212)
                          +   0.000005 * sin(52.9691   * julian_century_tt + 0.4444)
                          +   0.000002 * sin(21.3299   * julian_century_tt + 5.5431)
                          +   0.000010 * sin(628.3076  * julian_century_tt + 4.2490)) / 86400;
  return mjd_tdb;
}

//------------------------------------------------------------------------------
//  double CalculateJulianCenturyTT(const double tt) const
//------------------------------------------------------------------------------
/** Calculates julian centuries in TT from TT modified julian date.
  * 
  * \param TT TT modified julian date.
  * 
  * \return double TT in julian centuries.
  */ 
double SimulationTime::CalculateJulianCenturyTT(const double tt) const
{
  double tt_julian_century = ((kMjdEpoch - kJd2000Epoch) + tt) / kJdCentury;
  return tt_julian_century;
} 

//------------------------------------------------------------------------------
//  void Mjd2Gregorian(const double mjd)
//------------------------------------------------------------------------------
/** Converts modified julian date to gregorian date and time using class 
  * members.
  * 
  * \param mjd Modified julian date.
  */ 
void SimulationTime::Mjd2Gregorian(const double mjd)
{
  double jd = mjd + kMjdEpoch;
  double z = std::trunc(jd + 0.5);
  double fday = jd + 0.5 - z;

  year_ = 0;
  month_ = 0;
  day_ = 0;
  hour_ = 0;
  min_ = 0;
  sec_ = 0;

  if(fday < 0)
  {
    fday = fday + 1;
    z = z - 1;
  }

  double a = 0;

  if(z < 2299161)
  {
    a = z;
  }
  else
  {
    double alpha = std::trunc((z - 1867216.25) / 36524.25);
    a = z + 1 + alpha - std::trunc(alpha / 4);
  }

  double b = a + 1524;
  double c = std::trunc((b - 122.1) / 365.25);
  double d = std::trunc(365.25 * c);
  double e = std::trunc((b - d) / 30.6001);
  day_ = b - d - std::trunc(30.6001 * e) + fday;

  if(e < 14)
  {
    month_ = e - 1;
  }
  else
  {
    month_ = e - 13;
  }

  if(month_ > 2)
  {
    year_ = c - 4716;
  }
  else
  {
    year_ = c - 4715;
  }

  hour_ = std::abs(day_ - std::trunc(day_)) * 24;
  min_  = std::abs(hour_ - std::trunc(hour_)) * 60;
  sec_  = std::abs(min_ - std::trunc(min_)) * 60;

  day_  = std::trunc(day_);
  hour_ = std::trunc(hour_);
  min_  = std::trunc(min_);
}

} // end namespace polaris