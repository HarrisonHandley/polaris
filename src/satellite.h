//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Satellite Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_SATELLITE_H_
#define POLARIS_SATELLITE_H_

#include <memory>
#include <vector>

#include "../lib/eigen3/Eigen/Dense"

#include "earth_magnetic_field.h"
#include "earth_orientation.h"

namespace polaris {

/** \brief Class definition for the Satellite class.
  * 
  * This class defines a satellite, an orbiting object. This class maintains
  * the orbital position, velocity, and acceleration of the satellite in both
  * the international terrestrial reference frame and international celestial 
  * reference frame. This includes calculating all external forces applied to
  * the satellite.
  */  

class Satellite
{
public:
  Satellite(const std::string     &satellite_id, 
            const Eigen::Vector3d &icrf_pos,
            const Eigen::Vector3d &icrf_vel,
            const double           mass, 
            const double           cross_section,
            const double           drag_coeff, 
            const double           radiation_pressure_coeff);
  Satellite(const Satellite &sat);
  virtual ~Satellite();
  Satellite& operator=(const Satellite &sat);

          std::string                   getSatelliteID()                  const;
          Eigen::Vector3d               getLatLonPosition()               const;
          Eigen::Vector3d               getITRFPosition()                 const;
          Eigen::Vector3d               getICRFPosition()                 const;
          Eigen::Vector3d               getICRFVelocity()                 const;
          Eigen::Vector3d               getICRFAcceleration()             const;
          double                        getEccentricity()                 const;
          double                        getSemiMajorAxis()                const;
          double                        getInclincation()                 const;
          double                        getAscendingNode()                const;
          double                        getArgumentOfPeriapsis()          const;
          double                        getTrueAnomaly()                  const;
          Eigen::Vector3d               getAtmosphericDragForce()         const;
          Eigen::Vector3d               getEarthGravityForce()            const;
          Eigen::Vector3d               getPlanetaryGravityForce()        const;
          Eigen::Vector3d               getSolarPressureForce()           const;  
          Eigen::Vector3d               getTotalThrusterForce()           const;
          Eigen::Vector3d               getAppliedForce()                 const;
          double                        getMass()                         const;
          double                        getCrossSection()                 const;
          double                        getDragCoeff()                    const;
          double                        getRadiationPressureCoeff()       const;

  virtual Eigen::Quaterniond            getAttitude()                     const;
  virtual Eigen::Vector3d               getAngularVelocity()              const;
  virtual Eigen::Vector3d               getAngularAcceleration()          const; 
  virtual Eigen::Vector3d               getAtmosphericDragTorque()        const;
  virtual Eigen::Vector3d               getGravityGradientTorque()        const;
  virtual Eigen::Vector3d               getMagneticTorque()               const;
  virtual Eigen::Vector3d               getSolarPressureTorque()          const;
  virtual Eigen::Vector3d               getTotalThrusterTorque()          const;
  virtual Eigen::Vector3d               getAppliedTorque()                const;
  virtual double                        getNumberOfMagnetorquers()        const;
  virtual double                        getNumberOfReactionWheels()       const;
  virtual double                        getNumberOfThrusters()            const;
  virtual std::vector<std::string>      getMagnetorquersState()           const;
  virtual std::vector<Eigen::Vector3d>  getMagnetorquersMagneticMoment()  const;
  virtual std::vector<double>           getMagnetorquersOperatingLoad()   const;
  virtual std::vector<std::string>      getReactionWheelsState()          const;
  virtual std::vector<double>           getReactionWheelsAngularVel()     const;
  virtual std::vector<double>           getReactionWheelsAngularAccel()   const;  
  virtual std::vector<double>           getReactionWheelsMotorTorque()    const;
  virtual std::vector<double>           getThrustersThrottle()            const;
  virtual std::vector<Eigen::Vector3d>  getThrustersForce()               const; 
  virtual std::vector<Eigen::Vector3d>  getThrustersTorque()              const;   
  virtual std::vector<double>           getThrustersMassFlowRate()        const;
  virtual Eigen::Vector3d               getAngularMomentum()              const;
  virtual Eigen::Vector3d               getAppliedAngularMomentumDelta()  const;
  virtual Eigen::Vector3d               getRWAngularMomentum()            const;  

          void setEarthGravitationalForce    (const Eigen::Vector3d &icrf_accel);
          void setPlanetaryGravitationalForce(const Eigen::Vector3d &icrf_accel);
          void setDragForce                  (const Eigen::Vector3d &icrf_accel);
          void setSolarPressureForce         (const Eigen::Vector3d &icrf_accel);
          void setThrustersForce             (const Eigen::Vector3d &icrf_force);
  virtual void setMagnetorquersLoad          (const Eigen::VectorXd &operating_load);
  virtual void setReactionWheelsMotorTorque  (const Eigen::VectorXd &motor_torque);
  virtual void setThrustersThrottle          (const Eigen::VectorXd &throttle);

          void InitializeTrajectoryRefFrames(const EarthOrientation &earth_orientation);
          void CalculateTrajectoryState();
          void PropagateTrajectory(const double timestep);

  virtual void InitializeAttitudeRefFrames();
  virtual void CalculateAttitudeState(const double mjd_utc, 
                                      std::unique_ptr<EarthMagneticField> &magnetic_field,
                                      const EarthOrientation &earth_orientation);
  virtual void PropagateAttitude(const double timestep);

protected:
  std::string     satellite_id_;
  Eigen::Vector3d icrf_position_;
  Eigen::Vector3d itrf_position_;
  Eigen::Vector3d lat_lon_position_;
  Eigen::Vector3d icrf_velocity_;
  Eigen::Vector3d itrf_velocity_;
  Eigen::Vector3d icrf_acceleration_;
  Eigen::Vector3d atmospheric_drag_force_icrf_; 
  Eigen::Vector3d earth_gravitational_force_icrf_;
  Eigen::Vector3d planterary_gravitational_force_icrf_;
  Eigen::Vector3d solar_pressure_force_icrf_;
  Eigen::Vector3d thrusters_force_icrf_;
  Eigen::Vector3d applied_force_icrf_;
  double          eccentricity_;
  double          semimajor_axis_;
  double          inclination_;
  double          ascending_node_;
  double          arg_periapsis_;
  double          true_anomaly_;
  double          mass_;
  double          mass_dot_;
  double          cross_section_;
  double          drag_coeff_;
  double          radiation_pressure_coeff_;

          void            CalculateNewMass(const double timestep);
          void            StateVectorToOrbitalElements();
  virtual Eigen::Vector3d CalculateThrustersForce();
  virtual double          CalculateThrusterMassFlowRate();          
};

} // end namespace polaris

#endif  //  POLARIS_SATELLITE_H_