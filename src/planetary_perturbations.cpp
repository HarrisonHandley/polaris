//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  PlanetaryPertubrations Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "planetary_perturbations.h"

#include <fstream>
#include <iostream>

namespace polaris {

//------------------------------------------------------------------------------
//  PlanetaryPerturbations(const std::string &model_name) 
//------------------------------------------------------------------------------
/** Default constructor requiring Planetary perturbation model configuration 
  * data.
  *
  * \param model_name Name of the planetary pertubration model used
  */
PlanetaryPerturbations::PlanetaryPerturbations(const std::string &model_name) : 
                                               model_name_           (model_name),
                                               sun_position_icrf_    (Eigen::Vector3d::Zero()),
                                               mercury_position_icrf_(Eigen::Vector3d::Zero()),
                                               venus_position_icrf_  (Eigen::Vector3d::Zero()),
                                               moon_position_icrf_   (Eigen::Vector3d::Zero()),
                                               mars_position_icrf_   (Eigen::Vector3d::Zero()),
                                               jupiter_position_icrf_(Eigen::Vector3d::Zero()),
                                               saturn_position_icrf_ (Eigen::Vector3d::Zero()),
                                               uranus_position_icrf_ (Eigen::Vector3d::Zero()),
                                               neptune_position_icrf_(Eigen::Vector3d::Zero()),
                                               pluto_position_icrf_  (Eigen::Vector3d::Zero())
{

}

//------------------------------------------------------------------------------
//  PlanetaryPerturbations(const PlanetaryPerturbations &model) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param model The object being copied.
  */
PlanetaryPerturbations::PlanetaryPerturbations(const PlanetaryPerturbations &model) :
                                               model_name_           (model.model_name_),
                                               sun_position_icrf_    (model.sun_position_icrf_),
                                               mercury_position_icrf_(model.mercury_position_icrf_),
                                               venus_position_icrf_  (model.venus_position_icrf_),
                                               moon_position_icrf_   (model.moon_position_icrf_),
                                               mars_position_icrf_   (model.mars_position_icrf_),
                                               jupiter_position_icrf_(model.jupiter_position_icrf_),
                                               saturn_position_icrf_ (model.saturn_position_icrf_),
                                               uranus_position_icrf_ (model.uranus_position_icrf_),
                                               neptune_position_icrf_(model.neptune_position_icrf_),
                                               pluto_position_icrf_  (model.pluto_position_icrf_)
{

}

//------------------------------------------------------------------------------
//  ~PlanetaryPerturbations() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
PlanetaryPerturbations::~PlanetaryPerturbations()
{

}

//------------------------------------------------------------------------------
//  PlanetaryPerturbations& operator=(const PlanetaryPerturbations &model) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param model The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
PlanetaryPerturbations& PlanetaryPerturbations::operator=(const PlanetaryPerturbations &model)
{
  if(&model == this)
  {
    return *this;
  }
  model_name_            = model.model_name_;
  sun_position_icrf_     = model.sun_position_icrf_;
  mercury_position_icrf_ = model.mercury_position_icrf_;
  venus_position_icrf_   = model.venus_position_icrf_;
  moon_position_icrf_    = model.moon_position_icrf_;
  mars_position_icrf_    = model.mars_position_icrf_;
  jupiter_position_icrf_ = model.jupiter_position_icrf_;
  saturn_position_icrf_  = model.saturn_position_icrf_;
  uranus_position_icrf_  = model.uranus_position_icrf_;
  neptune_position_icrf_ = model.neptune_position_icrf_;
  pluto_position_icrf_   = model.pluto_position_icrf_;
  return *this;
}

//------------------------------------------------------------------------------
//  std::string getModelName()
//------------------------------------------------------------------------------
/**
  * Get the name of the model used to calculate the planetary positions. 
  *
  * \return std::string of the name of the model used.
  */
std::string PlanetaryPerturbations::getModelName() const
{
  return model_name_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getSunPositionICRF() const 
//------------------------------------------------------------------------------
/** Get the position vector of the Sun in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Sun position vector in ICRF with earth origin 
  *         \f$[m]\f$.
  */
Eigen::Vector3d PlanetaryPerturbations::getSunPositionICRF() const
{
  return sun_position_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getMercuryPositionICRF() const 
//------------------------------------------------------------------------------
/** Get the position vector of the Mercury in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Mercury position vector in ICRF with earth origin 
  *         \f$[m]\f$.
  */
Eigen::Vector3d PlanetaryPerturbations::getMercuryPositionICRF() const
{
  return mercury_position_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getVenusPositionICRF() const 
//------------------------------------------------------------------------------
/** Get the position vector of the Venus in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Venus position vector in ICRF with earth origin 
  *         \f$[m]\f$.
  */
Eigen::Vector3d PlanetaryPerturbations::getVenusPositionICRF() const
{
  return venus_position_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getEarthPositionICRFBarycenter() const 
//------------------------------------------------------------------------------
/** Get the position vector of the Earth in ICRF with solar system barycenter
  * origin.
  * 
  * \return Eigen::Vector3d Earth position vector in ICRF with solar system 
  *         barycenter origin \f$[m]\f$.
  */
Eigen::Vector3d PlanetaryPerturbations::getEarthPositionICRFBarycenter() const
{
  return earth_position_icrf_barycenter_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getMoonPositionICRF() const 
//------------------------------------------------------------------------------
/** Get the position vector of the Moon in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Moon position vector in ICRF with earth origin 
  *         \f$[m]\f$.
  */
Eigen::Vector3d PlanetaryPerturbations::getMoonPositionICRF() const
{
  return moon_position_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getMarsPositionICRF() const 
//------------------------------------------------------------------------------
/** Get the position vector of the Mars in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Mars position vector in ICRF with earth origin 
  *         \f$[m]\f$.
  */
Eigen::Vector3d PlanetaryPerturbations::getMarsPositionICRF() const
{
  return mars_position_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getJupiterPositionICRF() const 
//------------------------------------------------------------------------------
/** Get the position vector of the Jupiter in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Jupiter position vector in ICRF with earth origin 
  *         \f$[m]\f$.
  */
Eigen::Vector3d PlanetaryPerturbations::getJupiterPositionICRF() const
{
  return jupiter_position_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getSaturnPositionICRF() const 
//------------------------------------------------------------------------------
/** Get the position vector of the Saturn in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Saturn position vector in ICRF with earth origin 
  *         \f$[m]\f$.
  */
Eigen::Vector3d PlanetaryPerturbations::getSaturnPositionICRF() const
{
  return saturn_position_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getUranusPositionICRF() const 
//------------------------------------------------------------------------------
/** Get the position vector of the Uranus in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Uranus position vector in ICRF with earth origin 
  *         \f$[m]\f$.
  */
Eigen::Vector3d PlanetaryPerturbations::getUranusPositionICRF() const
{
  return uranus_position_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getNeptunePositionICRF() const 
//------------------------------------------------------------------------------
/** Get the position vector of the Neptune in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Neptune position vector in ICRF with earth origin 
  *         \f$[m]\f$.
  */
Eigen::Vector3d PlanetaryPerturbations::getNeptunePositionICRF() const
{
  return neptune_position_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getPlutoPositionICRF() const 
//------------------------------------------------------------------------------
/** Get the position vector of the Pluto in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Pluto position vector in ICRF with earth origin 
  *         \f$[m]\f$.
  */
Eigen::Vector3d PlanetaryPerturbations::getPlutoPositionICRF() const
{
  return pluto_position_icrf_;
}

} // end namespace polaris