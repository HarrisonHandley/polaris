//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Satellite Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "satellite.h"

#include "constants.h"

namespace polaris {

//------------------------------------------------------------------------------
//  Satellite(const std::string     &satellite_id, 
//            const Eigen::Vector3d &icrf_pos,
//            const Eigen::Vector3d &icrf_vel,
//            const double           mass, 
//            const double           cross_section,
//            const double           drag_coeff, 
//            const double           radiation_pressure_coeff)
//------------------------------------------------------------------------------
/** Default constructor requiring satellite configuration.
  *
  * \param satellite_id Identification string for the satellite object.
  * \param icrf_pos Satellite position vector in ICRF with earth origin in \f$[m]\f$.
  * \param icrf_vel Satellite velocity vector in ICRF with earth origin in \f$[m]\f$.
  * \param mass Mass of satellite in \f$[kg]\f$.
  * \param cross_section Cross sectional area of satellite parallel to velocity vector \f$[m^2]\f$.
  * \param drag_coeff Drag coefficient of satellite.
  * \param radiation_pressure_coeff Radiation pressure coefficient of satellite.
  */
Satellite::Satellite(const std::string     &satellite_id, 
                     const Eigen::Vector3d &icrf_pos,
                     const Eigen::Vector3d &icrf_vel,
                     const double           mass, 
                     const double           cross_section,
                     const double           drag_coeff, 
                     const double           radiation_pressure_coeff) :
                     satellite_id_                       (satellite_id),
                     icrf_position_                      (icrf_pos),
                     itrf_position_                      (Eigen::Vector3d::Zero()),
                     lat_lon_position_                   (Eigen::Vector3d::Zero()),
                     icrf_velocity_                      (icrf_vel),
                     itrf_velocity_                      (Eigen::Vector3d::Zero()),
                     icrf_acceleration_                  (Eigen::Vector3d::Zero()),
                     atmospheric_drag_force_icrf_        (Eigen::Vector3d::Zero()),
                     earth_gravitational_force_icrf_     (Eigen::Vector3d::Zero()),
                     planterary_gravitational_force_icrf_(Eigen::Vector3d::Zero()),
                     solar_pressure_force_icrf_          (Eigen::Vector3d::Zero()),
                     thrusters_force_icrf_               (Eigen::Vector3d::Zero()),
                     applied_force_icrf_                 (Eigen::Vector3d::Zero()),
                     eccentricity_                       (0),
                     semimajor_axis_                     (0),
                     inclination_                        (0),
                     ascending_node_                     (0),
                     arg_periapsis_                      (0),
                     true_anomaly_                       (0),
                     mass_                               (mass),
                     mass_dot_                           (0),
                     cross_section_                      (cross_section), 
                     drag_coeff_                         (drag_coeff), 
                     radiation_pressure_coeff_           (radiation_pressure_coeff)
{

}

//------------------------------------------------------------------------------
//  Satellite(const Satellite &sat) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param sat The object being copied.
  */
Satellite::Satellite(const Satellite &sat) :
                     satellite_id_                       (sat.satellite_id_),
                     icrf_position_                      (sat.icrf_position_),
                     itrf_position_                      (sat.itrf_position_),
                     lat_lon_position_                   (sat.lat_lon_position_),
                     icrf_velocity_                      (sat.icrf_velocity_),
                     itrf_velocity_                      (sat.itrf_velocity_),
                     icrf_acceleration_                  (sat.icrf_acceleration_),
                     atmospheric_drag_force_icrf_        (sat.atmospheric_drag_force_icrf_),
                     earth_gravitational_force_icrf_     (sat.earth_gravitational_force_icrf_),
                     planterary_gravitational_force_icrf_(sat.planterary_gravitational_force_icrf_),
                     solar_pressure_force_icrf_          (sat.solar_pressure_force_icrf_),
                     thrusters_force_icrf_               (sat.thrusters_force_icrf_),
                     applied_force_icrf_                 (sat.applied_force_icrf_),
                     eccentricity_                       (sat.eccentricity_),
                     semimajor_axis_                     (sat.semimajor_axis_),
                     inclination_                        (sat.inclination_),
                     ascending_node_                     (sat.ascending_node_),
                     arg_periapsis_                      (sat.arg_periapsis_),
                     true_anomaly_                       (sat.true_anomaly_),
                     mass_                               (sat.mass_),
                     mass_dot_                           (sat.mass_dot_),
                     cross_section_                      (sat.cross_section_), 
                     drag_coeff_                         (sat.drag_coeff_), 
                     radiation_pressure_coeff_           (sat.radiation_pressure_coeff_)
{

}

//------------------------------------------------------------------------------
//  ~Satellite() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
Satellite::~Satellite()
{

}

//------------------------------------------------------------------------------
//  Satellite& operator=(const Satellite &sat) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param sat The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
Satellite& Satellite::operator=(const Satellite &sat)
{
  if(&sat == this)
  {
    return *this;
  }
  satellite_id_                        = sat.satellite_id_;
  icrf_position_                       = sat.icrf_position_;
  itrf_position_                       = sat.itrf_position_;
  lat_lon_position_                    = sat.lat_lon_position_;
  icrf_velocity_                       = sat.icrf_velocity_;
  itrf_velocity_                       = sat.itrf_velocity_;
  icrf_acceleration_                   = sat.icrf_acceleration_;
  atmospheric_drag_force_icrf_         = sat.atmospheric_drag_force_icrf_;
  earth_gravitational_force_icrf_      = sat.earth_gravitational_force_icrf_;
  planterary_gravitational_force_icrf_ = sat.planterary_gravitational_force_icrf_;
  solar_pressure_force_icrf_           = sat.solar_pressure_force_icrf_;
  thrusters_force_icrf_                = sat.thrusters_force_icrf_;
  applied_force_icrf_                  = sat.applied_force_icrf_;
  eccentricity_                        = sat.eccentricity_;
  semimajor_axis_                      = sat.semimajor_axis_;
  inclination_                         = sat.inclination_;
  ascending_node_                      = sat.ascending_node_;
  arg_periapsis_                       = sat.arg_periapsis_;
  true_anomaly_                        = sat.true_anomaly_;
  mass_                                = sat.mass_;
  mass_dot_                            = sat.mass_dot_;
  cross_section_                       = sat.cross_section_; 
  drag_coeff_                          = sat.drag_coeff_;
  radiation_pressure_coeff_            = sat.radiation_pressure_coeff_;
  
  return *this;
} 

//------------------------------------------------------------------------------
//  std::string getSatelliteID() const 
//------------------------------------------------------------------------------
/** Get satellites identification string.
  * 
  * \return std::string satellites identification string.
  */ 
std::string Satellite::getSatelliteID() const
{
  return satellite_id_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getLatLonPosition() const 
//------------------------------------------------------------------------------
/** Get satellites latitude and longitude position vector in ITRF geodesic.
  * 
  * \return Eigen::Vector3d Satellites latitude and longitude position vector 
  *         \f$[lat, lon, altitude]\f$ in \f$[m]\f$.
  */ 
Eigen::Vector3d Satellite::getLatLonPosition() const
{
  return lat_lon_position_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getITRFPosition() const 
//------------------------------------------------------------------------------
/** Get satellites position vector in ITRF.
  * 
  * \return Eigen::Vector3d Satellites ITRF position vector \f$[x, y, z]\f$ in 
  *         \f$[m]\f$.
  */ 
Eigen::Vector3d Satellite::getITRFPosition() const
{
  return itrf_position_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getICRFPosition() const 
//------------------------------------------------------------------------------
/** Get satellites position vector in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Satellites ICRF position vector \f$[x, y, z]\f$ in 
  *         \f$[m]\f$.
  */ 
Eigen::Vector3d Satellite::getICRFPosition() const
{
  return icrf_position_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getICRFVelocity() const 
//------------------------------------------------------------------------------
/** Get satellites velocity vector in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Satellites ICRF velocity vector \f$[x, y, z]\f$ in 
  *         \f$[m/s]\f$.
  */ 
Eigen::Vector3d Satellite::getICRFVelocity() const
{
  return icrf_velocity_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getICRFAcceleration() const 
//------------------------------------------------------------------------------
/** Get satellites acceleration vector in ICRF with earth origin.
  * 
  * \return Eigen::Vector3d Satellites ICRF acceleration vector \f$[x, y, z]\f$ 
  *         in \f$[m/s^2]\f$.
  */ 
Eigen::Vector3d Satellite::getICRFAcceleration() const
{
  return icrf_acceleration_;
}

//------------------------------------------------------------------------------
//  double getEccentricity() const 
//------------------------------------------------------------------------------
/** Get satellites orbital eccentricity in ICRF with earth origin.
  * 
  * \return double Satellites orbital eccentricity.
  */ 
double Satellite::getEccentricity() const
{
  return eccentricity_;
}

//------------------------------------------------------------------------------
//  double getSemiMajorAxis() const 
//------------------------------------------------------------------------------
/** Get satellites orbital semi-major axis in ICRF with earth origin.
  * 
  * \return double Satellites orbital semi-major axis \f$[m]\f$.
  */ 
double Satellite::getSemiMajorAxis() const
{
  return semimajor_axis_;
}

//------------------------------------------------------------------------------
//  double getInclination() const 
//------------------------------------------------------------------------------
/** Get satellites orbital inclination in ICRF with earth origin.
  * 
  * \return double Satellites orbital inclination \f$[rad]\f$.
  */ 
double Satellite::getInclincation() const
{
  return inclination_;
}

//------------------------------------------------------------------------------
//  double getAscendingNode() const 
//------------------------------------------------------------------------------
/** Get satellites orbital ascending node in ICRF with earth origin.
  * 
  * \return double Satellites orbital ascending node \f$[rad]\f$.
  */ 
double Satellite::getAscendingNode() const
{
  return ascending_node_;
}

//------------------------------------------------------------------------------
//  double getArgumentOfPeriapsis() const 
//------------------------------------------------------------------------------
/** Get satellites orbital argument of periapsis in ICRF with earth origin.
  * 
  * \return double Satellites orbital argument of periapsis \f$[rad]\f$.
  */ 
double Satellite::getArgumentOfPeriapsis() const
{
  return arg_periapsis_;
}

//------------------------------------------------------------------------------
//  double getTrueAnomaly() const 
//------------------------------------------------------------------------------
/** Get satellites orbital true anomaly in ICRF with earth origin.
  * 
  * \return double Satellites orbital true anomaly \f$[rad]\f$.
  */ 
double Satellite::getTrueAnomaly() const
{
  return true_anomaly_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAtmosphericDragForce() const 
//------------------------------------------------------------------------------
/** Get force vector acting on satellite from atmospheric drag in ICRF.
  * 
  * \return Eigen::Vector3d Force vector \f$[x, y, z]\f$ acting on satellite 
  *         from atmospheric drag \f$[N]\f$.
  */ 
Eigen::Vector3d Satellite::getAtmosphericDragForce() const
{
  return atmospheric_drag_force_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getEarthGravityForce() const 
//------------------------------------------------------------------------------
/** Get force vector acting on satellite from earth gravity in ICRF.
  * 
  * \return Eigen::Vector3d Force vector \f$[x, y, z]\f$ acting on satellite 
  *         from earth's gravity \f$[N]\f$.
  */ 
Eigen::Vector3d Satellite::getEarthGravityForce() const
{
  return earth_gravitational_force_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getPlanetaryGravityForce() const 
//------------------------------------------------------------------------------
/** Get force vector acting on satellite from planetary gravity in ICRF.
  * 
  * \return Eigen::Vector3d Force vector \f$[x, y, z]\f$ acting on satellite 
  *         from planetary gravity (excluding earth) \f$[N]\f$
  */ 
Eigen::Vector3d Satellite::getPlanetaryGravityForce() const
{
  return planterary_gravitational_force_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getSolarPressureForce() const 
//------------------------------------------------------------------------------
/** Get force vector acting on satellite from solar pressure in ICRF.
  * 
  * \return Eigen::Vector3d Force vector \f$[x, y, z]\f$ acting on satellite 
  *         from solar pressure \f$[N]\f$.
  */ 
Eigen::Vector3d Satellite::getSolarPressureForce() const
{
  return solar_pressure_force_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getTotalThrusterForce() const 
//------------------------------------------------------------------------------
/** Get force vector acting on satellite from all spacecraft thrusters in ICRF.
  * 
  * \return Eigen::Vector3d Force vector \f$[x, y, z]\f$ acting on satellite 
  *         from all spacecraft thrusters \f$[N]\f$.
  */ 
Eigen::Vector3d Satellite::getTotalThrusterForce() const
{
  return thrusters_force_icrf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getTotalThrusterForce() const 
//------------------------------------------------------------------------------
/** Get total force vector of all forces acting on the satellite from all 
  * sources.
  * 
  * \return Eigen::Vector3d Force vector \f$[x, y, z]\f$ of all forces acting 
  *         on the satellite \f$[N]\f$.
  */ 
Eigen::Vector3d Satellite::getAppliedForce() const
{
  return applied_force_icrf_;
}

//------------------------------------------------------------------------------
//  double getMass() const 
//------------------------------------------------------------------------------
/** Get current mass of satellite.
  * 
  * \return double Mass of satellite \f$[kg]\f$.
  */ 
double Satellite::getMass() const
{
  return mass_;
}

//------------------------------------------------------------------------------
//  double getCrossSection() const 
//------------------------------------------------------------------------------
/** Get current cross sectional area of satellite.
  * 
  * \return double Cross sectional area of satellite \f$[m^2]\f$.
  */ 
double Satellite::getCrossSection() const
{
  return cross_section_;
}

//------------------------------------------------------------------------------
//  double getDragCoeff() const 
//------------------------------------------------------------------------------
/** Get drag coefficient of satellite.
  * 
  * \return double Drag coefficient of satellite.
  */ 
double Satellite::getDragCoeff() const
{
  return drag_coeff_;
}

//------------------------------------------------------------------------------
//  double getRadiationPressureCoeff() const 
//------------------------------------------------------------------------------
/** Get radiation pressure coefficient of satellite.
  * 
  * \return double Radiation pressure coefficient of satellite.
  */ 
double Satellite::getRadiationPressureCoeff() const
{
  return radiation_pressure_coeff_;
}

//------------------------------------------------------------------------------
//  Eigen::Quaterniond getAttitude() const 
//------------------------------------------------------------------------------
/** Get the spacecraft body frame attitude normalized quaternion with respect 
  * to ICRF. 
  * 
  * \return Normalized Eigen::Quaterniond attitude quaternion of spacecraft
  *         body reference frame with respect to ICRF.
  */ 
Eigen::Quaterniond Satellite::getAttitude() const
{
  return {1, 0, 0, 0};
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAngularVelocity() const 
//------------------------------------------------------------------------------
/** Get the spacecraft body frame angular velocity vector with respect to ICRF.
  * 
  * \return Eigen::Vector3d Spacecraft angular velocity vector \f$[x, y, z]\f$ 
  *         in \f$[rad/s]\f$ defined with respect to ICRF. 
  */ 
Eigen::Vector3d Satellite::getAngularVelocity() const
{
  return {0, 0, 0};
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAngularAcceleration() const 
//------------------------------------------------------------------------------
/** Get the spacecraft body frame angular acceleration vector with respect to 
  * ICRF.
  * 
  * \return Eigen::Vector3d Spacecraft angular acceleration vector 
  *         \f$[\omega_x, \omega_y, \omega_z]\f$ in \f$[rad/s^2]\f$ defined 
  *         with respect to ICRF. 
  */ 
Eigen::Vector3d Satellite::getAngularAcceleration() const
{
  return {0, 0, 0};
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAtmosphericDragTorque() const 
//------------------------------------------------------------------------------
/** Get the atmospheric drag torque vector experienced by the spacecraft in the
  * spacecraft body reference frame.
  * 
  * \return Eigen::Vector3d Atmospheric drag torque vector 
  *         \f$[\tau_x, \tau_y, \tau_z]\f$ in \f$[Nm]\f$ defined in the 
  *         spacecraft body reference frame. 
  */ 
Eigen::Vector3d Satellite::getAtmosphericDragTorque() const
{
  return {0, 0, 0};
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getGravityGradientTorque() const 
//------------------------------------------------------------------------------
/** Get the gravity gradient torque vector experienced by the spacecraft in the
  * spacecraft body reference frame.
  * 
  * \return Eigen::Vector3d Gravity gradient torque vector 
  *         \f$[\tau_x, \tau_y, \tau_z]\f$ in \f$[Nm]\f$ defined in the 
  *         spacecraft body reference frame. 
  */
Eigen::Vector3d Satellite::getGravityGradientTorque() const
{
  return {0, 0, 0};
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getMagneticTorque() const 
//------------------------------------------------------------------------------
/** Get the magnetic torque vector experienced by the spacecraft in the
  * spacecraft body reference frame.
  * 
  * \return Eigen::Vector3d Magnetic torque vector \f$[\tau_x, \tau_y, \tau_z]\f$ 
  *         in \f$[Nm]\f$ defined in the spacecraft body reference frame.
  */ 
Eigen::Vector3d Satellite::getMagneticTorque() const
{
  return {0, 0, 0};
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getSolarPressureTorque() const 
//------------------------------------------------------------------------------
/** Get the solar pressure torque vector experienced by the spacecraft in the
  * spacecraft body reference frame.
  * 
  * \return Eigen::Vector3d Solar pressure torque vector 
  *         \f$[\tau_x, \tau_y, \tau_z]\f$ in \f$[Nm]\f$ defined in the 
  *         spacecraft body reference frame.
  */ 
Eigen::Vector3d Satellite::getSolarPressureTorque() const
{
  return {0, 0, 0};
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getTotalThrusterTorque() const 
//------------------------------------------------------------------------------
/** Get the total torque vector from spacecraft thrusters in the
  * spacecraft body reference frame.
  * 
  * \return Eigen::Vector3d Total torque vector from spacecraft thrutsers 
  *         \f$[\tau_x, \tau_y, \tau_z]\f$ in \f$[Nm]\f$ defined in the 
  *         spacecraft body reference frame.
  */ 
Eigen::Vector3d Satellite::getTotalThrusterTorque() const
{
  return {0, 0, 0};
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAppliedTorque() const 
//------------------------------------------------------------------------------
/** Get the total torque applied to the spacecraft in the spacecraft body
  * reference frame.
  * 
  * \return Eigen::Vector3d Total applied torque vector to the spacecraft 
  *         \f$[\tau_x, \tau_y, \tau_z]\f$ in \f$[Nm]\f$ defined in the 
  *         spacecraft body reference frame.
  */ 
Eigen::Vector3d Satellite::getAppliedTorque() const
{
  return {0, 0, 0};
}

//------------------------------------------------------------------------------
//  double getNumberOfMagnetorquers() const 
//------------------------------------------------------------------------------
/** Get the total number of magnetorquer actuators on the spacecraft. 
  * 
  * \return double Number of magnetorquer actuators on the spacecraft.
  */
double Satellite::getNumberOfMagnetorquers() const
{
  return 0;
}

//------------------------------------------------------------------------------
//  double getNumberOfReactionWheels() const 
//------------------------------------------------------------------------------
/** Get the total number of reaction wheel actuators on the spacecraft. 
  * 
  * \return double Number of reaction wheel actuators on the spacecraft.
  */
double Satellite::getNumberOfReactionWheels() const
{
  return 0;
}

//------------------------------------------------------------------------------
//  double getNumberOfThrusters() const 
//------------------------------------------------------------------------------
/** Get the total number of thruster actuators on the spacecraft. 
  * 
  * \return double Number of thruster actuators on the spacecraft.
  */
double Satellite::getNumberOfThrusters() const
{
  return 0;
}

//------------------------------------------------------------------------------
//  std::vector<std::string> getMagnetorquersState() const 
//------------------------------------------------------------------------------
/** Get the state of each magnetorquer actuator on the spacecraft.
  * 
  * \return std::vector<std::string> Vector of all magnetorquer states as 
  *         std::string. The order of values in the vector corresponds to the 
  *         same order used in the spacecraft XML configuration file.
  */
std::vector<std::string> Satellite::getMagnetorquersState() const
{
  std::vector<std::string> output;
  return output;
}

//------------------------------------------------------------------------------
//  std::vector<Eigen::Vector3d> getMagnetorquersMagneticMoment() const 
//------------------------------------------------------------------------------
/** Get the magnetic moment of each magnetorquer actuator on the spacecraft in
  * the spacecraft body reference frame.
  * 
  * \return std::vector<Eigen::Vector3d> Vector of all magnetorquer magnetic 
  *         moment Eigen::Vector3d in \f$[A/m^2]\f$ in the spacecraft body 
  *         reference frame. The order of values in the vector corresponds to 
  *         the same order used in the spacecraft XML configuration file.
  */
std::vector<Eigen::Vector3d> Satellite::getMagnetorquersMagneticMoment() const
{
  std::vector<Eigen::Vector3d> output;
  return output;
}

//------------------------------------------------------------------------------
//  std::vector<double> getMagnetorquersOperatingLoad() const 
//------------------------------------------------------------------------------
/** Get the operating load percentage of each magnetorquer actuator on the 
  * spacecraft.
  * 
  * \return std::vector<double> Vector of each magnetorquer operating load 
  *         percentage in \f$[\%]\f$ from \f$[0 \to 1]\f$. The order of values 
  *         in the vector corresponds to the same order used in the spacecraft 
  *         XML configuration file.
  */
std::vector<double> Satellite::getMagnetorquersOperatingLoad() const
{
  std::vector<double> output;
  return output;
}

//------------------------------------------------------------------------------
//  std::vector<std::string> getReactionWheelsState() const 
//------------------------------------------------------------------------------
/** Get the state of each reaction wheel actuator on the spacecraft.
  * 
  * \return std::vector<std::string> Vector of each reaction wheel state as 
  *         std::string. The order of values in the vector corresponds to the 
  *         same order used in the spacecraft XML configuration file.
  */
std::vector<std::string> Satellite::getReactionWheelsState() const
{
  std::vector<std::string> output;
  return output;
}

//------------------------------------------------------------------------------
//  std::vector<double> getReactionWheelsAngularVel() const 
//------------------------------------------------------------------------------
/** Get the angular velocity of each reaction wheel actuator on the spacecraft
  * about the spin axis of the reaction wheel.
  * 
  * \return std::vector<double> Vector of each reaction wheel angular velocity 
  *         \f$[rad/s]\f$ about the spin axis of the reaction wheel. The order 
  *         of values in the vector corresponds to the same order used in the
  *         spacecraft XML configuration file.
  */
std::vector<double> Satellite::getReactionWheelsAngularVel() const
{
  std::vector<double> output;
  return output;
}

//------------------------------------------------------------------------------
//  std::vector<double> getReactionWheelsAngularAccel() const 
//------------------------------------------------------------------------------
/** Get the angular acceleration of each reaction wheel actuator on the 
  * spacecraft about the spin axis of the reaction wheel.
  * 
  * \return std::vector<double> Vector of each reaction wheel angular 
  *         acceleration \f$[rad/s^2]\f$ about the spin axis of the reaction 
  *         wheel. The order of values in the vector corresponds to the same 
  *         order used in the spacecraft XML configuration file.
  */
std::vector<double> Satellite::getReactionWheelsAngularAccel() const
{
  std::vector<double> output;
  return output;
}

//------------------------------------------------------------------------------
//  std::vector<double> getReactionWheelsMotorTorque() const 
//------------------------------------------------------------------------------
/** Get the applied motor torque for each reaction wheel actuator on the 
  * spacecraft.
  * 
  * \return std::vector<double> Vector of the applied motor torque for each 
  *         reaction wheel \f$[Nm]\f$. The order of values in the vector 
  *         corresponds to the same order used in the spacecraft XML 
  *         configuration file.
  */
std::vector<double> Satellite::getReactionWheelsMotorTorque() const
{
  std::vector<double> output;
  return output;
}

//------------------------------------------------------------------------------
//  std::vector<double> getThrustersThrottle() const 
//------------------------------------------------------------------------------
/** Get the applied throttle percentage for each thruster actuator on the 
  * spacecraft.
  * 
  * \return std::vector<double> Vector of the applied throttle percentage for 
  *         each thruster \f$[\%]\f$ from \f$[0 \to 1]\f$. The order of values 
  *         in the vector corresponds to the same order used in the spacecraft 
  *         XML configuration file.
  */
std::vector<double> Satellite::getThrustersThrottle() const
{
  std::vector<double> output;
  return output;
}

//------------------------------------------------------------------------------
//  std::vector<Eigen::Vector3d> getThrustersForce() const 
//------------------------------------------------------------------------------
/** Get the generated thrust force of each thruster actuator on the 
  * spacecraft in spacecraft body reference frame.
  * 
  * \return std::vector<Eigen::Vector3d> Vector of each thrusters generated 
  *         thrust force \f$[N]\f$ in spacecraft body reference frame. The order
  *         of values in the vector corresponds to the same order used in the 
  *         spacecraft XML configuration file.
  */
std::vector<Eigen::Vector3d> Satellite::getThrustersForce() const
{
  std::vector<Eigen::Vector3d> output;
  return output;
}

//------------------------------------------------------------------------------
//  std::vector<Eigen::Vector3d> getThrustersTorque() const 
//------------------------------------------------------------------------------
/** Get the generated thrust torque of each thruster actuator on the 
  * spacecraft in spacecraft body reference frame.
  * 
  * \return std::vector<Eigen::Vector3d> Vector of each thrusters generated 
  *         thrust torque \f$[Nm]\f$ in spacecraft body reference frame. The 
  *         order of values in the vector corresponds to the same order used in 
  *         the spacecraft XML configuration file.
  */
std::vector<Eigen::Vector3d> Satellite::getThrustersTorque() const
{
  std::vector<Eigen::Vector3d> output;
  return output;
}

//------------------------------------------------------------------------------
//  std::vector<double> getThrustersMassFlowRate() const 
//------------------------------------------------------------------------------
/** Get the mass flow rate of each thruster actuator on the 
  * spacecraft.
  * 
  * \return std::vector<double> Vector of each thrusters mass flowrate 
  *         \f$[kg/s]\f$. The order of values in the vector corresponds to the 
  *         same order used in the spacecraft XML configuration file.
  */
std::vector<double> Satellite::getThrustersMassFlowRate() const
{
  std::vector<double> output;
  return output;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAngularMomentum() const 
//------------------------------------------------------------------------------
/** Get the angular momentum vector of the in the spacecraft body reference
  * frame.
  * 
  * \return Eigen::Vector3d Spacecraft angular momentum vector 
  *         \f$[L_x, L_y, L_z]\f$ \f$[kg \cdot m^2/s]\f$ in spacecraft body 
  *         reference frame.
  */
Eigen::Vector3d Satellite::getAngularMomentum() const
{
  return {0, 0, 0};
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAppliedAngularMomentumDelta() const 
//------------------------------------------------------------------------------
/** Get the angular momentum delta vector applied to the spacecraft from 
  * external torques in the spacecraft body reference frame.
  * 
  * \return Eigen::Vector3d Applied angular momentum delta to the spacecraft 
  *         from external torques \f$[\Delta L_x, \Delta L_y, \Delta L_z]\f$ 
  *         \f$[kg \cdot m^2/s]\f$ in spacecraft body reference frame.
  */
Eigen::Vector3d Satellite::getAppliedAngularMomentumDelta() const
{
  return {0, 0, 0};
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getRWAngularMomentum() const 
//------------------------------------------------------------------------------
/** Get the angular momentum vector applied to the spacecraft from 
  * reaction wheel actuators in the spacecraft body reference frame.
  * 
  * \return Eigen::Vector3d  Reaction wheel angular momentum applied to the 
  *         spacecraft \f$[L_x, L_y, L_z]\f$ \f$[kg \cdot m^2/s]\f$ in 
  *         spacecraft body reference frame.
  */
Eigen::Vector3d Satellite::getRWAngularMomentum() const
{
  return {0, 0, 0};
}

//------------------------------------------------------------------------------
//  void setEarthGravitationalForce(const Eigen::Vector3d &icrf_accel)
//------------------------------------------------------------------------------
/** Set the force applied to the satellite from the Earth's gravity field.
  * 
  * \param icrf_accel Acceleration from Earth's gravity field ICRF vector 
  *                   \f$[x, y, z]\f$ in \f$[m/s^2]\f$ 
  */
void Satellite::setEarthGravitationalForce(const Eigen::Vector3d &icrf_accel)
{
  earth_gravitational_force_icrf_ = icrf_accel * mass_;
}

//------------------------------------------------------------------------------
//  void setPlanetaryGravitationalForce(const Eigen::Vector3d &icrf_accel)
//------------------------------------------------------------------------------
/** Set the force applied to the satellite from the planets gravity field.
  * 
  * \param icrf_accel Acceleration from the planets gravity field ICRF vector 
  *                   \f$[x, y, z]\f$ in \f$[m/s^2]\f$ 
  */
void Satellite::setPlanetaryGravitationalForce(const Eigen::Vector3d &icrf_accel)
{
  planterary_gravitational_force_icrf_ = icrf_accel * mass_;
}

//------------------------------------------------------------------------------
//  void setDragForce((const Eigen::Vector3d &icrf_accel)
//------------------------------------------------------------------------------
/** Set the force applied to the satellite from atmospheric drag.
  * 
  * \param icrf_accel Acceleration from atmospheric drag ICRF vector 
  *                   \f$[x, y, z]\f$ in \f$[m/s^2]\f$ 
  */
void Satellite::setDragForce(const Eigen::Vector3d &icrf_accel)
{
  atmospheric_drag_force_icrf_ = icrf_accel * mass_;
}

//------------------------------------------------------------------------------
//  void setSolarPressureForce(const Eigen::Vector3d &icrf_accel)
//------------------------------------------------------------------------------
/** Set the force applied to the satellite from solar pressure.
  * 
  * \param icrf_accel Acceleration from solar pressure ICRF vector 
  *                   \f$[x, y, z]\f$ in \f$[m/s^2]\f$ 
  */
void Satellite::setSolarPressureForce(const Eigen::Vector3d &icrf_accel)
{
  solar_pressure_force_icrf_ = icrf_accel * mass_;
}

//------------------------------------------------------------------------------
//  void setThrustersForce(const Eigen::Vector3d &icrf_force)
//------------------------------------------------------------------------------
/** Set the force applied to the satellite from all thrusters.
  * 
  * \param icrf_force Force from all thrusters ICRF vector 
  *                   \f$[x, y, z]\f$ in \f$[N]\f$ 
  */
void Satellite::setThrustersForce(const Eigen::Vector3d &icrf_force)
{
  thrusters_force_icrf_ = icrf_force;
}

//------------------------------------------------------------------------------
//  void setMagnetorquersLoad(const Eigen::VectorXd &operating_load)
//------------------------------------------------------------------------------
/** Virtual method to set the load percentage for each magnetorquer actuator on 
  * the spacecraft.
  * 
  * \param operating_load Eigen::VectorXd of each magnetorquer operating load 
  *                       percentage value. The first value in the vector 
  *                       corresponds to applied load percentage for 
  *                       magnetorquer 1, snd so on for each magnetorquers.
  */
void Satellite::setMagnetorquersLoad(const Eigen::VectorXd &operating_load)
{

}

//------------------------------------------------------------------------------
//  void setReactionWheelsMotorTorque(const Eigen::VectorXd &motor_torque)
//------------------------------------------------------------------------------
/** Set the applied motor torque to each reaction wheel actuator on the 
  * spacecraft.
  * 
  * \param motor_torque Eigen::VectorXd of each reaction wheel applied motor 
  *                     torque value \f$[Nm]\f$. The order of values in the 
  *                     vector corresponds to the same order used in the 
  *                     spacecraft XML configuration file. 
  */
void Satellite::setReactionWheelsMotorTorque(const Eigen::VectorXd &motor_torque)
{
  
}

//------------------------------------------------------------------------------
//  void setThrustersThrottle(const Eigen::VectorXd &throttle)
//------------------------------------------------------------------------------
/** Set the applied throttle percentage for each thruster actuator on the 
  * spacecraft.
  * 
  * \param throttle Eigen::VectorXd of each thruster throttle percentage 
  *                 value \f$[\%]\f$ from \f$[0 \to 1]\f$. The order of 
  *                 values in the vector corresponds to the same order used 
  *                 in the spacecraft XML configuration file. 
  */
void Satellite::setThrustersThrottle(const Eigen::VectorXd &throttle)
{
  
}

//------------------------------------------------------------------------------
//  void InitializeTrajectoryRefFrames(const EarthOrientation &earth_orientation)
//------------------------------------------------------------------------------
/** Initializes the reference frames of the satellites position and velocity in
  * ITRF, Lat/Lon reference frame, and kepler orbital parameters in ICRF. These 
  * are initalized from the ICRF state vector that is generated during forward 
  * propagation.
  * 
  * \param earth_orientation EarthOrientation object that defines the rotation
  *                          matrices to convert from ICRF to ITRF and Lat/Lon
  *                          reference frames.
  */
void Satellite::InitializeTrajectoryRefFrames(const EarthOrientation &earth_orientation)
{
  itrf_position_ = earth_orientation.ICRS2ITRS(icrf_position_);
  itrf_velocity_ = earth_orientation.ICRS2ITRS(itrf_velocity_);

  lat_lon_position_ = earth_orientation.Cart2Geodetic(itrf_position_);

  StateVectorToOrbitalElements();
}

//------------------------------------------------------------------------------
//  void CalculateTrajectoryState()
//------------------------------------------------------------------------------
/** Calculates the applied force to the spacecraft in ICRF and subsequent
  * acceleration using equation \f$ F = ma \f$
  */
void Satellite::CalculateTrajectoryState()
{
  thrusters_force_icrf_ = CalculateThrustersForce();
  mass_dot_ = CalculateThrusterMassFlowRate();

  applied_force_icrf_ = earth_gravitational_force_icrf_ 
                      + planterary_gravitational_force_icrf_
                      + atmospheric_drag_force_icrf_
                      + solar_pressure_force_icrf_
                      + thrusters_force_icrf_;

  icrf_acceleration_ = applied_force_icrf_ / mass_;                                                              
}

//------------------------------------------------------------------------------
//  void PropagateTrajectory(const double timestep)
//------------------------------------------------------------------------------
/** Propagates the satellites trajectory forward by a given t seconds. The state 
  * is propagated forward using euler's method with equations:
  * \f$\vec{r}_{t_1} = \vec{r}_{t_0} + \vec{v}_{t_0} \times t + \frac{1}{2}
  * \vec{a}_{t_0} t^2\f$
  * 
  * \f$\vec{v}_{t_1} = \vec{v}_{t_0} \times t + \vec{a}_{t_0} t\f$
  * 
  * \param timestep Time in seconds to propagate forward.
  */
void Satellite::PropagateTrajectory(const double timestep)
{
  icrf_position_ = icrf_position_ + icrf_velocity_ * timestep + 
                   0.5 * icrf_acceleration_ * pow(timestep, 2);
  icrf_velocity_ = icrf_velocity_ + icrf_acceleration_ * timestep;

  CalculateNewMass(timestep);
}

//------------------------------------------------------------------------------
//  void InitializeAttitudeRefFrames()
//------------------------------------------------------------------------------
/** Initialize the rotation matrix between ICRF and spacecraft
  * body reference frame based on current attitude quaternion.
  */
void Satellite::InitializeAttitudeRefFrames()
{

}

//------------------------------------------------------------------------------
//  void CalculateAttitudeState()
//------------------------------------------------------------------------------
/** Calculates the applied torque and angular acceleration of the spacecraft 
  * in spacecraft body reference frame using equations:
  * 
  * \f$ \vec{\alpha}_{sat} = (I_{sat} - I_{rw})^{-1}(\vec{\tau}_{applied} - 
  * \vec{\tau}_{rw_motors} - \vec{\omega}_{sat} \times (I_{rw}\vec{\omega}_{rw} 
  * + I_{sat}\vec{\omega}_{sat}) )\f$
  * 
  * This method also calculates the angular momentum of the spacecraft, and
  * reaction wheels.
  * 
  * \f$\vec{M}_{sat} = I_{sat}\vec{\omega}_{sat}\f$
  * 
  * \f$\vec{M}_{rw} = I_{rw}\vec{\omega}_{rw}\f$
  * 
  */
void Satellite::CalculateAttitudeState(const double mjd_utc, 
                                       std::unique_ptr<EarthMagneticField> &magnetic_field,
                                       const EarthOrientation &earth_orientation)
{

}

//------------------------------------------------------------------------------
//  void PropagateAttitude(const double timestep)
//------------------------------------------------------------------------------
/** Propagates the spacecrafts attitude forward by a given t seconds. The state 
  * is propagated using euler's method using equations:
  * 
  * \f$\vec{\omega}_{t_1} = \vec{\omega}_{t_0} + \vec{\alpha}_{t_0} t\f$
  * 
  * \param timestep Time in seconds to propagate forward.
  */
void Satellite::PropagateAttitude(const double timestep)
{

}

//------------------------------------------------------------------------------
//  void CalculateNewMass(const double timestep)
//------------------------------------------------------------------------------
/** Calculates the mass of the satellite when accounting for all mass delta 
  * changes during the timestep using equation:
  * \f$ M_{t_1} = M_{t_0} + \dot{m}_{t_0} t \f$
  * 
  * \param timestep Time in seconds to propagate forward.
  */
void Satellite::CalculateNewMass(const double timestep)
{
  mass_ = mass_ + mass_dot_ * timestep;
  mass_dot_ = 0;
}

//------------------------------------------------------------------------------
//  void StateVectorToOrbitalElements()
//------------------------------------------------------------------------------
/** Calculates kepler orbital elements from the current state vector of the
  * satellite in an earth centered reference frame using equations:
  * 
  * \f$ \vec{e} = \vec{v} \times \frac{\vec{r} \times \vec{v}}{GM_{Earth}} - 
  * \frac{\vec{r}}{\|r\|}\f$
  * 
  * \f$ e = \|\vec{e}\| \f$
  * 
  * \f$ a = \frac{1}{(\frac{2}{\|\vec{r}\|}) - (\frac{{\|\vec{v}\|}^2}
  * {\|\vec{r}\|})}\f$
  * 
  * \f$ h = \vec{r} \times \vec{v} \f$
  * 
  * \f$ i = cos^{-1}(\frac{[0, 0, h_z]}{\|h\|}) \f$
  * 
  * \f$ \Omega = cos^{-1}(\frac{[0, h_x, 0]}{\|[-h_y, h_x, 0]\|}) \f$
  * 
  * \f$ \omega = cos^{-1}(\frac{[-h_y, h_x, 0] \cdot \vec{e}}
  * {\|\vec{[-h_y, h_x, 0]}\|\|\vec{e}\|}) \f$
  * 
  * \f$ \mu = cos^{-1}(\frac{\vec{e} \cdot \vec{r}}{\|\vec{e}\|\|\vec{r}\|}) \f$
  */
void Satellite::StateVectorToOrbitalElements()
{
  Eigen::Vector3d h = icrf_position_.cross(icrf_velocity_);
  Eigen::Vector3d e = (icrf_velocity_.cross(h) / kEarthGM) - 
                      (icrf_position_ / icrf_position_.norm());
  Eigen::Vector3d n = {-h(1), h(0), 0};

  eccentricity_ = e.norm();
  semimajor_axis_ = 1 / ((2/icrf_position_.norm()) - 
                         (pow(icrf_velocity_.norm(), 2)/kEarthGM));
  inclination_ = acos(h(2)/h.norm());

  if(n(1) >= 0)
  {
    ascending_node_ = acos(n(0)/n.norm());
  }
  else
  {
    ascending_node_ = 2*kPI - acos(n(0)/n.norm());
  }
  
  if(e(2) >= 0)
  {
    arg_periapsis_ = acos(n.dot(e)/(n.norm()*e.norm()));
  }
  else
  {
    arg_periapsis_ = 2*kPI - acos(n.dot(e)/(n.norm()*e.norm()));
  }

  if(icrf_position_.dot(icrf_velocity_) >= 0)
  {
    true_anomaly_ = acos(e.dot(icrf_position_) / (e.norm() * icrf_position_.norm()));
  }
  else
  {
    true_anomaly_ = 2*kPI - acos(e.dot(icrf_position_) / (e.norm() * icrf_position_.norm()));
  }

  eccentricity_   = eccentricity_   - 2*kPI * floor(eccentricity_   / (2*kPI));
  inclination_    = inclination_    - 2*kPI * floor(inclination_    / (2*kPI));
  ascending_node_ = ascending_node_ - 2*kPI * floor(ascending_node_ / (2*kPI));
  arg_periapsis_  = arg_periapsis_  - 2*kPI * floor(arg_periapsis_  / (2*kPI));
  true_anomaly_   = true_anomaly_   - 2*kPI * floor(true_anomaly_   / (2*kPI));
  // Eigen::Vector3d h = icrf_position_.cross(icrf_velocity_);
  // Eigen::Vector3d e = (icrf_velocity_.cross(h) / kEarthGM) - 
  //                     (icrf_position_ / icrf_position_.norm());
  // Eigen::Vector3d n = {-h(1), h(0), 0};

  // eccentricity_ = e.norm();
  // semimajor_axis_ = 1 / ((2/icrf_position_.norm()) - 
  //                        (pow(icrf_velocity_.norm(), 2)/kEarthGM));
  // inclination_ = acos(h(2)/h.norm());
  // ascending_node_ = acos(n(1)/n.norm());
  // arg_periapsis_ = acos(n.dot(e)/(n.norm()*e.norm()));
  // true_anomaly_ = acos(e.dot(icrf_position_) / (e.norm() * icrf_position_.norm()));

  // eccentricity_   = eccentricity_   - 2*kPI * floor(eccentricity_   / (2*kPI));
  // inclination_    = inclination_    - 2*kPI * floor(inclination_    / (2*kPI));
  // ascending_node_ = ascending_node_ - 2*kPI * floor(ascending_node_ / (2*kPI));
  // arg_periapsis_  = arg_periapsis_  - 2*kPI * floor(arg_periapsis_  / (2*kPI));
  // true_anomaly_   = true_anomaly_   - 2*kPI * floor(true_anomaly_   / (2*kPI));
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateThrustersForce()
//------------------------------------------------------------------------------
/** Calculates the total force vector applied to the spacecraft from all 
  * spacecraft thrusters in spacecraft body reference frame.
  * 
  * \return Eigen::Vector3d Total applied thrust force vector 
  *         \f$[F_x, F_y, F_z]\f$ to the spacecraft in the spacecraft body 
  *         reference frame \f$[N]\f$.
  */
Eigen::Vector3d Satellite::CalculateThrustersForce()
{
  return {0, 0, 0};
}

//------------------------------------------------------------------------------
//  double CalculateThrusterMassFlowRate()
//------------------------------------------------------------------------------
/** Calculates the total mass flow rater from all thrusters on the spacecraft. 
  * 
  * \return double Total mass flow rate of spacecraft from thrusters firing 
  *         \f$[kg/s]\f$.
  */
double Satellite::CalculateThrusterMassFlowRate()
{
  return 0;
}
} // end namespace polaris