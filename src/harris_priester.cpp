//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  HarrisPriester Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "harris_priester.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>

#include "constants.h"

namespace polaris {

//------------------------------------------------------------------------------
//  HarrisPriester(const std::string &filepath, 
//                    const std::string &model_name, 
//                    const int max_degree)
//------------------------------------------------------------------------------
/** Default constructor requiring Harris Priester model configuration data.
  *
  * \param hp_coeff_filepath Harris Priester model coefficients filepath.
  * \param model_name Name of the earth atmospheric density model used.
  * \param hp_parameter The Harris Priester coefficient: 
  *                     2 + (orbital inclination in deg)/22.5 deg.
  * \param min_altitude Minimum altitude that Harris Priester model is valid for
  *                     in \f$[km]\f$.
  * \param max_altitude Maximum altitude that Harris Priester model is valid for
  *                     in \f$[km]\f$.
  * \param f107 10.7 cm wavelength Solar Flux measurement at the earth's orbit
  *             Acceptable Lookup Table Values: 65, 75, 100, 125, 150, 175, 
  *                                             200, 225, 250, 275
  */
HarrisPriester::HarrisPriester(const std::string &hp_coeff_filepath, 
                               const std::string &model_name, 
                               const int hp_parameter, 
                               const double min_altitude, 
                               const double max_altitude, 
                               const int f107) : 
                               AtmosphericModel       (model_name), 
                               hp_parameter_          (hp_parameter), 
                               min_altitude_          (min_altitude), 
                               max_altitude_          (max_altitude), 
                               f107_index_            (f107),
                               harris_priester_coeffs_(Eigen::MatrixXd::Zero(0, 0)),
                               h_coeffs_              (Eigen::VectorXd::Zero(0)),
                               c_min_coeffs_          (Eigen::VectorXd::Zero(0)),
                               c_max_coeffs_          (Eigen::VectorXd::Zero(0))
{
  harris_priester_coeffs_ = getCoefficientsFromCSVFile(hp_coeff_filepath);
  InitializeHarrisPriesterCoefficients(f107_index_);
}

//------------------------------------------------------------------------------
//  HarrisPriester(const HarrisPriester &model) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param model The object being copied.
  */
HarrisPriester::HarrisPriester(const HarrisPriester &model) :
                               AtmosphericModel                 (model),
                               hp_parameter_          (model.hp_parameter_), 
                               min_altitude_          (model.min_altitude_), 
                               max_altitude_          (model.max_altitude_), 
                               f107_index_            (model.f107_index_),
                               harris_priester_coeffs_(model.harris_priester_coeffs_),
                               h_coeffs_              (model.h_coeffs_),
                               c_min_coeffs_          (model.c_min_coeffs_),
                               c_max_coeffs_          (model.c_max_coeffs_)
{

}

//------------------------------------------------------------------------------
//  ~HarrisPriester() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
HarrisPriester::~HarrisPriester()
{

}

//------------------------------------------------------------------------------
//  HarrisPriester& operator = (const HarrisPriester &model) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param model The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
HarrisPriester& HarrisPriester::operator=(const HarrisPriester &model)
{
  if(&model == this)
  {
    return *this;
  }
  AtmosphericModel::operator = (model),

  hp_parameter_           = model.hp_parameter_;
  min_altitude_           = model.min_altitude_; 
  max_altitude_           = model.max_altitude_; 
  f107_index_             = model.f107_index_;
  harris_priester_coeffs_ = model.harris_priester_coeffs_;
  h_coeffs_               = model.h_coeffs_;
  c_min_coeffs_           = model.c_min_coeffs_;
  c_max_coeffs_           = model.c_max_coeffs_;
  return *this;
}

//------------------------------------------------------------------------------
//  void setF107(const int f107) 
//------------------------------------------------------------------------------
/** Harris-Priester Atmospheric Model uses look-up table coefficients based 
  * on the solar radio flux of the 10.7 cm wavelength. Use this method to set 
  * a new value for the F10.7 solar flux, and the associated lookup table 
  * coefficients.
  * 
  * \param f107 10.7 cm wavelength Solar Flux measurement at the earth's orbit
  *             Acceptable Lookup Table Values: 65, 75, 100, 125, 150, 175, 
  *                                             200, 225, 250, 275
  */
void HarrisPriester::setF107(const int f107)
{
  f107_index_ = f107;
  InitializeHarrisPriesterCoefficients(f107_index_);
}

//------------------------------------------------------------------------------
//  void setHPParameter(const int hp_parameter) 
//------------------------------------------------------------------------------
/** Harris-Priester Atmospheric Model uses a Harris Priester parameter in the 
  * atmospheric density calculation to account for the atmospheric difference
  * at higher latitudes. The parameter can be calculated using this equation:
  * 
  * \f$2 + \frac{i_{inclination}}{22.5^{\circ}}\f$
  * 
  * \param hp_parameter Harris Priester parameter.
  */
void HarrisPriester::setHPParameter(const int hp_parameter)
{
  hp_parameter_ = hp_parameter;
}

//------------------------------------------------------------------------------
//  double CalculateAtmosphericDensity(const Eigen::Vector3d  &r_sun,
//                                     const Eigen::Vector3d  &r_sat,
//                                     const EarthOrientation &earth_orientation) const
//------------------------------------------------------------------------------
/** Inherited method from the abstract class AtmosphericModel that is overriden 
  * to calculate the Atmospheric Density at the location of the spacecraft in 
  * ICRF using Harris Priester model.
  * 
  * 1) If the calculated altitude of the spacecraft wrt to the earth surface 
  *    (in km) is within the valid altitude range for the Harris-Priester 
  *    Atmospheric Model. This method will return a Harris-Priester calculated 
  *    density value in SI units. 
  * 2) Otherwise, if the altitude exceeds the range of the model, it will 
  *    return 0. Indicating the spacecraft is no longer measurably affected by 
  *    the drag from earths atmosphere.
  * 
  * \param r_sun Eigen::Vector3d Position of the sun in ICRF with Earth origin 
  *              \f$[m]\f$.
  * \param r_sat Eigen::Vector3d Position of satellite in ICRF with Earth origin
  *              \f$[m]\f$.
  * \param earth_orientation EarthOrientation object defining the conversion 
  *                          between reference frames ICRF, ITRF, LOD and 
  *                          Lat/Lon.
  * 
  * \return double Atmospheric density at given satellite position \f$[kg/m^3]\f$.
  */
double HarrisPriester::CalculateAtmosphericDensity(const Eigen::Vector3d  &r_sun,
                                                   const Eigen::Vector3d  &r_sat,
                                                   const EarthOrientation &earth_orientation) const
{
  Eigen::Vector3d r_sat_tod = earth_orientation.ICRS2TOD(r_sat);

  double altitude = CalculateTrueAltitude(r_sat_tod) / 1000;
  double density = 0;

  if (altitude < min_altitude_)
  {
    std::cout << "Error: Altitude below Model Limit | Altitude: " << altitude << "\n";
  }
  else if (altitude > max_altitude_)
  {
    density = 0;
  }
  else
  {
    density = CalculateHPModelDensity(r_sun, r_sat_tod, altitude);
  }
  return density;
}

//------------------------------------------------------------------------------
//  void InitializeHarrisPriesterCoefficients(const int f107_index)
//------------------------------------------------------------------------------
/** Initializes the Harris Priester Coefficients h_coeffs_, c_min_coeffs_, 
  * c_max_coeffs_ from the harris_priester_coeffs_ lookup table based on the 
  * provided f107_index value. The value is calculated by accessing the 
  * different rows of Harris-Priester Coefficients lookup table based 
  * on the f107_index value and  assigns h_coeffs_, c_min_coeffs_, and 
  * c_max_coeffs_ vectors. Coefficients are formatted in the lookup table as 
  * [h_coeff, c_min_coeff, c_max_coeff] repeated.
  * 
  * \param f107 10.7 cm wavelength Solar Flux measurement at the earth's orbit
  *             Acceptable Lookup Table Values: 65, 75, 100, 125, 150, 175, 
  *                                             200, 225, 250, 275
  */
void HarrisPriester::InitializeHarrisPriesterCoefficients(int f107_index)
{
  Eigen::MatrixXd temp;
  int num_cols = harris_priester_coeffs_.cols();
  switch (f107_index)
  {
  case 65:
    temp = harris_priester_coeffs_(0, Eigen::seq(0, num_cols - 1, 3));
    h_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(0, Eigen::seq(1, num_cols - 1, 3));
    c_min_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(0, Eigen::seq(2, num_cols - 1, 3));
    c_max_coeffs_ = temp.transpose();
    break;
  case 75:
    temp = harris_priester_coeffs_(1, Eigen::seq(0, num_cols - 1, 3));
    h_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(1, Eigen::seq(1, num_cols - 1, 3));
    c_min_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(1, Eigen::seq(2, num_cols - 1, 3));
    c_max_coeffs_ = temp.transpose();
    break;
  case 100:
    temp = harris_priester_coeffs_(2, Eigen::seq(0, num_cols - 1, 3));
    h_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(2, Eigen::seq(1, num_cols - 1, 3));
    c_min_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(2, Eigen::seq(2, num_cols - 1, 3));
    c_max_coeffs_ = temp.transpose();
    break;
  case 125:
    temp = harris_priester_coeffs_(3, Eigen::seq(0, num_cols - 1, 3));
    h_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(3, Eigen::seq(1, num_cols - 1, 3));
    c_min_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(3, Eigen::seq(2, num_cols - 1, 3));
    c_max_coeffs_ = temp.transpose();
    break;
  case 150:
    temp = harris_priester_coeffs_(4, Eigen::seq(0, num_cols - 1, 3));
    h_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(4, Eigen::seq(1, num_cols - 1, 3));
    c_min_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(4, Eigen::seq(2, num_cols - 1, 3));
    c_max_coeffs_ = temp.transpose();
    break;
  case 175:
    temp = harris_priester_coeffs_(5, Eigen::seq(0, num_cols - 1, 3));
    h_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(5, Eigen::seq(1, num_cols - 1, 3));
    c_min_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(5, Eigen::seq(2, num_cols - 1, 3));
    c_max_coeffs_ = temp.transpose();
    break;
  case 200:
    temp = harris_priester_coeffs_(6, Eigen::seq(0, num_cols - 1, 3));
    h_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(6, Eigen::seq(1, num_cols - 1, 3));
    c_min_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(6, Eigen::seq(2, num_cols - 1, 3));
    c_max_coeffs_ = temp.transpose();
    break;
  case 225:
    temp = harris_priester_coeffs_(7, Eigen::seq(0, num_cols - 1, 3));
    h_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(7, Eigen::seq(1, num_cols - 1, 3));
    c_min_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(7, Eigen::seq(2, num_cols - 1, 3));
    c_max_coeffs_ = temp.transpose();
    break;
  case 250:
    temp = harris_priester_coeffs_(8, Eigen::seq(0, num_cols - 1, 3));
    h_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(8, Eigen::seq(1, num_cols - 1, 3));
    c_min_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(8, Eigen::seq(2, num_cols - 1, 3));
    c_max_coeffs_ = temp.transpose();
    break;
  case 275:
    temp = harris_priester_coeffs_(9, Eigen::seq(0, num_cols - 1, 3));
    h_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(9, Eigen::seq(1, num_cols - 1, 3));
    c_min_coeffs_ = temp.transpose();
    temp = harris_priester_coeffs_(9, Eigen::seq(2, num_cols - 1, 3));
    c_max_coeffs_ = temp.transpose();
    break;
  }
}

//------------------------------------------------------------------------------
//  double CalculateHPModelDensity(const Eigen::Vector3d &r_sun,
//                                 const Eigen::Vector3d &r_sat, 
//                                 const double altitude) const
//------------------------------------------------------------------------------
/** Calculates the Atmospheric Density using the Harris Priester Atmospheric 
  * Model using Harris-Priester coefficients.
  * 1)  Finds the bounding altitude heights of the Harris_Priester coefficient 
  *     lookup table for a provided spacecraft altitude using <algorithm> 
  *     lower_bound.
  * 2)  Finds the associated bounding scale heights (h_min and h_max) using 
  *     exponential interpolation.
  * 3)  Finds the density at the bounding scale heights using exponential 
  *     interpolation.
  * 4)  The Harris-Priester model accounts for the diurnal density variation. 
  *     This requires calculating the unit vector of the diurnal apex 
  *     (r_diurnal_bulge) wrt to the ICRF Reference Frame.
  * 5)  Using the angle between the diurnal bulge and the spacecraft position 
  *     vector in ICRF (psi). Calculate the atmospheric density using cosine 
  *     variation method.
  * 6)  Convert calculated density from [kg/km^3] to [kg/m^3].
  * 
  * \param r_sun Eigen::Vector3d Position of the sun in ICRF with Earth origin 
  *              \f$[m]\f$.
  * \param r_sat Eigen::Vector3d Position of satellite in TOD reference frame
  *              \f$[m]\f$.
  * \param altitude Altitude of object above above earth geodetic in \f$[km]\f$.
  * 
  * \return double Atmospheric density in \f$[kg/m^3]\f$
  */
double HarrisPriester::CalculateHPModelDensity(const Eigen::Vector3d &r_sun,
                                               const Eigen::Vector3d &r_sat, 
                                               const double altitude) const
{
  int index = std::lower_bound(h_coeffs_.begin(), h_coeffs_.end(), altitude) - h_coeffs_.begin() - 1;

  double h_min = (h_coeffs_(index) - h_coeffs_(index + 1)) / 
                  log(c_min_coeffs_(index + 1) / c_min_coeffs_(index));
  double h_max = (h_coeffs_(index) - h_coeffs_(index + 1)) / 
                  log(c_max_coeffs_(index + 1) / c_max_coeffs_(index));

  double d_min = c_min_coeffs_(index) * exp((h_coeffs_(index) - altitude) / h_min);
  double d_max = c_max_coeffs_(index) * exp((h_coeffs_(index) - altitude) / h_max);

  Eigen::Vector3d r_sun_spherical = getSphericalCoordinates(r_sun);
  Eigen::Vector3d r_diurnal_bulge = {cos(r_sun_spherical(1)) * cos(r_sun_spherical(2) + kRightAscensionLag),
                                     cos(r_sun_spherical(1)) * sin(r_sun_spherical(2) + kRightAscensionLag),
                                     sin(r_sun_spherical(1))};
  double c_psi2 = (0.5 + 0.5 * r_sat.dot(r_diurnal_bulge) / r_sat.norm());

  double density = d_min + (d_max - d_min) * (pow(c_psi2, hp_parameter_ / 2));
  return density * 1e-9; 
}

//------------------------------------------------------------------------------
//  double CalculateTrueAltitude(const Eigen::Vector3d &ecef_pos) const
//------------------------------------------------------------------------------
/** Calculates the altitude of an ECEF position vector wrt to a Geodetic Datum
  * Geodetic Datum is set in constants.h by value of kEarthRadius and 
  * kEarthFlattening. The true altitude is calculated using Bowring's method.
  * Using the convergence criteria of epsilon value (smallest value 
  * representable by floating point) * Earth Radius.
  * 
  * 
  * \param ecef_pos Eigen::Vector3d Position vector in ECEF Reference Frame 
  *                 \f$[x, y, z]\f$ in \f$[m]\f$.
  * 
  * \return double Altitude of object above above earth geodetic in \f$[m]\f$.
  */
double HarrisPriester::CalculateTrueAltitude(const Eigen::Vector3d &ecef_pos) const
{
  double rho_sq = pow(ecef_pos(0), 2) + pow(ecef_pos(1), 2);
  double e2 = 2 * kEarthFlattening + pow(kEarthFlattening, 2);
  double eps_requirement = std::numeric_limits<double>::epsilon() * kEarthRadius;

  //  Iteration
  double sinphi = 0;
  double zdz = 0;
  double nh = 0;
  double n = 0;
  double dz = e2 * ecef_pos(2);
  double dz_new = 0;

  while (abs(dz - dz_new) > eps_requirement)
  {
    zdz = ecef_pos(2) + dz;
    nh = sqrt(rho_sq + zdz * zdz);
    sinphi = zdz / nh;
    n = kEarthRadius / sqrt(1.0 - e2 * sinphi * sinphi);
    dz = dz_new;
    dz_new = n * e2 * sinphi;
  }
  double altitude = nh - n;
  return altitude;
}

//------------------------------------------------------------------------------
//  Eigen::ArrayXXd getCoefficientsFromCSVFile(const std::string &filepath) const
//------------------------------------------------------------------------------
/** Reads values from a csv file into a Eigen::ArrayXXd with same row and 
  * columns.
  *
  * \param filepath Filepath to csv file.
  * 
  * \return Eigen::ArrayXXd 2D Array of all values read from the csv file in the
  *         same row and columns as the given file.
  */
Eigen::MatrixXd HarrisPriester::getCoefficientsFromCSVFile(const std::string &filepath) const
{
  std::ifstream data(filepath);
  std::string line;
  std::vector<double> values;
  int rows = 0;

  while (std::getline(data, line))
  {
    Eigen::MatrixXd output;
    std::stringstream lineStream(line);
    std::string cell;
    while (std::getline(lineStream, cell, ','))
    {
      values.push_back(std::stod(cell));
    }
    ++rows;
  }
  return Eigen::Map<Eigen::MatrixXd>(values.data(), values.size() / rows, rows).transpose();
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getSphericalCoordinates(const Eigen::Vector3d &cart_vec) const
//------------------------------------------------------------------------------
/** Converts a cartesian position vector to Spherical Coordinate System.
  *  
  * \param cart_vec Input cartesian position vector to be converted to spherical
  *                 coordinates.
  * \return Eigen::Vector3d of position vector in spherical coordinates 
  *         \f$[radial distance (rho), polar angle (theta), azimuth (phi)]\f$.
  */
Eigen::Vector3d HarrisPriester::getSphericalCoordinates(const Eigen::Vector3d &cart_vec) const
{
  Eigen::Vector3d spherical_position;

  // Rho
  spherical_position(0) = cart_vec.norm();

  // Theta
  double xy_projection = sqrt(pow(cart_vec(0), 2) + pow(cart_vec(1), 2));
  if ((cart_vec(2) == 0) && (xy_projection == 0))
  {
    spherical_position(1) = 0;
  }
  else
  {
    spherical_position(1) = atan2(cart_vec(2), xy_projection);
  }

  // Azimuth
  if ((cart_vec(0) == 0) && (cart_vec(1) == 0))
  {
    spherical_position(2) = 0;
  }
  else
  {
    spherical_position(2) = atan2(cart_vec(1), cart_vec(0));
  }
  if (spherical_position(2) < 0)
  {
    spherical_position(2) = spherical_position(2) + 2 * kPI;
  }
  return spherical_position;
}

} // end namespace polaris