//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  EarthOrientation Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------


#include "earth_orientation.h"

#include <fstream>
#include <iostream>

#include "constants.h"

namespace polaris {

//------------------------------------------------------------------------------
//  EarthOrientation()
//------------------------------------------------------------------------------
/** Default constructor.
  * 
  */
EarthOrientation::EarthOrientation()
{

}

//------------------------------------------------------------------------------
//  EarthOrientation(const std::vector<std::string> &filepaths,
//                   const std::string              &model_name)
//------------------------------------------------------------------------------
/** Default constructor requiring configuration of earth orientation model.
  *
  * \param filepaths Vector of the Earth orientation parameters filepaths in 
  *                  order:
  *                  - IERS EOP Coefficients
  *                  - Luni-Solar Nutation Coefficients
  *                  - Planetary Nutation Coefficients
  *                  - CIO S0 Coefficients
  *                  - CIO S1 Coefficients
  *                  - CIO S2 Coefficients
  *                  - CIO S3 Coefficients
  *                  - CIO S4 Coefficients
  * \param model_name Name of the earth orientation model used. 
  */
EarthOrientation::EarthOrientation(const std::vector<std::string> &filepaths, 
                                   const std::string              &model_name) :
                                   model_name_                         (model_name),
                                   eop_data_observed_                  (Eigen::ArrayXXd::Zero(1,1)),
                                   eop_data_predicted_                 (Eigen::ArrayXXd::Zero(1,1)),
                                   eop_data_period_                    (""),
                                   eop_points_observed_                (0),
                                   eop_points_predicted_               (0),
                                   eopp_ta_                            (0),
                                   eopp_a_                             (0),
                                   eopp_b_                             (0),
                                   eopp_c_                             (Eigen::Array2d::Zero()),
                                   eopp_d_                             (Eigen::Array2d::Zero()),
                                   eopp_p_                             (Eigen::Array2d::Zero()),
                                   eopp_e_                             (0),
                                   eopp_f_                             (0),
                                   eopp_g_                             (Eigen::Array2d::Zero()),
                                   eopp_h_                             (Eigen::Array2d::Zero()),
                                   eopp_q_                             (Eigen::Array2d::Zero()),
                                   eopp_tb_                            (0),
                                   eopp_i_                             (0),
                                   eopp_j_                             (0),
                                   eopp_k_                             (Eigen::Array4d::Zero()),
                                   eopp_l_                             (Eigen::Array4d::Zero()),
                                   eopp_r_                             (Eigen::Array4d::Zero()),
                                   eopp_tai_utc_offset_                (0),
                                   x_pole_                             (0),
                                   y_pole_                             (0),
                                   ut1_utc_offset_                     (0),
                                   lod_                                (0),
                                   tai_utc_offset_                     (0),
                                   luni_solar_nutation_series_         (Eigen::ArrayXXd::Zero(1,1)),
                                   planetary_nutation_series_          (Eigen::ArrayXXd::Zero(1,1)),
                                   cio_s0_terms_                       (Eigen::ArrayXXd::Zero(1,1)),
                                   cio_s1_terms_                       (Eigen::ArrayXXd::Zero(1,1)),
                                   cio_s2_terms_                       (Eigen::ArrayXXd::Zero(1,1)),
                                   cio_s3_terms_                       (Eigen::ArrayXXd::Zero(1,1)),
                                   cio_s4_terms_                       (Eigen::ArrayXXd::Zero(1,1)),
                                   polar_motion_rotation_matrix_       (Eigen::Matrix3d::Zero()),
                                   precession_nutation_rotation_matrix_(Eigen::Matrix3d::Zero()),
                                   sidereal_rotation_matrix_           (Eigen::Matrix3d::Zero()),
                                   icrs2itrs_                          (Eigen::Matrix3d::Zero())
{
  ReadEarthOrientationParametersFileAuxiliary(filepaths[0]);
  ReadEarthOrientationParametersFile(filepaths[0]);
  luni_solar_nutation_series_ = getCoefficientsFromCSVFile(filepaths[1]);
  planetary_nutation_series_ = getCoefficientsFromCSVFile(filepaths[2]);
  cio_s0_terms_ = getCoefficientsFromCSVFile(filepaths[3]);
  cio_s1_terms_ = getCoefficientsFromCSVFile(filepaths[4]);
  cio_s2_terms_ = getCoefficientsFromCSVFile(filepaths[5]);
  cio_s3_terms_ = getCoefficientsFromCSVFile(filepaths[6]);
  cio_s4_terms_ = getCoefficientsFromCSVFile(filepaths[7]);
}

//------------------------------------------------------------------------------
//  EarthOrientation(const EarthOrientation &model) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param model The object being copied.
  */
EarthOrientation::EarthOrientation(const EarthOrientation &model) :
                                   model_name_                         (model.model_name_),
                                   eop_data_observed_                  (model.eop_data_observed_),
                                   eop_data_predicted_                 (model.eop_data_predicted_),
                                   eop_data_period_                    (model.eop_data_period_),
                                   eop_points_observed_                (model.eop_points_observed_),
                                   eop_points_predicted_               (model.eop_points_predicted_),
                                   eopp_ta_                            (model.eopp_ta_),
                                   eopp_a_                             (model.eopp_a_),
                                   eopp_b_                             (model.eopp_b_),
                                   eopp_c_                             (model.eopp_c_),
                                   eopp_d_                             (model.eopp_d_),
                                   eopp_p_                             (model.eopp_p_),
                                   eopp_e_                             (model.eopp_e_),
                                   eopp_f_                             (model.eopp_f_),
                                   eopp_g_                             (model.eopp_g_),
                                   eopp_h_                             (model.eopp_h_),
                                   eopp_q_                             (model.eopp_q_),
                                   eopp_tb_                            (model.eopp_tb_),
                                   eopp_i_                             (model.eopp_i_),
                                   eopp_j_                             (model.eopp_j_),
                                   eopp_k_                             (model.eopp_k_),
                                   eopp_l_                             (model.eopp_l_),
                                   eopp_r_                             (model.eopp_r_),
                                   eopp_tai_utc_offset_                (model.eopp_tai_utc_offset_),
                                   x_pole_                             (model.x_pole_),
                                   y_pole_                             (model.y_pole_),
                                   ut1_utc_offset_                     (model.ut1_utc_offset_),
                                   lod_                                (model.lod_),
                                   tai_utc_offset_                     (model.tai_utc_offset_),
                                   luni_solar_nutation_series_         (model.luni_solar_nutation_series_),
                                   planetary_nutation_series_          (model.planetary_nutation_series_),
                                   cio_s0_terms_                       (model.cio_s0_terms_),
                                   cio_s1_terms_                       (model.cio_s1_terms_),
                                   cio_s2_terms_                       (model.cio_s2_terms_),
                                   cio_s3_terms_                       (model.cio_s3_terms_),
                                   cio_s4_terms_                       (model.cio_s4_terms_),
                                   polar_motion_rotation_matrix_       (model.polar_motion_rotation_matrix_),
                                   precession_nutation_rotation_matrix_(model.precession_nutation_rotation_matrix_),
                                   sidereal_rotation_matrix_           (model.sidereal_rotation_matrix_),
                                   icrs2itrs_                          (model.icrs2itrs_)
{

}

//------------------------------------------------------------------------------
//  ~EarthOrientation() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
EarthOrientation::~EarthOrientation()
{

}

//------------------------------------------------------------------------------
//  EarthOrientation& operator = (const EarthOrientation &model) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param model The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
EarthOrientation& EarthOrientation::operator=(const EarthOrientation &model)
{
  if(&model == this)
  {
    return *this;
  }
  model_name_                          = model.model_name_;
  eop_data_observed_                   = model.eop_data_observed_;
  eop_data_predicted_                  = model.eop_data_predicted_;
  eop_data_period_                     = model.eop_data_period_;
  eop_points_observed_                 = model.eop_points_observed_;
  eop_points_predicted_                = model.eop_points_predicted_;
  eopp_ta_                             = model.eopp_ta_;
  eopp_a_                              = model.eopp_a_;
  eopp_b_                              = model.eopp_b_;
  eopp_c_                              = model.eopp_c_;
  eopp_d_                              = model.eopp_d_;
  eopp_p_                              = model.eopp_p_;
  eopp_e_                              = model.eopp_e_;
  eopp_f_                              = model.eopp_f_;
  eopp_g_                              = model.eopp_g_;
  eopp_h_                              = model.eopp_h_;
  eopp_q_                              = model.eopp_q_;
  eopp_tb_                             = model.eopp_tb_;
  eopp_i_                              = model.eopp_i_;
  eopp_j_                              = model.eopp_j_;
  eopp_k_                              = model.eopp_k_;
  eopp_l_                              = model.eopp_l_;
  eopp_r_                              = model.eopp_r_;
  eopp_tai_utc_offset_                 = model.eopp_tai_utc_offset_;
  x_pole_                              = model.x_pole_;
  y_pole_                              = model.y_pole_;
  ut1_utc_offset_                      = model.ut1_utc_offset_;
  lod_                                 = model.lod_;
  tai_utc_offset_                      = model.tai_utc_offset_;
  luni_solar_nutation_series_          = model.luni_solar_nutation_series_;
  planetary_nutation_series_           = model.planetary_nutation_series_;
  cio_s0_terms_                        = model.cio_s0_terms_;
  cio_s1_terms_                        = model.cio_s1_terms_;
  cio_s2_terms_                        = model.cio_s2_terms_;
  cio_s3_terms_                        = model.cio_s3_terms_;
  cio_s4_terms_                        = model.cio_s4_terms_;
  polar_motion_rotation_matrix_        = model.polar_motion_rotation_matrix_;
  precession_nutation_rotation_matrix_ = model.precession_nutation_rotation_matrix_;
  sidereal_rotation_matrix_            = model.sidereal_rotation_matrix_;
  icrs2itrs_                           = model.icrs2itrs_;
  return *this;
}

//------------------------------------------------------------------------------
//  std::string getModelName()
//------------------------------------------------------------------------------
/** Get the name of the model used to calculate the Earth orientation. 
  *
  * \return std::string of the name of the model used.
  */
std::string EarthOrientation::getModelName() const
{
  return model_name_;
}

//------------------------------------------------------------------------------
//  double getXPole() const 
//------------------------------------------------------------------------------
/** Get x coordinate of the Celestial Ephemeris Pole.
  * 
  * \return double x cordinate of the Celestial Ephemeris Pole \f$[rad]\f$.
  */ 
double EarthOrientation::getXPole() const
{
  return x_pole_;
}

//------------------------------------------------------------------------------
//  double getYPole() const 
//------------------------------------------------------------------------------
/** Get y coordinate of the Celestial Ephemeris Pole.
  * 
  * \return double y cordinate of the Celestial Ephemeris Pole \f$[rad]\f$.
  */ 
double EarthOrientation::getYPole() const
{
  return y_pole_;
}

//------------------------------------------------------------------------------
//  double getTAIUTCOffset() const 
//------------------------------------------------------------------------------
/** Get time offset of TAI from UTC time.
  * 
  * \return double TAI - UTC time\f$[s]\f$.
  */ 
double EarthOrientation::getTAIUTCOffset() const
{
  return tai_utc_offset_;
}

//------------------------------------------------------------------------------
//  double getUT1UTCOffset() const 
//------------------------------------------------------------------------------
/** Get time offset of UT1 from UTC time.
  * 
  * \return double UT1 - UTC time\f$[s]\f$.
  */ 
double EarthOrientation::getUT1UTCOffset() const
{
  return ut1_utc_offset_;
}

//------------------------------------------------------------------------------
//  double getLOD() const 
//------------------------------------------------------------------------------
/** Get earth length of day offset from 86400 seconds.
  * 
  * \return double Earth length of day offset from 86400 seconds \f$[s]\f$.
  */ 
double EarthOrientation::getLOD() const
{
  return lod_;
}

//------------------------------------------------------------------------------
//  std::string getEOPDataPeriod() const 
//------------------------------------------------------------------------------
/** Get name of the earth orientation data source.
  * 
  * \return std::string Name of the earth orientation data source 
  *         (observed, predicted, NGA predicted).
  */ 
std::string EarthOrientation::getEOPDataPeriod() const
{
  return eop_data_period_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d ICRS2ITRS(const Eigen::Vector3d &icrs_vector) const 
//------------------------------------------------------------------------------
/** Converts a ICRS vector to ITRS.
  * 
  * \return Eigen::Vector3d vector in ITRS.
  */ 
Eigen::Vector3d EarthOrientation::ICRS2ITRS(const Eigen::Vector3d &icrs_vector) const
{
  Eigen::Vector3d itrs_vector;
  itrs_vector = icrs2itrs_ * icrs_vector;
  return itrs_vector;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d ICRS2TOD(const Eigen::Vector3d &icrs_vector) const 
//------------------------------------------------------------------------------
/** Converts a ICRS vector to TOD reference frame.
  * 
  * \return Eigen::Vector3d vector in TOD reference frame.
  */ 
Eigen::Vector3d EarthOrientation::ICRS2TOD(const Eigen::Vector3d &icrs_vector) const
{
  Eigen::Vector3d tod_vector;
  tod_vector = precession_nutation_rotation_matrix_ * icrs_vector;
  return tod_vector;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d ITRS2ICRS(const Eigen::Vector3d &itrs_vector) const 
//------------------------------------------------------------------------------
/** Converts a ITRS vector to ICRS.
  * 
  * \return Eigen::Vector3d vector in ICRS.
  */ 
Eigen::Vector3d EarthOrientation::ITRS2ICRS(const Eigen::Vector3d &itrs_vector) const
{
  Eigen::Vector3d icrs_vector;
  Eigen::Matrix3d inv_icrs2itrs = icrs2itrs_.transpose();
  icrs_vector = inv_icrs2itrs * itrs_vector;
  return icrs_vector;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d TOD2ICRS(const Eigen::Vector3d &tod_vector) const 
//------------------------------------------------------------------------------
/** Converts a TOD reference frame vector to ICRS.
  * 
  * \return Eigen::Vector3d vector in ICRS.
  */ 
Eigen::Vector3d EarthOrientation::TOD2ICRS(const Eigen::Vector3d &tod_vector) const
{
  Eigen::Vector3d icrs_vector;
  Eigen::Matrix3d inv_rotation_matrix = precession_nutation_rotation_matrix_.transpose();
  icrs_vector = inv_rotation_matrix * tod_vector;
  return icrs_vector;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d Cart2Geodetic(const Eigen::Vector3d &ecef_vector) const 
//------------------------------------------------------------------------------
/** Converts a cartesian vector to a lat, lon, altitude position vector for a 
  * Geodetc Datum (defined in constants.h by value of kEarthRadius and 
  * kEarthFlattening) for a given ECEF position vector using Bowring's method. 
  * Convergence criteria is epsilon value (smallest value representable by 
  * floating point) * Earth Radius.
  * 
  * \param ecef_vector cartesian vector in a ECEF reference frame \f$[m]\f$.
  * 
  * \return Eigen::Vector3d Lat, Lon, Altitude vector in given ECEF reference 
  *         frame \f$[rad]\f$ and \f$[m]\f$.
  */ 
Eigen::Vector3d EarthOrientation::Cart2Geodetic(const Eigen::Vector3d &ecef_vector) const
{
  double rho_sq = pow(ecef_vector(0),2) + pow(ecef_vector(1),2);
  double e2 = 2 * kEarthFlattening + pow(kEarthFlattening, 2);
  double eps_requirement = std::numeric_limits<double>::epsilon() * kEarthRadius;

  //  Iteration
  double sinphi = 0;
  double zdz = 0;
  double nh = 0;
  double n = 0;
  double dz = e2 * ecef_vector(2);
  double dz_new = 0;

  while(abs(dz - dz_new) > eps_requirement)
  {
    zdz = ecef_vector(2) + dz;
    nh = sqrt(rho_sq + zdz*zdz);
    sinphi = zdz / nh;
    n = kEarthRadius / sqrt(1.0 - e2 * sinphi * sinphi);
    dz = dz_new;
    dz_new = n * e2 * sinphi;
  }

  double altitude = nh - n;
  double lon = atan2(ecef_vector(1), ecef_vector(0));
  double lat = atan2(zdz, sqrt(rho_sq));
  Eigen::Vector3d lat_lon_vector = {lat, lon, altitude};
  return lat_lon_vector;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d Geodetic2Cart(const Eigen::Vector3d &geodetic_vector) const 
//------------------------------------------------------------------------------
/** Converts a geodetic lat, lon, altitude position vector for a 
  * Geodetc Datum to a given ECEF position vector.
  * 
  * \param geodetic_vector Eigen::Vector3d Lat, Lon, Altitude vector in given 
  *                        ECEF reference frame \f$[rad]\f$ and \f$[m]\f$.
  * 
  * \return Eigen::Vector3d Cartesian vector in given ECEF reference frame 
  *         \f$[m]\f$.
  */ 
Eigen::Vector3d EarthOrientation::Geodetic2Cart(const Eigen::Vector3d &geodetic_vector) const
{
  double e_sq = 2 * kEarthFlattening - pow(kEarthFlattening, 2);
  double n_lat_coeff = kEarthRadius / sqrt(1 - e_sq * pow(sin(geodetic_vector(0)), 2));
  double x = (n_lat_coeff + geodetic_vector(2)) * cos(geodetic_vector(0)) * cos(geodetic_vector(1));
  double y = (n_lat_coeff + geodetic_vector(2)) * cos(geodetic_vector(0)) * sin(geodetic_vector(1));
  double z = ((1 - e_sq) * n_lat_coeff + geodetic_vector(2)) * sin(geodetic_vector(0));
  Eigen::Vector3d cart_vector = {x, y, z};
  return cart_vector;  
}

//------------------------------------------------------------------------------
//  void InitializeEarthOrientationParameters(const double mjd_utc)
//------------------------------------------------------------------------------
/** Initializes all the coefficients and earth orientation parameters
  * for a given modified julian date based on 3 periods using Earth Orientation 
  * Parameters lookup table and linear interpolation.
  * 
  * Observed Earth Orientation Parameters (Past) using EOP data
  * Predicted Earth Orientation Parameters (Short-term future) using EOP data
  * Future Earth Orientation Parameters (Long-term future) using NGA coefficients
  * 
  * These parameters and coefficients are required for computation of the 
  * reference frame rotation matrices.
  * 
  * Method initializes 
  *   - x_pole_
  *   - y_pole_
  *   - ut1_utc_offset_
  *   - lod_
  *   - dx_pole_
  *   - dy_pole_
  *   - tai_utc_offset_
  * 
  * \param mjd_utc UTC modified julian date.
  */
void EarthOrientation::InitializeEarthOrientationParameters(const double mjd_utc)
{ 
  double time_in_day = mjd_utc - std::floor(mjd_utc);

  if(mjd_utc >= eop_data_predicted_(eop_points_predicted_ - 1, 0))
  {
    eop_data_period_ = "NGA";

    Eigen::Array2d x_pole_series = eopp_c_ * sin(2*kPI*(mjd_utc - eopp_ta_)/eopp_p_) 
                                 + eopp_d_ * cos(2*kPI*(mjd_utc - eopp_ta_)/eopp_p_);
    x_pole_ = eopp_a_ + eopp_b_*(mjd_utc - eopp_ta_) + x_pole_series.sum();
  
    Eigen::Array2d y_pole_series = eopp_g_ * sin(2*kPI*(mjd_utc - eopp_ta_)/eopp_q_) 
                                 + eopp_h_ * cos(2*kPI*(mjd_utc - eopp_ta_)/eopp_q_);
    y_pole_ = eopp_e_ + eopp_f_*(mjd_utc - eopp_ta_) + y_pole_series.sum();

    Eigen::Array4d ut1_utc_series = eopp_k_ * sin(2*kPI*(mjd_utc - eopp_tb_)/eopp_r_)
                                  + eopp_l_ * cos(2*kPI*(mjd_utc - eopp_tb_)/eopp_r_);
    ut1_utc_offset_ = eopp_i_ + eopp_j_*(mjd_utc - eopp_tb_) + ut1_utc_series.sum();

    lod_ = eop_data_predicted_(eop_data_predicted_.rows() - 1, 4);
    tai_utc_offset_ = eopp_tai_utc_offset_;
  }
  else if(mjd_utc >= eop_data_predicted_(0,0))
  {
    int prior_row = std::floor(mjd_utc) - eop_data_predicted_(0, 0); 
    int post_row  = prior_row + 1;
    eop_data_period_ = "Predicted";

    x_pole_ =         Lerp(eop_data_predicted_(prior_row, 1), eop_data_predicted_(post_row, 1), time_in_day);
    y_pole_ =         Lerp(eop_data_predicted_(prior_row, 2), eop_data_predicted_(post_row, 2), time_in_day);
    ut1_utc_offset_ = Lerp(eop_data_predicted_(prior_row, 3), eop_data_predicted_(post_row, 3), time_in_day);
    lod_ =            Lerp(eop_data_predicted_(prior_row, 4), eop_data_predicted_(post_row, 4), time_in_day);

    tai_utc_offset_ = Lerp(eop_data_predicted_(prior_row, 7), eop_data_predicted_(post_row, 7), time_in_day);
  }
  else if(mjd_utc > eop_data_observed_(0, 0))
  {
    int prior_row = std::floor(mjd_utc) - eop_data_observed_(0, 0); 
    int post_row  = prior_row + 1;
    eop_data_period_ = "Observed";

    x_pole_ =         Lerp(eop_data_observed_(prior_row, 1), eop_data_observed_(post_row, 1), time_in_day);
    y_pole_ =         Lerp(eop_data_observed_(prior_row, 2), eop_data_observed_(post_row, 2), time_in_day);
    ut1_utc_offset_ = Lerp(eop_data_observed_(prior_row, 3), eop_data_observed_(post_row, 3), time_in_day);
    lod_ =            Lerp(eop_data_observed_(prior_row, 4), eop_data_observed_(post_row, 4), time_in_day);
    tai_utc_offset_ = Lerp(eop_data_observed_(prior_row, 7), eop_data_observed_(post_row, 7), time_in_day);
  }
  else
  {
    std::cout << "Date provided is earlier than EOP Data File\n";
    eop_data_period_ = "Pre-EOP";
  }
  x_pole_ = x_pole_ / kRadToSec;
  y_pole_ = y_pole_ / kRadToSec;
}

//------------------------------------------------------------------------------
//  void CalculateRotationMatrices(const double julian_century_tt,
//                                 const double mjd_ut1)
//------------------------------------------------------------------------------
/** Calculates the rotation matrix for ICRS to ITRS conversion. The rotation
  * matrix is the multiplication of the:
  * 
  * Polar Motion Rotation Matrix 
  *  (Requires: Terrestrial Intermediate Origin Location)
  * 
  * Bias-Precession Nutation Rotation Matrix 
  *  (Requires: Planetary Longitudes/Nutations, and Luni-Solar Anomalies/Nutations)
  * 
  * Greenwich Apparent Sidereal Rotation Matrix 
  *  (Requires: Celestial Intermediate Origin Location and Greenwich Sidereal Time)
  * 
  * \param julian_century_tt Terrestrial time in julian centuries.
  * \param mjd_ut1 UT1 time in modified julian date.
  */ 
void EarthOrientation::CalculateRotationMatrices(const double julian_century_tt,
                                                 const double mjd_ut1)
{
  double tio_locator_s = CalculateTIOLocatorS(julian_century_tt);
  polar_motion_rotation_matrix_ = CalculatePolarMotionRotationMatrix(tio_locator_s, x_pole_, y_pole_);

  precession_nutation_rotation_matrix_ = CalculatePrecessionNutationRotationMatrix(julian_century_tt);

  double cio_locator_s = CalculateCIOLocatorS(julian_century_tt, precession_nutation_rotation_matrix_);
  double greenwich_sidereal_time = CalculateGreenwichSiderealTime(cio_locator_s, mjd_ut1,    
                                                            precession_nutation_rotation_matrix_);
  sidereal_rotation_matrix_ = CalculateSiderealRotationMatrix(greenwich_sidereal_time);

  icrs2itrs_ = polar_motion_rotation_matrix_
             * sidereal_rotation_matrix_
             * precession_nutation_rotation_matrix_;
}

//------------------------------------------------------------------------------
//  void ReadEarthOrientationParametersFileAuxiliary(const std::string &filepath)
//------------------------------------------------------------------------------
/** Reads the auxiliary earth orientation parameter data and coefficients. 
  *
  * \param filepath std::string Earth orientation parameters file filepath.
  */
void EarthOrientation::ReadEarthOrientationParametersFileAuxiliary(const std::string &filepath)
{
  std::ifstream data(filepath);
  std::string line;
  std::vector<std::string> nga_data;
  bool reading_nga_data_flag = false;

  while(std::getline(data, line))
  {
    if(line.find("BEGIN NGA_COEFFICIENTS") != std::string::npos)
    {
      reading_nga_data_flag = true;
    }
    else if(line.find("END NGA_COEFFICIENTS") != std::string::npos)
    {
      reading_nga_data_flag = false;
      SetNGACoefficients(nga_data);
    }
    else if(reading_nga_data_flag == true)
    {
      nga_data.push_back(line);
    }
    else if(line.find("NUM_OBSERVED_POINTS") != std::string::npos)
    {
      sscanf(line.c_str(), "%*s %i", &eop_points_observed_);
    }
    else if(line.find("NUM_PREDICTED_POINTS") != std::string::npos)
    {
      sscanf(line.c_str(), "%*s %i", &eop_points_predicted_);
    }
  }
}

//------------------------------------------------------------------------------
//  void SetNGACoefficients(const std::vector<std::string> &nga_data)
//------------------------------------------------------------------------------
/** Sets all the NGA Earth orientation predicted parameters coefficients.
  *
  * \param nga_data A vector of the NGA coefficient lines from the earth
  *                 orientation parameters file.
  */
void EarthOrientation::SetNGACoefficients(const std::vector<std::string> &nga_data)
{
  //  https://earth-info.nga.mil/GandG/sathtml/eoppdoc.html
  eopp_ta_   = std::stod(nga_data[0].substr(0,  10));
  eopp_a_    = std::stod(nga_data[0].substr(10, 10));
  eopp_b_    = std::stod(nga_data[0].substr(20, 10));
  eopp_c_[0] = std::stod(nga_data[0].substr(30, 10));
  eopp_c_[1] = std::stod(nga_data[0].substr(40, 10));
  eopp_d_[0] = std::stod(nga_data[0].substr(50, 10));
  eopp_d_[1] = std::stod(nga_data[0].substr(60, 10));
  eopp_p_[0] = std::stod(nga_data[0].substr(70,  6));

  eopp_p_[1] = std::stod(nga_data[1].substr(0,   6));
  eopp_e_    = std::stod(nga_data[1].substr(6,  10));
  eopp_f_    = std::stod(nga_data[1].substr(16, 10));
  eopp_g_[0] = std::stod(nga_data[1].substr(26, 10));
  eopp_g_[1] = std::stod(nga_data[1].substr(36, 10));
  eopp_h_[0] = std::stod(nga_data[1].substr(46, 10));
  eopp_h_[1] = std::stod(nga_data[1].substr(56, 10));
  eopp_q_[0] = std::stod(nga_data[1].substr(66,  6));
  eopp_q_[1] = std::stod(nga_data[1].substr(72,  6));

  eopp_tb_   = std::stod(nga_data[2].substr(0,  10));
  eopp_i_    = std::stod(nga_data[2].substr(10, 10));
  eopp_j_    = std::stod(nga_data[2].substr(20, 10));
  eopp_k_[0] = std::stod(nga_data[2].substr(30, 10));
  eopp_k_[1] = std::stod(nga_data[2].substr(40, 10));
  eopp_k_[2] = std::stod(nga_data[2].substr(50, 10));
  eopp_k_[3] = std::stod(nga_data[2].substr(60, 10));

  eopp_l_[0] = std::stod(nga_data[3].substr(0,  10));
  eopp_l_[1] = std::stod(nga_data[3].substr(10, 10));
  eopp_l_[2] = std::stod(nga_data[3].substr(20, 10));
  eopp_l_[3] = std::stod(nga_data[3].substr(30, 10));
  eopp_r_[0] = std::stod(nga_data[3].substr(40,  9));
  eopp_r_[1] = std::stod(nga_data[3].substr(49,  9));
  eopp_r_[2] = std::stod(nga_data[3].substr(58,  9));
  eopp_r_[3] = std::stod(nga_data[3].substr(67,  9));

  eopp_tai_utc_offset_ = std::stod(nga_data[4].substr(0, 4));
}

//------------------------------------------------------------------------------
//  void ReadEarthOrientationParametersFile(const std::string &filepath)
//------------------------------------------------------------------------------
/** Reads the earth orientation parameters file to extract the required data.
  *
  * \param nga_data A vector of the NGA coefficient lines from the earth
  *                 orientation parameters file.
  */
void EarthOrientation::ReadEarthOrientationParametersFile(const std::string &filepath)
{
  eop_data_observed_  = Eigen::ArrayXXd::Zero(eop_points_observed_,  8);
  eop_data_predicted_ = Eigen::ArrayXXd::Zero(eop_points_predicted_, 8);

  std::vector<float> data_float;
  data_float.reserve(8);

  std::ifstream data(filepath);
  std::string line;
  int observed_row = 0;
  int predicted_row = 0;
  bool begin_observed_flag = false;
  bool begin_predicted_flag = false;

  while (std::getline(data, line))
  {
    if(line.find("BEGIN OBSERVED") != std::string::npos)
    {
      begin_observed_flag = true;
    }
    else if(line.find("END OBSERVED") != std::string::npos)
    {
      begin_observed_flag = false;
    }
    else if(begin_observed_flag == true)
    {
      sscanf(line.c_str(), "%*i %*i %*i %f %f %f %f %f %*f %*f %f %f %f",  &data_float[0], &data_float[1], 
                                                                           &data_float[2], &data_float[3], 
                                                                           &data_float[4], &data_float[5], 
                                                                           &data_float[6], &data_float[7]);
      Eigen::VectorXf temp = Eigen::Map<Eigen::VectorXf>(data_float.data(), 8);
      eop_data_observed_.row(observed_row) = temp.cast<double>();            
      observed_row += 1;
    }
    else if(line.find("BEGIN PREDICTED") != std::string::npos)
    {
      begin_predicted_flag = true;
    }
    else if(line.find("END PREDICTED") != std::string::npos)
    {
      begin_predicted_flag = false;
    }
    else if(begin_predicted_flag == true)
    {
      sscanf(line.c_str(), "%*i %*i %*i %f %f %f %f %f %*f %*f %f %f %f", &data_float[0], &data_float[1], 
                                                                          &data_float[2], &data_float[3], 
                                                                          &data_float[4], &data_float[5], 
                                                                          &data_float[6], &data_float[7]);
      Eigen::VectorXf temp = Eigen::Map<Eigen::VectorXf>(data_float.data(), 8);
      eop_data_predicted_.row(predicted_row) = temp.cast<double>();            
      predicted_row += 1;
    }
  }
}

//------------------------------------------------------------------------------
//  Eigen::ArrayXXd getCoefficientsFromCSVFile(const std::string &filepath) const
//------------------------------------------------------------------------------
/** Reads values from a csv file into a Eigen::ArrayXXd with same row and 
  * columns.
  *
  * \param filepath filepath to csv file.
  * 
  * \return Eigen::ArrayXXd 2D Array of all values read from the csv file in the
  *         same row and columns as the given file.
  */
Eigen::ArrayXXd EarthOrientation::getCoefficientsFromCSVFile(const std::string &filepath) const
{
  std::ifstream data(filepath);
  std::string line;
  std::vector<double> values;
  int rows = 0;

  while(std::getline(data, line))
  {
    Eigen::MatrixXd output;
    std::stringstream lineStream(line);
    std::string cell;
    while(std::getline(lineStream, cell, ','))
    {
      values.push_back(std::stod(cell));
    }
    ++rows;
  }
  return Eigen::Map<Eigen::ArrayXXd>(values.data(), values.size()/rows, rows).transpose();
}

//------------------------------------------------------------------------------
//  double Lerp(const double prior, 
//              const double post, 
//              const double interval) const
//------------------------------------------------------------------------------
/** Calculates the linear interpolated value.
  *
  * \param prior Lower bound value
  * \param post Upper bound value
  * \param interval Interval from 0 to 1 to interpolate by.
  * 
  * \return double linear interpolated value.
  */
double EarthOrientation::Lerp(const double prior, 
                              const double post, 
                              const double interval) const
{
  return prior + (post - prior) * interval;
}

//------------------------------------------------------------------------------
//  Eigen::Matrix3d CalculatePolarMotionRotationMatrix(const double tio_locator_s, 
//                                                     const double x_pole,
//                                                     const double y_pole) const
//------------------------------------------------------------------------------
/** Calculates the polar motion rotation matrix.
  *
  * \param tio_locator_s Location of terrestrial intermediate origin \f$[rad]\f$.
  * \param x_pole x cordinate of the Celestial Ephemeris Pole \f$[rad]\f$.
  * \param y_pole y cordinate of the Celestial Ephemeris Pole \f$[rad]\f$.
  * 
  * \return Eigen::Matrix3d Polar motion rotation matrix.
  */
Eigen::Matrix3d EarthOrientation::CalculatePolarMotionRotationMatrix(const double tio_locator_s, 
                                                                     const double x_pole,
                                                                     const double y_pole) const
{
  Eigen::Matrix3d polar_motion_rotation_matrix;
  polar_motion_rotation_matrix = Eigen::AngleAxisd(-tio_locator_s, Eigen::Vector3d::UnitZ())
                               * Eigen::AngleAxisd(y_pole, Eigen::Vector3d::UnitX())
                               * Eigen::AngleAxisd(x_pole, Eigen::Vector3d::UnitY());
  return polar_motion_rotation_matrix;
}

//------------------------------------------------------------------------------
//  double CalculateTIOLocatorS(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the location of the terrestrial intermediate origin.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Terrestrial intermediate origin location \f$[rad]\f$.
  */
double EarthOrientation::CalculateTIOLocatorS(const double julian_century_tt) const
{
  double tio_locator_s = kAvgChandlerianAndAnnualWobble * julian_century_tt * kArcsecToRad;
  return tio_locator_s;
}

//------------------------------------------------------------------------------
//  Eigen::Matrix3d CalculatePrecessionNutationRotationMatrix(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the Precession nutation rotation matrix. Based on Standards of 
  * Fundamental Astronomy Software Collection Function - iauPnm06a.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return Eigen::Matrix3d Precession Nutation rotation matrix.
  */
Eigen::Matrix3d EarthOrientation::CalculatePrecessionNutationRotationMatrix(const double julian_century_tt) const
{
  double psi = (-0.041775     +
               (5038.481484   +
               (1.5584175     +
               (-0.00018522   +
               (-0.000026452  +
               (-0.0000000148 ) * julian_century_tt) * julian_century_tt) * julian_century_tt) * julian_century_tt) * julian_century_tt) * kArcsecToRad;

  double phi = (84381.412819 +
               (-46.811016   +
               (0.0511268    +
               (0.00053289   +
               (-0.000000440 +
               (-0.0000000176 ) * julian_century_tt) * julian_century_tt) * julian_century_tt) * julian_century_tt) * julian_century_tt) * kArcsecToRad;
      
  double gam = (-0.052928    +
               (10.556378    +
               (0.4932044    +
               (-0.00031238  +
               (-0.000002788 +
               (0.0000000260 ) * julian_century_tt) * julian_century_tt) * julian_century_tt) * julian_century_tt) * julian_century_tt) * kArcsecToRad;

  double eps  = (84381.406    +
               (-46.836769   +
               (-0.0001831   +
               (0.00200340   +
               (-0.000000576 +
               (-0.0000000434) * julian_century_tt) * julian_century_tt) * julian_century_tt) * julian_century_tt) * julian_century_tt) * kArcsecToRad;

  double fj2 = -0.0000027774 * julian_century_tt;
  
  Eigen::VectorXd temp;
  temp = CalculateIAUNutation2000A(julian_century_tt);

  double dp = temp[0];
  double de = temp[1];
    
  double dpsi = dp + dp * (0.000004697 + fj2);
  double deps = de + de * fj2;

  Eigen::Matrix3d inv_rnpb;
  inv_rnpb = Eigen::AngleAxisd(gam, Eigen::Vector3d::UnitZ())
           * Eigen::AngleAxisd(phi, Eigen::Vector3d::UnitX())
           * Eigen::AngleAxisd(-(psi + dpsi), Eigen::Vector3d::UnitZ())
           * Eigen::AngleAxisd(-(eps + deps), Eigen::Vector3d::UnitX());

  return inv_rnpb.transpose();
}

//------------------------------------------------------------------------------
//  Eigen::Vector2d CalculateIAUNutation2000A(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the nutation in longitude and nutation in obliquity. Based on 
  * the Standards of Fundamental Astronomy Software Collection Function - 
  * iauNut00a.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return Eigen::Vector2d The nutation rates of [psi, epsilon] in \f$[arcsecs]\f$.
  */
Eigen::Vector2d EarthOrientation::CalculateIAUNutation2000A(const double julian_century_tt) const
{
  Eigen::Vector2d output(0, 0);
  output = CalculateLuniSolarNutation(julian_century_tt);
  output = output + CalculatePlanetaryNutation(julian_century_tt);
  return output;
}

//------------------------------------------------------------------------------
//  Eigen::Vector2d CalculateLuniSolarNutation(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the nutation in longitude and nutation in obliquity from the 
  * lunar and solar bodies. Based on the Standards of Fundamental Astronomy 
  * Software Collection Function - iauNut00a.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return Eigen::Vector2d The nutation rates of [psi, epsilon] in \f$[arcsecs]\f$.
  */
Eigen::Vector2d EarthOrientation::CalculateLuniSolarNutation(const double julian_century_tt) const
{
  Eigen::Vector2d output(0, 0);
  double el = MoonMeanAnomaly(julian_century_tt);
  double elp = SunMeanAnomaly(julian_century_tt);
  double f = MoonMeanLongitude(julian_century_tt);
  double d = MoonMeanElongation_Sun(julian_century_tt);
  double om = MoonAscendingNode(julian_century_tt);
  Eigen::VectorXd arg = luni_solar_nutation_series_.col(0) * el 
                      + luni_solar_nutation_series_.col(1) * elp 
                      + luni_solar_nutation_series_.col(2) * f 
                      + luni_solar_nutation_series_.col(3) * d 
                      + luni_solar_nutation_series_.col(4) * om;
  Eigen::ArrayXd arg_norm = arg.array() - (2 * kPI * floor(arg.array()/(2*kPI)));

  Eigen::ArrayXd sarg = arg_norm.sin();
  Eigen::ArrayXd carg = arg_norm.cos();
  
  Eigen::ArrayXd dp_vector = (luni_solar_nutation_series_.col(5).array() 
                            + luni_solar_nutation_series_.col(6).array()  * julian_century_tt) * sarg 
                            + luni_solar_nutation_series_.col(7).array()  * carg;
  Eigen::ArrayXd de_vector = (luni_solar_nutation_series_.col(8).array() 
                            + luni_solar_nutation_series_.col(9).array()  * julian_century_tt) * carg 
                            + luni_solar_nutation_series_.col(10).array() * sarg;

  double dp_sum = dp_vector.sum();
  double de_sum = de_vector.sum();

  //Convert from 0.1 microarcsec units to radians
  output[0] = dp_sum * kArcsecToRad * 1e-7;
  output[1] = de_sum * kArcsecToRad * 1e-7;

  return output;
}

//------------------------------------------------------------------------------
//  Eigen::Vector2d CalculatePlanetaryNutation(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the nutation in longitude and nutation in obliquity from 
  * planetary bodies. Based on the Standards of Fundamental Astronomy 
  * Software Collection Function - iauNut00a.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return Eigen::Vector2d The nutation rates of [psi, epsilon] in \f$[arcsecs]\f$.
  */
Eigen::Vector2d EarthOrientation::CalculatePlanetaryNutation(const double julian_century_tt) const
{
  Eigen::Vector2d output(0, 0);
 
  //  Mean anomaly of the Moon (MHB2000). 
  double al = fmod(2.35555598 + 8328.6914269554 * julian_century_tt, 2*kPI);
  //  Mean longitude of the Moon minus that of the ascending node (MHB2000). 
  double af = fmod(1.627905234 + 8433.466158131 * julian_century_tt, 2*kPI);
  //  Mean elongation of the Moon from the Sun (MHB2000)
  double ad = fmod(5.198466741 + 7771.3771468121 * julian_century_tt, 2*kPI);
  //  Mean longitude of the ascending node of the Moon (MHB2000)
  double aom = fmod(2.18243920 - 33.757045 * julian_century_tt, 2*kPI);
  //  Mean longitude of Mercury (IERS Conventions 2003)
  double alme = MercuryMeanLongitude(julian_century_tt);
  //  Mean longitude of Venus (IERS Conventions 2003)
  double alve = VenusMeanLongitude(julian_century_tt);
  //  Mean longitude of Earth (IERS Conventions 2003)
  double alea = EarthMeanLongitude(julian_century_tt);
  //  Mean longitude of Mars (IERS Conventions 2003)
  double alma = MarsMeanLongitude(julian_century_tt);
  //  Mean longitude of Jupiter (IERS Conventions 2003)
  double alju = JupiterMeanLongitude(julian_century_tt);
  //  Mean longitude of Saturn (IERS Conventions 2003)
  double alsa = SaturnMeanLongitude(julian_century_tt);
  //  Mean longitude of Uranus (IERS Conventions 2003)
  double alur = UranusMeanLongitude(julian_century_tt);
  //  Neptune longitude (MHB2000). 
  double alne = fmod(5.321159000 + 3.8127774000 * julian_century_tt, 2*kPI);
  //  General accumulated precession in longitude (IERS 2003)
  double apa = GeneralPrecessionLongitude(julian_century_tt);
 
  Eigen::VectorXd arg = planetary_nutation_series_.col(0)*al 
                      + planetary_nutation_series_.col(1)*af 
                      + planetary_nutation_series_.col(2)*ad 
                      + planetary_nutation_series_.col(3)*aom 
                      + planetary_nutation_series_.col(4)*alme 
                      + planetary_nutation_series_.col(5)*alve 
                      + planetary_nutation_series_.col(6)*alea 
                      + planetary_nutation_series_.col(7)*alma 
                      + planetary_nutation_series_.col(8)*alju 
                      + planetary_nutation_series_.col(9)*alsa 
                      + planetary_nutation_series_.col(10)*alur 
                      + planetary_nutation_series_.col(11)*alne 
                      + planetary_nutation_series_.col(12)*apa;
                      
  Eigen::ArrayXd arg_norm = arg.array() - (2*kPI * floor(arg.array()/(2*kPI)));

  Eigen::ArrayXd sarg = arg_norm.sin();
  Eigen::ArrayXd carg = arg_norm.cos();
  
  Eigen::ArrayXd dp_vector = planetary_nutation_series_.col(13).array() * sarg 
                           + planetary_nutation_series_.col(14).array() * carg;
  Eigen::ArrayXd de_vector = planetary_nutation_series_.col(15).array() * sarg 
                           + planetary_nutation_series_.col(16).array() * carg;
  
  double dp_sum = dp_vector.sum();
  double de_sum = de_vector.sum();

  //Convert from 0.1 microarcsec units to radians
  output[0] = dp_sum * kArcsecToRad * 1e-7;
  output[1] = de_sum * kArcsecToRad * 1e-7;

  return output;
}

//------------------------------------------------------------------------------
//  Eigen::Matrix3d CalculateSiderealRotationMatrix(const double greenwich_sidereal_time) const
//------------------------------------------------------------------------------
/** Calculates the Sidereal rotation matrix. Based on Standards of Fundamental 
  * Astronomy Software Collection Function - iauGst06.
  *
  * \param greenwich_sidereal_time Greenwich sidereal time in \f$[rad]\f$.
  * 
  * \return Eigen::Matrix3d Sidereal rotation matrix.
  */
Eigen::Matrix3d EarthOrientation::CalculateSiderealRotationMatrix(const double greenwich_sidereal_time) const
{
  Eigen::Matrix3d inv_rotation_matrix;
  inv_rotation_matrix = Eigen::AngleAxisd(greenwich_sidereal_time, Eigen::Vector3d::UnitZ());
  return inv_rotation_matrix.transpose();
}

//------------------------------------------------------------------------------
//  double CalculateGreenwichSiderealTime(const double cio_locator_s,
//                                        const double mjd_ut1, 
//                                        const Eigen::Matrix3d &nutation_rotation_matrix) const
//------------------------------------------------------------------------------
/** Calculates the greenwich sidereal time. Based on Standards of Fundamental 
  * Astronomy Software Collection Function - iauEra00, and iauEors06a.
  *
  * \param cio_locator_s Position of Celestial intermediate origin in \f$[rad]\f$.
  * \param mjd_ut1 UT1 time in modified julian date.
  * \param nutation_rotation_matrix Precession nutation rotation matrix.
  * 
  * \return double Greenwich sidereal time in \f$[rad]\f$.
  */
double EarthOrientation::CalculateGreenwichSiderealTime(const double cio_locator_s, 
                                                        const double mjd_ut1, 
                                                        const Eigen::Matrix3d &nutation_rotation_matrix) const
{
  double t_ut1 = mjd_ut1 + kMjdEpoch - kJd2000Epoch;
  double era = fmod((fmod(t_ut1,1) + 0.7790572732640 + 0.00273781191135448 * t_ut1) * 2*kPI, 2*kPI);
  if(era < 0)
  {
    era = era + 2*kPI;
  }

  double x = nutation_rotation_matrix(2,0);
  double ax = x / (1.0 + nutation_rotation_matrix(2,2));
  double xs = 1.0 - ax * x;
  double ys = -ax * nutation_rotation_matrix(2,1);
  double zs = -x;
  double p = nutation_rotation_matrix(0,0) * xs + nutation_rotation_matrix(0,1) * ys + 
             nutation_rotation_matrix(0,2) * zs;
  double q = nutation_rotation_matrix(1,0) * xs + nutation_rotation_matrix(1,1) * ys + 
             nutation_rotation_matrix(1,2) * zs;
  double eors = ((p != 0) || (q != 0)) ? cio_locator_s - atan2(q, p) : cio_locator_s;

  return era - eors;
}

//------------------------------------------------------------------------------
//  double CalculateCIOLocatorS(const double julian_century_tt, 
//                              const Eigen::Matrix3d &nutation_rotation_matrix) const
//------------------------------------------------------------------------------
/** Calculates the celestial intermediate origin position. Based on Standards 
  * of Fundamental Astronomy Software Collection Function - iauS06 and iauBpn2xy.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * \param nutation_rotation_matrix Precession nutation rotation matrix.
  * 
  * \return double Position of celestial intermediate origin \f$[rad]\f$.
  */
double EarthOrientation::CalculateCIOLocatorS(const double julian_century_tt, 
                                              const Eigen::Matrix3d &nutation_rotation_matrix) const
{
  double cip_x = nutation_rotation_matrix(2,0);
  double cip_y = nutation_rotation_matrix(2,1);

  Eigen::VectorXd FundArgs(8);

  FundArgs(0) = MoonMeanAnomaly(julian_century_tt);
  FundArgs(1) = SunMeanAnomaly(julian_century_tt);
  FundArgs(2) = MoonMeanLongitude(julian_century_tt);
  FundArgs(3) = MoonMeanElongation_Sun(julian_century_tt);
  FundArgs(4) = MoonAscendingNode(julian_century_tt);
  FundArgs(5) = VenusMeanLongitude(julian_century_tt);
  FundArgs(6) = EarthMeanLongitude(julian_century_tt);
  FundArgs(7) = GeneralPrecessionLongitude(julian_century_tt);

  double w0 = 94.00e-6;
  double w1 = 3808.65e-6;
  double w2 = -122.68e-6;
  double w3 = -72574.11e-6;
  double w4 = 27.98e-6;
  double w5 = 15.62e-6;

  std::vector<int> col_indices = {0,1,2,3,4,5,6,7};
  
  //s0 and w0 Calculation
  w0 = CalculateCIOCoefficient(w0, cio_s0_terms_, col_indices, FundArgs);
  w1 = CalculateCIOCoefficient(w1, cio_s1_terms_, col_indices, FundArgs);
  w2 = CalculateCIOCoefficient(w2, cio_s2_terms_, col_indices, FundArgs);
  w3 = CalculateCIOCoefficient(w3, cio_s3_terms_, col_indices, FundArgs);
  w4 = CalculateCIOCoefficient(w4, cio_s4_terms_, col_indices, FundArgs);

  double cio_locator_s = (w0 + (w1 + (w2 + (w3 + (w4 + w5
                         * julian_century_tt) * julian_century_tt) 
                         * julian_century_tt) * julian_century_tt) 
                         * julian_century_tt) * kArcsecToRad - cip_x*cip_y/2.0;
  return cio_locator_s;
}

//------------------------------------------------------------------------------
//  double CalculateCIOCoefficient(const double starting_coeff, 
//                                 const Eigen::MatrixXd &s_terms, 
//                                 const std::vector<int> &col_indices, 
//                                 const Eigen::VectorXd &fundamental_args) const
//------------------------------------------------------------------------------
/** Calculates the celestial intermediate origin weight from given coefficients.
  * Based on Standards of Fundamental Astronomy Software Collection Function - 
  * iauS06 and iauBpn2xy.
  *
  * \param starting_coeff First term coefficient.
  * \param s_terms Celestial intermediate origin s coefficient terms.
  * \param col_indices Vector of column indices.
  * \param fundamental_args Vector of fundamental arguments.
  * 
  * \return double Weight coefficient for celestial intermediate origin 
  *         calculations.
  */
double EarthOrientation::CalculateCIOCoefficient(const double starting_coeff, 
                                                 const Eigen::MatrixXd &s_terms, 
                                                 const std::vector<int> &col_indices, 
                                                 const Eigen::VectorXd &fundamental_args) const
{
  Eigen::ArrayXd temp;
  temp = s_terms(Eigen::all, col_indices)*fundamental_args;
  double coeff = starting_coeff + (s_terms.col(8).array()*sin(temp) + s_terms.col(9).array()*cos(temp)).sum();
  return coeff;
}

//------------------------------------------------------------------------------
//  double MoonMeanAnomaly(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the mean anomaly of the Moon in accordance with IERS Convetions 
  * 2003 based on Standards of Fundamental Astronomy Software Collection 
  * Function - iauFal03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Moon mean anomaly at a given time in \f$[rad]\f$.
  */
double EarthOrientation::MoonMeanAnomaly(const double julian_century_tt) const
{
  double a = fmod(485868.249036  +
             julian_century_tt * (1717915923.2178 +
             julian_century_tt * (31.8792 +
             julian_century_tt * (0.051635 +
             julian_century_tt * (-0.00024470)))), kTurnToArcsec ) * kArcsecToRad;
  return a;
}

//------------------------------------------------------------------------------
//  double SunMeanAnomaly(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the mean anomaly of the Sun in accordance with IERS Convetions 
  * 2003 based on Standards of Fundamental Astronomy Software Collection 
  * Function - iauFalp03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Sun mean anomaly at a given time in \f$[rad]\f$.
  */
double EarthOrientation::SunMeanAnomaly(const double julian_century_tt) const
{
  double a = fmod(1287104.79305  +
             julian_century_tt * (129596581.0481  +
             julian_century_tt * (-0.5532  +
             julian_century_tt * (0.000136  +
             julian_century_tt * (-0.00001149)))), kTurnToArcsec ) * kArcsecToRad;
  return a;
}

//------------------------------------------------------------------------------
//  double MoonMeanElongation_Sun(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the Moon mean elongation from the Sun in accordance 
  * with IERS Convetions 2003 based on Standards of Fundamental Astronomy 
  * Software Collection Function - iauFad03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Moon mean elongation from the sun at a given time in 
  *         \f$[rad]\f$.
  */
double EarthOrientation::MoonMeanElongation_Sun(const double julian_century_tt) const
{
  double a = fmod(1072260.70369  +
             julian_century_tt * (1602961601.2090  +
             julian_century_tt * (-6.3706  +
             julian_century_tt * (0.006593  +
             julian_century_tt * (-0.00003169)))), kTurnToArcsec ) * kArcsecToRad;
  return a;
}

//------------------------------------------------------------------------------
//  double MoonAscendingNode(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the Moon ascending node in accordance with IERS Convetions 2003 
  * based on Standards of Fundamental Astronomy Software Collection Function - 
  * iauFaom03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Moon ascending node at a given time in \f$[rad]\f$.
  */
double EarthOrientation::MoonAscendingNode(const double julian_century_tt) const
{
  double a = fmod(450160.398036 +
             julian_century_tt * (-6962890.5431 +
             julian_century_tt * (7.4722 +
             julian_century_tt * (0.007702 +
             julian_century_tt * (-0.00005939 )))), kTurnToArcsec ) * kArcsecToRad;
  return a;
}

//------------------------------------------------------------------------------
//  double MoonMeanLongitude(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the Moon mean longitude in accordance with IERS Convetions 2003 
  * based on Standards of Fundamental Astronomy Software Collection Function - 
  * iauFaf03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Moon mean longitude at a given time in \f$[rad]\f$.
  */
double EarthOrientation::MoonMeanLongitude(const double julian_century_tt) const
{
  double a = fmod(335779.526232 +
             julian_century_tt * (1739527262.8478 +
             julian_century_tt * (-12.7512 +
             julian_century_tt * (-0.001037 +
             julian_century_tt * (0.00000417 )))), kTurnToArcsec ) * kArcsecToRad;
  return a;
}

//------------------------------------------------------------------------------
//  double MercuryMeanLongitude(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the Mercury mean longitude in accordance with IERS Convetions 2003 
  * based on Standards of Fundamental Astronomy Software Collection Function - 
  * iauFame03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Mercury mean longitude at a given time in \f$[rad]\f$.
  */
double EarthOrientation::MercuryMeanLongitude(const double julian_century_tt) const 
{
   double a = fmod(4.402608842 + 2608.7903141574 * julian_century_tt, 2*kPI);
   return a;
}

//------------------------------------------------------------------------------
//  double VenusMeanLongitude(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the Venus mean longitude in accordance with IERS Convetions 2003 
  * based on Standards of Fundamental Astronomy Software Collection Function - 
  * iauFave03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Venus mean longitude at a given time in \f$[rad]\f$.
  */
double EarthOrientation::VenusMeanLongitude(const double julian_century_tt) const 
{
  double a = fmod(3.176146697 + 1021.3285546211 * julian_century_tt, 2*kPI);
  return a;
}

//------------------------------------------------------------------------------
//  double EarthMeanLongitude(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the Earth mean longitude in accordance with IERS Convetions 2003 
  * based on Standards of Fundamental Astronomy Software Collection Function - 
  * iauFae03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Earth mean longitude at a given time in \f$[rad]\f$.
  */
double EarthOrientation::EarthMeanLongitude(const double julian_century_tt) const
{
  double a = fmod(1.753470314 + 628.3075849991 * julian_century_tt, 2*kPI);
  return a;
}

//------------------------------------------------------------------------------
//  double MarsMeanLongitude(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the Mars mean longitude in accordance with IERS Convetions 2003 
  * based on Standards of Fundamental Astronomy Software Collection Function - 
  * iauFama03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Mars mean longitude at a given time in \f$[rad]\f$.
  */
double EarthOrientation::MarsMeanLongitude(const double julian_century_tt) const
{
   double a = fmod(6.203480913 + 334.0612426700 * julian_century_tt, 2*kPI);
   return a;
}

//------------------------------------------------------------------------------
//  double JupiterMeanLongitude(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the Jupiter mean longitude in accordance with IERS Convetions 2003 
  * based on Standards of Fundamental Astronomy Software Collection Function - 
  * iauFaju03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Jupiter mean longitude at a given time in \f$[rad]\f$.
  */
double EarthOrientation::JupiterMeanLongitude(const double julian_century_tt) const
{
   double a = fmod(0.599546497 + 52.9690962641 * julian_century_tt, 2*kPI);
   return a;
}

//------------------------------------------------------------------------------
//  double SaturnMeanLongitude(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the Saturn mean longitude in accordance with IERS Convetions 2003 
  * based on Standards of Fundamental Astronomy Software Collection Function - 
  * iauFasa03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Saturn mean longitude at a given time in \f$[rad]\f$.
  */
double EarthOrientation::SaturnMeanLongitude(const double julian_century_tt) const
{
   double a = fmod(0.874016757 + 21.3299104960 * julian_century_tt, 2*kPI);
   return a;
}

//------------------------------------------------------------------------------
//  double UranusMeanLongitude(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the Uranus mean longitude in accordance with IERS Convetions 2003 
  * based on Standards of Fundamental Astronomy Software Collection Function - 
  * iauFaur03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Uranus mean longitude at a given time in \f$[rad]\f$.
  */
double EarthOrientation::UranusMeanLongitude(const double julian_century_tt) const
{
   double a = fmod(5.481293872 + 7.4781598567 * julian_century_tt, 2*kPI);
   return a;
}

//------------------------------------------------------------------------------
//  double NeptuneMeanLongitude(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the Neptune mean longitude in accordance with IERS Convetions 2003 
  * based on Standards of Fundamental Astronomy Software Collection Function - 
  * iauFane03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double Neptune mean longitude at a given time in \f$[rad]\f$.
  */
double EarthOrientation::NeptuneMeanLongitude(const double julian_century_tt) const
{
   double a = fmod(5.311886287 + 3.8133035638 * julian_century_tt, 2*kPI);
   return a;
}

//------------------------------------------------------------------------------
//  double GeneralPrecessionLongitude(const double julian_century_tt) const
//------------------------------------------------------------------------------
/** Calculates the general precession in longitude in accordance with IERS 
  * Convetions 2003 based on Standards of Fundamental Astronomy Software 
  * Collection Function - iauFapa03 using a canonical model.
  *
  * \param julian_century_tt Terrestrial time in julian centuries.
  * 
  * \return double General precession of longitude at a given time in \f$[rad]\f$.
  */
double EarthOrientation::GeneralPrecessionLongitude(const double julian_century_tt) const
{
  double a = (0.024381750 + 0.00000538691 * julian_century_tt) * julian_century_tt;
  return a;
}

} // end namespace polaris













 



