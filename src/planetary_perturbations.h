//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  PlanetaryPerturbations Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_PLANETARYPERTURBATIONS_H_
#define POLARIS_PLANETARYPERTURBATIONS_H_

#include <string>
#include <vector>

#include "../lib/eigen3/Eigen/Dense"

#include "constants.h"

namespace polaris {

/** \brief Abstract class header defining the PlanetaryPertubration class.
  * 
  * All planetary pertubration model implementations must be derived from this 
  * class for inheritance/polymorphism of the virtual 
  * CalculatePlanetaryPositions method and planetary position members.
  */ 

class PlanetaryPerturbations
{
 public:
  PlanetaryPerturbations(const std::string &model_name);
  PlanetaryPerturbations(const PlanetaryPerturbations &model);
  virtual ~PlanetaryPerturbations();
  PlanetaryPerturbations& operator=(const PlanetaryPerturbations &model);

  std::string     getModelName()                   const;
  Eigen::Vector3d getSunPositionICRF()             const;
  Eigen::Vector3d getMercuryPositionICRF()         const;
  Eigen::Vector3d getVenusPositionICRF()           const;
  Eigen::Vector3d getEarthPositionICRFBarycenter() const;
  Eigen::Vector3d getMoonPositionICRF()            const;
  Eigen::Vector3d getMarsPositionICRF()            const;
  Eigen::Vector3d getJupiterPositionICRF()         const;
  Eigen::Vector3d getSaturnPositionICRF()          const;
  Eigen::Vector3d getUranusPositionICRF()          const;
  Eigen::Vector3d getNeptunePositionICRF()         const;
  Eigen::Vector3d getPlutoPositionICRF()           const;

//------------------------------------------------------------------------------
//  void CalculatePlanetaryPositions(const double mjd_tdb) 
//------------------------------------------------------------------------------
/** Virtual method to calculate the position of all planetary bodies with earth 
  * origin at the given UTC modified julian date in \f$[m]\f$ in ICRF. 
  * 
  * \param mjd_tdb UTC modified julian date.
  */
  virtual void CalculatePlanetaryPositions(const double mjd_tdb) = 0;

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &icrf_pos) 
//------------------------------------------------------------------------------
/** Virtual method to calculate the acceleration acting on the satellite from 
  * all planetary bodies in ICRF.
  * 
  * \param icrf_pos Position vector of the satellite in ICRF \f$[m]\f$. 
  */
  virtual Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &icrf_pos) = 0;

 protected:
  std::string model_name_;

  Eigen::Vector3d sun_position_icrf_;
  Eigen::Vector3d mercury_position_icrf_; 
  Eigen::Vector3d venus_position_icrf_;
  Eigen::Vector3d earth_position_icrf_barycenter_;
  Eigen::Vector3d moon_position_icrf_;
  Eigen::Vector3d mars_position_icrf_;
  Eigen::Vector3d jupiter_position_icrf_;
  Eigen::Vector3d saturn_position_icrf_; 
  Eigen::Vector3d uranus_position_icrf_;
  Eigen::Vector3d neptune_position_icrf_;
  Eigen::Vector3d pluto_position_icrf_;
};

} // end namespace polaris

#endif  // POLARIS_PLANETARYPERTURBATIONS_H_