//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Polaris Simulator Numerical Constants
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_CONSTANTS_H_
#define POLARIS_CONSTANTS_H_

namespace polaris {

/** \brief Polaris Simulator Mathematical and Astronomical constants defintion
  * 
  * Header file defining the Polaris simulator compile-time constants. Constants
  * included:
  *   - Mathematical Constants
  *   - Time Based Constants
  *   - IERS Earth Orientation Parameters Constants
  *   - IAU2006/2009 Astronomical Constants
  *   - WGS84 Geodetic Constants
  *   - Auxiliary Constants
  */ 
 
//  Mathematical Constants
constexpr double kPI = 3.14159265358979323846264338327950288;
constexpr double kArcsecToRad = 0.00000484813681109536;
constexpr double kTurnToArcsec = 1296000;
constexpr double kRadToSec = 206264.806247096;

//  Time Based Constants
constexpr double kMjdEpoch = 2400000.5;
constexpr double kJd2000Epoch = 2451545;
constexpr double kJd2005Epoch = 2453372;
constexpr double kJd2010Epoch = 2455198;
constexpr double kJd2015Epoch = 2457024;
constexpr double kJd2020Epoch = 2458850;
constexpr double kJdCentury = 36525;

//  IERS Earth Orientation Parameters Constants
constexpr double kEarthMoonRatio = 1 / (1 + 81.3005682168675747);
constexpr double kRightAscensionLag = 0.523599;             //  [rad]
constexpr double kEarthRotationRate = 7.29211514670639e-05; //  [rad/s]
constexpr double kLODScale = 0.843994809 * 1e-9;
constexpr double kAvgChandlerianAndAnnualWobble = -0.000047;

//  IAU2006/2009 Astronomical Constants
constexpr double kLightspeed = 2.99792458e8;                //  [m/s]
constexpr double kAu = 1.49597870700e11;                    //  [m]
constexpr double kSunGM = 1.327124400419394e+20;            //  [m^3/kg*s]
constexpr double kMercuryGM = 2.203178e+13;                 //  [m^3/kg*s]
constexpr double kVenusGM = 3.24858592e+14;                 //  [m^3/kg*s]
constexpr double kEarthGM = 3.9860044188e+14;               //  [m^3/kg*s]
constexpr double kMoonGM = 4.902800196140593e+12;           //  [m^3/kg*s]
constexpr double kMarsGM = 4.2828375214e+13;                //  [m^3/kg*s]
constexpr double kJupiterGM = 1.267127641334462e+17;        //  [m^3/kg*s]
constexpr double kSaturnGM = 3.79405852e+16;                //  [m^3/kg*s]
constexpr double kUranusGM = 5.794556465751793e+15;         //  [m^3/kg*s]
constexpr double kNeptuneGM = 6.83652710058e+15;            //  [m^3/kg*s]
constexpr double kPlutoGM = 9.755011758767654e+11;          //  [m^3/kg*s]

//  WGS84 Geodetic Constants
constexpr double kEarthRadius =  6.3781366e6;               //  [m]
constexpr double kEarthFlattening = 0.0033528106811823; 

//  Auxiliary Constants
constexpr double kSunRadius = 696000000;                    //  [m]
constexpr double kMoonRadius = 1738000;                     //  [m]

} // end namespace polaris

#endif  // POLARIS_CONSTANTS_H_