//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  SimulationTime Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_SIMULATIONTIME_H_
#define POLARIS_SIMULATIONTIME_H_

#include <string>

namespace polaris {

/** 
  * \brief Class header defining the implementation of the SimulationTime class.
  * 
  * The SimulationTime class is responsible for managing the current simulation 
  * time and associated timescales (Gregorian, UTC, TT, TDB, JulianCentury).
  * Incrementing the time by timestep [s] only updates UTC and Gregorian time.
  * Timescales TT, UT1, TDB require input of the TAI_UTC and UT1_UTC offsets 
  * provided from the IERS Earth Orientation Parameters.
  */ 

class SimulationTime
{
 public:
  SimulationTime();
  SimulationTime(const int    year,
                 const int    month, 
                 const int    day, 
                 const double hour, 
                 const double min, 
                 const double sec);
  SimulationTime(const SimulationTime &time);
  ~SimulationTime();
  SimulationTime& operator=(const SimulationTime &time);                 

  double      getMjdUTC()               const;
  double      getMjdTT()                const;
  double      getMjdUT1()               const;
  double      getMjdTDB()               const;
  double      getJulianCenturyTT()      const;
  std::string getGregorianTime()        const;
  double      getSimulationIterations() const;
  double      getRuntime()              const;
  double      getTimestep()             const;


  void IncrementTime(const double timestep);
  void UpdateTimescales(const double tai_utc_offset, 
                        const double ut1_utc_offset);

 private:
  double year_;
  double month_;
  double day_;
  double hour_;
  double min_;
  double sec_;

  double timestep_;
  double runtime_;
  int    iterations_;

  double mjd_utc_;
  double mjd_tt_;
  double mjd_ut1_;
  double mjd_tdb_;
  double julian_century_tt_;

  double CalculateMjdUTC(const int year, 
                         const int month, 
                         const int day, 
                         const double hour, 
                         const double min, 
                         const double sec) const;
  double CalculateMjdTT(const double mjd_utc, const double tai_utc_offset) const;
  double CalculateMjdUT1(const double mjd_utc, const double ut1_utc_offset) const;
  double CalculateMjdTDB(const double mjd_tt, const double julian_century_tt) const;
  double CalculateJulianCenturyTT(const double tt) const;

  void Mjd2Gregorian(const double mjd);
};

} // end namespace polaris

#endif  // POLARIS_SIMULATIONTIME_H_