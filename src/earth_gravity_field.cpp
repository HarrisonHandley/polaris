//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  EarthGravityField Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "earth_gravity_field.h"

namespace polaris {

//------------------------------------------------------------------------------
//  EarthGravityField(const std::string &model_name)
//------------------------------------------------------------------------------
/** Default constructor requiring configuration of reaction wheel.
  *
  * \param model_name Name of the earth gravity field model used. 
  */
EarthGravityField::EarthGravityField(const std::string &model_name) : 
                                     model_name_(model_name)
{

}

//------------------------------------------------------------------------------
//  EarthGravityField(const EarthGravityField &model) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param model The object being copied.
  */
EarthGravityField::EarthGravityField(const EarthGravityField &model) :
                                     model_name_(model.model_name_)
{

}

//------------------------------------------------------------------------------
//  ~EarthGravityField() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
EarthGravityField::~EarthGravityField()
{

}

//------------------------------------------------------------------------------
//  EarthGravityField& operator=(const EarthGravityField &model) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param model The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
EarthGravityField& EarthGravityField::operator=(const EarthGravityField &model)
{
  if(&model == this)
  {
    return *this;
  }
  model_name_ = model.model_name_;
  return *this;
}

//------------------------------------------------------------------------------
//  std::string getModelName()
//------------------------------------------------------------------------------
/** Get the name of the model used to calculate the earth gravity field. 
  *
  * \return std::string of the name of the model used.
  */
std::string EarthGravityField::getModelName() const
{
  return model_name_;
}

} // end namespace polaris