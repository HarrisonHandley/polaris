//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Atmospheric Model Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_ATMOSPHERICMODEL_H_
#define POLARIS_ATMOSPHERICMODEL_H_

#include <memory>
#include <string>

#include "../lib/Eigen3/Eigen/Dense"

#include "earth_orientation.h"
#include "satellite.h"

namespace polaris {

/** \brief Abstract class header defining the AtmosphericModel class.
  * 
  * All atmospheric model implementations must be derived from this class for 
  * inheritance of the getAccelFromAerodynamicDragICRF and polymorphism of the 
  * virtual CalculateAtmosphericDensity methods.
  */ 

class AtmosphericModel
{
 public:
  AtmosphericModel(const std::string &model_name);
  AtmosphericModel(const AtmosphericModel &model);
  virtual ~AtmosphericModel();
  AtmosphericModel& operator=(const AtmosphericModel &model);

  std::string     getModelName() const;
  Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &r_sun,
                               const EarthOrientation &earth_orientation,
                               const std::shared_ptr<Satellite> &sat);

 protected:
  std::string model_name_;

//------------------------------------------------------------------------------
//  double CalculateAtmosphericDensity(const Eigen::Vector3d  &r_sun, 
//                                     const Eigen::Vector3d  &r_sat,
//                                     const EarthOrientation &earth_orientation)
//------------------------------------------------------------------------------
/** Virtual method for calculting the Atmospheric Density at location of 
  * spacecraft in ICRF
  * 
  * \param r_sun Eigen::Vector3d Position of the sun in ICRF with Earth origin 
  *              \f$[m]\f$.
  * \param r_sat Eigen::Vector3d Position of satellite in ICRF with Earth origin
  *              \f$[m]\f$.
  * \param earth_orientation EarthOrientation object defining the conversion 
  *                          between reference frames ICRF, ITRF, LOD and 
  *                          Lat/Lon.
  * 
  * \return double Atmospheric density at given satellite position \f$[kg/m^3]\f$
  */
  virtual double CalculateAtmosphericDensity(const Eigen::Vector3d  &r_sun, 
                                             const Eigen::Vector3d  &r_sat,
                                             const EarthOrientation &earth_orientation) const = 0;
};

} // end namespace polaris

#endif  // POLARIS_ATMOSPHERICMODEL_H_
