//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                 Spacecraft Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "spacecraft.h"

#include "constants.h"

namespace polaris {

//------------------------------------------------------------------------------
//  Satellite(const std::string     &satellite_id, 
//            const Eigen::Vector3d &icrf_pos,
//            const Eigen::Vector3d &icrf_vel,
//            const double           mass, 
//            const double           cross_section,
//            const double           drag_coeff, 
//            const double           radiation_pressure_coeff)
//------------------------------------------------------------------------------
/** Default constructor requiring configuration of reaction wheel.
  *
  * \param satellite_id Identification string for the satellite object
  * \param icrf_pos Satellite position vector in ICRF with earth origin in \f$[m]\f$
  * \param icrf_vel Satellite velocity vector in ICRF with earth origin in \f$[m]\f$
  * \param mass Mass of satellite in \f$[kg]\f$
  * \param cross_section Cross sectional area of satellite parallel to velocity vector \f$[m^2]\f$
  * \param drag_coeff Drag coefficient of satellite
  * \param radiation_pressure_coeff Radiation pressure coefficient of satellite
  * \param inertia_matrix Spacecraft inertia matrix \f$[kg \dot m^2]\f$
  * \param center_of_mass_offset Spacecraft center of mass offset from body frame origin \f$[m]\f$
  * \param static_magnetic_moment Spacecraft static magnetic moment in body frame \f$[A \dot m^2]\f$
  * \param quaternion Spacecraft attitude quaternion with respect to ICRF.
  * \param angular_velocity_bf Spacecraft angular velocity with respect to ICRF \f$[rad/s]\f$
  * \param magnetorquers Vector of magnetorquer actuator objects equipped to the spacecraft
  * \param reaction_wheels Vector of reaction wheel actuator objects equipped to the spacecraft
  * \param thrusters Vector of thruster actuator objects equipped to the spacecraft
  */
Spacecraft::Spacecraft(const std::string                &satellite_id,
                       const Eigen::Vector3d            &icrf_pos, 
                       const Eigen::Vector3d            &icrf_vel, 
                       const double                      mass, 
                       const double                      cross_section, 
                       const double                      drag_coeff, 
                       const double                      radiation_pressure_coeff,
                       const Eigen::Matrix3d            &inertia_matrix,
                       const Eigen::Vector3d            &center_of_mass_offset,
                       const Eigen::Vector3d            &static_magnetic_moment,
                       const Eigen::Quaterniond         &quaternion,
                       const Eigen::Vector3d            &angular_velocity_bf,
                       const std::vector<Magnetorquer>  &magnetorquers,
                       const std::vector<ReactionWheel> &reaction_wheels,
                       const std::vector<Thruster>      &thrusters) :
                       Satellite(satellite_id,
                                 icrf_pos, 
                                 icrf_vel,
                                 mass, 
                                 cross_section, 
                                 drag_coeff, 
                                 radiation_pressure_coeff),
                       inertia_matrix_                (inertia_matrix),
                       center_of_mass_offset_         (center_of_mass_offset.normalized()),
                       static_magnetic_moment_        (static_magnetic_moment),
                       quaternion_                    (quaternion),
                       angular_vel_bf_                (angular_velocity_bf),
                       angular_accel_bf_              (Eigen::Vector3d::Zero()),
                       sc_angular_momentum_           (Eigen::Vector3d::Zero()),
                       applied_angular_momentum_delta_(Eigen::Vector3d::Zero()),
                       rw_angular_momentum_           (Eigen::Vector3d::Zero()),                                      
                       magnetorquers_                 (magnetorquers),
                       reaction_wheels_               (reaction_wheels),
                       thrusters_                     (thrusters),
                       bf2icrf_rot_matrix_            (Eigen::Matrix3d::Zero()),
                       icrf2bf_rot_matrix_            (Eigen::Matrix3d::Zero()),
                       atmospheric_drag_torque_       (Eigen::Vector3d::Zero()),
                       gravity_gradient_torque_       (Eigen::Vector3d::Zero()),
                       magnetic_torque_               (Eigen::Vector3d::Zero()),
                       solar_pressure_torque_         (Eigen::Vector3d::Zero()),
                       thruster_torque_               (Eigen::Vector3d::Zero()),
                       applied_torque_                (Eigen::Vector3d::Zero())
{

}

//------------------------------------------------------------------------------
//  Spacecraft(const Spacecraft &sc) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param sc The object being copied.
  */
Spacecraft::Spacecraft(const Spacecraft &sc) :
                       Satellite                      (sc),
                       inertia_matrix_                (sc.inertia_matrix_),
                       center_of_mass_offset_         (sc.center_of_mass_offset_),
                       static_magnetic_moment_        (sc.static_magnetic_moment_),
                       quaternion_                    (sc.quaternion_),
                       angular_vel_bf_                (sc.angular_vel_bf_),
                       angular_accel_bf_              (sc.angular_accel_bf_),
                       sc_angular_momentum_           (sc.sc_angular_momentum_),
                       applied_angular_momentum_delta_(sc.applied_angular_momentum_delta_),
                       rw_angular_momentum_           (sc.rw_angular_momentum_),                                      
                       magnetorquers_                 (sc.magnetorquers_),
                       reaction_wheels_               (sc.reaction_wheels_),
                       thrusters_                     (sc.thrusters_),
                       bf2icrf_rot_matrix_            (sc.bf2icrf_rot_matrix_),
                       icrf2bf_rot_matrix_            (sc.icrf2bf_rot_matrix_),
                       atmospheric_drag_torque_       (sc.atmospheric_drag_torque_),
                       gravity_gradient_torque_       (sc.gravity_gradient_torque_),
                       magnetic_torque_               (sc.magnetic_torque_),
                       solar_pressure_torque_         (sc.solar_pressure_torque_),
                       thruster_torque_               (sc.thruster_torque_),
                       applied_torque_                (sc.applied_torque_)
{
 
}

//------------------------------------------------------------------------------
//  ~Spacecraft() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
Spacecraft::~Spacecraft()
{

}

//------------------------------------------------------------------------------
//  Spacecraft& operator=(const Spacecraft &sc) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param sc The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
Spacecraft& Spacecraft::operator=(const Spacecraft &sc)
{
  if(&sc == this)
  {
    return *this;
  }
  Satellite::operator=(sc);

  inertia_matrix_                 = sc.inertia_matrix_;
  center_of_mass_offset_          = sc.center_of_mass_offset_;
  static_magnetic_moment_         = sc.static_magnetic_moment_;
  quaternion_                     = sc.quaternion_;
  angular_vel_bf_                 = sc.angular_vel_bf_;
  angular_accel_bf_               = sc.angular_accel_bf_;
  sc_angular_momentum_            = sc.sc_angular_momentum_;
  applied_angular_momentum_delta_ = sc.applied_angular_momentum_delta_;
  rw_angular_momentum_            = sc.rw_angular_momentum_;                 
  magnetorquers_                  = sc.magnetorquers_;
  reaction_wheels_                = sc.reaction_wheels_;
  thrusters_                      = sc.thrusters_;
  bf2icrf_rot_matrix_             = sc.bf2icrf_rot_matrix_;
  icrf2bf_rot_matrix_             = sc.icrf2bf_rot_matrix_;
  atmospheric_drag_torque_        = sc.atmospheric_drag_torque_;
  gravity_gradient_torque_        = sc.gravity_gradient_torque_;
  magnetic_torque_                = sc.magnetic_torque_;
  solar_pressure_torque_          = sc.solar_pressure_torque_;
  thruster_torque_                = sc.thruster_torque_;
  applied_torque_                 = sc.applied_torque_;

  return *this;
}

//------------------------------------------------------------------------------
//  Eigen::Quaterniond getAttitude() const 
//------------------------------------------------------------------------------
Eigen::Quaterniond Spacecraft::getAttitude() const
{
  return quaternion_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAngularVelocity() const 
//------------------------------------------------------------------------------
Eigen::Vector3d Spacecraft::getAngularVelocity() const
{
  return angular_vel_bf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAngularAcceleration() const 
//------------------------------------------------------------------------------
Eigen::Vector3d Spacecraft::getAngularAcceleration() const
{
  return angular_accel_bf_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAtmosphericDragTorque() const 
//------------------------------------------------------------------------------
Eigen::Vector3d Spacecraft::getAtmosphericDragTorque() const
{
  return atmospheric_drag_torque_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getGravityGradientTorque() const 
//------------------------------------------------------------------------------
Eigen::Vector3d Spacecraft::getGravityGradientTorque() const
{
  return gravity_gradient_torque_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getMagneticTorque() const 
//------------------------------------------------------------------------------
Eigen::Vector3d Spacecraft::getMagneticTorque() const
{
  return magnetic_torque_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getSolarPressureTorque() const 
//------------------------------------------------------------------------------
Eigen::Vector3d Spacecraft::getSolarPressureTorque() const
{
  return solar_pressure_torque_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getTotalThrusterTorque() const 
//------------------------------------------------------------------------------
Eigen::Vector3d Spacecraft::getTotalThrusterTorque() const
{
  return thruster_torque_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAppliedTorque() const 
//------------------------------------------------------------------------------
Eigen::Vector3d Spacecraft::getAppliedTorque() const
{
  return applied_torque_;
}

//------------------------------------------------------------------------------
//  double getNumberOfMagnetorquers() const 
//------------------------------------------------------------------------------
double Spacecraft::getNumberOfMagnetorquers() const
{
  return magnetorquers_.size();
}

//------------------------------------------------------------------------------
//  double getNumberOfReactionWheels() const 
//------------------------------------------------------------------------------
double Spacecraft::getNumberOfReactionWheels() const
{
  return reaction_wheels_.size();
}

//------------------------------------------------------------------------------
//  double getNumberOfThrusters() const 
//------------------------------------------------------------------------------
double Spacecraft::getNumberOfThrusters() const
{
  return thrusters_.size();
}

//------------------------------------------------------------------------------
//  std::vector<std::string> getMagnetorquersState() const 
//------------------------------------------------------------------------------
std::vector<std::string> Spacecraft::getMagnetorquersState() const
{
  std::vector<std::string> state;
  for(auto &magnetorquer : magnetorquers_)
  {
    state.push_back(magnetorquer.getMode());
  }
  return state;
}

//------------------------------------------------------------------------------
//  std::vector<Eigen::Vector3d> getMagnetorquersMagneticMoment() const 
//------------------------------------------------------------------------------
std::vector<Eigen::Vector3d> Spacecraft::getMagnetorquersMagneticMoment() const
{
  std::vector<Eigen::Vector3d> magnetic_moment;
  for(auto &magnetorquer : magnetorquers_)
  {
    magnetic_moment.push_back(magnetorquer.getMagneticMoment());
  }
  return magnetic_moment;
}

//------------------------------------------------------------------------------
//  std::vector<double> getMagnetorquersOperatingLoad() const 
//------------------------------------------------------------------------------
std::vector<double> Spacecraft::getMagnetorquersOperatingLoad() const
{
  std::vector<double> operating_load;
  for(auto &magnetorquer : magnetorquers_)
  {
    operating_load.push_back(magnetorquer.getOperatingLoad());
  }
  return operating_load;
}

//------------------------------------------------------------------------------
//  std::vector<std::string> getReactionWheelsState() const 
//------------------------------------------------------------------------------
std::vector<std::string> Spacecraft::getReactionWheelsState() const
{
  std::vector<std::string> state;
  for(auto &reaction_wheel : reaction_wheels_)
  {
    state.push_back(reaction_wheel.getMode());
  }
  return state;
}

//------------------------------------------------------------------------------
//  std::vector<double> getReactionWheelsAngularVel() const 
//------------------------------------------------------------------------------
std::vector<double> Spacecraft::getReactionWheelsAngularVel() const
{
  std::vector<double> angular_velocity;
  for(auto &reaction_wheel : reaction_wheels_)
  {
    angular_velocity.push_back(reaction_wheel.getAngularVelocity());
  }
  return angular_velocity;
}

//------------------------------------------------------------------------------
//  std::vector<double> getReactionWheelsAngularAccel() const 
//------------------------------------------------------------------------------
std::vector<double> Spacecraft::getReactionWheelsAngularAccel() const
{
  std::vector<double> angular_acceleration;
  for(auto &reaction_wheel : reaction_wheels_)
  {
    angular_acceleration.push_back(reaction_wheel.getAngularAcceleration());
  }
  return angular_acceleration;
}

//------------------------------------------------------------------------------
//  std::vector<double> getReactionWheelsMotorTorque() const 
//------------------------------------------------------------------------------
std::vector<double> Spacecraft::getReactionWheelsMotorTorque() const
{
  std::vector<double> motor_torque;
  for(auto &reaction_wheel : reaction_wheels_)
  {
    motor_torque.push_back(reaction_wheel.getMotorTorque());
  }
  return motor_torque;
}

//------------------------------------------------------------------------------
//  std::vector<double> getThrustersThrottle() const 
//------------------------------------------------------------------------------
std::vector<double> Spacecraft::getThrustersThrottle() const
{
  std::vector<double> throttle;
  for(auto &thruster : thrusters_)
  {
    throttle.push_back(thruster.getThrottle());
  }
  return throttle;
}

//------------------------------------------------------------------------------
//  std::vector<Eigen::Vector3d> getThrustersForce() const 
//------------------------------------------------------------------------------
std::vector<Eigen::Vector3d> Spacecraft::getThrustersForce() const
{
  std::vector<Eigen::Vector3d> thruster_force;
  for(auto &thruster : thrusters_)
  {
    thruster_force.push_back(thruster.getThrusterForce());
  }
  return thruster_force;
}

//------------------------------------------------------------------------------
//  std::vector<Eigen::Vector3d> getThrustersTorque() const 
//------------------------------------------------------------------------------
std::vector<Eigen::Vector3d> Spacecraft::getThrustersTorque() const
{
  std::vector<Eigen::Vector3d> thruster_torque;
  for(auto &thruster : thrusters_)
  {
    thruster_torque.push_back(thruster.getThrusterTorque());
  }
  return thruster_torque;
}

//------------------------------------------------------------------------------
//  std::vector<double> getThrustersMassFlowRate() const 
//------------------------------------------------------------------------------
std::vector<double> Spacecraft::getThrustersMassFlowRate() const
{
  std::vector<double> thrust_mass_flow_rate;
  for(auto &thruster : thrusters_)
  {
    thrust_mass_flow_rate.push_back(thruster.getMassFlowRate());
  }
  return thrust_mass_flow_rate;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAngularMomentum() const 
//------------------------------------------------------------------------------
Eigen::Vector3d Spacecraft::getAngularMomentum() const
{
  return sc_angular_momentum_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAppliedAngularMomentumDelta() const 
//------------------------------------------------------------------------------
Eigen::Vector3d Spacecraft::getAppliedAngularMomentumDelta() const
{
  return applied_angular_momentum_delta_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getRWAngularMomentum() const 
//------------------------------------------------------------------------------
Eigen::Vector3d Spacecraft::getRWAngularMomentum() const
{
  return rw_angular_momentum_;
}

//------------------------------------------------------------------------------
//  void setMagnetorquersLoad()
//------------------------------------------------------------------------------
void Spacecraft::setMagnetorquersLoad(const Eigen::VectorXd &operating_load)
{
  for(std::size_t i = 0; i < magnetorquers_.size(); ++i)
  {
    magnetorquers_[i].setOperatingFraction(operating_load(i));
  }
}

//------------------------------------------------------------------------------
//  void setReactionWheelsMotorTorque()
//------------------------------------------------------------------------------
void Spacecraft::setReactionWheelsMotorTorque(const Eigen::VectorXd &motor_torque)
{
  for(std::size_t i = 0; i < reaction_wheels_.size(); ++i)
  {
    reaction_wheels_[i].setMotorTorque(motor_torque(i));
  }
}

//------------------------------------------------------------------------------
//  void setThrustersThrottle()
//------------------------------------------------------------------------------
void Spacecraft::setThrustersThrottle(const Eigen::VectorXd &throttle)
{
  for(std::size_t i = 0; i < thrusters_.size(); ++i)
  {
    thrusters_[i].setThrottle(throttle(i));
    thrusters_[i].CalculateThrust();
  }
}

//------------------------------------------------------------------------------
//  void InitializeAttitudeRefFrames()
//------------------------------------------------------------------------------
void Spacecraft::InitializeAttitudeRefFrames()
{
  bf2icrf_rot_matrix_ = CalculateBF2ICRFRotationMatrix();
  icrf2bf_rot_matrix_ = bf2icrf_rot_matrix_.inverse();
}

//------------------------------------------------------------------------------
//  void CalculateAttitudeState()
//------------------------------------------------------------------------------
void Spacecraft::CalculateAttitudeState(const double mjd_utc, 
                                        std::unique_ptr<EarthMagneticField> &magnetic_field,
                                        const EarthOrientation &earth_orientation)
{
  applied_torque_ = CalculateAppliedTorque(mjd_utc, magnetic_field, earth_orientation);

  Eigen::Matrix3d rw_inertia = Eigen::Matrix3d::Zero();
  Eigen::Vector3d rw_ang_vel = {0, 0, 0};
  Eigen::Vector3d rw_torque = {0, 0, 0};
  Eigen::Vector3d rw_spin_axis = {0, 0, 0};
  Eigen::Matrix3d identity_matrix = Eigen::Matrix3d::Identity();

  for(auto &reaction_wheel : reaction_wheels_)
  {
    rw_spin_axis = reaction_wheel.getSpinAxis();
    Eigen::Matrix3d temp = identity_matrix.array().colwise() * (reaction_wheel.getInertia() * rw_spin_axis).array();
    rw_inertia = rw_inertia + temp;
    rw_ang_vel = rw_ang_vel + (reaction_wheel.getAngularVelocity() * rw_spin_axis);
    rw_torque  = rw_torque  + (reaction_wheel.getMotorTorque()     * rw_spin_axis);
  }

  Eigen::Matrix3d inv_inertia = (inertia_matrix_ - rw_inertia).inverse();

  angular_accel_bf_ = inv_inertia * (applied_torque_ - rw_torque - 
                      angular_vel_bf_.cross(rw_inertia * rw_ang_vel + inertia_matrix_ * angular_vel_bf_));
  
  for(auto &reaction_wheel : reaction_wheels_)
  { 
    reaction_wheel.CalculateAngularAcceleration(angular_accel_bf_);
  }

  rw_angular_momentum_ = rw_inertia * rw_ang_vel;
  applied_rw_motor_torque_ = rw_torque;
  sc_angular_momentum_ = (inertia_matrix_ - rw_inertia) * angular_vel_bf_;
}

//------------------------------------------------------------------------------
//  void PropagateAttitude(const double timestep)
//------------------------------------------------------------------------------
void Spacecraft::PropagateAttitude(const double timestep)
{
  applied_angular_momentum_delta_ = (applied_torque_ ) * timestep;

  angular_vel_bf_ = angular_vel_bf_ + angular_accel_bf_ * timestep;

  CalculateNewAttitude(timestep);

  for(auto &reaction_wheel : reaction_wheels_)
  {
    reaction_wheel.CalculateAngularVelocity(timestep);
  }
}

//------------------------------------------------------------------------------
//  Eigen::Matrix3d CalculateBF2ICRFRotationMatrix()
//------------------------------------------------------------------------------
/**
  * Calculates the rotation matrix between the spacecraft body reference frame
  * and ICRF based on the current attitude quaternion of the spacecraft.
  *
  * \return Eigen::Matrix3d rotation matrix for spacecraft body reference frame
  *         to ICRF.
  */
Eigen::Matrix3d Spacecraft::CalculateBF2ICRFRotationMatrix()
{
  Eigen::Matrix3d bf2icrf_rot_matrix;
  quaternion_.normalize();
  bf2icrf_rot_matrix(0,0) = pow(quaternion_.x(), 2) - pow(quaternion_.y(), 2) 
                          - pow(quaternion_.z(), 2) + pow(quaternion_.w(), 2); 
  bf2icrf_rot_matrix(0,1) = 2 * quaternion_.x() * quaternion_.y() 
                          - 2 * quaternion_.w() * quaternion_.z();
  bf2icrf_rot_matrix(0,2) = 2 * quaternion_.x() * quaternion_.z() 
                          + 2 * quaternion_.w() * quaternion_.y();
  bf2icrf_rot_matrix(1,0) = 2 * quaternion_.x() * quaternion_.y() 
                          + 2 * quaternion_.w() * quaternion_.z();
  bf2icrf_rot_matrix(1,1) = - pow(quaternion_.x(), 2) + pow(quaternion_.y(), 2) 
                            - pow(quaternion_.z(), 2) + pow(quaternion_.w(), 2); 
  bf2icrf_rot_matrix(1,2) = 2 * quaternion_.y() * quaternion_.z() 
                          - 2 * quaternion_.w() * quaternion_.x();
  bf2icrf_rot_matrix(2,0) = 2 * quaternion_.x() * quaternion_.z() 
                          - 2 * quaternion_.w() * quaternion_.y();
  bf2icrf_rot_matrix(2,1) = 2 * quaternion_.y() * quaternion_.z() 
                          + 2 * quaternion_.w() * quaternion_.x();
  bf2icrf_rot_matrix(2,2) = - pow(quaternion_.x(), 2) - pow(quaternion_.y(), 2) 
                            + pow(quaternion_.z(), 2) + pow(quaternion_.w(), 2); 
  return bf2icrf_rot_matrix; 
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateAppliedTorque(const double mjd_utc, 
//                                         std::unique_ptr<EarthMagneticField> &magnetic_field,
//                                         const EarthOrientation &earth_orientation)
//------------------------------------------------------------------------------
/**
  * Calculates the total applied torque to the spacecraft in the spacecraft
  * body reference frame. This includes torque from earth gravity gradient,
  * solar pressure, atmospheric drag, magnetic field, and spacecraft thrusters.
  *
  * \return Eigen::Vector3d torque vector \f$[\tau_x, \tau_y, \tau_z]\f$ in 
  *         \f$[Nm]\f$
  */
Eigen::Vector3d Spacecraft::CalculateAppliedTorque(const double mjd_utc, 
                                                   std::unique_ptr<EarthMagneticField> &magnetic_field,
                                                   const EarthOrientation &earth_orientation)
{
  gravity_gradient_torque_ = CalculateGravityGradientTorque();
  solar_pressure_torque_   = CalculateSolarPressureTorque();
  atmospheric_drag_torque_ = CalculateAtmosphericDragTorque();
  magnetic_torque_         = CalculateMagneticTorque(mjd_utc, magnetic_field, earth_orientation);
  thruster_torque_         = CalculateThrustersTorque();

  Eigen::Vector3d applied_torque = gravity_gradient_torque_ +
                                   solar_pressure_torque_ +
                                   atmospheric_drag_torque_ +
                                   magnetic_torque_ +
                                   thruster_torque_;
  return applied_torque;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateGravityGradientTorque()
//------------------------------------------------------------------------------
/**
  * Calculates the torque vector acting on the spacecraft from the earth gravity
  * gradient in the spacecraft reference frame.
  *
  * \return Eigen::Vector3d earth gravity gradient torque vector acting on the 
  *         spacecraft in the spacecraft body reference frame
  *         \f$[\tau_x, \tau_y, \tau_z]\f$ in \f$[Nm]\f$
  */
Eigen::Vector3d Spacecraft::CalculateGravityGradientTorque()
{
  Eigen::Vector3d rc = icrf2bf_rot_matrix_ * icrf_position_;
  Eigen::Vector3d gravity_gradient_torque = (3 * kEarthGM / pow(rc.norm(), 5)) * rc.cross(inertia_matrix_ * rc);
  return gravity_gradient_torque;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateSolarPressureTorque()
//------------------------------------------------------------------------------
/**
  * Calculates the torque vector acting on the spacecraft from solar pressure
  * in the spacecraft reference frame.
  *
  * \return Eigen::Vector3d solar pressure torque vector acting on the 
  *         spacecraft in the spacecraft body reference frame
  *         \f$[\tau_x, \tau_y, \tau_z]\f$ in \f$[Nm]\f$
  */
Eigen::Vector3d Spacecraft::CalculateSolarPressureTorque()
{
  Eigen::Vector3d solar_pressure_torque = center_of_mass_offset_.cross 
                                         (icrf2bf_rot_matrix_ * solar_pressure_force_icrf_);
  return solar_pressure_torque;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateAtmosphericDragTorque()
//------------------------------------------------------------------------------
/**
  * Calculates the torque vector acting on the spacecraft from atmospheric drag
  * in the spacecraft reference frame.
  *
  * \return Eigen::Vector3d atmospheric drag torque vector acting on the 
  *         spacecraft in the spacecraft body reference frame
  *         \f$[\tau_x, \tau_y, \tau_z]\f$ in \f$[Nm]\f$
  */
Eigen::Vector3d Spacecraft::CalculateAtmosphericDragTorque()
{
  Eigen::Vector3d atmospheric_drag_torque = center_of_mass_offset_.cross
                                           (icrf2bf_rot_matrix_ * atmospheric_drag_force_icrf_);
  return atmospheric_drag_torque;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateMagneticTorque(const double mjd_utc, 
//                                          std::unique_ptr<EarthMagneticField> &magnetic_field,
//                                          const EarthOrientation &earth_orientation)
//------------------------------------------------------------------------------
/**
  * Calculates the torque vector acting on the spacecraft from the earths 
  * magnetic field and the spacecrafts magnetic field in the spacecraft 
  * reference frame.
  *
  * \return Eigen::Vector3d magnetic field torque vector acting on the 
  *         spacecraft in the spacecraft body reference frame
  *         \f$[\tau_x, \tau_y, \tau_z]\f$ in \f$[Nm]\f$
  */
Eigen::Vector3d Spacecraft::CalculateMagneticTorque(const double mjd_utc, 
                                                    std::unique_ptr<EarthMagneticField> &magnetic_field,
                                                    const EarthOrientation &earth_orientation)
{
  Eigen::Vector3d magnetic_moment = static_magnetic_moment_;

  for(auto &magnetorquer : magnetorquers_)
  {
    magnetic_moment = magnetic_moment + magnetorquer.CalculateMagneticMoment();
  }

  Eigen::Vector3d magnetic_field_itrf = magnetic_field->CalculateMagneticField(lat_lon_position_, mjd_utc);
  Eigen::Vector3d magnetic_field_icrf = earth_orientation.ITRS2ICRS(magnetic_field_itrf); 
  Eigen::Vector3d magnetic_field_bf = icrf2bf_rot_matrix_ * magnetic_field_icrf;
  Eigen::Vector3d magnetic_torque = magnetic_moment.cross(magnetic_field_bf);
  return magnetic_torque;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateThrustersTorque()
//------------------------------------------------------------------------------
/**
  * Calculates the torque vector acting on the spacecraft from thruster 
  * operation in the spacecraft reference frame.
  *
  * \return Eigen::Vector3d thruster torque vector acting on the 
  *         spacecraft in the spacecraft body reference frame
  *         \f$[\tau_x, \tau_y, \tau_z]\f$ in \f$[Nm]\f$
  */
Eigen::Vector3d Spacecraft::CalculateThrustersTorque()
{
  Eigen::Vector3d thrusters_torque = {0, 0, 0};

  for(auto &thruster : thrusters_)
  {
    thrusters_torque = thrusters_torque + thruster.getThrusterTorque();
  }

  return thrusters_torque;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateThrustersForce()
//------------------------------------------------------------------------------
Eigen::Vector3d Spacecraft::CalculateThrustersForce()
{
  Eigen::Vector3d thrusters_force = {0, 0, 0};

  for(auto &thruster : thrusters_)
  {
    thrusters_force = thrusters_force + thruster.getThrusterForce();
  }

  return bf2icrf_rot_matrix_ * thrusters_force;
}

//------------------------------------------------------------------------------
//  double CalculateThrusterMassFlowRate()
//------------------------------------------------------------------------------
double Spacecraft::CalculateThrusterMassFlowRate()
{
  double mass_flow_rate = 0;
  for(auto &thruster : thrusters_)
  {
    mass_flow_rate = mass_flow_rate + thruster.getMassFlowRate();
  }
  return mass_flow_rate;
}

//------------------------------------------------------------------------------
//  void CalculateNewAttitude(const double timestep)
//------------------------------------------------------------------------------
/** Calculates the new attitude quaternion for the spacecraft given an 
  * incremental timestep using equation:
  * 
  * \f$ q_{n+1} = [cos(\frac{\|\omega\| \Delta t}{2}) I 
  * + \frac{1}{\|\omega\|} sin(\frac{\|\omega\| \Delta t}{2}) \Omega_{n}] q_{n}\f$
  * 
  * \f$\Omega_{n} = \begin{pmatrix} 
  *                   0         & \omega_z &-\omega_y &\omega_x \\
  *                   -\omega_z & 0        & \omega_x &\omega_y \\
  *                    \omega_y &-\omega_x & 0        &\omega_z \\
  *                   -\omega_x &-\omega_y &-\omega_z &0
  *                 \end{pmatrix} \f$
  * 
  * \param timestep Time in seconds to propagate forward.
  */
void Spacecraft::CalculateNewAttitude(const double timestep)
{
  if(angular_vel_bf_.norm() != 0)
  {
    double cosarg = cos(angular_vel_bf_.norm()  * timestep / 2);
    double sinarg = (1 / angular_vel_bf_.norm()) * sin(angular_vel_bf_.norm() * timestep / 2);

    Eigen::Quaterniond new_quaternion;
    new_quaternion.w() =   cosarg                      * quaternion_.w()
                         + sinarg * angular_vel_bf_(2) * quaternion_.x()
                         - sinarg * angular_vel_bf_(1) * quaternion_.y()
                         + sinarg * angular_vel_bf_(0) * quaternion_.z();
    new_quaternion.x() = - sinarg * angular_vel_bf_(2) * quaternion_.w()
                         + cosarg                      * quaternion_.x()
                         + sinarg * angular_vel_bf_(0) * quaternion_.y()
                         + sinarg * angular_vel_bf_(1) * quaternion_.z();
    new_quaternion.y() =   sinarg * angular_vel_bf_(1) * quaternion_.w()
                         - sinarg * angular_vel_bf_(0) * quaternion_.x()
                         + cosarg                      * quaternion_.y()
                         + sinarg * angular_vel_bf_(2) * quaternion_.z(); 
    new_quaternion.z() = - sinarg * angular_vel_bf_(0) * quaternion_.w()
                         - sinarg * angular_vel_bf_(1) * quaternion_.x()
                         - sinarg * angular_vel_bf_(2) * quaternion_.y()
                         + cosarg                      * quaternion_.z();

    quaternion_ = new_quaternion.normalized();
  }
}

} // end namespace polaris