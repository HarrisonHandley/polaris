//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Magnetorquer Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_MAGNETORQUER_H_
#define POLARIS_MAGNETORQUER_H_

#include <string>

#include "../lib/eigen3/Eigen/Dense"

namespace polaris {

/** \brief Class definition for the Magnetorquer actuator class.
  * 
  * This class represents one magnetorquer actuator used for torque
  * attitude control of a spacecraft. This class maintains the state of the 
  * magnetorquer in the spacecraft body reference frame. 
  */  

class Magnetorquer
{
public:
  Magnetorquer(const Eigen::Vector3d &max_magnetic_moment);
  Magnetorquer(const Magnetorquer    &mt);
  ~Magnetorquer();
  Magnetorquer& operator=(const Magnetorquer &mt);

  std::string     getMode()           const;
  Eigen::Vector3d getMagneticMoment() const;
  double          getOperatingLoad()  const;

  void            setOperatingFraction(const double load);
  Eigen::Vector3d CalculateMagneticMoment();

private:
  std::string     mode_;
  Eigen::Vector3d magnetic_moment_;
  Eigen::Vector3d max_magnetic_moment_;
  double          load_;
};

} // end namespace polaris

#endif  //  POLARIS_MAGNETORQUER_H_