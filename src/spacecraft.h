//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Spacecraft Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_SPACECRAFT_H_
#define POLARIS_SPACECRAFT_H_

#include "satellite.h"

#include <memory>
#include <vector>

// #include "../lib/eigen3/Eigen/Dense"

#include "earth_magnetic_field.h"
#include "earth_orientation.h"
#include "magnetorquer.h"
#include "reaction_wheel.h"
#include "thruster.h"

namespace polaris {

/** \brief Class definition for the Spacecraft class.
  * 
  * This class defines a spacecraft, the class inherits from Satellite object. 
  * This class maintains the attitude of the spacecraft, ranging from attitude, 
  * angular velocity, angular acceleration, and applied torques in the 
  * spacecraft body referenceframe. 
  */ 

class Spacecraft : public Satellite
{
 public:
  Spacecraft(const std::string                &satellite_id,
             const Eigen::Vector3d            &icrf_pos, 
             const Eigen::Vector3d            &icrf_vel, 
             const double                      mass, 
             const double                      cross_section, 
             const double                      drag_coeff, 
             const double                     radiation_pressure_coeff,
             const Eigen::Matrix3d            &inertia_matrix,
             const Eigen::Vector3d            &center_of_mass_offset,
             const Eigen::Vector3d            &static_magnetic_moment,
             const Eigen::Quaterniond         &quaternion,
             const Eigen::Vector3d            &angular_velocity_bf,
             const std::vector<Magnetorquer>  &magnetorquers,
             const std::vector<ReactionWheel> &reaction_wheels,
             const std::vector<Thruster>      &thrusters);
  Spacecraft(const Spacecraft &sc);
  ~Spacecraft();
  Spacecraft& operator=(const Spacecraft &sc);

  Eigen::Quaterniond            getAttitude()                     const override;
  Eigen::Vector3d               getAngularVelocity()              const override;
  Eigen::Vector3d               getAngularAcceleration()          const override;
  Eigen::Vector3d               getAtmosphericDragTorque()        const override;
  Eigen::Vector3d               getGravityGradientTorque()        const override;
  Eigen::Vector3d               getMagneticTorque()               const override;
  Eigen::Vector3d               getSolarPressureTorque()          const override;
  Eigen::Vector3d               getTotalThrusterTorque()          const override;
  Eigen::Vector3d               getAppliedTorque()                const override;
  double                        getNumberOfMagnetorquers()        const override;
  double                        getNumberOfReactionWheels()       const override;
  double                        getNumberOfThrusters()            const override;
  std::vector<std::string>      getMagnetorquersState()           const override;
  std::vector<Eigen::Vector3d>  getMagnetorquersMagneticMoment()  const override;
  std::vector<double>           getMagnetorquersOperatingLoad()   const override;
  std::vector<std::string>      getReactionWheelsState()          const override;
  std::vector<double>           getReactionWheelsAngularVel()     const override;
  std::vector<double>           getReactionWheelsAngularAccel()   const override;  
  std::vector<double>           getReactionWheelsMotorTorque()    const override;
  std::vector<double>           getThrustersThrottle()            const override;
  std::vector<Eigen::Vector3d>  getThrustersForce()               const override;
  std::vector<Eigen::Vector3d>  getThrustersTorque()              const override;  
  std::vector<double>           getThrustersMassFlowRate()        const override;
  Eigen::Vector3d               getAngularMomentum()              const override;
  Eigen::Vector3d               getAppliedAngularMomentumDelta()  const override;
  Eigen::Vector3d               getRWAngularMomentum()            const override;         

  void setMagnetorquersLoad        (const Eigen::VectorXd &operating_fraction) override;
  void setReactionWheelsMotorTorque(const Eigen::VectorXd &motor_torque) override;
  void setThrustersThrottle        (const Eigen::VectorXd &throttle) override;

  void InitializeAttitudeRefFrames() override;
  void CalculateAttitudeState(const double mjd_utc, 
                              std::unique_ptr<EarthMagneticField> &magnetic_field,
                              const EarthOrientation &earth_orientation) override;
  void PropagateAttitude(const double timestep) override;


 private:
  Eigen::Matrix3d             inertia_matrix_;
  Eigen::Vector3d             center_of_mass_offset_;
  Eigen::Vector3d             static_magnetic_moment_;
  Eigen::Vector3d             sc_magnetic_moment_;  
  Eigen::Quaterniond          quaternion_;
  Eigen::Vector3d             angular_vel_bf_;
  Eigen::Vector3d             angular_accel_bf_; 
  Eigen::Vector3d             sc_angular_momentum_; 
  Eigen::Vector3d             applied_angular_momentum_delta_;
  Eigen::Vector3d             rw_angular_momentum_;
  Eigen::Vector3d             applied_rw_motor_torque_;
  std::vector<Magnetorquer>   magnetorquers_;
  std::vector<ReactionWheel>  reaction_wheels_;
  std::vector<Thruster>       thrusters_;
  Eigen::Matrix3d             bf2icrf_rot_matrix_;
  Eigen::Matrix3d             icrf2bf_rot_matrix_; 
  Eigen::Vector3d             atmospheric_drag_torque_;
  Eigen::Vector3d             gravity_gradient_torque_;
  Eigen::Vector3d             magnetic_torque_;
  Eigen::Vector3d             solar_pressure_torque_;
  Eigen::Vector3d             thruster_torque_; 
  Eigen::Vector3d             applied_torque_;

  Eigen::Matrix3d CalculateBF2ICRFRotationMatrix();

  Eigen::Vector3d CalculateAppliedTorque(const double mjd_utc, 
                                         std::unique_ptr<EarthMagneticField> &magnetic_field,
                                         const EarthOrientation &earth_orientation);
  Eigen::Vector3d CalculateGravityGradientTorque();
  Eigen::Vector3d CalculateSolarPressureTorque();
  Eigen::Vector3d CalculateAtmosphericDragTorque();
  Eigen::Vector3d CalculateMagneticTorque(const double mjd_utc, 
                                          std::unique_ptr<EarthMagneticField> &magnetic_field,
                                          const EarthOrientation &earth_orientation);
  Eigen::Vector3d CalculateThrustersTorque();
  Eigen::Vector3d CalculateThrustersForce();
  
  double CalculateThrusterMassFlowRate();
  void CalculateNewAttitude(const double timestep);
};

} // end namespace polaris

#endif  //  POLARIS_SPACECRAFT_H_