//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  JPLEphemerides Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "jpl_ephemerides.h"

#include <fstream>
#include <iostream>

#include "constants.h"

namespace polaris {

//------------------------------------------------------------------------------
//  JPLEphemerides(const std::string &header_filepath, 
//                 const std::string &coeff_filepath,
//                 const std::string &model_name)
//------------------------------------------------------------------------------
/** Default constructor requiring JPL Ephemerides model configuration data.
  *
  * \param header_filepath JPL Ephemerides model header filepath.
  * \param coeff_filepath JPL Ephemerides model coefficients filepath.
  * \param model_name Name of the planetary pertubration model used.
  */
JPLEphemerides::JPLEphemerides(const std::string &header_filepath, 
                               const std::string &coeff_filepath,
                               const std::string &model_name) : 
                               PlanetaryPerturbations(model_name),
                               ephemeris_table_                (Eigen::ArrayXXd::Zero(0, 0)),
                               ephemeris_interval_size_jd_     (0),
                               interval_time_                  (0),
                               interval_coefficients_          (Eigen::VectorXd::Zero(0)),
                               sun_acceleration_icrf_          (Eigen::Vector3d::Zero()),
                               mercury_acceleration_icrf_      (Eigen::Vector3d::Zero()),
                               venus_acceleration_icrf_        (Eigen::Vector3d::Zero()),
                               moon_acceleration_icrf_         (Eigen::Vector3d::Zero()),
                               mars_acceleration_icrf_         (Eigen::Vector3d::Zero()),
                               jupiter_acceleration_icrf_      (Eigen::Vector3d::Zero()),
                               saturn_acceleration_icrf_       (Eigen::Vector3d::Zero()),
                               uranus_acceleration_icrf_       (Eigen::Vector3d::Zero()),
                               neptune_acceleration_icrf_      (Eigen::Vector3d::Zero()),
                               pluto_acceleration_icrf_        (Eigen::Vector3d::Zero()),
                               mercury_constants_              (EphemerisConstants()),
                               venus_constants_                (EphemerisConstants()),
                               earth_moon_constants_           (EphemerisConstants()),
                               mars_constants_                 (EphemerisConstants()),
                               jupiter_constants_              (EphemerisConstants()),
                               saturn_constants_               (EphemerisConstants()),
                               uranus_constants_               (EphemerisConstants()),
                               neptune_constants_              (EphemerisConstants()),
                               pluto_constants_                (EphemerisConstants()),
                               moon_constants_                 (EphemerisConstants()),
                               sun_constants_                  (EphemerisConstants()),
                               earth_nutations_constants_      (EphemerisConstants()),
                               lunar_libration_constants_      (EphemerisConstants()),
                               lunar_mantle_velocity_constants_(EphemerisConstants()),
                               tt_tdb_constants_               (EphemerisConstants())
{
  ReadJPLEphemerisHeaderFile(header_filepath);
  ephemeris_table_ = ReadJPLEphemerisCoefficientsFile(coeff_filepath);
}

//------------------------------------------------------------------------------
//  JPLEphemerides(const JPLEphemerides &model) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param model The object being copied.
  */
JPLEphemerides::JPLEphemerides(const JPLEphemerides &model) :
                               PlanetaryPerturbations(model),
                               ephemeris_table_                (model.ephemeris_table_),
                               ephemeris_interval_size_jd_     (model.ephemeris_interval_size_jd_),
                               interval_time_                  (model.interval_time_),
                               interval_coefficients_          (model.interval_coefficients_),
                               sun_acceleration_icrf_          (model.sun_acceleration_icrf_),
                               mercury_acceleration_icrf_      (model.mercury_acceleration_icrf_),
                               venus_acceleration_icrf_        (model.venus_acceleration_icrf_),
                               moon_acceleration_icrf_         (model.moon_acceleration_icrf_),
                               mars_acceleration_icrf_         (model.mars_acceleration_icrf_),
                               jupiter_acceleration_icrf_      (model.jupiter_acceleration_icrf_),
                               saturn_acceleration_icrf_       (model.saturn_acceleration_icrf_),
                               uranus_acceleration_icrf_       (model.uranus_acceleration_icrf_),
                               neptune_acceleration_icrf_      (model.neptune_acceleration_icrf_),
                               pluto_acceleration_icrf_        (model.pluto_acceleration_icrf_),
                               mercury_constants_              (model.mercury_constants_),
                               venus_constants_                (model.venus_constants_),
                               earth_moon_constants_           (model.earth_moon_constants_),
                               mars_constants_                 (model.mars_constants_),
                               jupiter_constants_              (model.jupiter_constants_),
                               saturn_constants_               (model.saturn_constants_),
                               uranus_constants_               (model.uranus_constants_),
                               neptune_constants_              (model.neptune_constants_),
                               pluto_constants_                (model.pluto_constants_),
                               moon_constants_                 (model.moon_constants_),
                               sun_constants_                  (model.sun_constants_),
                               earth_nutations_constants_      (model.earth_nutations_constants_),
                               lunar_libration_constants_      (model.lunar_libration_constants_),
                               lunar_mantle_velocity_constants_(model.lunar_mantle_velocity_constants_),
                               tt_tdb_constants_               (model.tt_tdb_constants_)
{

}

//------------------------------------------------------------------------------
//  ~JPLEphemerides() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
JPLEphemerides::~JPLEphemerides()
{

}

//------------------------------------------------------------------------------
//  JPLEphemerides& operator=(const JPLEphemerides &model) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param model The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
JPLEphemerides& JPLEphemerides::operator=(const JPLEphemerides &model)
{
  if(&model == this)
  {
    return *this;
  }
  PlanetaryPerturbations::operator = (model),

  ephemeris_table_                 = model.ephemeris_table_;
  ephemeris_interval_size_jd_      = model.ephemeris_interval_size_jd_;
  interval_time_                   = model.interval_time_;
  interval_coefficients_           = model.interval_coefficients_;
  sun_acceleration_icrf_           = model.sun_acceleration_icrf_;
  mercury_acceleration_icrf_       = model.mercury_acceleration_icrf_;
  venus_acceleration_icrf_         = model.venus_acceleration_icrf_;
  moon_acceleration_icrf_          = model.moon_acceleration_icrf_;
  mars_acceleration_icrf_          = model.mars_acceleration_icrf_;
  jupiter_acceleration_icrf_       = model.jupiter_acceleration_icrf_;
  saturn_acceleration_icrf_        = model.saturn_acceleration_icrf_;
  uranus_acceleration_icrf_        = model.uranus_acceleration_icrf_;
  neptune_acceleration_icrf_       = model.neptune_acceleration_icrf_;
  pluto_acceleration_icrf_         = model.pluto_acceleration_icrf_;
  mercury_constants_               = model.mercury_constants_;
  venus_constants_                 = model.venus_constants_;
  earth_moon_constants_            = model.earth_moon_constants_;
  mars_constants_                  = model.mars_constants_;
  jupiter_constants_               = model.jupiter_constants_;
  saturn_constants_                = model.saturn_constants_;
  uranus_constants_                = model.uranus_constants_;
  neptune_constants_               = model.neptune_constants_;
  pluto_constants_                 = model.pluto_constants_;
  moon_constants_                  = model.moon_constants_;
  sun_constants_                   = model.sun_constants_;
  earth_nutations_constants_       = model.earth_nutations_constants_;
  lunar_libration_constants_       = model.lunar_libration_constants_;
  lunar_mantle_velocity_constants_ = model.lunar_mantle_velocity_constants_;
  tt_tdb_constants_                = model.tt_tdb_constants_;
  return *this;
}

//------------------------------------------------------------------------------
//  void CalculatePlanetaryPositions(const double mjd_tdb) 
//------------------------------------------------------------------------------
/** Inherited method from abstract class PlanetaryPertubrations that is 
  * overriden to calculate the position of all planetary bodies with earth 
  * origin at the given UTC modified julian date in \f$[m]\f$ in ICRF. Using the
  * JPL Ephemerides to calculate the position of each body using linear 
  * interpolation between the time based coefficients. All planetary bodies 
  * positions are initially calculated with respect to the solar system 
  * barycenter in ICRF and are converted to Earth origin using vector 
  * subtraction.
  * 
  * \param mjd_tdb UTC modified julian date.
  */
void JPLEphemerides::CalculatePlanetaryPositions(const double mjd_tdb)
{
  double jd_tdb = mjd_tdb + kMjdEpoch;
  double row_num = std::floor((jd_tdb - ephemeris_table_(0, 0)) / ephemeris_interval_size_jd_);
  interval_coefficients_ = ephemeris_table_.row(row_num);
  interval_time_ = mjd_tdb + kMjdEpoch - interval_coefficients_(0);

  moon_position_icrf_ = CalculateMoonPosition(mjd_tdb);
  earth_position_icrf_barycenter_ = CalculateEarthPosition(mjd_tdb) - kEarthMoonRatio * moon_position_icrf_;
 
  sun_position_icrf_     = CalculateSunPosition(mjd_tdb)     - earth_position_icrf_barycenter_;
  mercury_position_icrf_ = CalculateMercuryPosition(mjd_tdb) - earth_position_icrf_barycenter_;
  venus_position_icrf_   = CalculateVenusPosition(mjd_tdb)   - earth_position_icrf_barycenter_;
  mars_position_icrf_    = CalculateMarsPosition(mjd_tdb)    - earth_position_icrf_barycenter_;
  jupiter_position_icrf_ = CalculateJupiterPosition(mjd_tdb) - earth_position_icrf_barycenter_;
  saturn_position_icrf_  = CalculateSaturnPosition(mjd_tdb)  - earth_position_icrf_barycenter_;
  neptune_position_icrf_ = CalculateNeptunePosition(mjd_tdb) - earth_position_icrf_barycenter_;
  uranus_position_icrf_  = CalculateUranusPosition(mjd_tdb)  - earth_position_icrf_barycenter_;
  pluto_position_icrf_   = CalculatePlutoPosition(mjd_tdb)   - earth_position_icrf_barycenter_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &icrf_pos) 
//------------------------------------------------------------------------------
/** Inherited method from abstract class PlanetaryPertubrations that is 
  * overriden to calculate the acceleration acting on the satellite from all 
  * planetary bodies in ICRF. Using JPL Ephemerides to calculate the planetary
  * positions.
  * 
  * \param icrf_pos Position vector of the satellite in ICRF \f$[m]\f$. 
  */
Eigen::Vector3d JPLEphemerides::getAccelICRF(const Eigen::Vector3d &icrf_pos)
{
  sun_acceleration_icrf_     = CalculatePointMassAcceleration(icrf_pos, sun_position_icrf_, sun_gm_);
  mercury_acceleration_icrf_ = CalculatePointMassAcceleration(icrf_pos, mercury_position_icrf_, mercury_gm_);
  venus_acceleration_icrf_   = CalculatePointMassAcceleration(icrf_pos, venus_position_icrf_, venus_gm_);
  moon_acceleration_icrf_    = CalculatePointMassAcceleration(icrf_pos, moon_position_icrf_, moon_gm_);
  mars_acceleration_icrf_    = CalculatePointMassAcceleration(icrf_pos, mars_position_icrf_, mars_gm_);
  jupiter_acceleration_icrf_ = CalculatePointMassAcceleration(icrf_pos, jupiter_position_icrf_, jupiter_gm_);
  saturn_acceleration_icrf_  = CalculatePointMassAcceleration(icrf_pos, saturn_position_icrf_, saturn_gm_);
  uranus_acceleration_icrf_  = CalculatePointMassAcceleration(icrf_pos, uranus_position_icrf_, uranus_gm_);
  neptune_acceleration_icrf_ = CalculatePointMassAcceleration(icrf_pos, neptune_position_icrf_, neptune_gm_);
  pluto_acceleration_icrf_   = CalculatePointMassAcceleration(icrf_pos, pluto_position_icrf_, pluto_gm_);

  Eigen::Vector3d acceleration = sun_acceleration_icrf_ +
                                 mercury_acceleration_icrf_ +
                                 venus_acceleration_icrf_ +
                                 moon_acceleration_icrf_ +
                                 mars_acceleration_icrf_ +
                                 jupiter_acceleration_icrf_ +
                                 saturn_acceleration_icrf_ +
                                 uranus_acceleration_icrf_ +
                                 neptune_acceleration_icrf_ +
                                 pluto_acceleration_icrf_;
  return acceleration;
}

//------------------------------------------------------------------------------
//  void ReadJPLEphemerisHeaderFile(const std::string &filepath) 
//------------------------------------------------------------------------------
/** Parses the JPL Ephemerides header file to extract the parameters required 
  * to configure the model calculations. The extracted parameters are the time 
  * interval between coefficients (Group 1010) and Ephemeris constants for each 
  * orbiting object (Group 1050).
  * 
  * \param filepath JPL Ephemerides coefficients filepath.
  */
void JPLEphemerides::ReadJPLEphemerisHeaderFile(const std::string &filepath)
{
  Eigen::ArrayXXi ephemeris_constants = Eigen::ArrayXXi::Zero(4, 15);
  std::vector<int> values;
  values.reserve(15);

  std::ifstream data(filepath);
  std::string line;

  bool group_1030_flag = false;
  bool group_1050_flag = false;
  int row = 0;

  while(std::getline(data, line))
  {
    if(line.find("GROUP   1030") != std::string::npos)
    {
      group_1030_flag = true;
    }
    else if(line.find("GROUP   1040") != std::string::npos)
    {
      group_1030_flag = false;
    }
    else if(group_1030_flag == true)
    {
      sscanf(line.c_str(), "%*f %*f %i", &ephemeris_interval_size_jd_);
    }
    
    if(line.find("GROUP   1050") != std::string::npos)
    {
      group_1050_flag = true;
    }
    else if(line.find("GROUP   1070") != std::string::npos)
    {
      group_1050_flag = false;
    }
    else if(group_1050_flag == true)
    {
      if(line.find_first_not_of(' ') != std::string::npos)
      {
        sscanf(line.c_str(), "%i %i %i %i %i %i %i %i %i %i %i %i %i %i %i", &values[0],  &values[1],  
                                                                             &values[2],  &values[3],  
                                                                             &values[4],  &values[5],
                                                                             &values[6],  &values[7],  
                                                                             &values[8],  &values[9],
                                                                             &values[10], &values[11],
                                                                             &values[12], &values[13],
                                                                             &values[14]);
        ephemeris_constants.row(row) = Eigen::Map<Eigen::VectorXi>(values.data(), 15);
        row += 1;
      }
    }
  }
  ephemeris_constants.row(row) << 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 3, 3, 1;

  mercury_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(0)); 
  venus_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(1));
  earth_moon_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(2));
  mars_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(3));
  jupiter_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(4));
  saturn_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(5));
  uranus_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(6));
  neptune_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(7));
  pluto_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(8));
  moon_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(9));
  sun_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(10));
  earth_nutations_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(11));
  lunar_libration_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(12));
  lunar_mantle_velocity_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(13));
  tt_tdb_constants_ = JPLEphemerides::EphemerisConstants(ephemeris_constants.col(14));
}

//------------------------------------------------------------------------------
//  Eigen::ArrayXXd ReadJPLEphemerisCoefficientsFile(const std::string &filepath) const
//------------------------------------------------------------------------------
/** Parses the JPL Ephemerides coefficients file to extract the coefficients
  * for each time interval. The extracted data is returned as a 2D Array. With
  * each row corresponding to the coefficients required to compute the position
  * of the planets at that modified julian date. 
  * 
  * \param filepath JPL Ephemerides coefficients filepath.
  * 
  * \return Eigen::ArrayXXd JPL Ephemerides coefficients as a 2D array. With
  *         each row corresponding to the coefficients for the time interval.
  */
Eigen::ArrayXXd JPLEphemerides::ReadJPLEphemerisCoefficientsFile(const std::string &filepath) const
{
  std::ifstream data(filepath);
  std::string line;

  std::vector<double> values;
  float temp1;
  float temp2;
  float temp3;

  int row = 0;
  while(std::getline(data, line))
  {
    if(line.find("  1018") != std::string::npos)
    {
      row = row + 1;
    }
    else
    {
      std::replace(line.begin(), line.end(), 'D', 'E');
      sscanf(line.c_str(), "%f %f %f", &temp1, &temp2, &temp3);
      values.push_back(static_cast<double>(temp1));
      values.push_back(static_cast<double>(temp2));
      values.push_back(static_cast<double>(temp3));
    }
  }
  return Eigen::Map<Eigen::ArrayXXd>(values.data(), values.size()/row, row).transpose();
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateSunPosition(double mjd_tdb) const
//------------------------------------------------------------------------------
/** Calculates the position vector of the Sun with the solar system barycenter
  * origin in ICRF.
  * 
  * \param mjd_tdb Barycentric dynamical time modified julian date.
  * 
  * \return Eigen::Vector3d Position vector of the Sun with solar system 
  *         barycenter origin in ICRF \f%[m]\f%.
  */
Eigen::Vector3d JPLEphemerides::CalculateSunPosition(double mjd_tdb) const
{
  Eigen::MatrixXd coefficients = FindChebyshevCoefficients(sun_constants_);
  Eigen::Vector3d position = EvaluateChebyshevPolynomial(mjd_tdb, sun_constants_, coefficients) * 1e3;
  return position;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateMercuryPosition(double mjd_tdb) const
//------------------------------------------------------------------------------
/** Calculates the position vector of the Mercury with the solar system barycenter
  * origin in ICRF.
  * 
  * \param mjd_tdb Barycentric dynamical time modified julian date.
  * 
  * \return Eigen::Vector3d Position vector of the Mercury with solar system 
  *         barycenter origin in ICRF \f%[m]\f%.
  */
Eigen::Vector3d JPLEphemerides::CalculateMercuryPosition(double mjd_tdb) const
{
  Eigen::MatrixXd coefficients = FindChebyshevCoefficients(mercury_constants_);
  Eigen::Vector3d position = EvaluateChebyshevPolynomial(mjd_tdb, mercury_constants_, coefficients) * 1e3;
  return position;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateVenusPosition(double mjd_tdb) const
//------------------------------------------------------------------------------
/** Calculates the position vector of the Venus with the solar system barycenter
  * origin in ICRF.
  * 
  * \param mjd_tdb Barycentric dynamical time modified julian date.
  * 
  * \return Eigen::Vector3d Position vector of the Venus with solar system 
  *         barycenter origin in ICRF \f%[m]\f%.
  */
Eigen::Vector3d JPLEphemerides::CalculateVenusPosition(double mjd_tdb) const
{
  Eigen::MatrixXd coefficients = FindChebyshevCoefficients(venus_constants_);
  Eigen::Vector3d position = EvaluateChebyshevPolynomial(mjd_tdb, venus_constants_, coefficients) * 1e3;
  return position;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateEarthPosition(double mjd_tdb) const
//------------------------------------------------------------------------------
/** Calculates the position vector of the Earth with the solar system barycenter
  * origin in ICRF.
  * 
  * \param mjd_tdb Barycentric dynamical time modified julian date.
  * 
  * \return Eigen::Vector3d Position vector of the Earth with solar system 
  *         barycenter origin in ICRF \f%[m]\f%.
  */
Eigen::Vector3d JPLEphemerides::CalculateEarthPosition(double mjd_tdb) const
{
  Eigen::MatrixXd coefficients = FindChebyshevCoefficients(earth_moon_constants_);
  Eigen::Vector3d position = EvaluateChebyshevPolynomial(mjd_tdb, earth_moon_constants_, coefficients) * 1e3;
  return position;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateMoonPosition(double mjd_tdb) const
//------------------------------------------------------------------------------
/** Calculates the position vector of the Moon with the Earth-Moon barycenter
  * origin in ICRF.
  * 
  * \param mjd_tdb Barycentric dynamical time modified julian date.
  * 
  * \return Eigen::Vector3d Position vector of the Moon with Earth-Moon 
  *         barycenter origin in ICRF \f%[m]\f%.
  */
Eigen::Vector3d JPLEphemerides::CalculateMoonPosition(double mjd_tdb) const
{
  Eigen::MatrixXd coefficients = FindChebyshevCoefficients(moon_constants_);
  Eigen::Vector3d position = EvaluateChebyshevPolynomial(mjd_tdb, moon_constants_, coefficients) * 1e3;
  return position;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateMarsPosition(double mjd_tdb) const
//------------------------------------------------------------------------------
/** Calculates the position vector of the Mars with the solar system barycenter
  * origin in ICRF.
  * 
  * \param mjd_tdb Barycentric dynamical time modified julian date.
  * 
  * \return Eigen::Vector3d Position vector of the Mars with solar system 
  *         barycenter origin in ICRF \f%[m]\f%.
  */
Eigen::Vector3d JPLEphemerides::CalculateMarsPosition(double mjd_tdb) const
{
  Eigen::MatrixXd coefficients = FindChebyshevCoefficients(mars_constants_);
  Eigen::Vector3d position = EvaluateChebyshevPolynomial(mjd_tdb, mars_constants_, coefficients) * 1e3;
  return position;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateJupiterPosition(double mjd_tdb) const
//------------------------------------------------------------------------------
/** Calculates the position vector of the Jupiter with the solar system 
  * barycenter origin in ICRF.
  * 
  * \param mjd_tdb Barycentric dynamical time modified julian date.
  * 
  * \return Eigen::Vector3d Position vector of the Jupiter with solar system 
  *         barycenter origin in ICRF \f%[m]\f%.
  */
Eigen::Vector3d JPLEphemerides::CalculateJupiterPosition(double mjd_tdb) const
{
  Eigen::MatrixXd coefficients = FindChebyshevCoefficients(jupiter_constants_);
  Eigen::Vector3d position = EvaluateChebyshevPolynomial(mjd_tdb, jupiter_constants_, coefficients) * 1e3;
  return position;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateSaturnPosition(double mjd_tdb) const
//------------------------------------------------------------------------------
/** Calculates the position vector of the Saturn with the solar system 
  * barycenter origin in ICRF.
  * 
  * \param mjd_tdb Barycentric dynamical time modified julian date.
  * 
  * \return Eigen::Vector3d Position vector of the Saturn with solar system 
  *         barycenter origin in ICRF \f%[m]\f%.
  */
Eigen::Vector3d JPLEphemerides::CalculateSaturnPosition(double mjd_tdb) const
{
  Eigen::MatrixXd coefficients = FindChebyshevCoefficients(saturn_constants_);
  Eigen::Vector3d position = EvaluateChebyshevPolynomial(mjd_tdb, saturn_constants_, coefficients) * 1e3;
  return position;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateUranusPosition(double mjd_tdb) const
//------------------------------------------------------------------------------
/** Calculates the position vector of the Uranus with the solar system 
  * barycenter origin in ICRF.
  * 
  * \param mjd_tdb Barycentric dynamical time modified julian date.
  * 
  * \return Eigen::Vector3d Position vector of the Uranus with solar system 
  *         barycenter origin in ICRF \f%[m]\f%.
  */
Eigen::Vector3d JPLEphemerides::CalculateUranusPosition(double mjd_tdb) const
{
  Eigen::MatrixXd coefficients = FindChebyshevCoefficients(uranus_constants_);
  Eigen::Vector3d position = EvaluateChebyshevPolynomial(mjd_tdb, uranus_constants_, coefficients) * 1e3;
  return position;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateNeptunePosition(double mjd_tdb) const
//------------------------------------------------------------------------------
/** Calculates the position vector of the Neptune with the solar system 
  * barycenter origin in ICRF.
  * 
  * \param mjd_tdb Barycentric dynamical time modified julian date.
  * 
  * \return Eigen::Vector3d Position vector of the Neptune with solar system 
  *         barycenter origin in ICRF \f%[m]\f%.
  */
Eigen::Vector3d JPLEphemerides::CalculateNeptunePosition(double mjd_tdb) const
{
  Eigen::MatrixXd coefficients = FindChebyshevCoefficients(neptune_constants_);
  Eigen::Vector3d position = EvaluateChebyshevPolynomial(mjd_tdb, neptune_constants_, coefficients) * 1e3;
  return position;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculatePlutoPosition(double mjd_tdb) const
//------------------------------------------------------------------------------
/** Calculates the position vector of the Pluto with the solar system 
  * barycenter origin in ICRF.
  * 
  * \param mjd_tdb Barycentric dynamical time modified julian date.
  * 
  * \return Eigen::Vector3d Position vector of the Pluto with solar system 
  *         barycenter origin in ICRF \f%[m]\f%.
  */
Eigen::Vector3d JPLEphemerides::CalculatePlutoPosition(double mjd_tdb) const
{
  Eigen::MatrixXd coefficients = FindChebyshevCoefficients(pluto_constants_);
  Eigen::Vector3d position = EvaluateChebyshevPolynomial(mjd_tdb, pluto_constants_, coefficients) * 1e3;
  return position;
}

//------------------------------------------------------------------------------
//  Eigen::MatrixXd FindChebyshevCoefficients(const EphemerisConstants &orbiting_body) const
//------------------------------------------------------------------------------
/** Calculates the Chebyshev coefficients for a given set of ephemeris 
  * constants. It sets the Chebyshev coefficients based on the current 
  * subinterval.
  *
  * \param orbiting_body Ephemeris constants object for an orbiting body.
  * 
  * \return Eigen::MatrixXd Matrix of Chebyshev coefficients.
  */
Eigen::MatrixXd JPLEphemerides::FindChebyshevCoefficients(const EphemerisConstants &orbiting_body) const
{
  int num_coefficients = orbiting_body.getNumCoefficients();
  Eigen::MatrixXd coefficients (orbiting_body.getDimensionality(), num_coefficients);
  
  int subinterval = floor(interval_time_ / (ephemeris_interval_size_jd_/orbiting_body.getNumSubIntervals()));
  int starting_address = orbiting_body.getStartingAddress() - 1 
                       + subinterval * num_coefficients * orbiting_body.getDimensionality(); 
  
  for(int i = 0; i < orbiting_body.getDimensionality(); ++i)
  {
    coefficients.row(i) = interval_coefficients_.segment(starting_address + i*num_coefficients, 
                                                         num_coefficients);
  }
  return coefficients;
}

//------------------------------------------------------------------------------
//  Eigen::VectorXd EvaluateChebyshevPolynomial(const double mjd_tdb, 
//                                              const EphemerisConstants &orbiting_body, 
//                                              const Eigen::MatrixXd &coefficients) const
//------------------------------------------------------------------------------
/** Evaluates the Chebyshev polynomial at the given time, with the orbiting body
  * ephemeris constants and associated Chebyshev polynomial using linear 
  * interpolation.
  * 
  * \param mjd_tdb Barycentric dynamical time modified julian date.
  * \param orbiting_body Ephemeris constants object for an orbiting body
  * \param coefficients Chebyshev coefficients.
  * 
  * \return Eigen::VectorXd Position vector of orbiting object.
  */
Eigen::VectorXd JPLEphemerides::EvaluateChebyshevPolynomial(const double mjd_tdb, 
                                                            const EphemerisConstants &orbiting_body, 
                                                            const Eigen::MatrixXd &coefficients) const
{
  int subinterval = floor(interval_time_ / (ephemeris_interval_size_jd_/orbiting_body.getNumSubIntervals()));
  double t0 = interval_coefficients_(0) + (ephemeris_interval_size_jd_/orbiting_body.getNumSubIntervals()) 
            * subinterval;
  double t1 = t0 + (ephemeris_interval_size_jd_ / orbiting_body.getNumSubIntervals());
  
  double tau = (2*(mjd_tdb + kMjdEpoch) - t0 - t1) / (t1 - t0);

  Eigen::VectorXd f1 = Eigen::VectorXd::Zero(orbiting_body.getDimensionality());
  Eigen::VectorXd f2 = Eigen::VectorXd::Zero(orbiting_body.getDimensionality());
  Eigen::VectorXd temp = Eigen::VectorXd::Zero(orbiting_body.getDimensionality());

  for(int i = orbiting_body.getNumCoefficients() - 1; i > 0; --i)
  {
    temp = f1;
    f1 = 2 * tau * f1 - f2 + coefficients.col(i);
    f2 = temp;
  }
  f1 = tau * f1 - f2 + coefficients.col(0);
  return f1;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculatePointMassAcceleration(const Eigen::Vector3d &icrf_pos, 
//                                                 const Eigen::Vector3d &mass_icrf_pos, 
//                                                 const double gm) const
//------------------------------------------------------------------------------
/** Calculates the acceleration from a point mass at a given position using 
  * Newtons equation.
  * 
  * \param icrf_pos ICRF position vector of satellite in \f$[m]\f$.
  * \param mass_icrf_pos ICRF position vector of mass in \f$[m]\f$.
  * \param gm Gravitational constant of mass in \f$[m^3 \dot kg^{-1} \dot s^{-2}]\f$.
  * 
  * \return Eigen::VectorXd Position vector of orbiting object.
  */
Eigen::Vector3d JPLEphemerides::CalculatePointMassAcceleration(const Eigen::Vector3d &icrf_pos, 
                                                                     const Eigen::Vector3d &mass_icrf_pos, 
                                                                     const double gm) const
{
  Eigen::Vector3d distance = icrf_pos - mass_icrf_pos;
  Eigen::Vector3d acceleration = -gm * (distance/pow(distance.norm(),3) 
                                      + mass_icrf_pos/pow(mass_icrf_pos.norm(),3));
  return acceleration;
}

} // end namespace polaris