//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  SolarRadiationAnalytical Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "solar_radiation_analytical.h"

#include <iostream>

namespace polaris {

//------------------------------------------------------------------------------
//  SolarRadiationAnalytical(const std::string &model_name, 
//                           const double solar_intensity)
//------------------------------------------------------------------------------
/** Default constructor requiring the analytical solar pressure model 
  * configuration data.
  *
  * \param model_name Name of the solar pressure model used.
  * \param solar_intensity Solar intensity at 1 AU in \f$[W/m^2]\f$.
  */
SolarRadiationAnalytical::SolarRadiationAnalytical(const std::string &model_name, 
                                                   const double solar_intensity) :
                                                   SolarRadiation      (model_name), 
                                                   solar_intensity_1au_(solar_intensity),
                                                   solar_fraction_     (0),
                                                   solar_intensity_    (Eigen::Vector3d::Zero()),
                                                   au_                 (kAu),
                                                   earth_radius_       (kEarthRadius),
                                                   sun_radius_         (kSunRadius),
                                                   moon_radius_        (kMoonRadius)
{

}

//------------------------------------------------------------------------------
//  SolarRadiationAnalytical(const SolarRadiationAnalytical &model) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param model The object being copied.
  */
SolarRadiationAnalytical::SolarRadiationAnalytical(const SolarRadiationAnalytical &model) :
                                                   SolarRadiation      (model),
                                                   solar_intensity_1au_(model.solar_intensity_1au_), 
                                                   solar_fraction_     (model.solar_fraction_),
                                                   solar_intensity_    (model.solar_intensity_),
                                                   au_                 (model.au_), 
                                                   earth_radius_       (model.earth_radius_),
                                                   sun_radius_         (model.sun_radius_),
                                                   moon_radius_        (model.moon_radius_)
{

}

//------------------------------------------------------------------------------
//  ~SolarRadiationAnalytical() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
SolarRadiationAnalytical::~SolarRadiationAnalytical()
{

}

//------------------------------------------------------------------------------
//  SolarRadiationAnalytical& operator=(const SolarRadiationAnalytical &model) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param model The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
SolarRadiationAnalytical& SolarRadiationAnalytical::operator=(const SolarRadiationAnalytical &model)
{
  if(&model == this)
  {
    return *this;
  }
  SolarRadiation::operator = (model),

  solar_intensity_1au_ = model.solar_intensity_1au_;
  solar_fraction_      = model.solar_fraction_; 
  solar_intensity_     = model.solar_intensity_;
  au_                  = model.au_; 
  earth_radius_        = model.earth_radius_;
  sun_radius_          = model.sun_radius_;
  moon_radius_         = model.moon_radius_;
  return *this;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &r_sun,
//                               const Eigen::Vector3d &r_moon, 
//                               const std::shared_ptr<Satellite> &sat) 
//------------------------------------------------------------------------------
/** Inherited method from the abstract class Solar Radiation that is overriden 
  * to calculate the acceleration acting on a satellite from solar pressure at 
  * a given location in ICRF using an analytical model.
  * 
  * The analytical model calculates the acceleration using the following method:
  * 1) Calculates the fraction of visible solar disk due to the earth 
  *    occultation. The location of solar objects is in ICRF with origin at the 
  *    earth. If the earth does not occulted the sun, check for lunar 
  *    occultation.
  * 2) Calculates the acceleration acting on a spacecraft from solar radiation 
  *    pressure. The solar pressure value is multiplied by the fraction of 
  *    visible solar disk.
  *
  * \param r_sun Position vector of the Sun in ICRF with Earth origin 
  *              \f$[x, y, z]\f$ \f$[m]\f$.
  * \param r_moon Position vector of the Moon in ICRF with Earth origin 
  *               \f$[x, y, z]\f$ \f$[m]\f$.
  * \param sat Satellite Class Object required for satellite parameters in the 
  *            calculation (Cross sectional area, radiation pressure 
  *            coefficient, and position).
  * 
  * \return Eigen::Vector3d Acceleration vector acting on satellite in ICRF 
  *         \f$[m/s^2]\f$
  */
Eigen::Vector3d SolarRadiationAnalytical::getAccelICRF(const Eigen::Vector3d &r_sun,
                                                       const Eigen::Vector3d &r_moon, 
                                                       const std::shared_ptr<Satellite> &sat)
{
  Eigen::Vector3d r_earth = Eigen::Vector3d::Zero(3);
  solar_fraction_ = getVisibleSolarFraction(sat->getICRFPosition(), r_sun, r_earth, earth_radius_);
  if(solar_fraction_ == 1)
  {
    solar_fraction_ = getVisibleSolarFraction(sat->getICRFPosition(), r_sun, r_moon, moon_radius_);
  }
  Eigen::Vector3d sat_sun_r = sat->getICRFPosition() - r_sun;
  solar_intensity_ = -solar_fraction_ * solar_intensity_1au_ * (sat_sun_r / pow(sat_sun_r.norm(),3));
  Eigen::Vector3d acceleration = -solar_fraction_ * (solar_intensity_1au_ / kLightspeed)
                               *  sat->getRadiationPressureCoeff() 
                               * (sat->getCrossSection()/sat->getMass()) * (pow(au_,2)) 
                               * (sat_sun_r / pow(sat_sun_r.norm(),3));
  return acceleration;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getSolarIntensityICRF() 
//------------------------------------------------------------------------------
/** Inherited method from the abstract class Solar Radiation that is overriden 
  * to get the solar intensity acting on a satellite from solar pressure at 
  * a given location in ICRF using an analytical model.
  * 
  * \return Eigen::Vector3d Solar intensity vector acting on satellite in ICRF 
  *         \f$[W/m^2]\f$.
  */
Eigen::Vector3d SolarRadiationAnalytical::getSolarIntensityICRF()
{
  return solar_intensity_;
}

//------------------------------------------------------------------------------
//  double getVisibleSolarFraction(const Eigen::Vector3d &r_sat,  
//                                 const Eigen::Vector3d &r_sun,     
//                                 const Eigen::Vector3d &r_body, 
//                                 const double           body_radius) const
//------------------------------------------------------------------------------
/** Calculates the occultation of the sun for an occulting body from the 
  * spacecrafts point of view.
  *
  * \param r_sat Position vector of the satellite in ICRF with Earth origin 
  *              \f$[x, y, z]\f$ \f$[m]\f$.
  * \param r_sun Position vector of the Sun in ICRF with Earth origin 
  *              \f$[x, y, z]\f$ \f$[m]\f$.
  * \param r_body Position vector of the occulting body in ICRF with Earth 
  *               origin \f$[x, y, z]\f$ \f$[m]\f$.
  * \param body_radius Radius of the occulting body in \f$[m]\f$.
  * 
  * \return double Visible solar fraction. A value of 0 indicates no fraction
  *         of the Sun is visible. While a value of 1 indicates complete
  *         visibility.
  */
double SolarRadiationAnalytical::getVisibleSolarFraction(const Eigen::Vector3d &r_sat,  
                                                         const Eigen::Vector3d &r_sun,     
                                                         const Eigen::Vector3d &r_body, 
                                                         const double           body_radius) const
{
  double solar_fraction = 0;
  Eigen::Vector3d s = r_sat - r_body;
  double sun_dia = asin(sun_radius_/(r_sun - r_sat).norm());
  double body_dia = asin(body_radius/s.norm());
  double numerator = (-s.transpose()*(r_sun - r_sat));
  double denominator = (s.norm()*((r_sun - r_sat).norm()));
  double sep_sun_body = acos(numerator / denominator);

  if((sun_dia + body_dia) < sep_sun_body)
  {
    //  In Light
    solar_fraction = 1;
  }
  else if(abs(sun_dia - body_dia) > sep_sun_body)
  {
    //  In Shadow
    solar_fraction = 0;
  }
  else if((abs(sun_dia - body_dia) < sep_sun_body) && (sep_sun_body < (sun_dia + body_dia)))
  {
    double x = (pow(sep_sun_body,2) + pow(sun_dia,2) - pow(body_dia,2)) / (2*sep_sun_body);
    double y = sqrt(pow(sun_dia,2) - pow(x,2));
    // double occulted_area = pow(sun_dia,2) * sun_dia * cos(x/sun_dia) + pow(body_dia,2) 
    //                      * sun_dia * cos((sep_sun_body - x)/body_dia) - sep_sun_body * y;
    double occulted_area = pow(sun_dia,2) * acos(x/sun_dia) + pow(body_dia,2) 
                         * acos((sep_sun_body - x)/body_dia) - sep_sun_body * y;    
    solar_fraction = 1 - occulted_area/(kPI * pow(sun_dia,2));
    if(solar_fraction > 1)
    {
      std::cout << "Error Solar Fraction above 1!\n";
    }
  }
  return solar_fraction;
}

} // end namespace polaris