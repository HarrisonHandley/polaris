//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  HarrisPriester Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_HARRISPRIESTER_H_
#define POLARIS_HARRISPRIESTER_H_

#include "atmospheric_model.h"

#include "../lib/eigen3/Eigen/Dense"


#include "earth_orientation.h"

namespace polaris {

/** \brief Class header defining the implementation of the HarrisPriester class.
  * 
  * The HarrisPriester class is a derived class from the abstract 
  * AtmosphericModel Class defined in atmospheric_mode.h. The HarrisPriester 
  * class implements the Harris Priester Atmospheric Model and is accessed 
  * through the virtual function "CalculateAtmosphericDensity" via 
  * polymorphism. The members of the class contains all the relevant data 
  * required to compute the HarrisPriester Atmospheric Model for a set position 
  * in the True Of Date reference frame.  
  */

class HarrisPriester : public AtmosphericModel
{
 public:
  HarrisPriester(const std::string &hp_coeff_filepath, 
                 const std::string &model_name, 
                 const int hp_parameter, 
                 const double min_altitude,
                 const double max_altitude,
                 const int f107);
  HarrisPriester(const HarrisPriester &model);
  ~HarrisPriester();
  HarrisPriester& operator=(const HarrisPriester &model);                 

  double CalculateAtmosphericDensity(const Eigen::Vector3d  &r_sun,
                                     const Eigen::Vector3d  &r_sat,
                                     const EarthOrientation &earth_orientation) const override;
  
  void setF107(const int f107);

  void setHPParameter(const int hp_parameter);
                      
 private:
  int hp_parameter_;  // 2 for low inclination, 6 for polar
  int min_altitude_;
  int max_altitude_;
  int f107_index_;

  Eigen::MatrixXd harris_priester_coeffs_;
  Eigen::VectorXd h_coeffs_;      //  Height Coefficients 
  Eigen::VectorXd c_min_coeffs_;  //  Anatapex Density Coefficients
  Eigen::VectorXd c_max_coeffs_;  // Apex Density Coefficient

  void InitializeHarrisPriesterCoefficients(const int f107_index);
  double CalculateHPModelDensity(const Eigen::Vector3d &r_sun,
                                 const Eigen::Vector3d &r_sat, 
                                 const double           altitude) const;
  double CalculateTrueAltitude(const Eigen::Vector3d &ecef_pos) const;

  Eigen::MatrixXd getCoefficientsFromCSVFile(const std::string &filepath) const;

  Eigen::Vector3d getSphericalCoordinates(const Eigen::Vector3d &xyz_vec) const;
};

} // end namespace polaris

#endif  // POLARIS_HARRISPRIESTER_H_