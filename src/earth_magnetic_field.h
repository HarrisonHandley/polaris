//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  EarthMagneticField Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_EARTHMAGNETICFIELD_H_
#define POLARIS_EARTHMAGNETICFIELD_H_

#include <string>

#include "../lib/eigen3/Eigen/Dense"


namespace polaris {

/** \brief Abstract class header defining the EarthMagneticField class.
  * 
  * All earth magnetic field model implementations must be derived from this 
  * class for inheritance/polymorphism of the virtual CalculateMagneticField 
  * method.
  */ 

class EarthMagneticField
{
 public:
  EarthMagneticField(const std::string &model_name);
  EarthMagneticField(const EarthMagneticField &model);
  virtual ~EarthMagneticField();
  EarthMagneticField& operator=(const EarthMagneticField &model);

  std::string getModelName() const;

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateMagneticField(const Eigen::Vector3d &latlon_position,
//                                         const double mjd_utc)
//------------------------------------------------------------------------------
/** Virtual method for calculting the Earth's magnetic field at location of 
  * spacecraft in ITRF Lat/Lon.
  * 
  * \param latlon_position Eigen::Vector3d Position of the satellite in ITRF 
  *                        in Latitude/Longitude \f$[Lat, Lon, Alt]\f$ 
  *                        \f$[rad]\f$ and \f$[m]\f$.
  * \param mjd_utc double UTC modified julian date.
  * 
  * \return Eigen::Vector3d Earth magnetic field vector at satellite position
  *         \f$[M_x, M_y, M_z]\f$ \f$[T]\f$
  */
  virtual Eigen::Vector3d CalculateMagneticField(const Eigen::Vector3d &latlon_position,
                                                 const double           mjd_utc) = 0;
  
 protected:
  std::string model_name_;
};

} // end namespace polaris

#endif  // POLARIS_EARTHMAGNETICFIELD_H_