//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  IGRF Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_IGRF_H_
#define POLARIS_IGRF_H_

#include "earth_magnetic_field.h"

#include <string>

#include "../lib/eigen3/Eigen/Dense"

namespace polaris {

/** \brief Class header defining the implementation of the IGRF class.
  * 
  * The IGRF class is a derived class from the abstract 
  * EarthMagneticField Class defined in earth_magnetic_field.h. The IGRF 
  * class implements the IGRF earth magnetic field Model and is accessed 
  * through the virtual function "CalculateMagneticField" via 
  * polymorphism. The members of the class contains all the relevant data 
  * required to compute the IGRF earth magnetic field model for a set position 
  * in ECEF Lat/Lon reference frame.  
  */  

class Igrf : public EarthMagneticField
{
 public:
  Igrf(const std::string &filepath, 
       const std::string &model_name,  
       const int          max_degree);
  Igrf(const Igrf &model);
  ~Igrf();
  Igrf& operator=(const Igrf &model);
  
  Eigen::Vector3d CalculateMagneticField(const Eigen::Vector3d &latlon_position, 
                                         const double           mjd_utc) override;
  
 private:
  Eigen::ArrayXXd g2000_;
  Eigen::ArrayXXd h2000_;
  Eigen::ArrayXXd g2005_;
  Eigen::ArrayXXd h2005_;
  Eigen::ArrayXXd g2010_;
  Eigen::ArrayXXd h2010_;
  Eigen::ArrayXXd g2015_;
  Eigen::ArrayXXd h2015_;
  Eigen::ArrayXXd g2020_;
  Eigen::ArrayXXd h2020_;
  Eigen::ArrayXXd gsv_;
  Eigen::ArrayXXd hsv_;
  int             max_degree_;

  Eigen::ArrayXXd h_;
  Eigen::ArrayXXd g_;
  double          delta_t_;
  Eigen::ArrayXXd legendre_polynomials_;
  Eigen::ArrayXXd differential_legendre_polynomials_;

  void ReadIGRFCoefficientsFile(const std::string &filepath);
  void InitializeLegendrePolynomials(const double angle);
  void CalculateCoefficients(const double mjd_utc);
};

} // end namespace polaris

#endif  // POLARIS_IGRF_H_