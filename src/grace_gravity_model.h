//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  GraceGravityModel Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_GRACEGRAVITYMODEL_H_
#define POLARIS_GRACEGRAVITYMODEL_H_

#include "earth_gravity_field.h"

namespace polaris {

/** \brief Class header defining the implementation of the GraceGravityModel 
  *        class.
  * 
  * The GraceGravityModel class is a derived class from the abstract 
  * EarthGravityField class defined in earth_gravity_field.h. The 
  * GraceGravityModel class implements the Grace Gravity Model and is accessed
  * through the virtual function "getEarthGravityAccel" via polymorphism. The 
  * members of the GraceGravityModel class contains all the relevant data 
  * required to compute the Grace Gravity Model for a position in 
  * ITRF reference frame.
  */ 

class GraceGravityModel : public EarthGravityField
{
 public:
  GraceGravityModel(const std::string &filepath, 
                    const std::string &model_name, 
                    const int          max_degree);
  GraceGravityModel(const GraceGravityModel &model);
  ~GraceGravityModel();
  GraceGravityModel& operator=(const GraceGravityModel &model);                    

  Eigen::Vector3d getAccelITRF(const Eigen::Vector3d &itrf_pos) override;

 private:
  int             applied_max_degree_;
  int             model_max_degree_;
  double          earth_radius_;
  double          earth_gm_;
  Eigen::ArrayXXd cnm_;
  Eigen::ArrayXXd snm_;
  Eigen::ArrayXXd legendre_polynomials_;
  Eigen::ArrayXXd differential_legendre_polynomials_;

  void setGravitationalModelFileParameters(const std::string &filepath);
  void setGravitationalModelFileCoefficients(const std::string &filepath);

  void InitializeLegendrePolynomials(const double angle);
  Eigen::Vector3d getSphericalCoordinates(const Eigen::Vector3d &cart_vec) const;
};

#endif  //  POLARIS_GRACEGRAVITYMODEL_H_

} // end namespace polaris