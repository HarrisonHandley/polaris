//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Magnetorquer Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "magnetorquer.h"

namespace polaris {

//------------------------------------------------------------------------------
//  Magnetorquer(const Eigen::Vector3d &max_magnetic_moment)
//------------------------------------------------------------------------------
/** Default constructor requiring configuration of magnetorquer.
  *
  * \param max_magnetic_moment Maximum magnetic moment of magnetorquer as vector 
  *                            \f$[x, y, z]\f$ in spacecraft body reference frame
  *                            \f$[A \cdot m^2]\f$.
  */
Magnetorquer::Magnetorquer(const Eigen::Vector3d &max_magnetic_moment) :
                           mode_("Operational"),
                           magnetic_moment_(Eigen::Vector3d::Zero()),                           
                           max_magnetic_moment_(max_magnetic_moment),
                           load_(0)
{

}

//------------------------------------------------------------------------------
//  Magnetorquer(const Magnetorquer &mt) 
//------------------------------------------------------------------------------
/** Copy constructor. 
  * 
  * \param mt The object being copied.
  */
Magnetorquer::Magnetorquer(const Magnetorquer &mt) :
                           mode_(mt.mode_),
                           magnetic_moment_(mt.magnetic_moment_),                           
                           max_magnetic_moment_(mt.max_magnetic_moment_),
                           load_(mt.load_)
{

}

//------------------------------------------------------------------------------
//  ~Magnetorquer() 
//------------------------------------------------------------------------------
/** Default destructor. 
  */
Magnetorquer::~Magnetorquer()
{

}

//------------------------------------------------------------------------------
//  Magnetorquer& operator=(const Magnetorquer &mt) 
//------------------------------------------------------------------------------
/** Assignment operator. 
  * 
  * \param mt The object being copied.
  * 
  * \return This object, with parameters set to the input object's parameters.
  */
Magnetorquer& Magnetorquer::operator=(const Magnetorquer &mt)
{
  if(&mt == this)
  {
    return *this;
  }
  mode_                = mt.mode_;
  magnetic_moment_     = mt.magnetic_moment_;
  max_magnetic_moment_ = mt.max_magnetic_moment_;
  load_                = mt.load_;
  
  return *this;
}

//------------------------------------------------------------------------------
//  std::string getMode() const 
//------------------------------------------------------------------------------
/** Get operating mode of magnetorquer.
  * 
  * \return Magnetorquer current operating mode (Operational, Saturated).
  */ 
std::string Magnetorquer::getMode() const
{
  return mode_;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d getMagneticMoment() const 
//------------------------------------------------------------------------------
/** Get magnetic moment vector of magnetorquer in spacecraft body reference
  * frame.
  * 
  * \return Magnetorquer magnetic moment as \f$[x, y, z]\f$ vector  
  *         \f$[A \cdot m^2]\f$.
  */ 
Eigen::Vector3d Magnetorquer::getMagneticMoment() const
{
  return magnetic_moment_;
}

//------------------------------------------------------------------------------
//  double getOperatingLoad() const 
//------------------------------------------------------------------------------
/** Get operating load percentage of magnetorquer.
  * 
  * \return Magnetorquer operating load percentage \f$[\%]\f$ defined by
  *         \f$[0 \to 1]\f$.
  */ 
double Magnetorquer::getOperatingLoad() const
{
  return load_;
}

//------------------------------------------------------------------------------
//  void setOperatingFraction(const double load) 
//------------------------------------------------------------------------------
/** Sets the magnetorquer operating load percentage.
  *
  * \param load Applied load percentage to magnetorquer \f$[\%]\f$ defined by 
  *             \f$[0 \to 1]\f$.
  */
void Magnetorquer::setOperatingFraction(const double load)
{
  load_ = load;
}

//------------------------------------------------------------------------------
//  Eigen::Vector3d CalculateMagneticMoment()
//------------------------------------------------------------------------------
/** Calculates and sets the current magnetic moment of the magnetorquer.
  * Using equation:
  * 
  * \f$ M = M_{max} \times load_{operating} \f$
  * 
  * And returns the magnetic moment vector in spacecraft body reference frame.
  * 
  * \return Magnetorquer magnetic moment \f$[x, y, z]\f$ vector in body 
  *         reference frame \f$[A \cdot m^2]\f$.
  */
Eigen::Vector3d Magnetorquer::CalculateMagneticMoment()
{
  magnetic_moment_ = max_magnetic_moment_ * load_;
  return magnetic_moment_;
}

} //  end namespace polaris