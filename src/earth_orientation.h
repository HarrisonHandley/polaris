//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  EarthOrientation Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_EARTHORIENTATION_H_
#define POLARIS_EARTHORIENTATION_H_

#include <string>
#include <vector>

#include "../lib/eigen3/Eigen/Dense"


namespace polaris {

/** \brief Class header defining the implementation of the EarthOrientation class.
  * 
  * The EarthOrientation class implements the International Earth Rotation and 
  * Reference Systems Service Earth Orientation Rotation Matrices between the 
  * reference frames of:
  *   ECI: International Celestial Reference System - Origin: Earth
  *   ECEF: International Terrestrial Reference System - Origin: Earth
  *   True of Date Reference Frame - Origin: Earth
  * The rotation matrices are accessed through a set of methods e.g. 
  * "ICRS2ITRS". The class contains all relevant data required to compute 
  * these rotation matrices based on the input of a modified julian date.
  * Input File Source: https://celestrak.com/SpaceData/EOP-All.txt
  */ 

class EarthOrientation
{
 public:
  EarthOrientation();
  EarthOrientation(const std::vector<std::string> &filepaths, 
                   const std::string &model_name);
  EarthOrientation(const EarthOrientation &model);
  ~EarthOrientation();
  EarthOrientation& operator=(const EarthOrientation &model);

  std::string getModelName()     const;
  double      getXPole()         const;
  double      getYPole()         const;
  double      getTAIUTCOffset()  const;
  double      getUT1UTCOffset()  const;
  double      getLOD()           const;
  std::string getEOPDataPeriod() const;

  Eigen::Vector3d ICRS2ITRS    (const Eigen::Vector3d &icrs_vector)     const;
  Eigen::Vector3d ICRS2TOD     (const Eigen::Vector3d &icrs_vector)     const;
  Eigen::Vector3d ITRS2ICRS    (const Eigen::Vector3d &itrs_vector)     const;
  Eigen::Vector3d TOD2ICRS     (const Eigen::Vector3d &tod_vector )     const;
  Eigen::Vector3d Cart2Geodetic(const Eigen::Vector3d &ecef_vector)     const;
  Eigen::Vector3d Geodetic2Cart(const Eigen::Vector3d &geodetic_vector) const;

  void InitializeEarthOrientationParameters(const double mjd_utc);
  void CalculateRotationMatrices(const double julian_century_tt, 
                                 const double mjd_ut1);

 protected:
  std::string model_name_;
  
 private:
  Eigen::ArrayXXd eop_data_observed_;
  Eigen::ArrayXXd eop_data_predicted_;
  std::string eop_data_period_;
  int eop_points_observed_;
  int eop_points_predicted_;

  //  NGA EOPP Coefficients
  double eopp_ta_;
  double eopp_a_;
  double eopp_b_;
  Eigen::Array2d eopp_c_;
  Eigen::Array2d eopp_d_;
  Eigen::Array2d eopp_p_;
  double eopp_e_;
  double eopp_f_;
  Eigen::Array2d eopp_g_;
  Eigen::Array2d eopp_h_;
  Eigen::Array2d eopp_q_;
  double eopp_tb_;
  double eopp_i_;
  double eopp_j_;
  Eigen::Array4d eopp_k_;
  Eigen::Array4d eopp_l_;
  Eigen::Array4d eopp_r_;
  double eopp_tai_utc_offset_;

  double x_pole_;           
  double y_pole_;          
  double ut1_utc_offset_;   
  double lod_;                    
  double tai_utc_offset_;   

  Eigen::ArrayXXd luni_solar_nutation_series_;
  Eigen::ArrayXXd planetary_nutation_series_;
  Eigen::ArrayXXd cio_s0_terms_; 
  Eigen::ArrayXXd cio_s1_terms_;
  Eigen::ArrayXXd cio_s2_terms_;
  Eigen::ArrayXXd cio_s3_terms_;
  Eigen::ArrayXXd cio_s4_terms_;
    
  Eigen::Matrix3d polar_motion_rotation_matrix_; 
  Eigen::Matrix3d precession_nutation_rotation_matrix_; 
  Eigen::Matrix3d sidereal_rotation_matrix_;
  Eigen::Matrix3d icrs2itrs_;          

  void ReadEarthOrientationParametersFileAuxiliary(const std::string &filepath);
  void SetNGACoefficients(const std::vector<std::string> &nga_data);
  void ReadEarthOrientationParametersFile(const std::string &filepath);
  Eigen::ArrayXXd getCoefficientsFromCSVFile(const std::string &filepath) const;
  double Lerp(const double prior, 
              const double post, 
              const double interval) const;

  Eigen::Matrix3d CalculatePolarMotionRotationMatrix(const double tio_locator_s, 
                                                     const double x_pole,
                                                     const double y_pole) const;
  double CalculateTIOLocatorS(const double julian_century_tt) const;

  Eigen::Matrix3d CalculatePrecessionNutationRotationMatrix(const double julian_century_tt) const;
  Eigen::Vector2d CalculateIAUNutation2000A (const double julian_century_tt) const;
  Eigen::Vector2d CalculateLuniSolarNutation(const double julian_century_tt) const;
  Eigen::Vector2d CalculatePlanetaryNutation(const double julian_century_tt) const;

  Eigen::Matrix3d CalculateSiderealRotationMatrix(const double greenwich_sidereal_time) const;
  double CalculateGreenwichSiderealTime(const double           cio_locator_s, 
                                        const double           mjd_ut1, 
                                        const Eigen::Matrix3d &nutation_rotation_matrix) const;   
  double CalculateCIOLocatorS(const double           julian_century_tt, 
                              const Eigen::Matrix3d &nutation_rotation_matrix) const;
  double CalculateCIOCoefficient(const double            starting_coeff, 
                                 const Eigen::MatrixXd  &s_terms, 
                                 const std::vector<int> &col_indices, 
                                 const Eigen::VectorXd  &fundamental_args) const;

  double MoonMeanAnomaly           (const double julian_century_tt) const;
  double SunMeanAnomaly            (const double julian_century_tt) const; 
  double MoonMeanElongation_Sun    (const double julian_century_tt) const; 
  double MoonAscendingNode         (const double julian_century_tt) const; 
  double MoonMeanLongitude         (const double julian_century_tt) const; 
  double MercuryMeanLongitude      (const double julian_century_tt) const;
  double VenusMeanLongitude        (const double julian_century_tt) const;
  double EarthMeanLongitude        (const double julian_century_tt) const; 
  double MarsMeanLongitude         (const double julian_century_tt) const; 
  double JupiterMeanLongitude      (const double julian_century_tt) const;
  double SaturnMeanLongitude       (const double julian_century_tt) const; 
  double UranusMeanLongitude       (const double julian_century_tt) const;
  double NeptuneMeanLongitude      (const double julian_century_tt) const; 
  double GeneralPrecessionLongitude(const double julian_century_tt) const; 
};

} // end namespace polaris

#endif  // POLARIS_EARTHORIENTATION_H_