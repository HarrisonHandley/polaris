//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  SolarRadiationAnalytical Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_SOLARRADIATIONANALYTICAL_H_
#define POLARIS_SOLARRADIATIONANALYTICAL_H_

#include "solar_radiation.h"

#include <memory>
#include <string>

// #include "../lib/eigen3/Eigen/Dense"

#include "constants.h"
#include "satellite.h"

namespace polaris {

/** \brief Class header defining the implementation of the 
  * SolarRadiationAnalytical class. 
  * 
  * The SolarRadiationAnalytical class is a derived class from the abstract 
  * SolarRadiation Class defined in solar_radiation.h. The 
  * SolarRadiationAnalytical class implements an analytical solar pressure model
  * accessed through the virtual function "getAccelICRF" via polymorphism. 
  * The members of the class contains all the relevant data required to compute
  * the analytical solar pressure model for a given position in ICRF.  
  */

class SolarRadiationAnalytical: public SolarRadiation
{
 public:
  SolarRadiationAnalytical(const std::string &model_name, 
                           const double solar_intensity);
  SolarRadiationAnalytical(const SolarRadiationAnalytical &model);
  ~SolarRadiationAnalytical();
  SolarRadiationAnalytical& operator=(const SolarRadiationAnalytical &model);                           

  Eigen::Vector3d getAccelICRF(const Eigen::Vector3d            &r_sun, 
                               const Eigen::Vector3d            &r_moon,   
                               const std::shared_ptr<Satellite> &sat) override;
  Eigen::Vector3d getSolarIntensityICRF() override;

 private:
  double          solar_intensity_1au_;
  double          solar_fraction_;
  Eigen::Vector3d solar_intensity_;
  double          au_;
  double          earth_radius_;
  double          sun_radius_;
  double          moon_radius_;

  double getVisibleSolarFraction(const Eigen::Vector3d &r_sat,  
                                 const Eigen::Vector3d &r_sun,     
                                 const Eigen::Vector3d &r_body, 
                                 const double           body_radius) const;
};

} // end namespace polaris

#endif  // POLARIS_SOLARRADIATIONANALYTICAL_H_