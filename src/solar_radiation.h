//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  SolarRadiation Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_SOLARRADIATION_H_
#define POLARIS_SOLARRADIATION_H_

#include <memory>
#include <string>

// #include "../lib/eigen3/Eigen/Dense"

#include "constants.h"
#include "satellite.h"

namespace polaris {

/** \brief Abstract class header defining the SolarRadiation class.
  * 
  * All solar radiation pressure model implementations must be derived from this 
  * class for inheritance/polymorphism of the virtual getAccelICRF and 
  * getSolarIntensityICRF methods.
  */ 

class SolarRadiation
{
 public:
  SolarRadiation(const std::string &model_name);
  SolarRadiation(const SolarRadiation &model);
  virtual ~SolarRadiation();
  SolarRadiation& operator=(const SolarRadiation &model);

  virtual Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &r_sun, 
                                       const Eigen::Vector3d &r_moon,   
                                       const std::shared_ptr<Satellite> &sat);
  virtual Eigen::Vector3d getSolarIntensityICRF();
  std::string getModelName() const;
  
 protected:
  std::string model_name_;
};

} // end namespace polaris

#endif  // POLARIS_SOLARRADIATION_H_