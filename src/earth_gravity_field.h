//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  EarthGravityField Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_EARTHGRAVITYFIELD_H_
#define POLARIS_EARTHGRAVITYFIELD_H_

#include <string>

#include "../lib/Eigen3/Eigen/Dense"

namespace polaris {

/** \brief Abstract class defintion for the EarthGravityField class.
  * 
  * All Earth Gravity Field model implementations must be derived from this 
  * class for inheritance of the virtual getEarthGravityAccelITRF method.
  * 
  */  

class EarthGravityField
{
 public:
  EarthGravityField(const std::string &model_name);
  EarthGravityField(const EarthGravityField &model);
  virtual ~EarthGravityField();
  EarthGravityField& operator=(const EarthGravityField &model);

  std::string getModelName() const;

//------------------------------------------------------------------------------
//  Eigen::Vector3d getAccelITRF(const Eigen::Vector3d &itrf_pos)
//------------------------------------------------------------------------------
/** Virtual function to calculate the Acceleration acting on a spacecraft from 
  * earths gravity field in ITRF.
  * 
  * \param itrf_pos Eigen::Vector3d Position of object in ITRF \f$[x, y, z]\f$ 
  *                 \f$[m]\f$.
  * 
  * \return Eigen::Vector3d Acceleration vector acting on object from Earths's 
  *         gravity field \f$[\a_x, \a_y, \a_z]\f$ \f$[m/s^2]\f$.
  */
  virtual Eigen::Vector3d getAccelITRF(const Eigen::Vector3d &itrf_pos) = 0;

 protected:
  std::string model_name_;
};

} // end namespace polaris

#endif  // POLARIS_EARTHGRAVITYFIELD_H_