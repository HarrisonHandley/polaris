//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  JPLEphemerides Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------
//
//  Class header defining the implementation of the JPL Planetary and Lunar Ephemerides.
//  The PlanetaryPerturbations contains all relevant data required to compute the position
//  of planetary and lunar positions for a given time in barycentric dynamical time.
//  And the acceleration acting on a spacecraft from the gravitational forces of the orbiting
//  bodies through the getAccelPlanetaryPerturbationsICRF method.
//  for each orbiting body is used to calculate the acceleration acting on a spacecraft

#ifndef POLARIS_JPLEPHEMERIDES_H_
#define POLARIS_JPLEPHEMERIDES_H_

#include "planetary_perturbations.h"

#include <string>
#include <vector>

#include "../lib/eigen3/Eigen/Dense"
#include "constants.h"

namespace polaris {

/** \brief Class header defining the implementation of the JPLEphemerides class.
  *  
  * The JPLEphemerides class is a derived class from the abstract 
  * PlanetaryPertubrations Class defined in planetary_pertubrations.h. The 
  * JPLEphemerides class implements the JPL Ephemeris planetary position model
  * and is accessed through the virtual function "CalculatePlanetaryPositions" 
  * via polymorphism. The members of the class contains all the relevant data 
  * required to compute the Planetary positions using JPL ephemeris model for a  
  * given time.  
  */

class JPLEphemerides : public PlanetaryPerturbations
{
 public:
  JPLEphemerides(const std::string    &header_filepath, 
                 const std::string    &coeff_filepath,
                 const std::string    &model_name);
  JPLEphemerides(const JPLEphemerides &model);
  ~JPLEphemerides();
  JPLEphemerides& operator=(const JPLEphemerides &model);                 

  void CalculatePlanetaryPositions(const double mjd_tdb) override;
  Eigen::Vector3d getAccelICRF(const Eigen::Vector3d &icrf_pos) override;
  
 private:
  Eigen::ArrayXXd ephemeris_table_;
  int             ephemeris_interval_size_jd_;
  double          interval_time_;
  Eigen::VectorXd interval_coefficients_;

  Eigen::Vector3d sun_acceleration_icrf_;
  Eigen::Vector3d mercury_acceleration_icrf_; 
  Eigen::Vector3d venus_acceleration_icrf_;
  Eigen::Vector3d moon_acceleration_icrf_;
  Eigen::Vector3d mars_acceleration_icrf_;
  Eigen::Vector3d jupiter_acceleration_icrf_;
  Eigen::Vector3d saturn_acceleration_icrf_;
  Eigen::Vector3d uranus_acceleration_icrf_;
  Eigen::Vector3d neptune_acceleration_icrf_;
  Eigen::Vector3d pluto_acceleration_icrf_;

  double sun_gm_     = kSunGM;
  double mercury_gm_ = kMercuryGM;
  double venus_gm_   = kVenusGM;
  double moon_gm_    = kMoonGM;
  double mars_gm_    = kMarsGM;
  double jupiter_gm_ = kJupiterGM;
  double saturn_gm_  = kSaturnGM;
  double uranus_gm_  = kUranusGM;
  double neptune_gm_ = kNeptuneGM;
  double pluto_gm_   = kPlutoGM;

  class EphemerisConstants
  {
   public:
    EphemerisConstants() :
      starting_address_(0),
      num_coefficients_(0),
      num_subintervals_(0),
      dimensionality_  (0)
    {

    }
    EphemerisConstants(Eigen::VectorXi constants)
    {
      starting_address_ = constants[0];
      num_coefficients_ = constants[1];
      num_subintervals_ = constants[2];
      dimensionality_   = constants[3];
    }
    int getStartingAddress() const
    {
      return starting_address_;
    }
    int getNumCoefficients() const
    {
      return num_coefficients_;
    }
    int getNumSubIntervals() const
    {
      return num_subintervals_;
    }
    int getDimensionality() const
    {
      return dimensionality_;
    }
   private:
    int starting_address_;
    int num_coefficients_;
    int num_subintervals_;
    int dimensionality_;
  };

  EphemerisConstants mercury_constants_;
  EphemerisConstants venus_constants_;
  EphemerisConstants earth_moon_constants_;
  EphemerisConstants mars_constants_;
  EphemerisConstants jupiter_constants_;
  EphemerisConstants saturn_constants_;
  EphemerisConstants uranus_constants_;
  EphemerisConstants neptune_constants_;
  EphemerisConstants pluto_constants_;
  EphemerisConstants moon_constants_;
  EphemerisConstants sun_constants_;
  EphemerisConstants earth_nutations_constants_;
  EphemerisConstants lunar_libration_constants_;
  EphemerisConstants lunar_mantle_velocity_constants_;
  EphemerisConstants tt_tdb_constants_;

  void ReadJPLEphemerisHeaderFile(const std::string &filepath);
  Eigen::ArrayXXd ReadJPLEphemerisCoefficientsFile(const std::string &filepath) const;

  Eigen::Vector3d CalculateSunPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateMercuryPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateVenusPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateEarthPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateMoonPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateMarsPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateJupiterPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateSaturnPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateUranusPosition(double mjd_tdb) const;
  Eigen::Vector3d CalculateNeptunePosition(double mjd_tdb) const;
  Eigen::Vector3d CalculatePlutoPosition(double mjd_tdb) const;

  Eigen::MatrixXd FindChebyshevCoefficients(const EphemerisConstants &orbiting_body) const;
  Eigen::VectorXd EvaluateChebyshevPolynomial(const double mjd_tdb, const EphemerisConstants &orbiting_body, 
                                              const Eigen::MatrixXd &coefficients) const;

  Eigen::Vector3d CalculatePointMassAcceleration(const Eigen::Vector3d &icrf_pos, 
                                                       const Eigen::Vector3d &mass_icrf_pos, 
                                                       const double gm) const;
};

} // end namespace polaris

#endif  // POLARIS_JPLEPHEMERIDES_H_