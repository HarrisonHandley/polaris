//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Simulator Class Implementation
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#include "simulator.h"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iterator>
#include <iomanip> 
#include <iostream>
#include <sstream>
#include <random>
#include <utility>

#include "grace_gravity_model.h"
#include "harris_priester.h"
#include "igrf.h"
#include "jpl_ephemerides.h"
#include "magnetorquer.h"
#include "reaction_wheel.h"
#include "solar_radiation_analytical.h"
#include "spacecraft.h"
#include "thruster.h"

namespace polaris
{

//------------------------------------------------------------------------------
//  Simulator(const std::string &sim_config_xml_filepath, 
//            const std::vector<std::string> &sat_config_xml_filepaths,
//            const std::string &sim_identifier,
//            const bool         monte_carlo_method,
//            const int          monte_carlo_runs)
//------------------------------------------------------------------------------
/** Default constructor for simulator, requiring initial configuration.
  * The constructor parses the xml files to set the relevant environmental
  * objects and satellite objects based on configuration data.
  * 
  * \param sim_config_xml_filepath Simulation XML configuration filepath.
  * \param sat_config_xml_filepaths Satellite XML configuration filepaths.
  * \param sim_identifier Unique identifier for simulation run appended to 
  *                       results file. Default is set to "" if no arg is 
  *                       provided.
  * \param monte_carlo_method Optional boolean parameter for enabling Monte 
  *                           Carlo operating mode.
  * \param monte_carlo_runs Integer number of monte carlo simulations that will
  *                         be run if Monte Carlo method is enabled. 
  */
Simulator::Simulator(const std::string              &sim_config_xml_filepath, 
                     const std::vector<std::string> &sat_config_xml_filepaths,
                     const std::string              &sim_identifier,
                     const bool                      monte_carlo_method,
                     const int                       monte_carlo_runs) : 
                     monte_carlo_method_     (monte_carlo_method),
                     monte_carlo_runs_       (monte_carlo_runs),
                     time_                   (SimulationTime()),
                     earth_orientation_      (EarthOrientation()),
                     atmospheric_model_      (),
                     earth_gravity_field_    (),
                     earth_magnetic_field_   (),
                     planetary_perturbations_(),
                     solar_radiation_model_  (),
                     satellites_             (),
                     sim_config_log_file_    (),
                     satellite_log_files_    ()
{
  //  Validate input to ensure Monte Carlo runs is set to 0 if Monte Carlo is 
  //  not enabled.
  if(monte_carlo_method_ == false)
  {
    monte_carlo_runs_ = 0;
  }

  std::cout << "Initializing Simulation Environment Configuration from " << sim_config_xml_filepath << "\n";
  ParseSimulationConfiguration(sim_config_xml_filepath);
  std::string sim_config_log_filepath = results_directory_ + "/" + sim_identifier;
  if(sim_identifier != "")
  {
    sim_config_log_filepath = sim_config_log_filepath + "_";
  }
  sim_config_log_filepath = sim_config_log_filepath + "Simulation_Configuration" + ".csv";
  
  sim_config_log_file_.open(sim_config_log_filepath);
  if(sim_config_log_file_.fail())
  {
    std::cerr << "Cannot open file " << sim_config_log_filepath << "\n";
  }
  else
  {
    std::cout << "Created Simulation Configuration Log file: " << sim_config_log_filepath << "\n";  
  }
  LogSimHeader(sim_config_log_file_, sat_config_xml_filepaths.size(), sim_config_xml_filepath);

  for(auto &sat_config_xml_filepath : sat_config_xml_filepaths)
  {
    std::cout << "Initializing Satellite from " << sat_config_xml_filepath << "\n";
    for(std::size_t i = 0; i < (monte_carlo_runs_ + 1); ++i)
    {
      if(i == 0)
      {
        auto sat_object = ParseSatelliteParameters(sat_config_xml_filepath, "Nominal");
        std::ofstream file;
        file.open(getSatelliteLogFilepath(sim_identifier, sat_object->getSatelliteID(), "Nominal"));
        if(file.fail())
        {
          std::cerr << "Cannot open file Satellite Log Files\n";
        }
        LogSatHeader(file, sat_object, sat_config_xml_filepath, "Nominal");
        satellites_.push_back(std::move(sat_object));  
        satellite_log_files_.push_back(std::move(file));                                                   
      }
      else
      {
        auto sat_object = ParseSatelliteParameters(sat_config_xml_filepath, "Monte Carlo");
        std::ofstream file;
        file.open(getSatelliteLogFilepath(sim_identifier, sat_object->getSatelliteID(), "Monte Carlo", i));
        if(file.fail())
        {
          std::cerr << "Cannot open file Satellite Log Files\n";
        }
        LogSatHeader(file, sat_object, sat_config_xml_filepath, "Monte Carlo", i);
        satellites_.push_back(std::move(sat_object));               
        satellite_log_files_.push_back(std::move(file));                                               
      }
    }
  }
}

//------------------------------------------------------------------------------
//  ~Simulator::~Simulator() 
//------------------------------------------------------------------------------
/** Default destructor, closes all logs. 
  */
Simulator::~Simulator()
{
  sim_config_log_file_.close();
  for(auto &file : satellite_log_files_)
  {
    file.close();
  }
}

//------------------------------------------------------------------------------
//  void InitializeState()
//------------------------------------------------------------------------------
/** Initializes the state of the simulation environment and of each spacecraft.
  * This method is required to be used after simulation propagation to update
  * the state of the simulation in accordance with the new simulation time, and 
  * satellite/spacecraft dynamic state.
  * 
  * The method initializes 
  *   - The earth orientation parameters based on the UTC time.
  *   - Different timescales
  *   - Reference frame rotation matrices
  *   - Planetary positions
  */ 
void Simulator::InitializeState()
{
  earth_orientation_.InitializeEarthOrientationParameters(time_.getMjdUTC());
  time_.UpdateTimescales(earth_orientation_.getTAIUTCOffset(), earth_orientation_.getUT1UTCOffset());
  earth_orientation_.CalculateRotationMatrices(time_.getJulianCenturyTT(), time_.getMjdUT1());
  planetary_perturbations_->CalculatePlanetaryPositions(time_.getMjdTDB());

  for(auto &satellite : satellites_)
  {
    satellite->InitializeTrajectoryRefFrames(earth_orientation_);
    satellite->InitializeAttitudeRefFrames();
  }
}

//------------------------------------------------------------------------------
//  void CalculateTrajectory()
//------------------------------------------------------------------------------
/** Calculates the forces acting on the satellite and it's 
  * associated trajectory acceleration in ICRF.
  * 
  * The method calculates the  forces acting on the satellite
  * from the following sources. 
  *   - Planetary gravitational forces
  *   - Solar radiation pressure
  *   - Atmospheric drag
  *   - Planetary positions
  *   - Thrusters (if applicable)
  */ 
void Simulator::CalculateTrajectory()
{
  for(auto &satellite : satellites_)
  {
    Eigen::Vector3d sat_icrf_pos = satellite->getICRFPosition();
    Eigen::Vector3d sat_itrf_pos = satellite->getITRFPosition();
    Eigen::Vector3d planetary_accel = planetary_perturbations_->getAccelICRF(sat_icrf_pos);
    Eigen::Vector3d earth_gravity_accel_itrf = earth_gravity_field_->getAccelITRF(sat_itrf_pos);
    Eigen::Vector3d earth_gravity_accel = earth_orientation_.ITRS2ICRS(earth_gravity_accel_itrf);
    Eigen::Vector3d solar_pressure_accel = solar_radiation_model_->getAccelICRF(planetary_perturbations_->getSunPositionICRF(), planetary_perturbations_->getMoonPositionICRF(), satellite);
    Eigen::Vector3d drag_accel = atmospheric_model_->getAccelICRF(planetary_perturbations_->getSunPositionICRF(), earth_orientation_, satellite);

    satellite->setPlanetaryGravitationalForce(planetary_accel);
    satellite->setEarthGravitationalForce(earth_gravity_accel);
    satellite->setSolarPressureForce(solar_pressure_accel);
    satellite->setDragForce(drag_accel);
    satellite->CalculateTrajectoryState();
  }
}

//------------------------------------------------------------------------------
//  void CalculateAttitude()
//------------------------------------------------------------------------------
/** Calculates the torques and momentum acting on the satellite from actuators 
  * and environmental sources. And calculates the spacecrafts angular momentum
  * and angular acceleration at the current attitude.
  * 
  * The method calculates the torques and change in angular moment acting on the 
  * satellite from the following sources. 
  *   - Planetary gravitational forces
  *   - Solar radiation pressure
  *   - Atmospheric drag
  *   - Planetary positions
  *   - Static spacecraft magnetic field torque
  *   - Magnetorquers torque (if applicable)
  *   - Thrusters torque (if applicable)
  *   - Reaction wheels angular momentum (if applicable)
  */ 
void Simulator::CalculateAttitude()
{
  for(auto &satellite : satellites_)
  {
    satellite->CalculateAttitudeState(time_.getMjdUTC(),
                                      earth_magnetic_field_,
                                      earth_orientation_);
  }
}

//------------------------------------------------------------------------------
//  void LogData()
//------------------------------------------------------------------------------
/** This method logs the current state of the simulation into the associated
  * log files for each satellite/spacecraft and a separate file for all 
  * associated environmental data. 
  */ 
void Simulator::LogData()
{
  LogSimData(sim_config_log_file_);
  for(std::size_t i = 0; i < satellite_log_files_.size(); ++i)
  {
    LogSatData(satellite_log_files_[i], satellites_[i]);
  }
}

//------------------------------------------------------------------------------
//  std::vector<std::shared_ptr<Satellite>> getSatellitePointers()
//------------------------------------------------------------------------------
/** This method returns a vector of all the satellite and spacecraft object
  * shared pointers. This allows external access to the public get and set 
  * methods for each satellite and spacecraft object. Enabling external 
  * simulation tools to get the state of the satellite and set the state of
  * the spacecraft actuators. 
  * 
  * \return std::vector<std::shared_ptr<Satellite>> STL vector of satellite 
  *         shared_ptr objects currently being simulated.
  */ 
std::vector<std::shared_ptr<Satellite>> Simulator::getSatellitePointers()
{
  return satellites_;
}

//------------------------------------------------------------------------------
//  void Propagate(const double timestep)
//------------------------------------------------------------------------------
/** This method propagates the state of the simulation by timestep in seconds.
  * It calculates the new state vector and attitude of the satellite by 
  * propagating linear acceleration and angular acceleration forward.
  * 
  * \param timestep Time value in seconds to propagate simulation forward by.
  */ 
void Simulator::Propagate(const double timestep)
{
  time_.IncrementTime(timestep);  

  for(auto &satellite : satellites_)
  {
    satellite->PropagateTrajectory(timestep);
    satellite->PropagateAttitude(timestep);
  }
}

//------------------------------------------------------------------------------
//  std::string getSatelliteLogFilepath(const std::string &sim_identifier,
//                                      const std::string &satellite_id,
//                                      const std::string &operating_mode,
//                                      const int monte_carlo_run_num)
//------------------------------------------------------------------------------
/** This method creates gets the filepath to a satellite log file for a given
  * set of input args.
  * 
  * \param sim_identifier Simulation identifier string.
  * \param satellite_id Satellite identification string.
  * \param operating_mode Operating mode for the satellite object being logged
  *                       to this file. Either Nominal or Monte Carlo
  * \param monte_carlo_run_num If operating_mode is Monte Carlo. Append the 
  *                            Monte Carlo simulation run number to filename.
  * 
  * \return std::string Absolute filepath string.
  */ 
std::string Simulator::getSatelliteLogFilepath(const std::string &sim_identifier,
                                               const std::string &satellite_id,
                                               const std::string &operating_mode,
                                               const int monte_carlo_run_num)
{
  std::string filepath = results_directory_ + "/" + sim_identifier;
  if(sim_identifier != "")
  {
    filepath = filepath + "_";
  }
  filepath = filepath + satellite_id + "_" + operating_mode;
  if(operating_mode == "Monte Carlo")
  {
    filepath = filepath + "_" + "Run_" + std::to_string(monte_carlo_run_num);
  }
  filepath = filepath + ".csv";
  std::cout << "Created Satellite Log file created: " << filepath << "\n";
  return filepath;
}

//------------------------------------------------------------------------------
//  void LogEnvHeader(std::ofstream &file, int num_sats, std::string &XML_filepath)
//------------------------------------------------------------------------------
/** Writes simulation environmental log header to file.
  * 
  * \param file Simulation environmental log ofstream file.
  * \param num_sats Number of satellites being simulated.
  */ 
void Simulator::LogSimHeader(std::ofstream &file, int num_sats, const std::string &XML_filepath)
{
  file << "Simulation Configuration Log File" << "\n";

  file << "Generated from XML Configuration File: " << ",";
  file << XML_filepath << "\n";

  file << "Number of Satellites simulated: " << ",";
  file << num_sats << "\n";

  file << "Monte Carlo Method is Enabled: " << ",";
  file << monte_carlo_method_ << "\n";

  file << "Number of Monte Carlo runs per simulated Satellite: " << ",";
  file << monte_carlo_runs_ << "\n";

  file << "Logged Data output filepath: " << ",";
  file << results_directory_ << "\n";

  file << "END OF HEADER" << "\n";

  file << "Iterations" << ",";
  file << "Runtime [s]" << ",";
  file << "UTC [Gregorian]" << ",";
  file << "UTC [Modified Julian Date]" << ",";
  file << "TT [Modified Julian Date]" << ",";
  file << "UT1 [Modified Julian Date]" << ",";
  file << "TDB [Modified Julian Date]" << ",";
  file << "Earth Orientation Model Name" << ",";
  file << "Earth Atmospheric Density Model Name" << ",";
  file << "Earth Gravity Field Model Name" << ",";
  file << "Earth Magnetic Field Model Name" << ",";
  file << "Planetary Pertubrations Model Name" << ",";
  file << "Solar Radiation Model Name" << ",";
  file << "CIP X Pole [rad]" << ",";\
  file << "CIP Y Pole [rad]" << ",";
  file << "TAI-UTC Offset [s]" << ",";
  file << "UT1-UTC Offset [s]" << ",";
  file << "Length of Day [s]" << ",";
  file << "IERS EOP Data Period" << ",";
  file << "Sun X Position ECI(ICRF) [m]" << ",";
  file << "Sun Y Position ECI(ICRF) [m]" << ",";
  file << "Sun Z Position ECI(ICRF) [m]" << ",";
  file << "Mercury X Position ECI(ICRF) [m]" << ",";
  file << "Mercury Y Position ECI(ICRF) [m]" << ",";
  file << "Mercury Z Position ECI(ICRF) [m]" << ",";
  file << "Venus X Position ECI(ICRF) [m]" << ",";
  file << "Venus Y Position ECI(ICRF) [m]" << ",";
  file << "Venus Z Position ECI(ICRF) [m]" << ",";
  file << "Moon X Position ECI(ICRF) [m]" << ",";
  file << "Moon Y Position ECI(ICRF) [m]" << ",";
  file << "Moon Z Position ECI(ICRF) [m]" << ",";
  file << "Mars X Position ECI(ICRF) [m]" << ",";
  file << "Mars Y Position ECI(ICRF) [m]" << ",";
  file << "Mars Z Position ECI(ICRF) [m]" << ",";
  file << "Jupiter X Position ECI(ICRF) [m]" << ",";
  file << "Jupiter Y Position ECI(ICRF) [m]" << ",";
  file << "Jupiter Z Position ECI(ICRF) [m]" << ",";
  file << "Saturn X Position ECI(ICRF) [m]" << ",";
  file << "Saturn Y Position ECI(ICRF) [m]" << ",";
  file << "Saturn Z Position ECI(ICRF) [m]" << ",";
  file << "Uranus X Position ECI(ICRF) [m]" << ",";
  file << "Uranus Y Position ECI(ICRF) [m]" << ",";
  file << "Uranus Z Position ECI(ICRF) [m]" << ",";
  file << "Neptune X Position ECI(ICRF) [m]" << ",";
  file << "Neptune Y Position ECI(ICRF) [m]" << ",";
  file << "Neptune Z Position ECI(ICRF) [m]" << ",";
  file << "Pluto X Position ECI(ICRF) [m]" << ",";
  file << "Pluto Y Position ECI(ICRF) [m]" << ",";
  file << "Pluto Z Position ECI(ICRF) [m]" << ",";
  file << "\n";
}

//------------------------------------------------------------------------------
//  void LogEnvData(std::ofstream &file)
//------------------------------------------------------------------------------
/** Writes current simulation environmental data to file.
  * 
  * \param file Simulation environmental log ofstream file.
  */ 
void Simulator::LogSimData(std::ofstream &file)
{ 
  const static Eigen::IOFormat CSVFormat(Eigen::FullPrecision, Eigen::DontAlignCols, ",", ",");
  file << time_.getSimulationIterations() << ",";
  file << time_.getRuntime() << ",";
  file << time_.getGregorianTime() << ",";
  file << std::fixed << std::setprecision(12) << time_.getMjdUTC() << ",";
  file << std::fixed << std::setprecision(12) << time_.getMjdTT() << ",";
  file << std::fixed << std::setprecision(12) << time_.getMjdUT1() << ",";
  file << std::fixed << std::setprecision(12) << time_.getMjdTDB() << ",";
  file << earth_orientation_.getModelName() << ",";
  file << atmospheric_model_->getModelName() << ",";
  file << earth_gravity_field_->getModelName() << ",";
  file << earth_magnetic_field_->getModelName() << ",";
  file << planetary_perturbations_->getModelName() << ",";
  file << solar_radiation_model_->getModelName() << ",";  
  file << earth_orientation_.getXPole() << ",";
  file << earth_orientation_.getYPole() << ",";
  file << earth_orientation_.getTAIUTCOffset() << ",";
  file << earth_orientation_.getUT1UTCOffset() << ",";
  file << earth_orientation_.getLOD() << ",";
  file << earth_orientation_.getEOPDataPeriod() << ",";
  file << planetary_perturbations_->getSunPositionICRF().format(CSVFormat) << ",";
  file << planetary_perturbations_->getMercuryPositionICRF().format(CSVFormat) << ",";
  file << planetary_perturbations_->getVenusPositionICRF().format(CSVFormat) << ",";
  file << planetary_perturbations_->getMoonPositionICRF().format(CSVFormat) << ",";
  file << planetary_perturbations_->getMarsPositionICRF().format(CSVFormat) << ",";
  file << planetary_perturbations_->getJupiterPositionICRF().format(CSVFormat) << ",";
  file << planetary_perturbations_->getSaturnPositionICRF().format(CSVFormat) << ",";
  file << planetary_perturbations_->getUranusPositionICRF().format(CSVFormat) << ",";
  file << planetary_perturbations_->getNeptunePositionICRF().format(CSVFormat) << ",";
  file << planetary_perturbations_->getPlutoPositionICRF().format(CSVFormat) << ",";
  file << "\n";
}

//------------------------------------------------------------------------------
//  void LogSatHeader(std::ofstream &file, const std::shared_ptr<Satellite> &sat)
//------------------------------------------------------------------------------
/** Writes given simulation satellite log header to file.
  * 
  * \param file Simulation satellite log ofstream file.
  * \param sat Satellite object being logged.
  * \param XML_filepath XML filepath to Satellite configuration file.
  * \param operating_mode Simulation operating mode (Nominal or Monte Carlo)
  * \param monte_carlo_run_num Monte Carlo run number. Starting at 1.
  */ 
void Simulator::LogSatHeader(std::ofstream &file, 
                             const std::shared_ptr<Satellite> &sat,
                             const std::string &XML_filepath,
                             const std::string &operating_mode,
                             int monte_carlo_run_num)
{
  file << "Satellite ID: " << ",";
  file << sat->getSatelliteID() << "\n";

  file << "Generated from XML Configuration File: " << ",";
  file << XML_filepath <<"\n";

  file << "Logged Satellite Simulation Operating Mode: " << ",";
  file << operating_mode << "\n";
  
  if(operating_mode == "Monte Carlo")
  {
    file << "Monte Carlo Simulation Run: " << ",";
    file << monte_carlo_run_num << "\n";
  }
  file << "END OF HEADER" << "\n";

  file << "Iterations" << ",";
  file << "Runtime [s]" << ",";
  file << "UTC [Gregorian]" << ",";
  file << "X Position ECI(ICRF) [m]" << ",";
  file << "Y Position ECI(ICRF) [m]" << ",";
  file << "Z Position ECI(ICRF) [m]" << ",";
  file << "Velocity Magnitude ECI(ICRF) [m/s]" << ",";
  file << "X Velocity ECI(ICRF) [m/s]" << ",";
  file << "Y Velocity ECI(ICRF) [m/s]" << ",";
  file << "Z Velocity ECI(ICRF) [m/s]" << ",";
  file << "Acceleration Magnitude ECI(ICRF) [m/s^2]" << ",";
  file << "X Acceleration ECI(ICRF) [m/s^2]" << ",";
  file << "Y Acceleration ECI(ICRF) [m/s^2]" << ",";
  file << "Z Acceleration ECI(ICRF) [m/s^2]" << ","; 
  file << "X Position ECEF(ITRF) [m]" << ",";
  file << "Y Position ECEF(ITRF) [m]" << ",";
  file << "Z Position ECEF(ITRF) [m]" << ",";
  file << "Latitude WGS84(ITRF) [rad]" << ",";
  file << "Longitude WGS84(ITRF) [rad]" << ",";
  file << "Altitude WGS84(ITRF) [m]" << ","; 
  file << "Eccentricity ECI(ICRF)" << ",";
  file << "Semi-Major Axis ECI(ICRF) [m]" << ",";
  file << "Inclination ECI(ICRF) [deg]" << ","; 
  file << "Longitude of Ascending Node ECI(ICRF) [deg]" << ",";
  file << "Argument of Periapsis ECI(ICRF) [deg]" << ",";
  file << "True Anomaly ECI(ICRF) [deg]" << ",";
  file << "X Aerodynamic Drag Force ECI(ICRF) [N]" << ",";
  file << "Y Aerodynamic Drag Force ECI(ICRF) [N]" << ",";
  file << "Z Aerodynamic Drag Force ECI(ICRF) [N]" << ",";
  file << "X Earth Gravity Force ECI(ICRF) [N]" << ",";
  file << "Y Earth Gravity Force ECI(ICRF) [N]" << ",";
  file << "Z Earth Gravity Force ECI(ICRF) [N]" << ","; 
  file << "X Planetary Bodies Force ECI(ICRF) [N]" << ",";
  file << "Y Planetary Bodies Force ECI(ICRF) [N]" << ",";
  file << "Z Planetary Bodies Force ECI(ICRF) [N]" << ",";
  file << "X Solar Pressure Force ECI(ICRF) [N]" << ",";
  file << "Y Solar Pressure Force ECI(ICRF) [N]" << ",";
  file << "Z Solar Pressure Force ECI(ICRF) [N]" << ",";
  file << "X Propulsive Force ECI(ICRF) [N]" << ",";
  file << "Y Propulsive Force ECI(ICRF) [N]" << ",";
  file << "Z Propulsive Force ECI(ICRF) [N]" << ","; 
  file << "X Total Applied Force ECI(ICRF) [N]" << ",";
  file << "Y Total Applied Force ECI(ICRF) [N]" << ",";
  file << "Z Total Applied Force ECI(ICRF) [N]" << ",";
  file << "Mass [kg]" << ",";
  file << "Drag Coefficient" << ",";
  file << "Radiation Pressure Coefficient" << ",";

  file << "Attitude Quaternion Scalar" << ",";
  file << "Attitude Quaternion Vector i" << ",";
  file << "Attitude Quaternion Vector j" << ",";
  file << "Attitude Quaternion Vector k" << ",";
  file << "X Angular Velocity Body Frame [rad/s]" << ",";
  file << "Y Angular Velocity Body Frame [rad/s]" << ",";
  file << "Z Angular Velocity Body Frame [rad/s]" << ",";
  file << "X Angular Acceleration Body Frame [rad/s^2]" << ",";
  file << "Y Angular Acceleration Body Frame [rad/s^2]" << ",";
  file << "Z Angular Acceleration Body Frame [rad/s^2]" << ",";
  file << "Spacecraft X Angular Momentum Body Frame [kg*m^2/s]" << ",";
  file << "Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]" << ",";
  file << "Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]" << ",";    
  file << "Applied X Angular Momentum Delta Body Frame [kg*m^2/s]" << ",";
  file << "Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]" << ",";
  file << "Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]" << ",";    
  file << "RW X Angular Momentum Delta Body Frame [kg*m^2/s]" << ",";
  file << "RW Y Angular Momentum Delta Body Frame [kg*m^2/s]" << ",";
  file << "RW Z Angular Momentum Delta Body Frame [kg*m^2/s]" << ",";    

  file << "X Aerodynamic Drag Torque Body Frame [Nm]" << ",";
  file << "Y Aerodynamic Drag Torque Body Frame [Nm]" << ",";
  file << "Z Aerodynamic Drag Torque Body Frame [Nm]" << ",";
  file << "X Earth Gravity Torque Body Frame [Nm]" << ",";
  file << "Y Earth Gravity Torque Body Frame [Nm]" << ",";
  file << "Z Earth Gravity Torque Body Frame [Nm]" << ","; 
  file << "X Planetary Bodies Torque Body Frame [Nm]" << ",";
  file << "Y Planetary Bodies Torque Body Frame [Nm]" << ",";
  file << "Z Planetary Bodies Torque Body Frame [Nm]" << ",";
  file << "X Solar Pressure Torque Body Frame [Nm]" << ",";
  file << "Y Solar Pressure Torque Body Frame [Nm]" << ",";
  file << "Z Solar Pressure Torque Body Frame [Nm]" << ",";
  file << "X Propulsive Torque Body Frame [Nm]" << ",";
  file << "Y Propulsive Torque Body Frame [Nm]" << ",";
  file << "Z Propulsive Torque Body Frame [Nm]" << ","; 
  file << "X Total Applied Torque Body Frame [Nm]" << ",";
  file << "Y Total Applied Torque Body Frame [Nm]" << ",";
  file << "Z Total Applied Torque Body Frame [Nm]" << ",";

  for(std::size_t j = 0; j < sat->getNumberOfMagnetorquers(); ++j)
  {
    file << "Magnetorquer " << j << " State" << ",";
    file << "Magnetorquer " << j << " X Magnetic Moment [Am^2]" << ",";
    file << "Magnetorquer " << j << " Y Magnetic Moment [Am^2]" << ",";
    file << "Magnetorquer " << j << " Z Magnetic Moment [Am^2]" << ",";
    file << "Magnetorquer " << j << " Operating Load [%]" << ",";
  }

  for(std::size_t j = 0; j < sat->getNumberOfReactionWheels(); ++j)
  {
    file << "Reaction Wheel " << j << " State" << ",";
    file << "Reaction Wheel " << j << " Angular Velocity [rad/s]" << ",";
    file << "Reaction Wheel " << j << " Angular Acceleration [rad/s^2]" << ",";
    file << "Reaction Wheel " << j << " Motor Torque [Nm]" << ",";
  }  

  for(std::size_t j = 0; j < sat->getNumberOfThrusters(); ++j)
  {
    file << "Thruster " << j << " Throttle [%]" << ",";
    file << "Thruster " << j << " X Force [N]" << ",";
    file << "Thruster " << j << " Y Force [N]" << ",";
    file << "Thruster " << j << " Z Force [N]" << ",";      
    file << "Thruster " << j << " X Torque [Nm]" << ",";
    file << "Thruster " << j << " Y Torque [Nm]" << ",";
    file << "Thruster " << j << " Z Torque [Nm]" << ",";        
    file << "Thruster " << j << " Mass Flow Rate [kg/s]" << ",";
  }
    file << "\n";           
}

//------------------------------------------------------------------------------
//  void LogSatData(std::ofstream &file, const std::shared_ptr<Satellite> &sat)
//------------------------------------------------------------------------------
/** Writes given simulation satellite data to file.
  * 
  * \param file Simulation satellite log ofstream file.
  * \param sat Satellite object being logged.
  */ 
void Simulator::LogSatData(std::ofstream &file, const std::shared_ptr<Satellite> &sat)
{
  const static Eigen::IOFormat CSVFormat(Eigen::FullPrecision, Eigen::DontAlignCols, ",", ",");
  file << time_.getSimulationIterations() << ",";
  file << time_.getRuntime() << ",";
  file << time_.getGregorianTime() << ",";
  file << sat->getICRFPosition().format(CSVFormat) << ",";
  file << (sat->getICRFVelocity()).norm()  << ",";
  file << sat->getICRFVelocity().format(CSVFormat) << ",";
  file << (sat->getICRFAcceleration()).norm()  << ",";
  file << sat->getICRFAcceleration().format(CSVFormat) << ",";
  file << sat->getITRFPosition().format(CSVFormat) << ",";
  file << sat->getLatLonPosition().format(CSVFormat) << ",";
  file << std::fixed << std::setprecision(12) << sat->getEccentricity() << ",";    
  file << std::fixed << std::setprecision(12) << sat->getSemiMajorAxis() << ",";    
  file << std::fixed << std::setprecision(12) << sat->getInclincation()*(180/kPI) << ",";   
  file << std::fixed << std::setprecision(12) << sat->getAscendingNode()*(180/kPI) << ",";             
  file << std::fixed << std::setprecision(12) << sat->getArgumentOfPeriapsis()*(180/kPI) << ",";    
  file << std::fixed << std::setprecision(12) << sat->getTrueAnomaly()*(180/kPI) << ",";    
  file << sat->getAtmosphericDragForce().format(CSVFormat) << ","; 
  file << sat->getEarthGravityForce().format(CSVFormat) << ","; 
  file << sat->getPlanetaryGravityForce().format(CSVFormat) << ","; 
  file << sat->getSolarPressureForce().format(CSVFormat) << ","; 
  file << sat->getTotalThrusterForce().format(CSVFormat) << ",";
  file << sat->getAppliedForce().format(CSVFormat) << ",";    
  file << sat->getMass() << ",";  
  file << sat->getDragCoeff() << ",";
  file << sat->getRadiationPressureCoeff() << ",";

  file << sat->getAttitude().w() << ",";
  file << sat->getAttitude().vec().format(CSVFormat) << ",";
  file << sat->getAngularVelocity().format(CSVFormat) << ",";
  file << sat->getAngularAcceleration().format(CSVFormat) << ",";
  file << sat->getAngularMomentum().format(CSVFormat)<< ",";
  file << sat->getAppliedAngularMomentumDelta().format(CSVFormat) << ",";
  file << sat->getRWAngularMomentum().format(CSVFormat) << ",";  

  file << sat->getAtmosphericDragTorque().format(CSVFormat) << ",";
  file << sat->getGravityGradientTorque().format(CSVFormat) << ",";
  file << sat->getMagneticTorque().format(CSVFormat) << ",";
  file << sat->getSolarPressureTorque().format(CSVFormat) << ",";
  file << sat->getTotalThrusterTorque().format(CSVFormat) << ",";
  file << sat->getAppliedTorque().format(CSVFormat) << ",";

  std::vector<std::string> magnetorquer_state = sat->getMagnetorquersState();
  std::vector<Eigen::Vector3d> magnetic_moment = sat->getMagnetorquersMagneticMoment();
  std::vector<double> operating_load = sat->getMagnetorquersOperatingLoad();
  for(std::size_t j = 0; j < sat->getNumberOfMagnetorquers(); ++j)
  {
    file << magnetorquer_state[j] << ",";
    file << magnetic_moment[j].format(CSVFormat) << ",";
    file << operating_load[j] << ",";
  }

  std::vector<std::string> reaction_wheel_state = sat->getReactionWheelsState();
  std::vector<double> reaction_wheel_ang_vel = sat->getReactionWheelsAngularVel();
  std::vector<double> reaction_wheel_ang_acc = sat->getReactionWheelsAngularAccel();
  std::vector<double> reaction_wheel_motor_torque = sat->getReactionWheelsMotorTorque();
  for(std::size_t j = 0; j < sat->getNumberOfReactionWheels(); ++j)
  {
    file << reaction_wheel_state[j] << ",";
    file << reaction_wheel_ang_vel[j] << ",";
    file << reaction_wheel_ang_acc[j] << ",";
    file << reaction_wheel_motor_torque[j] << ",";
  }  

  std::vector<double> thruster_throttle = sat->getThrustersThrottle();
  std::vector<Eigen::Vector3d> thruster_force = sat->getThrustersForce();
  std::vector<Eigen::Vector3d> thruster_torque = sat->getThrustersTorque();  
  std::vector<double> thruster_mass_flow_rate = sat->getThrustersMassFlowRate();
  for(std::size_t j = 0; j < sat->getNumberOfThrusters(); ++j)
  {
    file << thruster_throttle[j] << ",";
    file << thruster_force[j].format(CSVFormat) << ",";
    file << thruster_torque[j].format(CSVFormat) << ",";    
    file << thruster_mass_flow_rate[j] << ",";
  }
  file << "\n";
}

//------------------------------------------------------------------------------
//  void ParseSimulationConfiguration(const std::string &sim_config)
//------------------------------------------------------------------------------
/** Parses the simulation XML configuration file to extract 
  *   - Log output directory
  *   - Simulation time
  *   - Earth Orientation model
  *   - Atmospheric Density model
  *   - Earth gravity model
  *   - Planetary perturbation model
  *   - Solar radiation pressure model
  * 
  * \param sim_config Simulation configration filepath.
  */ 
void Simulator::ParseSimulationConfiguration(const std::string &sim_config)
{
  try
  {
    rapidxml::file<> config_file(sim_config.c_str());
    rapidxml::xml_document<> config_doc;
    config_doc.parse<0>(config_file.data());
    rapidxml::xml_node<> *root_node = config_doc.first_node(); 

    // Read key simulation parameters
    rapidxml::xml_node<> *simulation_parameters_node = root_node->first_node("simulation_parameters");
    results_directory_ = simulation_parameters_node->first_node("results_directory")->value();

    time_                    = ParseSimulationTimeParameters    (root_node);
    earth_orientation_       = ParseEarthOrientationParameters  (root_node);
    atmospheric_model_       = ParseAtmosphericModelParameters  (root_node);
    earth_gravity_field_     = ParseEarthGravityFieldParameters (root_node);
    earth_magnetic_field_    = ParseEarthMagneticFieldParameters(root_node);
    planetary_perturbations_ = ParsePlanetaryEphemeris          (root_node);
    solar_radiation_model_   = ParseSolarRadiationParameters    (root_node);
  }
  catch (const std::runtime_error& e)
  {
    std::cerr << "Runtime error was: " << e.what() << std::endl;
  }
  catch (const rapidxml::parse_error& e)
  {
    std::cerr << "Parse error was: " << e.what() << std::endl;
  }
  catch (const std::exception& e)
  {
    std::cerr << "Error was: " << e.what() << std::endl;
  }
  catch (...)
  {
    std::cerr << "An unknown error occurred." << std::endl;
  }
}

//------------------------------------------------------------------------------
//  SimulationTime ParseSimulationTimeParameters(const rapidxml::xml_node<> *node)
//------------------------------------------------------------------------------
/** Parses the simulation XML configuration file to extract the simulation time
  * parameters and construct the SimulationTime object for the simulator.
  * The method extracts the start time in UTC Gregorian calendar.
  * 
  * \param node Pointer to XML root node.
  * 
  * \return SimulationTime Constructed SimulationTime object.
  */ 
SimulationTime Simulator::ParseSimulationTimeParameters(const rapidxml::xml_node<> *node)
{
  rapidxml::xml_node<> *simulation_time_node = node->first_node("simulation_time");
  std::string start_time = simulation_time_node->first_node("start_time")->value();

  char month_char[4];
  int year = 0;
  int month = 0;
  int day = 0;
  int hour = 0;
  int min = 0;
  float sec = 0;

  sscanf(start_time.c_str(), "%i-%3s-%i %i:%i:%f", &day, month_char, &year, &hour, &min, &sec);

  std::string month_str(month_char);
  if (month_str == "Jan")
    month = 1;
  else if (month_str == "Feb")
    month = 2;
  else if (month_str == "Mar")
    month = 3;
  else if (month_str == "Apr")
    month = 4;
  else if (month_str == "May")
    month = 5;
  else if (month_str == "Jun")
    month = 6;
  else if (month_str == "Jul")
    month = 7;
  else if (month_str == "Aug")
    month = 8;
  else if (month_str == "Sep")
    month = 9;
  else if (month_str == "Oct")
    month = 10;
  else if (month_str == "Nov")
    month = 11;
  else if (month_str == "Dec")
    month = 12;
  else
  {
    std::cout << "Error Invalid Starting Month Entered" << std::endl;
  }

  SimulationTime time(year, month, day, hour, min, sec);
  std::cout << "Simulation Start Time: " << time.getGregorianTime() << "\n";
  return time;
}

//------------------------------------------------------------------------------
//  EarthOrientation ParseEarthOrientationParameters(const rapidxml::xml_node<> *node)
//------------------------------------------------------------------------------
/** Parses the simulation XML configuration file to extract the earth 
  * orientation parameters and construct the EarthOrientation object for the 
  * simulator. The method extracts: 
  *   - Earth Orientation model name
  *   - Earth Orientation Parameters ASCII file filepath
  *   - Luni-Solar nutation series CSV file filepath
  *   - Planetary nutation series CSV file filepath
  *   - Celestial Intermediate Origin S0 terms CSV file filepath
  *   - Celestial Intermediate Origin S1 terms CSV file filepath
  *   - Celestial Intermediate Origin S2 terms CSV file filepath
  *   - Celestial Intermediate Origin S3 terms CSV file filepath
  *   - Celestial Intermediate Origin S4 terms CSV file filepath
  * 
  * \param node Pointer to XML root node.
  * 
  * \return EarthOrientation Constructed EarthOrientation object.
  */ 
EarthOrientation Simulator::ParseEarthOrientationParameters(const rapidxml::xml_node<> *node)
{
  rapidxml::xml_node<> *eop_node = node->first_node("earth_orientation_parameters");
  std::string eop_filepath = eop_node->first_node("eop_filepath")->value();
  std::string luni_solar_series_filepath = eop_node->first_node
                                           ("luni_solar_nutation_series_filepath")->value();
  std::string planetary_series_filepath = eop_node->first_node("planetary_nutation_series_filepath")->value();
  std::string CIO_s0_filepath = eop_node->first_node("CIO_s0_coefficients_filepath")->value();
  std::string CIO_s1_filepath = eop_node->first_node("CIO_s1_coefficients_filepath")->value();
  std::string CIO_s2_filepath = eop_node->first_node("CIO_s2_coefficients_filepath")->value();
  std::string CIO_s3_filepath = eop_node->first_node("CIO_s3_coefficients_filepath")->value();
  std::string CIO_s4_filepath = eop_node->first_node("CIO_s4_coefficients_filepath")->value();
  std::string model_name      = eop_node->first_node("model_name")->value();

  const std::vector<std::string> eop_files = {eop_filepath,
                                              luni_solar_series_filepath,
                                              planetary_series_filepath,
                                              CIO_s0_filepath,
                                              CIO_s1_filepath,
                                              CIO_s2_filepath,
                                              CIO_s3_filepath,
                                              CIO_s4_filepath};

  EarthOrientation earth_orientation(eop_files, model_name);
  std::cout << "Simulation Using Earth Orientation Model: " << model_name << "\n";
  return earth_orientation;
}

//------------------------------------------------------------------------------
//  std::unique_ptr<PlanetaryPerturbations> ParsePlanetaryEphemeris(const rapidxml::xml_node<> *node)
//------------------------------------------------------------------------------
/** Parses the simulation XML configuration file to extract the planetary 
  * perturbation model and construct the required objects for its implementation 
  * for use in the simulator. The method returns a unique_ptr of the planetary
  * pertubrateion model base class. This method parses and creates the required
  * objects for the following models: 
  *   - JPL Ephemerides (DE430 and later)
  * 
  * If invalid model is provided in XML configuration file this method will
  * return a nullptr. 
  * 
  * \param node Pointer to XML root node
  * 
  * \return std::unique_ptr<PlanetaryPerturbations> Unique_ptr to abstract base
  *         class for planetary perturbation model.  
  */ 
std::unique_ptr<PlanetaryPerturbations> Simulator::ParsePlanetaryEphemeris(const rapidxml::xml_node<> *node)
{
  rapidxml::xml_node<> *planetary_perturbations_node = node->first_node("planetary_perturbations");
  std::string model_name = planetary_perturbations_node->first_node("model_name")->value();

  std::unique_ptr<PlanetaryPerturbations> planetary_model(nullptr);

  if(model_name == "JPL Ephemerides")
  {
    std::string header_filepath = planetary_perturbations_node->first_node("JPL_ephemeris_header")->value();
    std::string ephemerides_filepath = planetary_perturbations_node->first_node("ephemerides_filepath")->value();
    planetary_model.reset(new JPLEphemerides(header_filepath, ephemerides_filepath, model_name));
  }
  else
  {
    std::cout << "Error invalid model input for Planetary Pertubrations\n";
  }

  std::cout << "Simulation Using Planetary Position Model: " << model_name << "\n";
  return planetary_model;
}

//------------------------------------------------------------------------------
//  std::unique_ptr<EarthGravityField> ParseEarthGravityFieldParameters(const rapidxml::xml_node<> *node)
//------------------------------------------------------------------------------
/** Parses the simulation XML configuration file to extract the Earthy gravity 
  * field model and construct the required objects for its implementation 
  * for use in the simulator. The method returns a unique_ptr of the earth 
  * gravity field model base class. This method parses and creates the required
  * objects for the following models: 
  *   - GGM03s
  * 
  * If invalid model is provided in XML configuration file this method will
  * return a nullptr. 
  * 
  * \param node Pointer to XML root node
  * 
  * \return std::unique_ptr<EarthGravityField> Unique_ptr to abstract base
  *         class for earth gravity field model.  
  */ 
std::unique_ptr<EarthGravityField> Simulator::ParseEarthGravityFieldParameters(const rapidxml::xml_node<> *node)
{
  rapidxml::xml_node<> *earth_gravity_model_node = node->first_node("earth_gravity_field_model");
  std::string model_name = earth_gravity_model_node->first_node("model_name")->value();

  std::unique_ptr<EarthGravityField> gravity_model(nullptr);

  if(model_name == "GGM03s")
  {
    std::string gravity_field_coefficients_filepath = earth_gravity_model_node->first_node  
                                                    ("earth_gravity_field_model_filepath")->value();
    double max_degree = std::stod(earth_gravity_model_node->first_node("max_degree_for_model")->value());
    
    gravity_model.reset(new GraceGravityModel(gravity_field_coefficients_filepath, model_name, max_degree));
  }
  else
  {
    std::cout << "Error invalid model input for Atmospheric Density Model" << "\n";
  }

  std::cout << "Simulation Using Earth Gravity Field Model: " << model_name << "\n";
  return gravity_model;
}

//------------------------------------------------------------------------------
//  std::unique_ptr<AtmosphericModel> ParseAtmosphericModelParameters(const rapidxml::xml_node<> *node)
//------------------------------------------------------------------------------
/** Parses the simulation XML configuration file to extract the Earth 
  * atmospheric density model and construct the required objects for its 
  * implementation for use in the simulator. The method returns a unique_ptr of 
  * the Earth atmospheric density model base class. This method parses and 
  * creates the required objects for the following models: 
  *   - Harris-Priester
  * 
  * If invalid model is provided in XML configuration file this method will
  * return a nullptr. 
  * 
  * \param node Pointer to XML root node
  * 
  * \return std::unique_ptr<AtmosphericModel> Unique_ptr to abstract base
  *         class for earth atmospheric density model.  
  */ 
std::unique_ptr<AtmosphericModel> Simulator::ParseAtmosphericModelParameters(const rapidxml::xml_node<> *node)
{
  rapidxml::xml_node<> *atmospheric_model_node = node->first_node("atmospheric_model");
  std::string model_name = atmospheric_model_node->first_node("model_name")->value();

  std::unique_ptr<AtmosphericModel> atmospheric_model(nullptr);
  if(model_name == "Harris-Priester")
  {
    std::string atmospheric_coefficients_filepath = atmospheric_model_node->first_node
                                                  ("atmospheric_coefficients_filepath")->value();
    int hp_parameter = std::stoi(atmospheric_model_node->first_node("harris_priester_parameter")->value());  
    double min_altitude = std::stod(atmospheric_model_node->first_node
                          ("minimum_height_model_valid")->value());
    double max_altitude = std::stod(atmospheric_model_node->first_node
                          ("maximum_height_model_valid")->value());
    int f107_index = std::stoi(atmospheric_model_node->first_node("f107_index")->value());
    
    atmospheric_model.reset(new HarrisPriester(atmospheric_coefficients_filepath, model_name, 
                                               hp_parameter, min_altitude, max_altitude, f107_index));
  }
  else
  {
    std::cout << "Error invalid model input for Atmospheric Density Model" << "\n";
  }

  std::cout << "Simulation Using Earth Atmospheric Density Model: " << model_name << "\n";
  return atmospheric_model;
}

//------------------------------------------------------------------------------
//  std::unique_ptr<SolarRadiation> ParseSolarRadiationParameters(const rapidxml::xml_node<> *node)
//------------------------------------------------------------------------------
/** Parses the simulation XML configuration file to extract the solar radiation 
  * pressure model and construct the required objects for its implementation 
  * for use in the simulator. The method returns a unique_ptr of the solar 
  * radiation pressure model base class. This method parses and creates the 
  * required objects for the following models: 
  *   - Analytical
  * 
  * If invalid model is provided in XML configuration file this method will
  * return a nullptr. 
  * 
  * \param node Pointer to XML root node.
  * 
  * \return std::unique_ptr<SolarRadiation> Unique_ptr to abstract base
  *         class for solar radiation pressure model.  
  */ 
std::unique_ptr<SolarRadiation> Simulator::ParseSolarRadiationParameters(const rapidxml::xml_node<> *node)
{
  rapidxml::xml_node<> *solar_radiation_model_node = node->first_node("solar_radiation_pressure_model");
  std::string model_name = solar_radiation_model_node->first_node("model_name")->value();

  std::unique_ptr<SolarRadiation> solar_radiation_model(nullptr);
  if(model_name == "Analytical")
  {
    double solar_intensity = std::stod(solar_radiation_model_node->first_node("solar_intensity_at_1AU")->value());
    solar_radiation_model.reset(new SolarRadiationAnalytical(model_name, solar_intensity));
  }
  else
  {
    std::cout << "Error invalid model input for Solar Radiation Pressure Model\n";
  }

  std::cout << "Simulation Using Solar Radiation Pressure Model: " << model_name << "\n";
  return solar_radiation_model;
}

//------------------------------------------------------------------------------
//  std::unique_ptr<EarthMagneticField> ParseEarthMagneticFieldParameters(const rapidxml::xml_node<> *node)
//------------------------------------------------------------------------------
/** Parses the simulation XML configuration file to extract the Earth magnetic 
  * field model and construct the required objects for its implementation 
  * for use in the simulator. The method returns a unique_ptr of the Earth 
  * magnetic field model base class. This method parses and creates the 
  * required objects for the following models: 
  *   - IGRF13
  * 
  * If invalid model is provided in XML configuration file this method will
  * return a nullptr. 
  * 
  * \param node Pointer to XML root node.
  * 
  * \return std::unique_ptr<EarthMagneticField> Unique_ptr to abstract base
  *         class for Earth magnetic field model.  
  */ 
std::unique_ptr<EarthMagneticField> Simulator::ParseEarthMagneticFieldParameters(const rapidxml::xml_node<> *node)
{
  rapidxml::xml_node<> *magnetic_model_node = node->first_node("earth_magnetic_field_model");
  std::string model_name = magnetic_model_node->first_node("model_name")->value();

  std::unique_ptr<EarthMagneticField> earth_magnetic_field_model(nullptr);
  if(model_name == "IGRF13")
  {
    std::string igrf_coefficients_filepath = magnetic_model_node->first_node("igrf_coefficients_filepath")->value();
    int max_degree = std::stoi(magnetic_model_node->first_node("max_degree")->value());  

    earth_magnetic_field_model.reset(new Igrf(igrf_coefficients_filepath, model_name, max_degree));
  }
  else
  {
    std::cout << "Error invalid model input for Earth Magnetic Field Model" << "\n";
  }

  std::cout << "Simulation Using Earth Magnetic Field Model: " << model_name << "\n";
  return earth_magnetic_field_model;
}

//------------------------------------------------------------------------------
//  std::unique_ptr<Satellite> ParseSatelliteParameters(const std::string &sat_config
//                                                      const std::string &mode)
//------------------------------------------------------------------------------
/** Parses the satellite XML configuration file to construct a satellite object
  * or derived spacecraft object if attitude parameters are provided. The method
  * returns a unique_ptr of the satellite base class to allow for polymorphism
  * of both types of objects.
  * 
  * \param sat_config Satellite configration filepath.
  * \param mode Simulation operational mode (Nominal vs Monte carlo).
  * 
  * \return std::unique_ptr<Satellite> Unique_ptr to satellite base class.  
  */ 
std::shared_ptr<Satellite> Simulator::ParseSatelliteParameters(const std::string &sat_config,
                                                               const std::string &mode)
{
  std::shared_ptr<Satellite> satellite(nullptr);

  //  Parse XML File
  rapidxml::file<> config_file(sat_config.c_str());
  rapidxml::xml_document<> config_doc;
  config_doc.parse<0>(config_file.data());
  rapidxml::xml_node<> *root_node = config_doc.first_node();

  //  Read Satellite ID
  std::string satellite_id = root_node->first_node("satellite_id")->value();

  // Read key simulation parameters
  rapidxml::xml_node<> *satellite_node = root_node->first_node("satellite_configuration");
  rapidxml::xml_node<> *orbit_node = satellite_node->first_node("orbit_configuration");

  Eigen::Vector3d icrf_pos;
  icrf_pos(0) = getParsedNumericValue(orbit_node, "icrf_pos_x", mode);
  icrf_pos(1) = getParsedNumericValue(orbit_node, "icrf_pos_y", mode);
  icrf_pos(2) = getParsedNumericValue(orbit_node, "icrf_pos_z", mode);

  Eigen::Vector3d icrf_vel;
  icrf_vel(0) = getParsedNumericValue(orbit_node, "icrf_vel_x", mode);
  icrf_vel(1) = getParsedNumericValue(orbit_node, "icrf_vel_y", mode);
  icrf_vel(2) = getParsedNumericValue(orbit_node, "icrf_vel_z", mode);

  rapidxml::xml_node<> *properties_node = satellite_node->first_node("satellite_properties");
  double mass = getParsedNumericValue(properties_node, "mass", mode);
  double drag_coeff = getParsedNumericValue(properties_node, "drag_coefficient", mode);
  double radiation_coeff = getParsedNumericValue(properties_node, "radiation_pressure_coefficient", mode);
  double cross_section_area = getParsedNumericValue(properties_node, "cross_section_area", mode); 

  rapidxml::xml_node<> *spacecraft_node = root_node->first_node("spacecraft_configuration");
  if(spacecraft_node != nullptr)
  {
    rapidxml::xml_node<> *structural_node = spacecraft_node->first_node("structural_properties");

    Eigen::Matrix3d inertia_matrix;
    inertia_matrix(0,0) = getParsedNumericValue(structural_node, "Ixx_inertia", mode);
    inertia_matrix(0,1) = getParsedNumericValue(structural_node, "Ixy_inertia", mode);
    inertia_matrix(0,2) = getParsedNumericValue(structural_node, "Ixz_inertia", mode);
    inertia_matrix(1,0) = getParsedNumericValue(structural_node, "Iyx_inertia", mode);
    inertia_matrix(1,1) = getParsedNumericValue(structural_node, "Iyy_inertia", mode);
    inertia_matrix(1,2) = getParsedNumericValue(structural_node, "Iyz_inertia", mode);
    inertia_matrix(2,0) = getParsedNumericValue(structural_node, "Izx_inertia", mode);
    inertia_matrix(2,1) = getParsedNumericValue(structural_node, "Izy_inertia", mode);
    inertia_matrix(2,2) = getParsedNumericValue(structural_node, "Izz_inertia", mode);

    Eigen::Vector3d center_of_mass_offset;
    center_of_mass_offset(0) = getParsedNumericValue(structural_node, "center_of_mass_offset_x", mode);
    center_of_mass_offset(1) = getParsedNumericValue(structural_node, "center_of_mass_offset_y", mode);
    center_of_mass_offset(2) = getParsedNumericValue(structural_node, "center_of_mass_offset_z", mode);

    Eigen::Vector3d static_magnetic_moment;
    static_magnetic_moment(0) = getParsedNumericValue(structural_node, "static_magnetic_moment_x", mode);
    static_magnetic_moment(1) = getParsedNumericValue(structural_node, "static_magnetic_moment_y", mode);
    static_magnetic_moment(2) = getParsedNumericValue(structural_node, "static_magnetic_moment_z", mode);

    rapidxml::xml_node<> *dynamics_node = spacecraft_node->first_node("initial_dynamics");

    Eigen::Quaterniond quaternion;
    quaternion.w() = getParsedNumericValue(dynamics_node, "quaternion_w");
    quaternion.x() = getParsedNumericValue(dynamics_node, "quaternion_i");
    quaternion.y() = getParsedNumericValue(dynamics_node, "quaternion_j");
    quaternion.z() = getParsedNumericValue(dynamics_node, "quaternion_k");
    quaternion.normalize();

    Eigen::Vector3d angular_velocity;
    angular_velocity(0) = getParsedNumericValue(dynamics_node, "body_frame_angular_velocity_x", mode);
    angular_velocity(1) = getParsedNumericValue(dynamics_node, "body_frame_angular_velocity_y", mode);
    angular_velocity(2) = getParsedNumericValue(dynamics_node, "body_frame_angular_velocity_z", mode);

    rapidxml::xml_node<> *attitude_node = spacecraft_node->first_node("attitude_control_configuration");

    rapidxml::xml_node<> *magnetorquer_node = attitude_node->first_node("magnetic_torque_rods");
    std::vector<Magnetorquer> magnetorquers;
    for (rapidxml::xml_node<> *child = magnetorquer_node->first_node(); child; child = child->next_sibling())
    {
      Eigen::Vector3d magnetic_moment;
      magnetic_moment(0) = getParsedNumericValue(child, "max_magnetic_moment_x", mode);
      magnetic_moment(1) = getParsedNumericValue(child, "max_magnetic_moment_y", mode);
      magnetic_moment(2) = getParsedNumericValue(child, "max_magnetic_moment_z", mode);
      magnetorquers.push_back(Magnetorquer(magnetic_moment));
    }

    rapidxml::xml_node<> *reaction_wheel_node = attitude_node->first_node("reaction_wheels");
    std::vector<ReactionWheel> reaction_wheels;
    for (rapidxml::xml_node<> *child = reaction_wheel_node->first_node(); child; child = child->next_sibling())
    {
      Eigen::Vector3d rw_spin_axis;
      rw_spin_axis(0)         = getParsedNumericValue(child, "spin_axis_x",      mode);
      rw_spin_axis(1)         = getParsedNumericValue(child, "spin_axis_y",      mode);
      rw_spin_axis(2)         = getParsedNumericValue(child, "spin_axis_z",      mode);
      double inertia          = getParsedNumericValue(child, "inertia",          mode);
      double max_speed        = getParsedNumericValue(child, "max_speed",        mode);
      double max_motor_torque = getParsedNumericValue(child, "max_motor_torque", mode);
      reaction_wheels.push_back(ReactionWheel(rw_spin_axis, 
                                              inertia, 
                                              max_speed, 
                                              max_motor_torque));
    }

    rapidxml::xml_node<> *thruster_node = attitude_node->first_node("thrusters");
    std::vector<Thruster> thrusters;
    for (rapidxml::xml_node<> *child = thruster_node->first_node(); child; child = child->next_sibling())
    {
      Eigen::Vector3d thrust_vector;
      thrust_vector(0) = getParsedNumericValue(child, "thrust_vector_x", mode);
      thrust_vector(1) = getParsedNumericValue(child, "thrust_vector_y", mode);
      thrust_vector(2) = getParsedNumericValue(child, "thrust_vector_z", mode);
      
      Eigen::Vector3d thruster_position;
      thruster_position(0)      = getParsedNumericValue(child, "thruster_position_x", mode);
      thruster_position(1)      = getParsedNumericValue(child, "thruster_position_y", mode);
      thruster_position(2)      = getParsedNumericValue(child, "thruster_position_z", mode);
      double max_mass_flow_rate = getParsedNumericValue(child, "max_mass_flow_rate",  mode);
      double max_exit_velocity  = getParsedNumericValue(child, "max_exit_velocity",   mode);

      thrusters.push_back(Thruster(thrust_vector, 
                                   thruster_position, 
                                   max_mass_flow_rate, 
                                   max_exit_velocity));
    }

    satellite.reset(new Spacecraft(satellite_id,
                                   icrf_pos, 
                                   icrf_vel,
                                   mass,
                                   cross_section_area, 
                                   drag_coeff,
                                   radiation_coeff, 
                                   inertia_matrix, 
                                   center_of_mass_offset,
                                   static_magnetic_moment, 
                                   quaternion, 
                                   angular_velocity,
                                   magnetorquers, 
                                   reaction_wheels,
                                   thrusters));

  }
  else
  {
    satellite.reset(new Satellite(satellite_id,
                                  icrf_pos, 
                                  icrf_vel,
                                  mass,
                                  cross_section_area, 
                                  drag_coeff,
                                  radiation_coeff));
  }
  return satellite;
}

//------------------------------------------------------------------------------
//  double getParsedNumericValue(const rapidxml::xml_node<> *node, 
//                               const std::string xml_tag, 
//                               const std::string mode)
//------------------------------------------------------------------------------
/** Parses the XML configuration file to extract a numeric value for a given 
  * xml_tag. If the operating mode of the simulator is Monte Carlo, extract the
  * upper and lower range of the numeric value. And evaluate the continuous 
  * uniform distribution for a random value, and return it. Otherwise if the
  * operating mode is Nominal it will directly return the parsed value.
  * 
  * \param node Pointer to XML root node.
  * \param xml_tag Numeric value XML tag.
  * \param mode Simulation operational mode (Nominal vs Monte carlo).
  * 
  * \return double Parsed numeric value for the given XML tag. If simulator
  *         operating mode is Monte Carlo. Value is calculated using a uniform
  *         normal distribution.  
  */ 
double Simulator::getParsedNumericValue(const rapidxml::xml_node<> *node, 
                                        const std::string xml_tag, 
                                        const std::string mode)
{
  double value;
  if(mode == "Monte Carlo")
  { 
    double nominal     = std::stod(node->first_node(xml_tag.c_str())->value());
    double upper_range = std::stod(node->first_node((xml_tag + "_upper_range").c_str())->value());
    double lower_range = std::stod(node->first_node((xml_tag + "_lower_range").c_str())->value());    
    value = nominal + UniformDistributionValue(lower_range, upper_range);
  }
  else
  {
    value =  std::stod(node->first_node(xml_tag.c_str())->value());
  }
  return value;
}

//------------------------------------------------------------------------------
//  double UniformDistributionValue(const double min, const double max)
//------------------------------------------------------------------------------
/** Calculates random value from a continuous uniform distribution with given
  * minimum and maximum values. The random value is seeded using the system 
  * clock.
  * 
  * \param min Minimum value of continuous uniform distribution.
  * \param max Maximum value of continuous uniform distribution.
  * 
  * \return double Return random numeric value from continuous uniform 
  *         distribution between a minimum and maximum value.
  */ 
double Simulator::UniformDistributionValue(const double min, const double max)
{
  auto seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::mt19937 generator(seed);
  std::uniform_real_distribution<double> distribution(min, max);
  return distribution(generator);  
}

}