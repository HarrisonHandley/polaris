//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Simulator Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_SIMULATOR_H_
#define POLARIS_SIMULATOR_H_

#include "atmospheric_model.h"
#include "earth_gravity_field.h"
#include "earth_magnetic_field.h"
#include "earth_orientation.h"
#include "planetary_perturbations.h"
#include "simulation_time.h"
#include "solar_radiation.h"

#include "satellite.h"

#include "../lib/RapidXML/rapidxml.hpp"
#include "../lib/RapidXML/rapidxml_utils.hpp"

#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <vector>

namespace polaris
{

/** 
  * \brief Class header defining the implementation of the Simulator class.
  * 
  * The Simulator class is the wrapper for the Polars simulation.
  */ 

class Simulator
{
 public:
  Simulator(const std::string              &sim_config_xml_filepath, 
            const std::vector<std::string> &sat_config_xml_filepaths,
            const std::string              &sim_identifier = "",
            const bool                      monte_carlo_method = false,
            const int                       monte_carlo_runs = 0);          
  ~Simulator();

  void                                    InitializeState();
  void                                    CalculateTrajectory();
  void                                    CalculateAttitude();
  void                                    LogData();
  std::vector<std::shared_ptr<Satellite>> getSatellitePointers();
  void                                    Propagate(const double timestep);

 private:
  bool monte_carlo_method_;
  int monte_carlo_runs_;
  std::string results_directory_;

  SimulationTime   time_;
  EarthOrientation earth_orientation_;

  std::unique_ptr<AtmosphericModel>       atmospheric_model_;
  std::unique_ptr<EarthGravityField>      earth_gravity_field_;
  std::unique_ptr<EarthMagneticField>     earth_magnetic_field_;
  std::unique_ptr<PlanetaryPerturbations> planetary_perturbations_;
  std::unique_ptr<SolarRadiation>         solar_radiation_model_;

  std::vector<std::shared_ptr<Satellite>> satellites_;

  std::ofstream sim_config_log_file_;
  std::vector<std::ofstream> satellite_log_files_;
  
  void                                    ParseSimulationConfiguration     (const std::string &sim_config);
  EarthOrientation                        ParseEarthOrientationParameters  (const rapidxml::xml_node<> *node);
  SimulationTime                          ParseSimulationTimeParameters    (const rapidxml::xml_node<> *node);
  std::unique_ptr<AtmosphericModel>       ParseAtmosphericModelParameters  (const rapidxml::xml_node<> *node);
  std::unique_ptr<EarthGravityField>      ParseEarthGravityFieldParameters (const rapidxml::xml_node<> *node);
  std::unique_ptr<EarthMagneticField>     ParseEarthMagneticFieldParameters(const rapidxml::xml_node<> *node);
  std::unique_ptr<PlanetaryPerturbations> ParsePlanetaryEphemeris          (const rapidxml::xml_node<> *node);
  std::unique_ptr<SolarRadiation>         ParseSolarRadiationParameters    (const rapidxml::xml_node<> *node);
  std::shared_ptr<Satellite>              ParseSatelliteParameters         (const std::string &sat_config, 
                                                                            const std::string &operational_mode);
  double getParsedNumericValue(const rapidxml::xml_node<> *node, 
                               const std::string xml_tag, 
                               const std::string mode = "Nominal");
  double UniformDistributionValue(const double min, const double max);                           

  std::string getSatelliteLogFilepath(const std::string &sim_identifier,
                                      const std::string &satellite_id,
                                      const std::string &operating_mode,
                                      const int monte_carlo_run_num = 0);
  void LogSimHeader(std::ofstream &file, int num_sats, const std::string &XML_filepath);
  void LogSimData  (std::ofstream &file);
  void LogSatHeader(std::ofstream &file, 
                    const std::shared_ptr<Satellite> &sat,
                    const std::string &XML_filepath,
                    const std::string &operating_mode,
                    int monte_carlo_run_num = 0);
  void LogSatData  (std::ofstream &file, const std::shared_ptr<Satellite> &sat);
};

} // end namespace polaris

#endif  // POLARIS_SIMULATOR_H_