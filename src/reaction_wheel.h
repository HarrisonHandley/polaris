//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Reaction Wheel Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_REACTIONWHEEL_H_
#define POLARIS_REACTIONWHEEL_H_

#include <string>

#include "../lib/eigen3/Eigen/Dense"

namespace polaris {

/** \brief Class definition for the ReactionWheel actuator class.
  * 
  * This class represents one reaction wheel actuator used for momentum exchange
  * attitude control of a spacecraft. This class maintains the state of the 
  * reaction wheel operation in the axis of spin reference frame. 
  */  
  
class ReactionWheel
{
public:
  ReactionWheel(const Eigen::Vector3d &spin_axis_bf,
                const double           inertia, 
                const double           max_speed, 
                const double           max_motor_torque);              
  ReactionWheel(const ReactionWheel &rw);
  ~ReactionWheel();   
  ReactionWheel& operator=(const ReactionWheel &rw);                               

  std::string     getMode()                 const;
  Eigen::Vector3d getSpinAxis()             const;
  double          getInertia()              const;
  double          getAngularVelocity()      const;
  double          getAngularAcceleration()  const;  
  double          getMotorTorque()          const;
  
  void            setMotorTorque(const double torque);

  void            CalculateAngularVelocity(const double timestep);
  void            CalculateAngularAcceleration(const Eigen::Vector3d &sat_ang_accel);

private:
  std::string     mode_;
  Eigen::Vector3d spin_axis_bf_;         
  double          inertia_;    
  double          angular_velocity_;
  double          angular_acceleration_;
  double          motor_torque_;
  double          max_angular_velocity_;  
  double          max_motor_torque_;
};

} // end namespace polaris

#endif  //  POLARIS_REACTIONWHEEL_H_