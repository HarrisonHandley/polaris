//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//                  Thruster Class Defintion
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//  This file is part of Polaris, a High Precision Orbit Propagator and 6 DOF 
//  Spacecraft Simulator
//------------------------------------------------------------------------------

#ifndef POLARIS_THRUSTER_H_
#define POLARIS_THRUSTER_H_

#include "../lib/eigen3/Eigen/Dense"

namespace polaris {

/** \brief Class definition for the Thruster actuator class.
  * 
  * This class represents one thruster actuator used for torque
  * attitude control of a spacecraft. This class maintains the state of the 
  * thrusters operation. 
  */  

class Thruster
{
public:
  Thruster(const Eigen::Vector3d &thrust_vector, 
           const Eigen::Vector3d &thruster_position, 
           const double           max_mass_flow_rate, 
           const double           max_exit_velocity);
  Thruster(const Thruster &th);
  ~Thruster();
  Thruster& operator=(const Thruster &th);

  Eigen::Vector3d getThrusterForce()                  const;
  Eigen::Vector3d getThrusterTorque()                 const;
  double          getThrottle()                       const;
  double          getMassFlowRate()                   const;

  void            setThrottle(const double throttle);
  void            CalculateThrust();  
  
private:
  Eigen::Vector3d thrust_vector_;
  Eigen::Vector3d thruster_position_;
  Eigen::Vector3d thruster_force_;
  Eigen::Vector3d thruster_torque_;
  double          throttle_;
  double          max_mass_flow_rate_;
  double          max_exit_velocity_;

};

} // end namespace polaris

#endif  //  POLARIS_THRUSTER_H_